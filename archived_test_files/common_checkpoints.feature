Feature: Common Checkpoints

  @p2
  Scenario Outline: Common Checkpoints
	Given I login as admin user
	And I navigate to Checklist Setup page
	When I select 'Common Checkpoints' checklist
	And I toggle common checkpoints <toggle> and save
	And I create an inspection via project 1, checklist 'CT032022'
	Then I should <check_visibility> common checkpoints on the inspection
	Examples:
	| toggle | check_visibility |
	| off    | not see          |
	| on     | see              |
