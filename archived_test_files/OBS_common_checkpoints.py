from pytest_bdd import scenario, when, then, parsers

from functions import *
from locators.inspection_page_locators import *
from locators.setup_locators import *


@pytest.mark.xray('DEV-5088')
@scenario("../../features/inspecting/common_checkpoints.feature", "Common Checkpoints")
def common_checkpoints():
    """test that common checkpoints work as expected"""


@when(parsers.parse("I toggle common checkpoints {toggle} and save"))
def toggle_common_checkpoints(selenium, toggle):
    toggle_status = selenium.execute_script("return arguments[0].firstElementChild.checked;", by_xpath(selenium, CHECKLIST_DETAILS_COMMON_TOGGLE))
    if toggle == "on":
        if toggle_status == True:
            pass
        else:
            click_toggle = by_xpath(selenium, CHECKLIST_DETAILS_COMMON_TOGGLE).click()
            click_save = by_xpath(selenium, CHECKLIST_DETAILS_SAVE).click()
            wait_invisible(selenium, CHECKLIST_DETAILS_SAVE)
            assert selenium.execute_script("return arguments[0].firstElementChild.checked;", by_xpath(selenium, CHECKLIST_DETAILS_COMMON_TOGGLE)) == True
    elif toggle == "off":
        if toggle_status == False:
            pass
        else:
            click_toggle = by_xpath(selenium, CHECKLIST_DETAILS_COMMON_TOGGLE).click()
            click_save = by_xpath(selenium, CHECKLIST_DETAILS_SAVE).click()
            wait_invisible(selenium, CHECKLIST_DETAILS_SAVE)
            assert selenium.execute_script("return arguments[0].firstElementChild.checked;", by_xpath(selenium, CHECKLIST_DETAILS_COMMON_TOGGLE)) == False


@then(parsers.parse("I should {check_visibility} common checkpoints on the inspection"))
def verify_common_checkpoints(selenium, check_visibility):
    all_checkpoint_names = by_xpath_multiple(selenium, CP_NAMES)
    checkpoint_names_list = [x.text for x in all_checkpoint_names]
    common_checkpoints = [x for x in checkpoint_names_list if "Common" in x]
    if check_visibility == "see":
        assert len(common_checkpoints) >= 1
    elif check_visibility == "not see":
        assert len(common_checkpoints) == 0

