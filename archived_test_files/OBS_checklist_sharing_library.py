from datetime import *
from pytest_bdd import scenario, then, when
from functions import *
from locators.checklist_sharing_library_locators import *
from locators.setup_locators import *


@pytest.mark.xray('DEV-5376')
@scenario("../../features/setup/checklist_sharing_library.feature", "Downloading from checklist sharing library")
def checklist_sharing_library_download():
    """test downloading a checklist from the checklist sharing library works as expected"""


@when("I select a checklist from Checklist Sharing Library")
def select_checklist_from_library(selenium):
    click_radio_button = by_xpath(selenium, CHECKLISTLIB_ROW_SELECT).click()


@when("I click button to download checklist")
def click_download_checklist(selenium):
    click_download_button = by_xpath(selenium, CHECKLISTLIB_DOWNLOAD_BUTTON).click()


@when("I confirm checklist download")
def confirm_checklist_download(selenium, pass_data_to_step):
    clear_checklist_name = by_xpath(selenium, CHECKLISTLIB_MODAL_TASK_NAME).clear()
    change_checklist_name = by_xpath(selenium, CHECKLISTLIB_MODAL_TASK_NAME).send_keys(f"Downloaded Checklist {datetime.datetime.now().strftime('%m/%d/%Y %H:%M')}")
    get_checklist_name = by_xpath(selenium, CHECKLISTLIB_MODAL_TASK_NAME).get_attribute("value")
    pass_data_to_step['downloaded_checklist_name'] = get_checklist_name
    click_download_on_modal = by_xpath(selenium, CHECKLISTLIB_MODAL_DOWNLOAD_BUTTON).click()


@then("my downloaded checklist is present")
def downloaded_checklist_present(selenium, pass_data_to_step):
    downloaded_checklist_name = pass_data_to_step.get("downloaded_checklist_name")
    get_checklist_names = by_xpath_multiple(selenium, SETUP_GRID_NAME)
    checklist_names_list = [x.text for x in get_checklist_names]
    assert downloaded_checklist_name in checklist_names_list
