import random

from pytest_bdd import scenario, when, then, given
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.select import Select

from functions import *
from locators.dashboard_locators import *
from locators.inspection_page_locators import *


@pytest.mark.xray('DEV-5405')
@scenario("../../../features/data_output/dashboards/priority_open_item_dashboard_calendar_view.feature", "Priority Open Item Dashboard - Calendar View")
def test_priority_open_item_dash_calendar_view():
    """Verify that priority open item dashboard calendar view works as expected"""


@given("I press Calendar View")
def calendar_view_on_dashboard(selenium):
    by_xpath(selenium, PRIORITYDASH_CALENDAR_TAB).click()
    wait_invisible_long(selenium, PROCESSING_MASK)


@when("I select 'Automation, Python' in 'Inspector' dashboard panel, which filters the calendar view")
def calendar_filters_with_panel_selection(selenium):
    wait_clickable(selenium, DASH_CALENDAR)

    # make changes
    panel_titles = by_xpath_multiple(selenium, ALLDASH_PANEL_TITLE)
    panel_title_list = [x.text for x in panel_titles]
    panel_to_edit = panel_title_list.index('Inspector') + 1
    items_in_panel_to_edit = by_xpath_multiple(selenium, f'({ALLDASH_PANEL_TITLE})[{panel_to_edit}]' + ALLDASH_PANEL_ITEM_TITLE)
    panel_to_edit_item_list = [x.text for x in items_in_panel_to_edit]
    item_index = panel_to_edit_item_list.index('Automation, Python') + 1
    click_item_in_panel = by_xpath(selenium, f'({ALLDASH_PANEL_TITLE})[{panel_to_edit}]' + f'{ALLDASH_PANEL_ITEM_TITLE}[{item_index}]').click()
    wait_invisible(selenium, PROCESSING_MASK)

    # verify changes reflected
    items_in_panel_edited = by_xpath_multiple(selenium, f'({ALLDASH_PANEL_TITLE})[{panel_to_edit}]' + ALLDASH_PANEL_ITEM_TITLE)
    assert len(items_in_panel_to_edit) > len(items_in_panel_edited)


@when("I search for 'quality' in calendar search field")
def search_in_dash_calendar(selenium, pass_data_to_step):
    by_xpath(selenium, DASH_CALENDAR_VIEW_SEARCH).send_keys('Quality', Keys.ENTER)
    wait_invisible(selenium, PROCESSING_MASK)
    time.sleep(2)
    calendar_items = by_xpath_multiple(selenium, PRIORITYDASH_CALENDAR_ITEMS)
    calendar_items_list = [x.text for x in calendar_items]
    for x in calendar_items_list:
        if len(x) > 0:
            assert 'Quality' in x
    pass_data_to_step['calendar_items_list'] = calendar_items_list


@when("I select an item from priority open dashboard calendar")
def select_item_in_dash_calendar(selenium, pass_data_to_step):
    calendar_items_list = pass_data_to_step.get('calendar_items_list')
    index = calendar_items_list.index(random.choice(calendar_items_list[:3])) + 1
    by_xpath(selenium, f"({PRIORITYDASH_CALENDAR_ITEMS})[{index}]").click()


@then("deficiency details are shown for the selected item")
def def_details_shown(selenium):
    wait_clickable(selenium, DASH_CALENDAR_ITEM_DETAIL_LINK)


@when("I open the inspection from calendar view")
def open_insp_from_dash_calendar(selenium, pass_data_to_step):
    checkpoint_data = by_xpath(selenium, DASH_CALENDAR_ITEM_DETAIL_LINK).get_attribute('hash')
    open_checkpoint = by_xpath(selenium, DASH_CALENDAR_ITEM_DETAIL_LINK).click()
    wait_clickable_long(selenium, INSP_HEADER)
    try:
        loading_data_modal_appears_and_loads(selenium)
    except:
        pass
    pass_data_to_step['checkpoint_hash'] = checkpoint_data


@when("I change the due date for the item selected")
def change_due_date_for_deficiency(selenium, pass_data_to_step):
    today = datetime.date.today()
    print(today)
    plus_three = today + datetime.timedelta(days=3)
    print(plus_three)
    month = plus_three.month
    day = plus_three.day
    by_xpath(selenium, INSP_CHECKPOINT(selenium, 'Quality Checkpoint') + CP_DUE_DATE_PICKER).click()
    select_month = Select(by_xpath(selenium, CP_DUE_DATE_PICKER_MONTHS)).select_by_value(int(month) - 1)
    days = [x.text for x in by_xpath_multiple(selenium, CP_DUE_DATE_PICKER_DAYS)]
    index = days.index(str(day)) + 1
    click_day = by_xpath(selenium, f"({CP_DUE_DATE_PICKER_DAYS})[{index}]")
    wait_invisible(selenium, CP_DUE_DATE_PICKER_MONTHS)
    get_new_cp_due_date = by_xpath(selenium, INSP_CHECKPOINT(selenium, 'Quality Checkpoint') + CP_DUE_DATE_PICKER).get_attribute('value')
    pass_data_to_step['new_cp_duedate'] = get_new_cp_due_date
    pass_data_to_step['due_date_picked'] = plus_three


@then("the new due date is reflected")
def item_shown_with_new_due_date(selenium, pass_data_to_step):
    checkpoint_hash = pass_data_to_step.get('checkpoint_hash')
    due_date_picked = pass_data_to_step.get('due_date_picked')
    date = due_date_picked.strftime("%Y-%m-%d")
    all_dates = [x.get_attribute('data-date') for x in by_xpath_multiple(selenium, DASH_CALENDAR_DAYS)]
    index = all_dates.index(due_date_picked) + 1
    try:
        by_xpath(selenium, f"({DASH_CALENDAR_DAYS})[{index}]")
    except:
        click_next_month = by_xpath(selenium, DASH_CALENDAR_NEXT_MONTH).click()
        by_xpath(selenium, f"({DASH_CALENDAR_DAYS})[{index}]")
    # finally:
    # cp_hash_list = []
    # for x in get_all_shown_checkpoints:
    #     x.click()
    #     cp_hash = by_xpath(selenium, DASH_CALENDAR_ITEM_DETAIL_LINK).get_attribute('hash')
    #     cp_hash_list.append(cp_hash)
    # print(cp_hash_list)
    # print(checkpoint_hash)
    # cp_index = cp_hash_list.index(checkpoint_hash) + 1
    # cell_index = by_xpath(selenium, f"(({PRIORITYDASH_CALENDAR_ITEMS})[{cp_index}])/..").get_attribute("cellIndex")
    # get_cell_date = by_xpath(selenium, f"({PRIORITYDASH_CALENDAR_ITEMS})[{cp_index}]/../../../../thead/tr/td[{int(cell_index) + 1}]").get_attribute("data-date")
    new_cp_due_date = pass_data_to_step.get('new_cp_duedate')
    # try:
    #     formatted_new_date = datetime.datetime.strptime(new_cp_due_date, '%b %d, %Y').strftime('%Y-%m-%d')
    # except:
    #     formatted_new_date = datetime.datetime.strptime(new_cp_due_date, '%d/%b/%Y').strftime('%Y-%m-%d')
    # try:
    #     formatted_cell_date = datetime.datetime.strptime(get_cell_date, '%d/%b/%Y').strftime('%Y-%m-%d')
    # except:
    #     formatted_cell_date = datetime.datetime.strptime(get_cell_date, '%Y-%m-%d').strftime('%Y-%m-%d')
    # assert formatted_new_date == formatted_cell_date
