Feature: Priority Open Item

  @p2
  Scenario: Priority Open Item Dashboard - Calendar View
	  Given I login as admin user
	  And I navigate to Priority Open Item Dashboard page
	  And I press Calendar View
	  When I select 'Automation, Python' in 'Inspector' dashboard panel, which filters the calendar view
	  And I search for 'quality' in calendar search field
	  And I select an item from priority open dashboard calendar
	  Then deficiency details are shown for the selected item
	  When I open the inspection from calendar view
	  And I change the due date for the item selected
	  And I sync inspection to server
	  Given I navigate to Priority Open Item Dashboard page
	  And I press Calendar View
	  Then the new due date is reflected
