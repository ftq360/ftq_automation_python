Feature: Checklist Sharing Library

  @p2
  Scenario: Downloading from checklist sharing library
    Given I login as admin user
    And I navigate to Checklist Sharing Library page
    When I select a checklist from Checklist Sharing Library
    And I click button to download checklist
    And I confirm checklist download
    Given I navigate to checklist setup page
    Then my downloaded checklist is present
