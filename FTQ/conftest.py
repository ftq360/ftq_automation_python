import random
import re
from datetime import *

import allure
from pytest_bdd import given, parsers, then, when
from pytest_xray import evidence
from selenium.webdriver.chrome import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys

from functions import *
from locators.customization_preference_page_locators import *
from locators.dashboard_locators import *
from locators.inspection_page_locators import *
from locators.listing_grid_locators import *
from locators.old_setup_page_locators import *
from locators.report_locators import *
from locators.selection_process_locators import *
from locators.setup_locators import *

# ----- Test Config -----

SESSION_BASE_URL = []


# Getting variables from environment variables (windows)
@pytest.fixture(scope="session")
def cred_data():
    data_dict = {
        "BASE_URL": os.environ['PROD_URL'],
        "ENVIRONMENT_CONTROL": os.environ['FTQ_version_page'],
        "GMAIL": os.environ['GMAIL'],
        "GMAIL_USER_INVITATIONS": os.environ['GMAIL_LABEL_user_invitations'],
        "GMAIL_USERNAME": os.environ['GMAIL_automation_username'],
        "GMAIL_PASSWORD": os.environ['GMAIL_automation_password'],
        "ADMIN_USER": os.environ['FTQ_USER_admin_username'],
        "STANDARD_USER": os.environ['FTQ_USER_standard_username'],
        "VIEW_PERMISSIONS_USER": os.environ['FTQ_USER_view_username'],
        "EDIT_PERMISSIONS_USER": os.environ['FTQ_USER_edit_username'],
        "ITP_PERMISSIONS_USER": os.environ['FTQ_USER_itp_permissions_username'],
        "NO_PERMISSIONS_USER": os.environ['FTQ_USER_none_username'],
        "RESTRICTED_USER": os.environ['FTQ_USER_restricted_username'],
        "LINKED_ACCOUNT_USER": os.environ['FTQ_USER_linked_account_username'],
        "BHP_USER": os.environ['FTQ_USER_BHP_username'],
        "PASSWORD": os.environ['FTQ_USER_password'],
        "GOOGLE_PASS": os.environ['FTQ_USER_googlepassword'],
        "SUPER_PASS": os.environ['FTQ_USER_superadmin_password'],
        "DATA_CREATION_USER": os.environ['FTQ_USER_creation_username'],
        "QA_INSPECT_USER": os.environ['FTQ_USER_qa_inspector_username'],
        "RESTRICTED_INSPECT_USER": os.environ['FTQ_USER_restricted_inspector_username'],
        "QA_NON_INSPECT_USER": os.environ['FTQ_USER_qa_non-inspect_username'],
        "SELF_INSPECT_USER": os.environ['FTQ_USER_self_insp_username'],
        "INSPECT_BY_ME_USER": os.environ['FTQ_USER_insp_by_me_username'],
        "PLAN_MODE_USER": os.environ['FTQ_USER_plan_mode_username'],
        "DEMOED_USER": os.environ['FTQ_USER_superadmin_username'],
        "HOMEPAGE_USER": os.environ['FTQ_USER_homepage_username'],
        "MICROSOFT_USER": os.environ['FTQ_USER_microsoft'],
        "GOOGLE_USER": os.environ['FTQ_USER_google'],
        "CREATE_INSPECTION": os.environ['FTQ_INSP-PAGE_create_inspection'],
        "RECENT_INSPECTIONS_LISTING": os.environ['FTQ_LISTING-GRID_recent_insp'],
        "RECENT_DEFICIENCIES_LISTING": os.environ['FTQ_LISTING-GRID_recent_def'],
        "INSPECTION_ARCHIVE_LISTING": os.environ['FTQ_LISTING-GRID_insp_archive'],
        "ITP_PROGRESS": os.environ['FTQ_LISTING-GRID_itp_progress'],
        "ITP_LISTING": os.environ['FTQ_LISTING-GRID_itp_listing'],
        "DEFICIENCY_ARCHIVE_LISTING": os.environ['FTQ_LISTING-GRID_def_archive'],
        "DASHBOARD_LANDING": os.environ['FTQ_DASHBOARD_view_all'],
        "PRIORITY_OPEN_ITEM_DASHBOARD": os.environ['FTQ_DASHBOARD_priority_def'],
        "AGING_OPEN_ITEM_DASHBOARD": os.environ['FTQ_DASHBOARD_aging_open_def'],
        "DEFICIENCY_BY_CHECKLIST_DASHBOARD": os.environ['FTQ_DASHBOARD_def_by_checklist-checkpoint'],
        "DEFICIENCY_BY_RP_DASHBOARD": os.environ['FTQ_DASHBOARD_def_by_rp'],
        "INSPECTION_ACTIVITY_BY_PROJECT_DASHBOARD": os.environ['FTQ_DASHBOARD_insp_activity_by_proj-insp'],
        "ITP_COMPLETION_DASHBOARD": os.environ['FTQ_DASHBOARD_itp_completion'],
        "ITP_PROGRESS_DASHBOARD": os.environ['FTQ_DASHBOARD_itp_progress'],
        "KEY_METRIC_DASHBOARD": os.environ['FTQ_DASHBOARD_key-metric_total'],
        "RISK_FACTOR_DASHBOARD": os.environ['FTQ_DASHBOARD_key-metric_rf'],
        "COST_LEVEL_DASHBOARD": os.environ['FTQ_DASHBOARD_key-metric_cl'],
        "MONEY_SPENT_DASHBOARD": os.environ['FTQ_DASHBOARD_key-metric_$'],
        "KEY_METRIC_TREND_DASHBOARD": os.environ['FTQ_DASHBOARD_key-metric_trend'],
        "TOTAL_DAYS_TO_FIX_DASHBOARD": os.environ['FTQ_DASHBOARD_total_days_to_fix'],
        "AVG_DAYS_TO_FIX_DASHBOARD": os.environ['FTQ_DASHBOARD_avg_days_to_fix'],
        "MONTHLY_VENDOR_DPK_DASHBOARD": os.environ['FTQ_DASHBOARD_monthly_dpk'],
        "INSPECTIONS": os.environ['FTQ_DASHBOARD_number_ftq_insp'],
        "NUMBER_INSP_BY_CHECKLIST_DASHBOARD": os.environ['FTQ_DASHBOARD_number_ftq_insp'],
        "DEFICIENCY_TREND_DASHBOARD": os.environ['FTQ_DASHBOARD_open-def_trend_by_proj'],
        "PERCENT_INSP_BY_CHECKLIST_DASHBOARD": os.environ['FTQ_DASHBOARD_percent_ftq_insp'],
        "DEFICIENCY_BY_REASON_CODE_DASHBOARD": os.environ['FTQ_DASHBOARD_reason-code_def_by_check'],
        "TREND_BY_REASON_CODE_DASHBOARD": os.environ['FTQ_DASHBOARD_reason-code_def_trend'],
        "RP_PERFORMANCE_DASHBOARD": os.environ['FTQ_DASHBOARD_rp_performance'],
        "INSPECTION_ACTIVITY_DASHBOARD": os.environ['FTQ_DASHBOARD_insp_activity'],
        "RUN_REPORTS_ONLINE": os.environ['FTQ_REPORTS_run_online'],
        "INTERACTIVE_REPORTS": os.environ['FTQ_REPORTS_interactive'],
        "ACTIVATE_REPORTS": os.environ['FTQ_REPORTS_activate'],
        "CHECKLIST_SETUP": os.environ['FTQ_SETUP_checklist_new'],
        "PROJECT_SETUP": os.environ['FTQ_SETUP_project_new'],
        "OLD_PROJECT_SETUP": os.environ['FTQ_SETUP_project'],
        "RESPONSIBLE_PARTY_SETUP": os.environ['FTQ_SETUP_vendor'],
        "OLD_USER_SETUP": os.environ['FTQ_SETUP_user_old'],
        "USER_SETUP": os.environ['FTQ_SETUP_user'],
        "CHECKLIST_SHARING_LIBRARY": os.environ['FTQ_SETUP_checklist_library'],
        "CUSTOMIZATION_PREFERENCES": os.environ['FTQ_SETUP_preferences'],
        "ACTIVITY_LOG": os.environ['FTQ_SETUP_activity'],
        "ACCOUNT_MANAGEMENT": os.environ['FTQ_SETUP_account'],
        "IMPORT": os.environ['FTQ_SETUP_import'],
        "API_SETUP": os.environ['FTQ_SETUP_api'],
        "KEY_METRIC_SETUP": os.environ['FTQ_SETUP_km'],
        "ATTACHMENT_FILE_MANAGER": os.environ['FTQ_SETUP_attachment'],
        "SYSTEM_DATA": os.environ['FTQ_HELP_system_data'],
        "LOGOUT": os.environ['FTQ_LOGOUT']
    }
    if len(SESSION_BASE_URL) == 0:
        SESSION_BASE_URL.append(data_dict["BASE_URL"])
    return data_dict


# Chrome preferences
# prefs = {"download.default_directory": os.getcwd() + "D:\\automation_downloads\\"}
prefs = {
    "download.default_directory": "C:\\Workspace\\AutomationDownloads\\",
    "profile.default_content_setting_values.media_stream_camera": 1,
    "profile.content_settings.exceptions.clipboard": {
        '*, *': {
            "setting": 1
        }
    }
}


@pytest.fixture
def items_in_use():
    checklist_codes = ['CT150', '03.10.01', 'CT147', 'CT245', 'CT0320221', 'CT291', 'CT125', 'CT135',
                       'CT390', 'CT510', 'CT457', 'CT648', 'CT932', 'CT320439', 'CT320440', 'CT320477', 'CT321',
                       'CT411', 'CT397']
    projects_names = ['Project 2', 'Project for Data Creation', 'ITP Project', 'Project 1', 'Project for References',
                      'Project for Selection Process Order', 'Project for ITP Selection Process Order',
                      'Project for Dictionary Updates',
                      'Project for permissions']
    responsible_parties = ['Responsible Party 2', 'FTQ Automation Group', 'Responsible Party for References']

    return checklist_codes, projects_names, responsible_parties


@pytest.hookimpl(hookwrapper=True)
def pytest_collection():
    yield


@pytest.hookimpl(hookwrapper=True)
def pytest_runtest_makereport(item, call):
    outcome = yield
    report = outcome.get_result()
    my_evidence = getattr(report, "evidences", [])
    if report.when == "call":
        item_request = item.funcargs['request']
        driver = item_request.getfixturevalue('selenium')
        data = driver.get_screenshot_as_png()
        my_evidence.append(evidence.png(data=data, filename="screenshot.png"))
    report.evidences = my_evidence


@pytest.hookimpl(hookwrapper=True)
def pytest_xray_results(results, session):
    yield
    results['info']['summary'] = f'Automated Test Execution for {session.config.getoption('-m')} tests'
    if SESSION_BASE_URL[0].find("www") >= 0:
        results['info']['testEnvironments'] = ["ProductionEnv"]
    elif SESSION_BASE_URL[0].find("devtest") >= 0:
        results['info']['testEnvironments'] = ["DevTest"]
    elif SESSION_BASE_URL[0].find("devprod") >= 0:
        results['info']['testEnvironments'] = ["DevProd"]
    else:
        results['info']['testEnvironments'] = ["ReleaseEnv"]


# Test setup and teardown
@pytest.fixture
def selenium():
    global selenium
    chrome_options = Options()
    # chrome_options.binary_location = 'C:\\Program Files\\Google\\Chrome Beta\\Application\\chrome.exe'
    chrome_options.add_argument('--log-level=3')
    chrome_options.add_argument('no-sandbox')
    chrome_options.add_experimental_option("prefs", prefs)
    # options.set_capability('goog:loggingPrefs', {'browser': 'ALL'})
    # chrome_options.add_argument('--start-maximized')  # SHOULD BE COMMENTED ALWAYS!!! ACTUAL ONLY FOR LOCAL LAUNCHES
    chrome_options.add_argument('--headless')
    # chrome_options.add_argument('--enable-geolocation')
    chrome_options.add_argument('--window-size=1920,1080')
    chrome_options.add_argument('--use-fake-ui-for-media-stream')
    chrome_options.add_argument('--use-fake-device-for-media-stream')

    selenium = webdriver.WebDriver(options=chrome_options)
    selenium.execute_cdp_cmd(
        cmd="Browser.grantPermissions",
        cmd_args={
            "permissions": ["geolocation"]
        }
    )
    selenium.execute_cdp_cmd(
        cmd="Emulation.setGeolocationOverride",
        cmd_args={
            "latitude": 37.7749,
            "longitude": 122.4194,
            "accuracy": 1000
        }
    )

    yield selenium
    allure.attach(
        selenium.get_screenshot_as_png(),
        name='Evidence',
        attachment_type=allure.attachment_type.PNG
    )
    selenium.quit()
    selenium.stop_client()


# ----- Fixtures -----

@pytest.fixture(autouse=True)
def skip_on_production_environment(request, cred_data):
    if request.node.get_closest_marker('skip_production_test'):
        if 'www' in cred_data['BASE_URL']:
            pytest.skip(f"{request.node.name} skipped on production environment")


def pytest_configure(config):
    config.addinivalue_line(
        "markers", "skip_production_test: skip test on production environment",
    )


# Login
@pytest.fixture
@given(parsers.parse('I login as {user_type} user'))
def login_step(selenium, navigate_to_ftq, user_type, cred_data):
    start_time = time.time()
    while True and time.time() - start_time < 60:
        user_login = text_to_credential(user_type)
        username = user_login + '_USER'
        try:
            enter_login_username = by_xpath(selenium, LG_USERNAME_FIELD).send_keys(cred_data[username])
            if username == 'DEMOED_USER':
                enter_login_password = by_xpath(selenium, LG_PASSWORD_FIELD).send_keys(cred_data['SUPER_PASS'])
            elif username == 'BHP_USER':
                enter_login_password = by_xpath(selenium, LG_PASSWORD_FIELD).send_keys(cred_data['SUPER_PASS'])
            else:
                enter_login_password = by_xpath(selenium, LG_PASSWORD_FIELD).send_keys(cred_data['PASSWORD'])
            break
        except:
            selenium.get(cred_data['BASE_URL'] + cred_data['LOGOUT'])
            wait_clickable(selenium, LG_USERNAME_FIELD)
            continue
    click_login_button = by_xpath(selenium, LG_LOGIN_BUTTON).click()
    wait_clickable(selenium, GL_TOP_MENU)


# Log out
@pytest.fixture
@given("I log out")
def log_out(selenium, cred_data):
    selenium.get(cred_data['BASE_URL'] + cred_data['LOGOUT'])
    wait_clickable(selenium, LG_USERNAME_FIELD)
    time.sleep(3)


# Navigate to FTQ
@pytest.fixture
@given('I navigate to FTQ360 app')
def navigate_to_ftq(selenium, cred_data):
    selenium.get(cred_data['BASE_URL'])
    assert cred_data['BASE_URL'] in selenium.current_url


# Navigate to dynamic page url
@pytest.fixture
@given(parsers.parse("I navigate to {page_name} page"))
def navigate_to_specific_page(selenium, page_name, cred_data, pass_data_to_step):
    page = text_to_credential(page_name)
    selenium.get(cred_data['BASE_URL'] + cred_data[page])
    assert selenium.current_url == cred_data['BASE_URL'] + cred_data[page]
    pass_data_to_step['page_name'] = page_name


# General
@pytest.fixture()
def pass_data_to_step():
    return {}


@pytest.fixture
@when("I refresh page")
def refresh_browser(selenium):
    selenium.refresh()


@pytest.fixture
def version_number_saved():
    return {}


@pytest.fixture
@given("I go into FTQ offline mode")
def go_to_offline(selenium):
    click_signal_icon = by_xpath(selenium, SIGNAL_ICON_GREEN).click()
    click_offline = by_xpath(selenium, SIGNAL_ICON_OFFLINE).click()
    wait_clickable(selenium, SIGNAL_ICON_RED)


@pytest.fixture
@given("I go into FTQ online mode")
def go_to_online(selenium):
    click_signal_icon = by_xpath(selenium, SIGNAL_ICON_RED).click()
    click_online = by_xpath(selenium, SIGNAL_ICON_ONLINE).click()
    time.sleep(2)
    selenium.refresh()
    wait_clickable(selenium, SIGNAL_ICON_GREEN)


@pytest.fixture
@then(parsers.parse("I see toast notification with '{message}' text"))
def toast_notification_text(selenium, message):
    wait_clickable(selenium, TOAST_MESSAGE)
    assert message in by_xpath(selenium, TOAST_MESSAGE).text


@pytest.fixture
@when("I click in popup to confirm")
def confirm_popup(selenium):
    wait_clickable(selenium, POPUP)
    try:
        try_by_xpath(selenium, POP_UP_CONFIRM).click()
    except:
        try_by_xpath(selenium, POPUP_OK_BUTTON).click()


# Creating inspections
@pytest.fixture
@then("I should see 'Loading data' modal")
def loading_modal_appears(selenium):
    try:
        modal_appears = wait_clickable_short(selenium, SP_DICT_LOADING_MODAL_OPEN)
    except:
        pass


@pytest.fixture
@then("I should wait until 'Loading data' modal will be closed")
def wait_for_modal_to_close(selenium):
    loading_complete = wait_present(selenium, SP_DICT_LOADING_MODAL_CLOSED)


@pytest.fixture
@when(parsers.parse("I select '{item}' in selection process"))
def select_itp_project(selenium, item):
    try:
        select_division = by_xpath(selenium, SPECIFIC_SELECTION(selenium, "(Undefined)")).click()
    except:
        pass
    select_item = by_xpath(selenium, SPECIFIC_SELECTION(selenium, item)).click()


@pytest.fixture
@when("I check plan mode checkbox")
def check_plan_mode(selenium):
    click_checkbox = by_xpath(selenium, SP_PLAN_MODE_CHECKBOX_INPUT).click()
    checkbox_status = selenium.find_element(By.NAME, "example").get_attribute("checked")
    assert checkbox_status == "true"


@pytest.fixture
@when("I create an inspection")
def create_inspection(selenium, cred_data):
    create_an_inspection(selenium, cred_data)


@pytest.fixture
@when(parsers.parse("I create an inspection via project 1, checklist '{checklist}'"))
def create_specific_inspection(selenium, checklist, cred_data):
    go_to_create_insp = selenium.get(cred_data['BASE_URL'] + cred_data['CREATE_INSPECTION'])
    try_dictionary_loading(selenium)
    try:
        select_division = by_xpath(selenium, SPECIFIC_SELECTION(selenium, "(Undefined)")).click()
    except:
        pass
    try:
        select_project1 = by_xpath(selenium, SPECIFIC_SELECTION(selenium, "Project 1 (1)")).click()
    except:
        pass

    try:
        try_by_xpath(selenium, SP_PLAN_MODE_CHECKED).click()
        try:
            try_by_xpath(selenium, SP_PLAN_MODE_CHECKED).click()
        except:
            pass
            # print("plan mode removed")
    except:
        pass

    select_specific_checklist = by_xpath(selenium, (SPECIFIC_SELECTION(selenium, checklist))).click()
    select_rp = by_xpath(selenium, SPECIFIC_SELECTION(selenium, "FTQ Automation Group (D1)")).click()
    check_inspection_created_successfully = wait_clickable(selenium, INSP_HEADER)
    assert "Inspection" in selenium.title
    click_save_btn = by_xpath(selenium, INSP_SAVE_BUTTON).click()
    wait_clickable_long(selenium, INSP_SYNCED_TEXT)


@pytest.fixture
@when(parsers.parse("I create an inspection via ITP Project, checklist '{checklist}'"))
def create_specific_inspection(selenium, checklist, cred_data):
    go_to_create_insp = selenium.get(cred_data['BASE_URL'] + cred_data['CREATE_INSPECTION'])
    try_dictionary_loading(selenium)
    try:
        select_division = by_xpath(selenium, SPECIFIC_SELECTION(selenium, "(Undefined)")).click()
    except:
        pass
    select_project1 = by_xpath(selenium, SPECIFIC_SELECTION(selenium, "ITP Project (4)")).click()
    select_phase = by_xpath(selenium, SPECIFIC_SELECTION(selenium, "ITP Phase 2 (4-4)")).click()
    try:
        try_by_xpath(selenium, SP_PLAN_MODE_CHECKED).click()
        try:
            try_by_xpath(selenium, SP_PLAN_MODE_CHECKED).click()
        except:
            pass
            # print("plan mode removed")
    except:
        pass
    try:
        select_equipment = by_xpath(selenium, SPECIFIC_SELECTION(selenium, "ITP Equipment (4-1)")).click()
    except:
        pass
    select_specific_checklist = by_xpath(selenium, (SPECIFIC_SELECTION(selenium, checklist))).click()
    select_rp = by_xpath(selenium, SPECIFIC_SELECTION(selenium, "FTQ Automation Group (D1)")).click()
    check_inspection_created_successfully = wait_clickable(selenium, INSP_HEADER)
    assert "Inspection" in selenium.title
    click_save_btn = by_xpath(selenium, INSP_SAVE_BUTTON).click()
    wait_clickable(selenium, INSP_SYNCED_TEXT)


@pytest.fixture
@when(parsers.parse("I create an inspection via project 2, checklist '{checklist}'"))
def create_specific_inspection(selenium, checklist, cred_data):
    go_to_create_insp = selenium.get(cred_data['BASE_URL'] + cred_data['CREATE_INSPECTION'])
    try_dictionary_loading(selenium)
    try:
        select_division = by_xpath(selenium, SPECIFIC_SELECTION(selenium, "(Undefined)")).click()
    except:
        pass
    select_project2 = by_xpath(selenium, SPECIFIC_SELECTION(selenium, "Project 2 (2)")).click()

    try:
        try_by_xpath(selenium, SP_PLAN_MODE_CHECKED).click()
        try:
            try_by_xpath(selenium, SP_PLAN_MODE_CHECKED).click()
        except:
            pass
            # print("plan mode removed after project")
    except:
        pass

    try:
        try_by_xpath(selenium, SP_PLAN_MODE_CHECKED).click()
        try:
            try_by_xpath(selenium, SP_PLAN_MODE_CHECKED).click()
        except:
            pass
            # print("plan mode removed after equipment")
    except:
        pass

    select_specific_checklist = by_xpath(selenium, (SPECIFIC_SELECTION(selenium, checklist))).click()
    select_rp = by_xpath(selenium, SPECIFIC_SELECTION(selenium, "FTQ Automation Group (D1)")).click()
    select_equipment = by_xpath(selenium, SPECIFIC_SELECTION(selenium, "equipment (2-1)")).click()
    check_inspection_created_successfully = wait_clickable(selenium, INSP_HEADER)
    assert "Inspection" in selenium.title
    click_save_btn = by_xpath(selenium, INSP_SAVE_BUTTON).click()


@pytest.fixture
@when(parsers.parse("I create an ITP inspection via '{project}', plan item '{plan_item}'"))
def create_specific_itp_inspection(selenium, project, plan_item, cred_data, pass_data_to_step):
    pass_data_to_step['plan_item'] = plan_item
    go_to_create_insp = selenium.get(cred_data['BASE_URL'] + cred_data['CREATE_INSPECTION'])
    try_dictionary_loading(selenium)
    try:
        select_division = by_xpath(selenium, SPECIFIC_SELECTION(selenium, "(Undefined)")).click()
    except:
        pass
    try:
        select_project = by_xpath(selenium, SPECIFIC_SELECTION(selenium, 'ITP Project (4)')).click()
        select_phase = by_xpath(selenium, SPECIFIC_SELECTION(selenium, 'ITP Phase 1 (4-3)')).click()
    except:
        pass
    if "[4-3]" in plan_item:
        select_equipment = by_xpath(selenium, SPECIFIC_SELECTION(selenium, 'No Equipment')).click()
    else:
        select_equipment = by_xpath(selenium, SPECIFIC_SELECTION(selenium, 'ITP Equipment (4-1)')).click()
    plan_mode = selenium.execute_script("return arguments[0].childNodes[1].checked;",
                                        by_xpath(selenium, SP_PLAN_MODE_CHECKBOX))
    if plan_mode != 'true':
        by_xpath(selenium, SP_PLAN_MODE_CHECKBOX_INPUT).click()
        time.sleep(2)
    select_specific_checklist = by_xpath(selenium, (SPECIFIC_SELECTION(selenium, plan_item))).click()
    try:
        try_by_xpath(selenium, POPUP_OK_BUTTON).click()
    except:
        pass

    # check itp created successfully
    itp_inspection = by_xpath(selenium, INSP_HEADER_ITP)
    assert "Inspection" in selenium.title


@pytest.fixture
@when("I sync inspection to server")
@then("Inspection syncs to server successfully")
@when("I click on 'Save' button on inspection page")
@then("I wait until Inspection ID value appears on inspection page")
def inspection_id_appears(selenium, pass_data_to_step):
    try:
        click_save_btn = by_xpath(selenium, INSP_SAVE_BUTTON).click()
    except:
        pass
    wait_clickable_long(selenium, INSP_SYNCED_TEXT)
    wait_clickable(selenium, INSP_ID)
    pass_data_to_step['insp_ID'] = by_xpath(selenium, INSP_ID).text


@pytest.fixture
@then("inspection created successfully")
def inspection_created(selenium):
    check_inspection_created_successfully = wait_clickable(selenium, INSP_HEADER)
    assert "Inspection" in selenium.title


@pytest.fixture
@when("I save inspection to device")
def save_insp_to_device(selenium):
    try:
        click_save_btn = by_xpath(selenium, INSP_SAVE_BUTTON).click()
    except:
        pass
    wait_clickable_long(selenium, INSP_SAVED_TO_DEVICE_TEXT)


@pytest.fixture
@then("inspection is saved to device")
def insp_saved_to_device(selenium):
    wait_clickable_long(selenium, INSP_SAVED_TO_DEVICE_TEXT)


@pytest.fixture
@then("I should see 'Synced to server' inspection status")
def synced_to_server(selenium):
    wait_clickable_long(selenium, INSP_SYNCED_TEXT)


@pytest.fixture
@when(parsers.parse("I override the inspection status to '{inspection_status}'"))
@when(parsers.parse("I override the inspection status to {inspection_status}"))
def override_status(selenium, inspection_status):
    click_insp_status_dropdown = by_xpath(selenium, INSP_HEADER_STATUS_DROPDOWN).click()
    status_options = by_xpath_multiple(selenium, INSP_HEADER_STATUS_DROPDOWN + INSP_HEADER_DROPDOWN_MENU_ITEMS)
    status_options_list = [x.text for x in status_options]
    index = status_options_list.index(inspection_status)
    click_override = by_xpath(selenium, INSP_HEADER_STATUS_OVERRIDE).click()
    click_status = by_xpath(selenium,
                            f"({INSP_HEADER_STATUS_DROPDOWN + INSP_HEADER_DROPDOWN_MENU_ITEMS})[{index + 1}]").click()


@pytest.fixture
@when(parsers.parse("I enter '{note}' in the Notes field in the inspection header"))
def enter_note_in_inspection_header(selenium, note, pass_data_to_step):
    if "time" in note:
        now = datetime.datetime.now().strftime('%m-%d-%Y_%H-%M-%S')
        combined_note = note + " " + now
        by_xpath(selenium, INSP_HEADER_NOTE).click()
        by_xpath(selenium, INSP_HEADER_NOTE_INPUT).send_keys(combined_note)
        by_xpath(selenium, INSP_SAVE_BUTTON).click()
        pass_data_to_step['header_note'] = combined_note
        time.sleep(1)
        assert by_xpath(selenium, INSP_HEADER_NOTE).text == combined_note
    else:
        by_xpath(selenium, INSP_HEADER_NOTE).click()
        by_xpath(selenium, INSP_HEADER_NOTE_INPUT).send_keys(note)
        by_xpath(selenium, INSP_SAVE_BUTTON).click()
        pass_data_to_step['header_note'] = note
        time.sleep(1)
        assert by_xpath(selenium, INSP_HEADER_NOTE).text == note
    try:
        try_by_xpath(selenium, INSP_SAVE_BUTTON).click()
    except:
        by_xpath(selenium, INSP_SYNCED_TEXT).click()
    time.sleep(2)


@pytest.fixture
@when("I refresh inspection from server")
def refresh_inspection_from_server(selenium):
    scroll_to_bottom(selenium)
    time.sleep(1)
    by_xpath(selenium, INSP_REFRESH_FROM_SERVER).click()
    by_xpath(selenium, POPUP_OK_BUTTON).click()
    wait_clickable(selenium, INSP_REFRESH_FROM_SERVER)


# Inspection Standard Checkpoints
@pytest.fixture
@when(parsers.parse("I click on '{status}' checkbox in '{checkpoint}' checkpoint on inspection page"))
def click_status_status(selenium, checkpoint, status, pass_data_to_step):
    checkpoint_name = by_xpath(selenium, INSP_CHECKPOINT(selenium, checkpoint))
    scroll_to_checkpoint = scroll_into_view(selenium, checkpoint_name)
    checkpoint_status_select = by_xpath(
        selenium, (INSP_CHECKPOINT(selenium, checkpoint)
                   + CHECKPOINT_STATUS()[status]))
    checkpoint_status_select.click()
    try:
        try_by_xpath(selenium, POP_UP_CONFIRM).click()
    except:
        pass
    time.sleep(2)
    pass_data_to_step['checkpoint_status_xpath'] = checkpoint_status_select


@pytest.fixture
@when(parsers.parse("I enter '{note}' note in '{checkpoint}' checkpoint observation field"))
def enter_note_in_checkpoint_observations_notes(selenium, note, pass_data_to_step, checkpoint):
    scroll_into_view(selenium, INSP_CHECKPOINT(selenium, checkpoint))
    by_xpath(selenium, INSP_CHECKPOINT(selenium, checkpoint) + CP_NOTE).click()
    if "time" in note:
        now = datetime.datetime.now().strftime('%m-%d-%Y_%H-%M-%S')
        combined_note = note + " " + now
        by_xpath(selenium, CP_ANY_NOTE_EDIT).send_keys(combined_note)
        pass_data_to_step['note'] = combined_note
        time.sleep(1)
        try:
            try_by_xpath(selenium, INSP_SAVE_BUTTON).click()
        except:
            by_xpath(selenium, INSP_SYNCED_TEXT).click()
        checkpoint_text_actual = by_xpath(selenium, INSP_CHECKPOINT(selenium, checkpoint) + CP_NOTE).text
        try:
            assert checkpoint_text_actual == combined_note
        except:
            pytest.fail(f"Checkpoint text '{checkpoint_text_actual}' is not the expected '{combined_note}' text")
    else:
        edit_note = by_xpath(selenium, CP_ANY_NOTE_EDIT)
        edit_note.clear()
        edit_note.send_keys(note)
        pass_data_to_step['note'] = note
        time.sleep(1)
        checkpoint_text_actual = by_xpath(selenium, INSP_CHECKPOINT(selenium, checkpoint) + CP_NOTE).text
        try:
            try_by_xpath(selenium, INSP_SAVE_BUTTON).click()
        except:
            by_xpath(selenium, INSP_SYNCED_TEXT).click()
        try:
            assert checkpoint_text_actual == note
        except:
            pytest.fail(f"Checkpoint text '{checkpoint_text_actual}' is not the expected '{note}' text")
    try:
        try_by_xpath(selenium, INSP_SAVE_BUTTON).click()
    except:
        by_xpath(selenium, INSP_SYNCED_TEXT).click()
    time.sleep(2)


@pytest.fixture
@when(parsers.parse("I attach '{attachment_number}' files to '{checkpoint_name}' checkpoint on inspection page"))
def attach_multiple_files_to_checkpoint(selenium, checkpoint_name, attachment_number, pass_data_to_step):
    for x in range(int(attachment_number)):
        click_camera_button = by_xpath(selenium, (INSP_CHECKPOINT(selenium, checkpoint_name) + CP_CAMERA)).click()
        selenium.execute_script("$('.cm-file').css('display', 'block')")
        file_input = by_xpath_simple(selenium, "//input[@class='cm-file']")
        file_input.send_keys(r'C:\Workspace\AutomationAttachments\FTQ-square.png')
        click_save = by_xpath(selenium, CP_ATTACHMENT_EDITOR_SAVE).click()
        wait_clickable(selenium, INSP_CHECKPOINT(selenium, checkpoint_name) + CP_ATTACHMENT)
    pass_data_to_step['checkpoint_name'] = checkpoint_name
    pass_data_to_step['attachment_number'] = attachment_number
    get_number_of_attachments = by_xpath_multiple(selenium, INSP_CHECKPOINT(selenium, checkpoint_name) + CP_ATTACHMENT)
    attachment_list = [x for x in get_number_of_attachments]
    assert len(attachment_list) == int(attachment_number)


@pytest.fixture
@when(parsers.parse("I attach file to '{checkpoint_name}' checkpoint on inspection page"))
def attach_file_to_checkpoint(selenium, checkpoint_name, pass_data_to_step):
    scroll_to_checkpoint = scroll_into_view(selenium, by_xpath(selenium, (INSP_CHECKPOINT(selenium, checkpoint_name))))
    click_camera_button = by_xpath(selenium, (INSP_CHECKPOINT(selenium, checkpoint_name) + CP_CAMERA)).click()
    selenium.execute_script("$('.cm-file').css('display', 'block')")
    file_input = by_xpath_simple(selenium, "//input[@class='cm-file']")
    file_input.send_keys(r'C:\Workspace\AutomationAttachments\FTQ-square.png')
    click_save = by_xpath(selenium, CP_ATTACHMENT_EDITOR_SAVE).click()
    wait_clickable(selenium, INSP_CHECKPOINT(selenium, checkpoint_name) + CP_ATTACHMENT)
    pass_data_to_step['checkpoint_name'] = checkpoint_name


@pytest.fixture
@when("I email the inspection to the testing email address with a custom number to identify the email")
def email_inspection(selenium, pass_data_to_step):
    email_recipient = "testing+{:12}@ftq360.com".format(random.randrange(1, 10 ** 12))
    pass_data_to_step['email_recipient'] = email_recipient
    click_email_button = by_xpath(selenium, INSP_FOOTER_EMAIL_BUTTON).click()
    wait_clickable(selenium, POPUP)
    click_to_enter_email = by_xpath(selenium, EMAIL_TO_ADDRESS_FIELD).click()
    enter_email = by_xpath(selenium, EMAIL_TO_ADDRESS_FIELD_INPUT).click()
    clear_emails = by_xpath(selenium, EMAIL_TO_ADDRESS_FIELD_INPUT).clear()
    assert by_xpath(selenium, EMAIL_TO_ADDRESS_FIELD_INPUT).get_attribute("innerText") == ""
    enter_email = by_xpath(selenium, EMAIL_TO_ADDRESS_FIELD_INPUT).send_keys(email_recipient)
    press_enter = by_xpath(selenium, EMAIL_TO_ADDRESS_FIELD_INPUT).send_keys(Keys.ENTER)
    click_to_enter_cc_email = by_xpath(selenium, EMAIL_CC_ADDRESS_FIELD).click()
    enter_email = by_xpath(selenium, EMAIL_CC_ADDRESS_FIELD_INPUT).click()
    clear_emails = by_xpath(selenium, EMAIL_CC_ADDRESS_FIELD_INPUT).clear()
    assert by_xpath(selenium, EMAIL_CC_ADDRESS_FIELD_INPUT).get_attribute("innerText") == ""
    send_email = by_xpath(selenium, POP_UP_CONFIRM).click()
    assert by_xpath(selenium, TOAST_MESSAGE).text == "Email sent"


@pytest.fixture
@when("I click get PDF on the inspection page")
def get_pdf(selenium):
    wait_clickable_long(selenium, INSP_GET_PDF_ICON_ENABLED)
    click_get_pdf_button = by_xpath(selenium, INSP_GET_PDF_BUTTON).click()
    time.sleep(1)
    # try:
    #     wait_clickable(selenium, INSP_GET_PDF_ICON_DISABLED)
    # except:
    # wait_clickable_long(selenium, INSP_GET_PDF_ICON_DISABLED)
    wait_clickable_long(selenium, INSP_GET_PDF_ICON_ENABLED)
    # by_xpath(selenium, INSP_GET_PDF_BUTTON).click()
    # wait_clickable(selenium, INSP_GET_PDF_ICON_DISABLED)


@pytest.fixture
@when("I click email button on inspection page")
def click_email_button_on_inspection_page(selenium):
    by_xpath(selenium, INSP_FOOTER_EMAIL_BUTTON).click()
    wait_present(selenium, EMAIL_DIALOG_HEADER)
    assert by_xpath(selenium, EMAIL_DIALOG_HEADER).text == "Send Notification"


# Setup Steps

@pytest.fixture
@given(parsers.parse("I navigate to '{tab_name}' process menu tab"))
def go_to_process_tab(selenium, tab_name):
    # time.sleep(5)
    wait_clickable_long(selenium, SETUP_PROCESS_MENU_TITLES)
    process_tabs = by_xpath_multiple(selenium, SETUP_PROCESS_MENU_TITLES)
    process_tabs_list = [x.text for x in process_tabs]
    index = process_tabs_list.index(tab_name) + 1
    process_button = "(" + SETUP_PROCESS_MENU_TITLES + f")[{index}]"
    try:
        scroll_into_view(selenium, process_button)
    except:
        pass
    click_project_radio_button = by_xpath(selenium, "(" + SETUP_PROCESS_MENU_TITLES + f")[{index}]").click()


@pytest.fixture
@when(parsers.parse("I select '{checklist_name}' checklist"))
def select_single_checklist(selenium, checklist_name):
    # ensure no other checklists are selected before beginning
    wait_clickable(selenium, SETUP_GRID)
    time.sleep(1)
    get_checkboxes = by_xpath_multiple(selenium, SETUP_GRID_CHECKBOX)
    checkbox_checked_list = [x.get_attribute("checked") for x in get_checkboxes]
    try:
        index = checkbox_checked_list.index("true") + 1
        click_checklist = by_xpath(selenium, f"({SETUP_GRID_CHECKBOX})[{index}]").click()
    except:
        pass

    time.sleep(1)
    get_checklist_names = by_xpath_multiple(selenium, SETUP_GRID_NAME)
    checklist_names_list = [x.text for x in get_checklist_names]
    index = checklist_names_list.index(checklist_name) + 1
    click_checklist = by_xpath(selenium, f"({SETUP_GRID_CHECKBOX})[{index}]").click()


@pytest.fixture
@when(parsers.parse("I select '{user}' user on User Setup page"))
def select_user(selenium, user, pass_data_to_step):
    pass_data_to_step['user'] = user
    users_list = by_xpath_multiple(selenium, USER_USERNAMES)
    users_names_list = [x.text for x in users_list]
    index = users_names_list.index(user) + 1
    click_user = by_xpath(selenium, f"({SETUP_GRID_CHECKBOX})[{index}]").click()


# Dashboard Steps
@pytest.fixture
@when("I change date filtering to 'Last 7 days (until now)'")
def date_filtering_last_7(selenium):
    by_xpath(selenium, DASH_DATE_FILTER).click()
    date_options = by_xpath_multiple(selenium, DASH_DROPDOWN_OPTIONS)
    date_options_list = [x.text for x in date_options]
    index = date_options_list.index('Last 7 days (until now)')
    by_xpath(selenium, f"({DASH_DROPDOWN_OPTIONS})[{index + 1}]").click()


@pytest.fixture
@when(parsers.parse(
    "I filter by '{filter_option}' in '{dash_filter}' filter dropdown on {dashboard} Dashboard, which filters the dashboard accordingly"))
def filter_specific_dash_by_date_range(selenium, filter_option, dash_filter, dashboard):
    # get data before filtering
    dash_data_before_filtering = []
    if dashboard in ['RP', 'Inspection Activity']:
        data_to_append = by_xpath(selenium, RP_DASH_TOTAL_DEF_NUMBER).text
        dash_data_before_filtering.append(data_to_append)
    elif dashboard in ['Priority Open Item']:
        data_to_append = by_xpath(selenium, PRIORITYDASH_INSPECTIONS_TOTAL).text
        dash_data_before_filtering.append(data_to_append)

    # perform filter
    if dash_filter == "date range":
        # open_date_filter_dropdown = by_xpath(selenium, RP_DASH_DATE_FILTER).click()
        date_options = by_xpath_multiple(selenium, ALLDASH_DATE_FILTER_OPTIONS)
        date_list = [x.text for x in date_options]
        index = date_list.index(filter_option) + 1
        click_correct_date_filter = by_xpath(selenium, f"({ALLDASH_DATE_FILTER_OPTIONS})[{index}]").click()
    elif dash_filter == "project":
        # open_proj_filter_dropdown = by_xpath(selenium, RP_DASH_PROJECT_FILTER).click()
        proj_options = by_xpath_multiple(selenium, ALLDASH_PROJECT_FILTER_OPTIONS)
        proj_list = [x.text for x in proj_options]
        index = proj_list.index(filter_option) + 1
        click_correct_date_filter = by_xpath(selenium, f"({ALLDASH_PROJECT_FILTER_OPTIONS})[{index}]").click()
    time.sleep(1)
    wait_invisible_long(selenium, PROCESSING_MASK)

    # verify changes reflected
    if dashboard in ['RP', 'Inspection Activity']:
        if dash_filter == 'project' and dashboard == 'RP':
            pass
        else:
            dash_data_after_filtering = by_xpath(selenium, RP_DASH_TOTAL_DEF_NUMBER).text
            assert int(dash_data_before_filtering[0]) < int(dash_data_after_filtering)
    elif dashboard in ['Priority Open Item']:
        dash_data_after_filtering = by_xpath(selenium, PRIORITYDASH_INSPECTIONS_TOTAL).text
        assert int(dash_data_before_filtering[0]) < int(dash_data_after_filtering)


@when(parsers.parse("I filter by '{filter_option}' in '{dash_filter}' filter dropdown"))
def filter_dash_by_date_range(selenium, filter_option, dash_filter):
    # perform filter
    if dash_filter == "date range":
        # open_date_filter_dropdown = by_xpath(selenium, RP_DASH_DATE_FILTER).click()
        date_options = by_xpath_multiple(selenium, ALLDASH_DATE_FILTER_OPTIONS)
        date_list = [x.text for x in date_options]
        index = date_list.index(filter_option) + 1
        click_correct_date_filter = by_xpath(selenium, f"({ALLDASH_DATE_FILTER_OPTIONS})[{index}]").click()
    elif dash_filter == "project":
        # open_proj_filter_dropdown = by_xpath(selenium, RP_DASH_PROJECT_FILTER).click()
        proj_options = by_xpath_multiple(selenium, ALLDASH_PROJECT_FILTER_OPTIONS)
        proj_list = [x.text for x in proj_options]
        index = proj_list.index(filter_option) + 1
        click_correct_date_filter = by_xpath(selenium, f"({ALLDASH_PROJECT_FILTER_OPTIONS})[{index}]").click()
    time.sleep(1)
    wait_invisible_long(selenium, PROCESSING_MASK)


@pytest.fixture
@when(parsers.parse("I select '{dash_data}' in '{dash_panel}' dashboard panel, which filters '{filtered_panel}' panel"))
def select_data_in_dashboard_panel(selenium, dash_data, dash_panel, filtered_panel, pass_data_to_step):
    wait_clickable(selenium, DASH_GRID)
    # get data of panels prior to any changes
    if "All" in dash_data:
        pass
    else:
        panel_titles = by_xpath_multiple(selenium, ALLDASH_PANEL_TITLE)
        panel_title_list = [x.text for x in panel_titles]
        panel_to_edit = panel_title_list.index(dash_panel) + 1
        panel_to_be_filtered = panel_title_list.index(filtered_panel) + 1
        panel_to_be_filtered_items = by_xpath_multiple(selenium,
                                                       f'({ALLDASH_PANEL_TITLE})[{panel_to_be_filtered}]' + ALLDASH_PANEL_ITEM)
        panel_data_before_filter = [x.text.splitlines() for x in panel_to_be_filtered_items]

        # make changes
        items_in_panel_to_edit = by_xpath_multiple(selenium,
                                                   f'({ALLDASH_PANEL_TITLE})[{panel_to_edit}]' + ALLDASH_PANEL_ITEM_TITLE)
        panel_to_edit_item_list = [x.text for x in items_in_panel_to_edit]
        item_index = panel_to_edit_item_list.index(dash_data) + 1
        click_item_in_panel = by_xpath(selenium,
                                       f'(({ALLDASH_PANEL_TITLE})[{panel_to_edit}]' + f'{ALLDASH_PANEL_ITEM_TITLE})[{item_index}]').click()
        wait_invisible(selenium, PROCESSING_MASK)

        # verify changes
        get_filtered_item_list_from_panel = by_xpath_multiple(selenium,
                                                              f'({ALLDASH_PANEL_TITLE})[{panel_to_be_filtered}]' + ALLDASH_PANEL_ITEM)
        panel_data_after_filter = [x.text.splitlines() for x in get_filtered_item_list_from_panel]
        pass_data_to_step['panel_data_after_filter'] = panel_data_after_filter
        pass_data_to_step['filtered_panel'] = filtered_panel
        assert panel_data_before_filter != panel_data_after_filter


# Listing Grid Steps
@pytest.fixture
@when("I open a recent inspection")
def open_recent_inspection(selenium):
    click_recent_insp_link = by_xpath(selenium, f"({GRID_ITEM_NAME})[{random.randrange(1, 10)}]").click()
    wait_clickable(selenium, INSP_HEADER)
    try_dictionary_loading(selenium)


@pytest.fixture
@when("I click on the Settings cog")
def click_settings_cog(selenium):
    wait_clickable(selenium, SETUP_GRID)
    selenium.execute_script("arguments[0].scrollleft = arguments[0].offsetWidth", SETUP_GRID_CONTAINER)
    time.sleep(1)
    by_xpath(selenium, SETUP_COG_ICON).click()


@pytest.fixture
@when(parsers.parse("I use manage columns to make '{column}' column visible"))
def manage_column_visible(selenium, column):
    click_manage_columns = by_xpath(selenium, SETUP_COG_MANAGE_COLUMNS).click()
    columns_names = [x.text for x in by_xpath_multiple(selenium, MANAGE_COLUMN_ALLTITLES)]
    index = columns_names.index(column) + 1
    current_visibility = by_xpath(selenium, f"({MANAGE_COLUMN_ALLTITLES})[{index}]/..//i").get_attribute("class")
    time.sleep(2)
    if "circular-mark-disabled" in current_visibility:
        by_xpath(selenium, f"({MANAGE_COLUMN_ALLTITLES})[{index}]/..//i").click()
        assert "circular-mark-enabled" in by_xpath(selenium,
                                                   f"({MANAGE_COLUMN_ALLTITLES})[{index}]/..//i").get_attribute("class")
    else:
        pass
    by_xpath(selenium, SETUP_POPUP_SAVE).click()
    wait_invisible(selenium, SETUP_POPUP_FRONT)
    time.sleep(1)


@pytest.fixture
@when(parsers.parse("I filter for '{project}' on ITP Progress page"))
def filter_itp_project(selenium, project):
    wait_clickable(selenium, ITPPROG_PLAN_ITEM_LINK)
    time.sleep(1)
    if by_xpath(selenium, ITPPROG_PROJ_FILTER_CURRENT).text != project:
        page_filter = by_xpath(selenium, ITPPROG_PROJ_FILTER_CURRENT).click()
        filter_options = by_xpath_multiple(selenium, ITPPROG_PROJ_FILTER_OPTIONS)
        filter_options_list = [x.text for x in filter_options]
        index = filter_options_list.index(project) + 1
        click_to_filter_for_project = by_xpath(selenium, f"({ITPPROG_PROJ_FILTER_OPTIONS})[{index}]").click()
    wait_clickable(selenium, ITPPROG_PLAN_ITEM_LINK)


# Customization Preferences Steps
@pytest.fixture
@given("I save changes on Customization Preferences page")
def save_customization_changes(selenium):
    save_preference = by_xpath(selenium, CUSTOMPREF_SAVE_BUTTON).click()
    wait_present(selenium, SETUP_ALL_SAVED_CONFIRMATION_MASK)
    time.sleep(2)


# Import Steps
@pytest.fixture
@then("the import should be finalized without any errors")
def import_successful(selenium):
    wait_clickable(selenium, IMPORT_MESSAGE)
    import_message = by_xpath(selenium, IMPORT_MESSAGE).get_attribute('outerText')
    writing = re.match(
        'Import process starting\.\.\.\nImport process step: Writing [a-zA-Z]+\.csv\nImport process ending \(any errors are reported on the process steps\)',
        import_message)
    processing = re.match(
        'Import process step: Writing [a-zA-Z]+\.csv\nImport process ending \(any errors are reported on the process steps\)',
        import_message)
    updating = re.match(
        'Import process starting\.\.\.\nImport process step: Writing [a-zA-Z]+\.csv\nImport process step: Updated filters for [0-9]+ record\(s\)\nImport process ending \(any errors are reported on the process steps\)',
        import_message)
    underscored = re.match(
        'Import process starting\.\.\.\nImport process step: Writing [a-zA-Z]+_[a-zA-Z]+\.csv\nImport process ending \(any errors are reported on the process steps\)',
        import_message)
    updating_underscored = re.match(
        'Import process starting\.\.\.\nImport process step: Writing [a-zA-Z]+_[a-zA-Z]+\.csv\nImport process step: Updated filters for [0-9]+ record\(s\)\nImport process ending \(any errors are reported on the process steps\)',
        import_message)
    assert any([writing, updating, processing, underscored, updating_underscored])
    assert "Missing" not in import_message


# Report Steps
@pytest.fixture
@given(parsers.parse("I select '{report}' report"))
@given(parsers.parse("I select a {report}"))
def select_report(selenium, report, pass_data_to_step):
    pass_data_to_step['report'] = report
    time.sleep(5)
    wait_clickable(selenium, REPORT_NAMES)
    report_names = by_xpath_multiple(selenium, REPORT_NAMES)
    report_names_list = [x.text for x in report_names]
    index = report_names_list.index(report) + 1
    click_report_radio_button = by_xpath(selenium, f"({REPORT_SELECT})[{index}]").click()
    wait_clickable(selenium, REPORT_RUN_REPORT_BUTTON)


@pytest.fixture
@when("I click on 'Run report' button")
def click_run_report(selenium):
    click_button = by_xpath(selenium, REPORT_RUN_REPORT_BUTTON).click()
    wait_clickable(selenium, REPORT_HISTORY_TABLE)


@pytest.fixture
@then("I should see the report on Report History page")
@then(parsers.parse("I should see the {report} on Report History page"))
def report_history_page_correct(selenium, report, pass_data_to_step):
    try:
        report = pass_data_to_step['report']
    except:
        pass
    wait_clickable(selenium, REPORT_HISTORY_REFRESH)
    if "FTQ-" in report:
        report_name_split = report.split(" ")
        report_number = report_name_split[0].strip("FTQ-")
        assert report_number in by_xpath(selenium, REPORT_HISTORY_TABLE_FIRST_ROW_NAME).text
    elif "Data Query" in report:
        report_number = report.split(" ")
        assert report_number[0] in by_xpath(selenium, REPORT_HISTORY_TABLE_FIRST_ROW_NAME).text
    else:
        pytest.fail("report not found")

    # get report data for assertions
    most_recent_report = by_xpath(selenium, "//tr[1]" + REPORT_REQUEST_DATE).text
    get_all_request_dates = by_xpath_multiple(selenium, REPORT_REQUEST_DATE)
    request_date_list = [x.text for x in get_all_request_dates]
    pass_data_to_step['report_list_len'] = len(request_date_list)
    pass_data_to_step['report_request_date'] = most_recent_report


@pytest.fixture
@then("query should complete processing")
@then("report should complete processing")
def report_complete(selenium):
    time.sleep(5)
    start_time = time.time()
    while by_xpath(selenium,
                   REPORT_HISTORY_TABLE_FIRST_ROW_STATUS).text != "Completed" and time.time() - start_time < 900:
        refresh_report_history = by_xpath(selenium, REPORT_HISTORY_REFRESH).click()
        time.sleep(2)
        wait_clickable(selenium, REPORT_HISTORY_REFRESH)
    assert by_xpath(selenium, REPORT_HISTORY_TABLE_FIRST_ROW_STATUS).text == "Completed"


# --- Old Setup Fixtures --- #
@pytest.fixture
@given(parsers.parse("I save changes on '{old_setup}' page"))
@given("I save changes on Activate Reports page")
def save_changes_on_activate_reports_page(selenium):
    scroll_into_view(selenium, by_xpath(selenium, SETUP_ALL_SAVE_BUTTONS))
    by_xpath(selenium, SETUP_ALL_SAVE_BUTTONS).click()
    try:
        try_by_xpath(selenium, POP_UP_CONFIRM).click()
        wait_present(selenium, SETUP_ALL_SAVED_CONFIRMATION_MASK)
    except:
        wait_present(selenium, SETUP_ALL_SAVED_CONFIRMATION_MASK)
