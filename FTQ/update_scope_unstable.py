import os

import xmltodict

listof_fixed_items = []
listof_unstable_items = []
for dirpath, dirnames, filenames in os.walk(r"C:\Workspace\Python-Autmoation\FTQ\tests"):
    for filename in filenames:
        if filename.endswith(".py"):
            path_and_file = os.path.join(dirpath, filename)
            file = open(path_and_file, "r")
            if "recently-fixed" in file.read():
                listof_fixed_items.append(path_and_file)
            elif "unstable" in file.read():
                listof_unstable_items.append(path_and_file)

for item in listof_fixed_items:
    try:
        listof_unstable_items.remove(item)
    except:
        pass

fixed_list = [x.replace(r"C:\Workspace\Python-Autmoation\FTQ", "") for x in listof_fixed_items]
unstable_list = [x.replace(r"C:\Workspace\Python-Autmoation\FTQ", "") for x in listof_unstable_items]

feature_list_fixed = [x.replace(r"\tests", r"features").replace("_tests", "").replace("test_", "").replace("dashboard\\", "dashboards\\").replace("listing_grid\\", "listing_grids\\").replace(".py", ".feature") for x in fixed_list]
feature_list_unstable = [x.replace(r"\tests", r"features").replace("_tests", "").replace("test_", "").replace("dashboard\\", "dashboards\\").replace("listing_grid\\", "listing_grids\\").replace(".py", ".feature") for x in unstable_list]

test_list_fixed = [x.replace(r"\tests", "tests") for x in fixed_list]
test_list_unstable = [x.replace(r"\tests", "tests") for x in unstable_list]

combinedlist_fixed = feature_list_fixed + test_list_fixed
combinedlist_unstable = feature_list_unstable + test_list_unstable

fixed_files = "||file:".join(combinedlist_fixed).replace("\\", "/")
unstable_files = "||file:".join(combinedlist_unstable).replace("\\", "/")

fixed_scope_dir = r"C:\Workspace\Python-Autmoation\.idea\scopes\Recently_Fixed_Tests.xml"
read_fixed_scope_file = open(fixed_scope_dir, "r")
fixed_scope_contents = xmltodict.parse(read_fixed_scope_file.read())
new_fixed_pattern = "file:" + fixed_files
fixed_scope_contents['component']['scope']['@pattern'] = new_fixed_pattern
write_scope_file = open(fixed_scope_dir, "w")
write_scope_file.write(xmltodict.unparse(fixed_scope_contents))

unstable_scope_dir = r"C:\Workspace\Python-Autmoation\.idea\scopes\Collected_Unstable_Tests.xml"
read_unstable_scope_file = open(unstable_scope_dir, "r")
unstable_scope_contents = xmltodict.parse(read_unstable_scope_file.read())
new_unstable_pattern = "file:" + unstable_files
unstable_scope_contents['component']['scope']['@pattern'] = new_unstable_pattern
write_scope_file = open(unstable_scope_dir, "w")
write_scope_file.write(xmltodict.unparse(unstable_scope_contents))
