import os

import xmltodict

listofitems = []
for dirpath, dirnames, filenames in os.walk(r"C:\Workspace\Python-Autmoation\FTQ\tests"):
    for filename in filenames:
        if filename.endswith(".py"):
            path_and_file = os.path.join(dirpath, filename)
            file = open(path_and_file, "r")
            if "recently-fixed" in file.read():
                listofitems.append(path_and_file)

newlist = [x.replace(r"C:\Workspace\Python-Autmoation\FTQ", "") for x in listofitems]
feature_list = [x.replace(r"\tests", r"features").replace("_tests", "").replace("test_", "").replace("dashboard\\", "dashboards\\").replace("listing_grid\\", "listing_grids\\").replace(".py", ".feature") for x in newlist]
newlist2 = [x.replace(r"\tests", "tests") for x in newlist]
combinedlist = newlist2 + feature_list
new_files = "||file:".join(combinedlist).replace("\\", "/")
scope_dir = r"C:\Workspace\Python-Autmoation\.idea\scopes\Recently_Fixed_Tests.xml"
read_scope_file = open(scope_dir, "r")
scope_contents = xmltodict.parse(read_scope_file.read())
pattern = scope_contents['component']['scope']['@pattern']
new_pattern = "file:" + new_files
scope_contents['component']['scope']['@pattern'] = new_pattern
write_scope_file = open(scope_dir, "w")
print(scope_contents)
write_scope_file.write(xmltodict.unparse(scope_contents))
