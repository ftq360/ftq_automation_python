from pytest_bdd import scenario, when, then, parsers

from functions import *


@pytest.mark.xray('DEV-8089')
@scenario("../features/MS_Google_login_sso.feature", "MS/Google Login")
def test_MS_Google_login_sso():
    """test users can login with SSO buttons"""


@when(parsers.parse("I click on '{sso_login}' button"))
def click_sso_button(selenium, sso_login, pass_data_to_step):
    if sso_login == "microsoft":
        by_xpath(selenium, LG_MICROSOFT).click()
    elif sso_login == "google":
        time.sleep(2)
        wait_clickable(selenium, LG_GOOGLE)
        by_xpath(selenium, LG_GOOGLE).click()
    pass_data_to_step['sso_login'] = sso_login


@then("an SSO window is opened")
def sso_window_opened(selenium, pass_data_to_step):
    sso_login = pass_data_to_step['sso_login']
    if sso_login == "microsoft":
        WebDriverWait(selenium, 20).until(EC.url_contains("login.microsoftonline.com"))
    elif sso_login == "google":
        WebDriverWait(selenium, 20).until(EC.url_contains("google"))


@then(parsers.parse("I can login with my {sso_credentials}"))
def login_with_sso(selenium, cred_data, sso_credentials, pass_data_to_step):
    sso_login = pass_data_to_step['sso_login']
    if sso_login == "microsoft":
        enter_microsoft_email = by_xpath(selenium, LG_SSO_EMAIL).send_keys(sso_credentials)
        click_next = by_xpath(selenium, LG_MICROSOFT_SSO_SUBMIT).click()
        enter_microsoft_password = by_xpath(selenium, LG_SSO_PASSWORD).send_keys(cred_data['GMAIL_PASSWORD'])
        click_next = by_xpath(selenium, LG_MICROSOFT_SSO_SUBMIT).click()
        try:
            click_next = by_xpath(selenium, LG_MICROSOFT_SSO_SUBMIT).click()
        except:
            pass
        wait_clickable(selenium, GL_TOP_MENU)
    elif sso_login == "google":
        enter_google_email = by_xpath(selenium, LG_SSO_EMAIL).send_keys(sso_credentials)
        click_next = by_xpath(selenium, LG_GOOGLE_SSO_NEXT).click()
        enter_google_password = by_xpath(selenium, LG_SSO_PASSWORD).send_keys(cred_data['GOOGLE_PASS'])
        # click_next = by_xpath(selenium, LG_GOOGLE_SSO_SIGNIN).click()
        # wait_clickable(selenium, GL_TOP_MENU)
        pass
