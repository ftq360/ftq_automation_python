from pytest_bdd import scenario, when, parsers, then
from functions import *
from locators.setup_locators import *


@pytest.mark.xray('DEV-5116')
@scenario("../../../features/setup/user_setup/send_user_invitation.feature", "Send User Invitation")
def test_send_user_invitation():
    """test sending a user invitation works as expected"""


@when("I click 'Send Invitation'")
def send_invitation(selenium):
    by_xpath(selenium, USER_SEND_INVITATION).click()
    by_xpath(selenium, POPUP_OK_BUTTON).click()


@then(parsers.parse("user '{user}' receives an invitation email"))
def invitation_email_received(selenium, user, cred_data):
    get_email_by_subject_past_5_min(cred_data, "FTQ360 Invitation")
