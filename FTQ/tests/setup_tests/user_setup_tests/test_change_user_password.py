from pytest_bdd import scenario, then, parsers

from functions import *


@pytest.mark.xray('DEV-5101')
@scenario("../../../features/setup/user_setup/change_user_password.feature", "Change User Password")
def test_change_user_password():
    """test changing a user's password works as expected"""


@then(parsers.parse("when user '{user}' logs back in, only the new password works"))
def login_with_new_password(selenium, user, pass_data_to_step):
    password = pass_data_to_step.get('password')
    enter_username = by_xpath(selenium, LG_USERNAME_FIELD).send_keys(user)
    enter_wrong_pw = by_xpath(selenium, LG_PASSWORD_FIELD).send_keys("abc")
    login_button = by_xpath(selenium, LG_LOGIN_BUTTON).click()
    try:
        wait_clickable_short(selenium, GL_TOP_MENU)
        pytest.fail("Successful login with incorrect password")
    except:
        clear_pw_field = by_xpath(selenium, LG_PASSWORD_FIELD).clear()
        enter_new_pw = by_xpath(selenium, LG_PASSWORD_FIELD).send_keys(password)
        login_button = by_xpath(selenium, LG_LOGIN_BUTTON).click()
        wait_clickable(selenium, GL_TOP_MENU)
