from pytest_bdd import scenario, parsers, then, given

from functions import *
from locators.listing_grid_locators import GRID_ITEM_NAME


@pytest.mark.xray('DEV-6751')
@scenario("../../../features/setup/user_setup/restricted_affiliation_basic.feature", "Restricted Affiliation Basic")
def BROKEN_test_restricted_affiliation_basic():
    """test Restricted Affiliation Basic works as expected"""


@given("I open the inspection")
def open_the_inspection(selenium, pass_data_to_step):
    try:
        insp_ID = pass_data_to_step.get('insp_ID')
        insp_links = [x.text for x in by_xpath_multiple(selenium, GRID_ITEM_NAME)]
        index = insp_links.index(insp_ID) + 1
        click_recent_insp_link = by_xpath(selenium, f"({GRID_ITEM_NAME})[{index}]").click()
        wait_clickable(selenium, INSP_HEADER)
        try_dictionary_loading(selenium)
    except:
        # no permissions user should fail above steps
        # assert grid body is empty - cannot view inspections
        assert by_xpath(selenium, "//tbody").get_attribute("innerText") == ""


@then(parsers.parse("I should be able to '{permission}' the inspection"))
def permission_applied(selenium, permission, pass_data_to_step):
    if permission == "not view":
        try:
            # verify no permissions user was not able to open an inspection
            wait_clickable_short(selenium, INSP_HEADER)
            pytest.fail('No permissions user opened an inspection')
        except:
            pass
    else:
        # get checkpoint statuses
        checkpoint = pass_data_to_step.get('checkpoint_status_xpath')
        checkpoint_status = selenium.execute_script('return arguments[0].childNodes[1].checked;', checkpoint)
        checkpoint_disabled = selenium.execute_script('return arguments[0].childNodes[1].disabled;', checkpoint)

        # assert view and edit checkpoint statuses are as expected
        if permission == "edit":
            assert checkpoint_status == True
            assert checkpoint_disabled == False
        elif permission == "view":
            assert checkpoint_status == False
            assert checkpoint_disabled == True
