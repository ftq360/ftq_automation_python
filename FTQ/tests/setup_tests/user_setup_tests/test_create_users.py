from pytest_bdd import scenario, when, then

from functions import *
from locators.setup_locators import *


@pytest.mark.xray('DEV-5063')
@scenario("../../../features/setup/user_setup/create_users.feature", "Create Users")
def test_create_users():
    """test creating users works as expected"""


@when("I set user password")
def set_user_pw(selenium, cred_data):
    click_change_pw = by_xpath(selenium, USER_CHANGE_PW).click()
    enter_pw = by_xpath(selenium, USER_CHANGE_PW_NEW).send_keys(cred_data['PASSWORD'])
    confirm_pw = by_xpath(selenium, USER_CHANGE_PW_CONFIRM).send_keys(cred_data['PASSWORD'])
    save_changes = by_xpath(selenium, SETUP_POPUP_FRONT + USER_ADD_NEW_CONFIRM_BUTTON).click()


@then("that user can login successfully")
def user_can_login(selenium, log_out, pass_data_to_step, cred_data):
    username = pass_data_to_step.get('username')
    enter_new_username = by_xpath(selenium, LG_USERNAME_FIELD).send_keys(username)
    enter_pw = by_xpath(selenium, LG_PASSWORD_FIELD).send_keys(cred_data['PASSWORD'])
    login_button = by_xpath(selenium, LG_LOGIN_BUTTON).click()
    # agree_to_terms = by_xpath(selenium, LG_AGREE_TO_TERMS).click()
    click_agree = by_xpath(selenium, LG_AGREE_BUTTON).click()
    wait_clickable(selenium, GL_TOP_MENU)
