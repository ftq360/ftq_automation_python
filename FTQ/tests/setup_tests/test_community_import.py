from locators.setup_locators import *
from locators.old_setup_page_locators import *
from pytest_bdd import scenario, when, then
from functions import *
import csv


@pytest.mark.xray('DEV-5390')
@scenario("../../features/setup/community_import.feature", "Community Import")
def BROKEN_test_community_import():
    """test importing project works as expected"""


@when("I import a project CSV file")
def import_equipment_csv(selenium, pass_data_to_step):
    # create csv data
    now = time.strftime('%j%M%S')
    pass_data_to_step['imported_project'] = now
    csv_data = [['CommunityCode', 'CommunityLabel', 'Division', 'CommunityNotes', 'Email', 'ExternKey', 'ReportsDistributionList', 'ExternKeyID', 'DisplaySequence', 'Active', 'ImportMode'],
               [f'{now}', f'z_Imported Project {now}', '', '', '', '', '0', '', '100', '1', '0']]
    with open(r'C:\Workspace\AutomationAttachments\communities.csv', 'w', newline='') as f:
        f.truncate()
        writer = csv.writer(f)
        writer.writerows(csv_data)
        f.close()
    # upload csv file
    file_input = by_xpath(selenium, IMPORT_FILEINPUT)
    file_input.send_keys(r'C:\Workspace\AutomationAttachments\communities.csv')
    time.sleep(1)
    click_submit = by_xpath(selenium, IMPORT_SUBMIT).click()


@then("I should see the import present on project setup grid")
def imported_project_shown(selenium, pass_data_to_step):
    get_project_codes = by_xpath_multiple(selenium, SETUP_GRID_CODE)
    project_list = [x.text for x in get_project_codes]
    imported_project = pass_data_to_step.get('imported_project')
    assert imported_project in project_list
