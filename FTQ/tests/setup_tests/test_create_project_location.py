from datetime import datetime

from pytest_bdd import scenario, given, then, when

from functions import *
from locators.selection_process_locators import *
from locators.setup_locators import *


#OBSOLETE
@pytest.mark.skip('Unstable')
@pytest.mark.xray('DEV-5377')
@scenario("../../features/setup/create_project_location.feature", "Create Project Location")
def test_create_project_location():
    """test a user creating project equipment"""


@given("I add a new location")
def add_location(selenium, pass_data_to_step):
    click_add_button = by_xpath(selenium, SETUP_ADD_NEW).click()
    location_name = f"Location {datetime.datetime.now().strftime('%m/%d/%Y %H:%M')}"
    add_location_description = by_xpath(selenium, LOCATION_ADD_NEW_NAME).send_keys(location_name)
    save_changes = by_xpath(selenium, LOCATION_ADD_NEW_CONFIRM).click()
    code_for_new_location = by_xpath(selenium, LOCATION_DETAILS_CODE).get_attribute("value")
    pass_data_to_step['location_name'] = location_name
    pass_data_to_step['location_code'] = code_for_new_location


@when("I get to location selection page")
def click_through_to_location(selenium):
    # select_equip = by_xpath(selenium, SP_SELECT_FIRST_AVAILABLE).click()
    select_phase = by_xpath(selenium, SP_SELECT_FIRST_AVAILABLE).click()
    pass


@then("my new location is listed to select from")
def new_location_is_listed(selenium, pass_data_to_step):
    location_code = pass_data_to_step.get('location_code')
    all_selections = by_xpath_multiple(selenium, SP_LIST)
    titles_list = [txt.text for txt in all_selections]
    print(titles_list)
    print(location_code)
    assert any(location_code in x for x in titles_list)
