import random
import re

from pytest_bdd import then, when, given, parsers

from functions import *
from locators.setup_locators import *


# --- Checklist Setup Steps --- #
@pytest.fixture
@when("I edit checklist title")
def edit_checklist_title(selenium, pass_data_to_step):
    wait_clickable(selenium, CHECKLIST_DETAILS_PANEL)
    clear_title = by_xpath(selenium, CHECKLIST_DETAILS_NAME).clear()
    enter_new_title = by_xpath(selenium, CHECKLIST_DETAILS_NAME).send_keys(f"Checklist for Editing - {datetime.datetime.now().time()}")
    get_checklist_name = by_xpath(selenium, CHECKLIST_DETAILS_NAME).get_attribute("value")
    pass_data_to_step['new_title'] = get_checklist_name


@pytest.fixture
@then("changes are reflected on the checklist setup grid")
def changes_reflected_on_checklist_grid(selenium, pass_data_to_step):
    time.sleep(2)
    new_title = pass_data_to_step.get('new_title')
    get_checklist_titles = by_xpath_multiple(selenium, SETUP_GRID_NAME)
    checklist_titles_list = [x.text for x in get_checklist_titles]
    assert new_title in checklist_titles_list


@pytest.fixture
@when("I select a checklist")
def select_single_checklist(selenium, pass_data_to_step, items_in_use):
    wait_clickable(selenium, SETUP_GRID)
    codes = by_xpath_multiple(selenium, SETUP_GRID_ROW + SETUP_GRID_CODE)
    codes_list = [x.text for x in codes]
    checklists_to_avoid = items_in_use[0]
    valid_code_option_by_index = [codes_list.index(x) for x in codes_list if x not in checklists_to_avoid]
    click_checklist = by_xpath(selenium, f"({SETUP_GRID_CHECKBOX})[{(random.choice(valid_code_option_by_index) + 1)}]").click()
    get_checklist_code = by_xpath(selenium, CHECKLIST_DETAILS_CODE).get_attribute("value")
    pass_data_to_step['original_code'] = get_checklist_code
    get_checklist_name = by_xpath(selenium, CHECKLIST_DETAILS_NAME).get_attribute("value")
    pass_data_to_step['checklist_name'] = get_checklist_name


@pytest.fixture
@when(parsers.parse("I change checklist type to '{checklist_type}'"))
def enter_checklist_name(selenium, checklist_type):
    time.sleep(1)
    click_dropdown = by_xpath(selenium, CHECKLIST_ADD_NEW_CHECKLIST_TYPE).click()
    time.sleep(1)
    if checklist_type == 'Equipment':
        click_equipment_type = by_xpath(selenium, CHECKLIST_ADD_NEW_CHECKLIST_TYPE_EQUIPMENT).click()
    elif checklist_type == 'Building Block':
        click_bb_type = by_xpath(selenium, CHECKLIST_ADD_NEW_CHECKLIST_TYPE_BB).click()


@pytest.fixture
@when(parsers.parse("I enter '{text}' text in Checklist Name field"))
def enter_checklist_name(selenium, text):
    enter_checklist_name_in_field = by_xpath(selenium, CHECKLIST_ADD_NEW_ENTER_NAME).send_keys(
        text + " " + datetime.datetime.now().strftime('%m/%d/%Y %H:%M'))


@pytest.fixture
@when("I click Create Checklist button to create the checklist (checklist1)")
@when("I click Create Checklist button to create the checklist")
@when("I click Create Checklist button to create the checklists")
def click_create_checklists_button(selenium, pass_data_to_step):
    click_to_create_checklist = by_xpath(selenium, CHECKLIST_CREATE_BUTTON).click()
    try:
        checklist_code = by_xpath(selenium, CHECKLIST_DETAILS_CODE).get_attribute('value')
        pass_data_to_step['checklist_code'] = checklist_code
    except:
        pass


@pytest.fixture
@when(parsers.parse("I select checklist code '{code}'"))
def select_checklist_by_code(selenium, code, pass_data_to_step):
    wait_clickable(selenium, SETUP_GRID)
    get_checklist_codes = by_xpath_multiple(selenium, SETUP_GRID_CODE)
    checklist_codes_list = [x.text for x in get_checklist_codes]
    index = checklist_codes_list.index(code) + 1
    click_checklist = by_xpath(selenium, f"({SETUP_GRID_CHECKBOX})[{index}]").click()
    pass_data_to_step['code'] = code


@pytest.fixture
@when("I edit checklist code")
def edit_checklist_code(selenium, pass_data_to_step):
    wait_clickable(selenium, CHECKLIST_DETAILS_PANEL)
    clear_code = by_xpath(selenium, CHECKLIST_DETAILS_CODE).clear()
    enter_new_code = by_xpath(selenium, CHECKLIST_DETAILS_CODE).send_keys(f"{datetime.datetime.now().time()}")
    get_checklist_code = by_xpath(selenium, CHECKLIST_DETAILS_CODE).get_attribute("value")
    pass_data_to_step['new_code'] = get_checklist_code


@pytest.fixture
@when("I save changes on checklist details panel")
def save_checklist_details(selenium):
    click_save = by_xpath(selenium, CHECKLIST_DETAILS_SAVE).click()
    wait_clickable(selenium, TOAST_SUCCESS)


# Checkpoint Setup Steps
@pytest.fixture
@when("I go to checkpoints")
def go_to_checkpoints(selenium):
    # exclude checklists with less than 5 checkpoints
    get_checklists_with_checkpoints = by_xpath_multiple(selenium, CHECKLIST_GO_TO_CHECKPOINTS_IN_GRID)
    list_of_cp_numbers = [entry.text for entry in get_checklists_with_checkpoints]
    remove_characters = [re.sub("[^0-9]", "", f"{entry}") for entry in list_of_cp_numbers]
    int_list = [int(x) for x in remove_characters]
    index = [i + 1 for i in range(len(int_list)) if int_list[i] > 4]
    go_to_checkpoints_not_empty_checklist = (f"(//*[@class='grid-link cl-go-to-cps'])[{index[1]}]")
    checkpoints_link = by_xpath(selenium, go_to_checkpoints_not_empty_checklist)
    scroll_into_view(selenium, checkpoints_link)
    click_checkpoints_link_in_grid = checkpoints_link.click()


@pytest.fixture
@given("I click Go To Checkpoints button")
def click_go_to_checkpoints_button(selenium):
    click_go_to_checkpoints = by_xpath(selenium, CHECKLIST_GO_TO_CHECKPOINTS).click()


@when("I select a checkpoint")
def select_a_checkpoint(selenium, pass_data_to_step):
    wait_clickable(selenium, CHECKPOINT_GRID)
    checkpoint = by_xpath(selenium, CHECKPOINT_GRID_NAME)
    checkpoint.click()
    get_checkpoint_code = by_xpath(selenium, CHECKPOINT_DETAILS_CODE).get_attribute("value")
    pass_data_to_step['original_code'] = get_checkpoint_code
    pass_data_to_step['checkpoint_name'] = checkpoint.text


# --- Universal Setup Steps --- #
@pytest.fixture
@given(parsers.parse("I click Add New on {setup_page} setup page"))
def click_add_new_project_button(selenium):
    click_add_new = by_xpath(selenium, SETUP_ADD_NEW).click()


@pytest.fixture
@given(parsers.parse("I set grid filter to '{filter_option}'"))
def select_a_filter(selenium, filter_option):
    click_filter_dropdown = by_xpath(selenium, SETUP_GRID_FILTER_DROPDOWN).click()
    filter_options = by_xpath_multiple(selenium, SETUP_GRID_FILTER_DROPDOWN_OPTIONS)
    filter_list = [x.text for x in filter_options]
    index = filter_list.index(filter_option) + 1
    click_filter = by_xpath(selenium, f"({SETUP_GRID_FILTER_DROPDOWN_OPTIONS})[{index}]").click()
    wait_clickable(selenium, SETUP_GRID_BODY)


@pytest.fixture
@then(parsers.parse("I should see '{window_header}' notification window"))
def notification_window(selenium, window_header):
    assert window_header in by_xpath(selenium, SETUP_POPUP_FRONT + SETUP_POPUP_HEADER).text


@pytest.fixture
@when("I unselect all items in grid")
def unselect_all_items_in_grid(selenium):
    if by_xpath(selenium, SETUP_GRID_SELECT_ALL).get_attribute("indeterminate"):
        by_xpath(selenium, SETUP_GRID_SELECT_ALL).click()
    elif by_xpath(selenium, SETUP_GRID_SELECT_ALL).get_attribute("checked"):
        by_xpath(selenium, SETUP_GRID_SELECT_ALL).click()


# --- Project Setup Steps --- #
@pytest.fixture
@when("I add a new project")
def add_new_proj(selenium, pass_data_to_step):
    click_add_button = by_xpath(selenium, SETUP_ADD_NEW).click()
    proj_name = "z_automation " + datetime.datetime.now().strftime('%m/%d/%Y %H:%M')
    enter_project_name_in_field = by_xpath(selenium, PROJECT_ADD_NEW_ENTER_NAME).send_keys(proj_name)
    click_to_create_project = by_xpath(selenium, PROJECT_ADD_NEW_CREATE_BUTTON).click()
    get_project_code = by_xpath(selenium, PROJECT_DETAILS_CODE).get_attribute("value")
    pass_data_to_step['proj_name'] = proj_name
    pass_data_to_step['project_code'] = get_project_code


@pytest.fixture
@given(parsers.parse("I select '{project_name}' project in project setup grid"))
@given(parsers.parse("I select '{project_name}' project in new project setup grid"))
def select_a_project(selenium, project_name):
    # enter_proj_name_in_search = by_xpath(selenium, GRID_SEARCH).send_keys(project_name)
    # time.sleep(1)
    projects_list = by_xpath_multiple(selenium, SETUP_GRID_NAME)
    project_names_list = [x.text for x in projects_list]
    print(project_names_list)
    index = project_names_list.index(project_name) + 1
    click_project = by_xpath(selenium, f"({SETUP_GRID_CHECKBOX})[{index}]").click()


@pytest.fixture
@given(parsers.parse("I navigate to project {sub_grid} grid"))
@given(parsers.parse("I navigate to project {sub_grid} grid"))
def go_to_project_sub_grid(selenium, sub_grid, pass_data_to_step):
    wait_clickable(selenium, PROJECT_GO_TO_DROPDOWN)
    click_navigation_dropdown = by_xpath(selenium, PROJECT_GO_TO_DROPDOWN).click()
    if sub_grid == "locations":
        click_go_to_locations = by_xpath(selenium, PROJECT_GO_TO_DROPDOWN_LOCATIONS).click()
    elif sub_grid == "equipment":
        click_go_to_equipment = by_xpath(selenium, PROJECT_GO_TO_DROPDOWN_EQUIPMENT).click()
    elif sub_grid == "itp":
        click_go_to_itp = by_xpath(selenium, PROJECT_GO_TO_DROPDOWN_ITP).click()
    elif sub_grid == "phases":
        click_go_to_itp = by_xpath(selenium, PROJECT_GO_TO_DROPDOWN_PHASE).click()
    time.sleep(1)
    try:
        wait_clickable(selenium, SETUP_GRID_ROW)
    except:
        by_xpath(selenium, SETUP_POPUP_FRONT + SETUP_POPUP_CANCEL).click()
    pass_data_to_step['sub_grid'] = sub_grid


@pytest.fixture
@when(parsers.parse("I click on back to {back_to_grid}"))
@when(parsers.parse("I go back to {back_to_grid} grid"))
def back_to_project_grid(selenium, back_to_grid):
    print("debug")
    if back_to_grid == "back button":
        by_xpath(selenium, SETUP_BACK_TO_PARENT_GRID_BUTTON).click()
    elif back_to_grid == "breadcrumb":
        by_xpath(selenium, SETUP_BREADCRUMB_LINK).click()
    else:
        by_xpath(selenium, SETUP_BACK_TO_PARENT_GRID_BUTTON).click()



@pytest.fixture
@given(parsers.parse("I select '{phase_name}' phase in phase setup grid"))
def select_a_phase(selenium, phase_name):
    phase_list = by_xpath_multiple(selenium, SETUP_GRID_NAME)
    phase_names_list = [x.text for x in phase_list]
    index = phase_names_list.index(phase_name) + 1
    click_project = by_xpath(selenium, f"({SETUP_GRID_CHECKBOX})[{index}]").click()


# --- RP Setup Steps --- #
@pytest.fixture
@when("I select a responsible party")
def select_single_rp(selenium, pass_data_to_step, items_in_use):
    wait_clickable(selenium, SETUP_GRID)
    rp_names = by_xpath_multiple(selenium, SETUP_GRID_NAME)
    rp_names_list = [x.text for x in rp_names]
    rp_names_list_clean = [x for x in rp_names_list if x != ""]
    rps_to_avoid = items_in_use[2]
    valid_name_option_by_index = [rp_names_list_clean.index(y) for y in rp_names_list_clean if y not in rps_to_avoid]
    click_rp = by_xpath(selenium, f"({SETUP_GRID_CHECKBOX})[{(random.choice(valid_name_option_by_index) + 1)}]").click()
    get_rp_code = by_xpath(selenium, RP_DETAILS_CODE).get_attribute("value")
    pass_data_to_step['original_code'] = get_rp_code
    get_rp_name = by_xpath(selenium, RP_DETAILS_NAME).get_attribute("value")
    pass_data_to_step['rp_name'] = get_rp_name


@pytest.fixture
@given(parsers.parse("I select Responsible Party '{rp_name}' in Responsible Party setup grid"))
def select_specific_rp(selenium, rp_name):
    rps_list = by_xpath_multiple(selenium, SETUP_GRID_NAME)
    rp_names_list = [x.text for x in rps_list]
    index = rp_names_list.index(rp_name) + 1
    click_rp = by_xpath(selenium, f"({SETUP_GRID_CHECKBOX})[{index}]").click()


@pytest.fixture
@given(parsers.parse("I navigate to Users grid for the RP"))
def go_to_project_sub_grid(selenium, pass_data_to_step):
    wait_clickable(selenium, RP_GO_TO_USER_GRID)
    click_go_to_users = by_xpath(selenium, RP_GO_TO_USER_GRID).click()
    time.sleep(1)
    wait_clickable(selenium, SETUP_GRID)


# --- User Setup Steps --- #
@pytest.fixture
@when("I add a new user")
def add_user(selenium, pass_data_to_step):
    wait_clickable(selenium, SETUP_GRID_ROW)
    click_add_button = by_xpath(selenium, SETUP_ADD_NEW).click()
    username = f"z_automation_user" + datetime.datetime.now().strftime('%m-%d-%Y_%H-%M-%S')
    enter_username = by_xpath(selenium, USER_ADD_NEW_USERNAME).send_keys(username)
    enter_firstname = by_xpath(selenium, USER_ADD_NEW_FIRSTNAME).send_keys('automation')
    enter_lastname = by_xpath(selenium, USER_ADD_NEW_LASTNAME).send_keys('added')
    enter_email = by_xpath(selenium, USER_ADD_NEW_EMAIL).send_keys(f"testing+{datetime.datetime.now().strftime('%m%d%Y%H%M%S')}@ftq360.com")
    save_changes = by_xpath(selenium, USER_ADD_NEW_CONFIRM_BUTTON).click()
    wait_clickable(selenium, TOAST_SUCCESS)
    pass_data_to_step['username'] = username


@pytest.fixture
@when("I change user password")
def change_password(selenium, pass_data_to_step):
    user = pass_data_to_step['user']
    password = datetime.datetime.now().strftime('%m-%d-%Y_%H-%M-%S')
    user_names = by_xpath_multiple(selenium, USER_USERNAMES)
    user_name_list = [x.text for x in user_names]
    index = user_name_list.index(user) + 1
    # click_user = by_xpath(selenium, f"({SETUP_GRID_CHECKBOX})[{index}]").click()
    click_change_pw = by_xpath(selenium, USER_CHANGE_PW).click()
    enter_pw = by_xpath(selenium, USER_CHANGE_PW_NEW).send_keys(password)
    confirm_pw = by_xpath(selenium, USER_CHANGE_PW_CONFIRM).send_keys(password)
    time.sleep(1)
    save_changes = by_xpath(selenium, SETUP_POPUP_FRONT + USER_ADD_NEW_CONFIRM_BUTTON).click()
    pass_data_to_step['password'] = password


#--- Universal ---
@pytest.fixture
@when("I deselect all in setup grid")
def deselect_all_in_grid(selenium):
    # select_all_checkbox = by_xpath(selenium, SETUP_SELECT_ALL)
    # if select_all_checkbox.get_attribute('checked') == True:
    #     click_select_all_checkbox = select_all_checkbox.click()
    # elif select_all_checkbox.get_attribute('indeterminate') == True:
    #     click_select_all_checkbox = select_all_checkbox.click()
    click_select_all_checkbox = by_xpath(selenium, SETUP_SELECT_ALL).click()
    time.sleep(1)
