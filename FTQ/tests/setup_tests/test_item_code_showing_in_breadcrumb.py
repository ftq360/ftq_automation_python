from pytest_bdd import scenario, when, then, parsers
from functions import *
from locators.setup_locators import *


@pytest.mark.xray('DEV-7214')
@scenario("../../features/setup/item_code_showing_in_breadcrumb.feature", "Item code showing in breadcrumb")
def test_item_code_showing_in_breadcrumb():
    """test that item code showing in breadcrumbs as expected"""


@when(parsers.parse("I navigate to {setup_page}"))
def navigate_to_page(selenium, setup_page, cred_data, pass_data_to_step):
    page = text_to_credential(setup_page)
    selenium.get(cred_data['BASE_URL'] + cred_data[page])
    assert selenium.current_url == cred_data['BASE_URL'] + cred_data[page]
    pass_data_to_step['setup_page'] = setup_page


@when("I select an item")
def select_item(selenium, pass_data_to_step):
    setup_page = pass_data_to_step.get('setup_page')
    click_item = by_xpath(selenium, SETUP_GRID_CHECKBOX).click()
    time.sleep(1)
    if setup_page == 'checklist setup':
        get_code = by_xpath(selenium, CHECKLIST_DETAILS_CODE).get_attribute("value")
        pass_data_to_step['item_code'] = get_code
    elif setup_page == 'project setup':
        get_code = by_xpath(selenium, PROJECT_DETAILS_CODE).get_attribute("value")
        pass_data_to_step['item_code'] = get_code


@when(parsers.parse("I then navigate to {subgrid}"))
def navigate_to_subgrid(selenium, subgrid):
    if subgrid == "phases":
        wait_clickable(selenium, PROJECT_GO_TO_DROPDOWN)
        click_navigation_dropdown = by_xpath(selenium, PROJECT_GO_TO_DROPDOWN).click()
        click_go_to = by_xpath(selenium, PROJECT_GO_TO_DROPDOWN_PHASE).click()
    elif subgrid == "checkpoints":
        click_go_to = by_xpath(selenium, CHECKLIST_GO_TO_CHECKPOINTS).click()
    time.sleep(1)
    try:
        wait_clickable_short(selenium, "//tr")
    except:
        try_by_xpath(selenium, SETUP_POPUP_FRONT + SETUP_POPUP_CANCEL).click()
        wait_clickable(selenium, "//tr")


@then(parsers.parse("I should see the code of {item} in the breadcrumbs title"))
def item_code_in_breadcrumb(selenium, pass_data_to_step):
    item_code = pass_data_to_step.get('item_code')
    get_breadcrumb_text = by_xpath(selenium, SETUP_BREADCRUMB_LINK).text
    assert item_code in get_breadcrumb_text
