from datetime import time

from pytest_bdd import scenario, when, then
from selenium.webdriver.common.keys import Keys

from functions import *
from locators.account_management_locators import *
from locators.old_setup_page_locators import *
from locators.report_locators import REPORT_EMAIL


@pytest.mark.xray('DEV-6739')
@scenario("../../../features/setup/admin/bcc_company_address.feature", "BCC Company Address")
def test_bcc_company_address():
    """Verify that BCC Company Address works as expected"""


@when("I add a new email address into the BCC email address field")
def enter_new_bcc_email(selenium, pass_data_to_step):
    new_BCC_email = f"testing+{time.strftime('%j%M%S')}@ftq360.com"
    company_settings = by_xpath_multiple(selenium, OLD_GRID_SETTING_NAMES)
    settings = [x.text for x in company_settings]
    index = settings.index('BCC email address') + 2  # skips header row
    clear_current_email = by_xpath(selenium, f"//tr[{index}]//input").clear()
    enter_new_email = by_xpath(selenium, f"//tr[{index}]//input").send_keys(new_BCC_email)
    pass_data_to_step['new_BCC_email'] = new_BCC_email
    save = by_xpath(selenium, SETUP_ALL_SAVE_BUTTONS).click()
    confirm = by_xpath(selenium, POP_UP_CONFIRM).click()
    wait_present(selenium, SETUP_ALL_SAVED_CONFIRMATION_MASK)


@when("I send the report to an invalid email address")
def send_to_invalid_email(selenium, pass_data_to_step):
    click_email_button = by_xpath(selenium, REPORT_EMAIL).click()
    wait_clickable(selenium, POPUP)
    invalid_email = "akfjkj@fkjfa.com"
    pass_data_to_step['invalid_email'] = invalid_email
    click_to_enter_email = by_xpath(selenium, EMAIL_TO_ADDRESS_FIELD).click()
    enter_email = by_xpath(selenium, EMAIL_TO_ADDRESS_FIELD_INPUT).click()
    clear_emails = by_xpath(selenium, EMAIL_TO_ADDRESS_FIELD_INPUT).clear()
    assert by_xpath(selenium, EMAIL_TO_ADDRESS_FIELD_INPUT).get_attribute("innerText") == ""
    enter_email = by_xpath(selenium, EMAIL_TO_ADDRESS_FIELD_INPUT).send_keys(invalid_email)
    press_enter = by_xpath(selenium, EMAIL_TO_ADDRESS_FIELD_INPUT).send_keys(Keys.ENTER)
    send_email = try_by_xpath(selenium, POP_UP_CONFIRM).click()
    assert by_xpath(selenium, TOAST_MESSAGE).text == "Email sent"


@then("the email is sent to the BCC email address")
def emailed_to_correct_address(selenium, pass_data_to_step, cred_data):
    recipient_email = pass_data_to_step.get('invalid_email')
    get_email_data = get_email_by_recipient(cred_data, recipient_email)
    print(get_email_data)
    # test_run_report(nodes.Item.runtest)
