from pytest_bdd import scenario, given, then, parsers
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.select import Select

from functions import *
from locators.account_management_locators import OLD_GRID_SETTING_NAMES
from locators.inspection_page_locators import *
from locators.customization_preference_page_locators import *


@pytest.mark.skip('Unstable')
@pytest.mark.xray('DEV-6741')
@scenario("../../../../features/setup/admin/customizations/project_email_preference.feature", "Project Email Preference")
def test_project_email_preference():
    """test that Project Email Preference works as expected"""


@given(parsers.parse("I set style setting '{setting_name}' to {value}"))
def set_preference_value(selenium, setting_name, value):
    get_settings = by_xpath_multiple(selenium, OLD_GRID_SETTING_NAMES)
    settings = [x.text for x in get_settings]
    index = settings.index(setting_name)
    print(index + 1)
    Select(by_xpath(selenium, f"(//tr/td/*[not(@id)])[{index + 1}]")).select_by_value(value)


@then(parsers.parse("I should see the recipient as {recipient}"))
def recipient_correct(selenium, recipient):
    email_filled = by_xpath(selenium, EMAIL_TO_ADDRESS_FIELD).get_attribute("innerText")
    assert recipient in email_filled
