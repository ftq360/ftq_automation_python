import time

from pytest_bdd import scenario, when, then
from functions import *
from locators.setup_locators import *


@pytest.mark.skip('Unstable')
@pytest.mark.xray('DEV-5587')
@scenario("../../../../features/setup/admin/customizations/project_permissions_setting_for_new_project.feature", "Project Permissions Setting for New Project")
def test_project_permissions_setting_for_new_project():
    """test that Project Permissions Setting for New Project works as expected"""


@when("I view project permissions")
def view_project_permissions(selenium, pass_data_to_step):
    project_name = pass_data_to_step.get('proj_name')
    try:
        open_project_access = try_by_xpath(selenium, PROJECT_DETAILS_ACCESS_INSPECTOR)
    except:
        try_by_xpath(selenium, PROJECT_DETAILS_ACCESS).click()
        wait_clickable(selenium, PROJECT_DETAILS_ACCESS_INSPECTOR)


@then("I see no users have any permissions for the new project")
def no_users_have_permissions_for_proj(selenium):
    answer = [True]
    start_time = time.time()
    while answer[-1] and time.time() - start_time < 60:
        if by_xpath(selenium, PROJECT_DETAILS_ACCESS_INSPECTOR).text == "loading...":
            time.sleep(1)
        else:
            answer.append(False)
    assert by_xpath(selenium, PROJECT_DETAILS_ACCESS_INSPECTOR).text == "None"

