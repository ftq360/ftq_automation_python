import pdfplumber
import pytest
from pytest_bdd import scenario, given, then, parsers
from selenium.webdriver.common.keys import Keys

from functions import by_xpath, return_downloaded_pdfs, wait_clickable_long
from locators.customization_preference_page_locators import *
from locators.inspection_page_locators import INSP_GET_PDF_ICON_ENABLED


#OBSOLETE
@pytest.mark.skip('Unstable')
@pytest.mark.xray('DEV-5386')
@scenario("../../../../features/setup/admin/customizations/report_images_per_row.feature", "Report Images Per Row")
def test_report_images_per_row():
    """test that setting for report images per row works as expected"""


@given(parsers.parse("I set 'Number of images per row in Inspection and Deficiency reports' to {imagePerRow}"))
def preference_setting_number(selenium, imagePerRow):
    search_preference = by_xpath(selenium, CUSTOMPREF_SEARCH).send_keys("Number of images per row")
    confirm_search = by_xpath(selenium, CUSTOMPREF_SEARCH).send_keys(Keys.ENTER)
    clear_current_preference = by_xpath(selenium, CUSTOMPREF_VALUE_FIRST_ROW).clear()
    enter_new_preference = by_xpath(selenium, CUSTOMPREF_VALUE_FIRST_ROW).send_keys(imagePerRow)


@then(parsers.parse("the PDF shows {imagePerRow} images per row"))
def pdf_has_x_images_per_row(selenium, imagePerRow):
    wait_clickable_long(selenium, INSP_GET_PDF_ICON_ENABLED)
    pdfs = return_downloaded_pdfs()
    correct_pdf = pdfs[-1]
    pdf_obj = pdfplumber.open(correct_pdf)
    pages = pdf_obj.pages
    images_in_page = pages[0].images
    image_bbox = [image['width'] for image in images_in_page]
    if imagePerRow == "2":
        for image in image_bbox[9:13]:
            assert int(image) in range(210, 215) #width of images when 2 per row
        assert len(pages) == 2
    elif imagePerRow == "4":
        for image in image_bbox[9:13]:
            assert int(image) in range(95, 100) #width of images when 4 per row
        assert len(pages) == 1
