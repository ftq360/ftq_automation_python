from pytest_bdd import scenario, then, given, parsers
from selenium.webdriver.common.keys import Keys

from functions import *
from locators.customization_preference_page_locators import *
from locators.old_setup_page_locators import *
from locators.setup_locators import *


@pytest.mark.skip('Unstable')
@pytest.mark.xray('DEV-5379')
@scenario("../../../../features/setup/admin/customizations/project_permissions_setting_for_new_user.feature", "Project Permissions Setting for New User")
def test_project_permissions_setting_for_new_user():
    """test that Project Permissions Setting for New User works as expected"""


@given(parsers.parse("I set 'New User automatic setting of ACTIVE checkbox on existing projects' to {setting_number}"))
def set_new_user_automatic_proj_permissions(selenium, setting_number):
    search_preference = by_xpath(selenium, CUSTOMPREF_SEARCH).send_keys("new user")
    confirm_search = by_xpath(selenium, CUSTOMPREF_SEARCH).send_keys(Keys.ENTER)
    clear_current_preference = by_xpath(selenium, CUSTOMPREF_VALUE_FIRST_ROW).clear()
    enter_new_preference = by_xpath(selenium, CUSTOMPREF_VALUE_FIRST_ROW).send_keys(setting_number)
    save = by_xpath(selenium, SETUP_ALL_SAVE_BUTTONS).click()
    wait_present(selenium, SETUP_ALL_SAVED_CONFIRMATION_MASK)


@then(parsers.parse("user is created with {permission} for all projects"))
def user_created_with_permission(selenium, permission, pass_data_to_step):
    username = pass_data_to_step.get('username')
    try:
        open_project_access = try_by_xpath(selenium, USER_PROJECT_ACCESS).click()
    except:
        try_by_xpath(selenium, USER_ACCESS_COLLAPSEEXPAND).click()
        open_project_access = by_xpath(selenium, USER_PROJECT_ACCESS).click()
    wait_clickable(selenium, SETUP_POPUP_FRONT + SETUP_ACCESS_HEADERS)
    view_permissions = by_xpath_multiple(selenium, SETUP_POPUP_FRONT + SETUP_ACCESS_VIEW_CHECKBOXES)
    for x in view_permissions:
        if '0' in permission:
            assert selenium.execute_script("return arguments[0].children[0].checked;", x) == False
        elif '1' in permission:
            assert selenium.execute_script("return arguments[0].children[0].checked;", x) == True
    edit_permissions = by_xpath_multiple(selenium, SETUP_POPUP_FRONT + SETUP_ACCESS_EDIT_CHECKBOXES)
    for y in edit_permissions:
        if '0' in permission:
            assert selenium.execute_script("return arguments[0].children[0].checked;", y) == False
        elif '1' in permission:
            assert selenium.execute_script("return arguments[0].children[0].checked;", y) == True
    create_permissions = by_xpath_multiple(selenium, SETUP_POPUP_FRONT + SETUP_ACCESS_CREATE_CHECKBOXES)
    for z in create_permissions:
        if '0' in permission:
            assert selenium.execute_script("return arguments[0].children[0].checked;", z) == False
        elif '1' in permission:
            assert selenium.execute_script("return arguments[0].children[0].checked;", z) == True
