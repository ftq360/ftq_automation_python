from pytest_bdd import scenario, given, then, when, parsers
from selenium.webdriver.common.keys import Keys

from functions import *
from locators.customization_preference_page_locators import *


@pytest.mark.skip("Deprecating feature")
@pytest.mark.xray('DEV-5385')
@scenario("../../../../features/setup/admin/customizations/page_numbers.feature", "Page Numbers")
def test_page_numbers():
    """test that page numbering settings work as expected"""


@given("I find preference titled 'Setting for selectable number of rows on listing grids'")
def find_page_preference(selenium):
    search_preference = by_xpath(selenium, CUSTOMPREF_SEARCH).send_keys("selectable number of rows")
    confirm_search = by_xpath(selenium, CUSTOMPREF_SEARCH).send_keys(Keys.ENTER)


@given(parsers.parse("I change the page numbering to {page_number_option}"))
def change_page_numbering(selenium, page_number_option):
    clear_current_preference = by_xpath(selenium, CUSTOMPREF_VALUE_FIRST_ROW).clear()
    enter_new_preference = by_xpath(selenium, CUSTOMPREF_VALUE_FIRST_ROW).send_keys(page_number_option)


@when(parsers.parse("I navigate to {page} page"))
def find_page_preference(selenium, cred_data, page):
    page_name = text_to_credential(page)
    selenium.get(cred_data['BASE_URL'] + cred_data[page_name])


@then(parsers.parse("page number options shown are {page_number_option}"))
def page_options_correct(selenium, page_number_option):
    scroll_to_bottom(selenium)
    if page_number_option == "b":
        get_page_options = by_xpath_multiple(selenium, PAGE_NUMBER_DROPDOWN_OPTION)
        actual_page_options = [x.text for x in get_page_options]
        assert actual_page_options == ['5', '10', '20']

    else:
        get_page_options = by_xpath_multiple(selenium, PAGE_NUMBER_DROPDOWN_OPTION)
        actual_page_options = [x.text for x in get_page_options]
        listed_number_options = page_number_option.split(",")
        assert listed_number_options == actual_page_options
