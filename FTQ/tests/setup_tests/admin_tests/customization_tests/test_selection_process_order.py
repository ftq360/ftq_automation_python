from pytest_bdd import scenario, given, then, parsers
from selenium.webdriver.common.keys import Keys

from functions import *
from locators.customization_preference_page_locators import *
from locators.inspection_page_locators import INSP_HEADER
from locators.selection_process_locators import *


@pytest.mark.xray('DEV-5391')
@scenario("../../../../features/setup/admin/customizations/selection_process_order.feature", "Selection Process Order")
def BROKEN_test_selection_process_order():
    """test that setting for selection process order works as expected"""


@given(parsers.parse("I set 'Setting for inspecting (without ITP)' to {insp_sequence}"))
def preference_setting_number(selenium, insp_sequence):
    by_xpath(selenium, CUSTOMPREF_SEARCH).clear()
    search_preference = by_xpath(selenium, CUSTOMPREF_SEARCH).send_keys("Setting for inspecting (without ITP)")
    confirm_search = by_xpath(selenium, CUSTOMPREF_SEARCH).send_keys(Keys.ENTER)
    clear_current_preference = by_xpath(selenium, CUSTOMPREF_VALUE_FIRST_ROW).clear()
    enter_new_preference = by_xpath(selenium, CUSTOMPREF_VALUE_FIRST_ROW).send_keys(insp_sequence)
    time.sleep(2)


@given(parsers.parse("I set 'Setting for inspecting by plan (ITP)' to {itp_sequence}"))
def preference_setting_number(selenium, itp_sequence):
    by_xpath(selenium, CUSTOMPREF_SEARCH).clear()
    search_preference = by_xpath(selenium, CUSTOMPREF_SEARCH).send_keys("Setting for inspecting by plan (ITP)")
    confirm_search = by_xpath(selenium, CUSTOMPREF_SEARCH).send_keys(Keys.ENTER)
    clear_current_preference = by_xpath(selenium, CUSTOMPREF_VALUE_FIRST_ROW).clear()
    enter_new_preference = by_xpath(selenium, CUSTOMPREF_VALUE_FIRST_ROW).send_keys(itp_sequence)
    time.sleep(2)


@then(parsers.parse("I create an inspection with {insp_sequence}"))
def create_insp_with_insp_sequence(selenium, insp_sequence):
    select_project = by_xpath(selenium, SPECIFIC_SELECTION(selenium, "Project for Selection Process Order (45)")).click()

    if insp_sequence == "1":
        select_location = by_xpath(selenium, SPECIFIC_SELECTION(selenium, "Location 1 (45-1)")).click()
        select_specific_checklist = by_xpath(selenium, (SPECIFIC_SELECTION(selenium, "Concrete-General Checklist (CT150)"))).click()
        select_rp = by_xpath(selenium, SPECIFIC_SELECTION(selenium, "FTQ Automation Group (D1)")).click()
        select_equipment = by_xpath(selenium, SPECIFIC_SELECTION(selenium, "Equipment 1 (45-1)")).click()
        check_inspection_created_successfully = wait_clickable(selenium, INSP_HEADER)
        assert "Inspection" in selenium.title 
    elif insp_sequence == "2":
        select_location = by_xpath(selenium, SPECIFIC_SELECTION(selenium, "Location 1 (45-1)")).click()
        select_equipment = by_xpath(selenium, SPECIFIC_SELECTION(selenium, "Equipment 1 (45-1)")).click()
        select_specific_checklist = by_xpath(selenium, (SPECIFIC_SELECTION(selenium, "Concrete-General Checklist (CT150)"))).click()
        select_rp = by_xpath(selenium, SPECIFIC_SELECTION(selenium, "FTQ Automation Group (D1)")).click()
        check_inspection_created_successfully = wait_clickable(selenium, INSP_HEADER)
    elif insp_sequence == "3":
        select_specific_checklist = by_xpath(selenium, (SPECIFIC_SELECTION(selenium, "Concrete-General Checklist (CT150)"))).click()
        select_location = by_xpath(selenium, SPECIFIC_SELECTION(selenium, "Location 1 (45-1)")).click()
        select_rp = by_xpath(selenium, SPECIFIC_SELECTION(selenium, "FTQ Automation Group (D1)")).click()
        select_equipment = by_xpath(selenium, SPECIFIC_SELECTION(selenium, "Equipment 1 (45-1)")).click()
        check_inspection_created_successfully = wait_clickable(selenium, INSP_HEADER)
    elif insp_sequence == "4":
        select_equipment = by_xpath(selenium, SPECIFIC_SELECTION(selenium, "Equipment 1 (45-1)")).click()
        select_specific_checklist = by_xpath(selenium, (SPECIFIC_SELECTION(selenium, "Concrete-General Checklist (CT150)"))).click()
        select_location = by_xpath(selenium, SPECIFIC_SELECTION(selenium, "Location 1 (45-1)")).click()
        select_rp = by_xpath(selenium, SPECIFIC_SELECTION(selenium, "FTQ Automation Group (D1)")).click()
        check_inspection_created_successfully = wait_clickable(selenium, INSP_HEADER)
    elif insp_sequence == "5":
        select_equipment = by_xpath(selenium, SPECIFIC_SELECTION(selenium, "Equipment 1 (45-1)")).click()
        select_location = by_xpath(selenium, SPECIFIC_SELECTION(selenium, "Location 1 (45-1)")).click()
        select_specific_checklist = by_xpath(selenium, (SPECIFIC_SELECTION(selenium, "Concrete-General Checklist (CT150)"))).click()
        select_rp = by_xpath(selenium, SPECIFIC_SELECTION(selenium, "FTQ Automation Group (D1)")).click()
        check_inspection_created_successfully = wait_clickable(selenium, INSP_HEADER)


@then(parsers.parse("I create an inspection with {itp_sequence}"))
def create_insp_with_itp_sequence(selenium, itp_sequence):
    select_project = by_xpath(selenium, SPECIFIC_SELECTION(selenium, "Project for ITP Selection Process Order (46)")).click()
    time.sleep(1)
    plan_mode = by_xpath(selenium, SP_PLAN_MODE_CHECKBOX)
    checked = selenium.execute_script("return arguments[0].children[0].checked;", plan_mode)
    print(checked)
    if checked != True:
        by_xpath(selenium, SP_PLAN_MODE_CHECKBOX_INPUT).click()
        time.sleep(2)

    if itp_sequence == "1":
        select_location = by_xpath(selenium, SPECIFIC_SELECTION(selenium, "Location 1 (46-1)")).click()
        select_specific_checklist = by_xpath(selenium, (SPECIFIC_SELECTION(selenium, "Concrete-General Checklist (CT150), [46-1]"))).click()
        select_rp = by_xpath(selenium, SPECIFIC_SELECTION(selenium, "FTQ Automation Group (D1)")).click()
        select_equipment = by_xpath(selenium, SPECIFIC_SELECTION(selenium, "Equipment 1 (46-1)")).click()
        check_inspection_created_successfully = wait_clickable(selenium, INSP_HEADER)
        assert "Inspection" in selenium.title 
    elif itp_sequence == "2":
        select_location = by_xpath(selenium, SPECIFIC_SELECTION(selenium, "Location 1 (46-1)")).click()
        select_equipment = by_xpath(selenium, SPECIFIC_SELECTION(selenium, "Equipment 1 (46-1)")).click()
        select_specific_checklist = by_xpath(selenium, (SPECIFIC_SELECTION(selenium, "Concrete-General Checklist (CT150), [46-1]"))).click()
        select_rp = by_xpath(selenium, SPECIFIC_SELECTION(selenium, "FTQ Automation Group (D1)")).click()
        check_inspection_created_successfully = wait_clickable(selenium, INSP_HEADER)
    elif itp_sequence == "3":
        select_specific_checklist = by_xpath(selenium, (SPECIFIC_SELECTION(selenium, "Concrete-General Checklist (CT150), [46-1]"))).click()
        select_location = by_xpath(selenium, SPECIFIC_SELECTION(selenium, "Location 1 (46-1)")).click()
        select_rp = by_xpath(selenium, SPECIFIC_SELECTION(selenium, "FTQ Automation Group (D1)")).click()
        select_equipment = by_xpath(selenium, SPECIFIC_SELECTION(selenium, "Equipment 1 (46-1)")).click()
        check_inspection_created_successfully = wait_clickable(selenium, INSP_HEADER)
    elif itp_sequence == "4":
        select_equipment = by_xpath(selenium, SPECIFIC_SELECTION(selenium, "Equipment 1 (46-1)")).click()
        select_specific_checklist = by_xpath(selenium, (SPECIFIC_SELECTION(selenium, "Concrete-General Checklist (CT150), [46-1]"))).click()
        select_location = by_xpath(selenium, SPECIFIC_SELECTION(selenium, "Location 1 (46-1)")).click()
        select_rp = by_xpath(selenium, SPECIFIC_SELECTION(selenium, "FTQ Automation Group (D1)")).click()
        check_inspection_created_successfully = wait_clickable(selenium, INSP_HEADER)
    elif itp_sequence == "5":
        select_equipment = by_xpath(selenium, SPECIFIC_SELECTION(selenium, "Equipment 1 (46-1)")).click()
        select_location = by_xpath(selenium, SPECIFIC_SELECTION(selenium, "Location 1 (46-1)")).click()
        select_specific_checklist = by_xpath(selenium, (SPECIFIC_SELECTION(selenium, "Concrete-General Checklist (CT150), [46-1]"))).click()
        select_rp = by_xpath(selenium, SPECIFIC_SELECTION(selenium, "FTQ Automation Group (D1)")).click()
        check_inspection_created_successfully = wait_clickable(selenium, INSP_HEADER)
