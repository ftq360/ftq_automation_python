from pytest_bdd import scenario, then, when, parsers
from functions import *
from locators.dashboard_locators import *
from locators.listing_grid_locators import *
from locators.selection_process_locators import *


@pytest.mark.skip('Unstable')
@pytest.mark.xray('DEV-5384')
@scenario("../../../features/setup/admin/account_management.feature", "Home Pages")
def home_pages():
    """test that home page settings work as expected"""


@when(parsers.parse("I log in as {user}"))
def login_as_usertype(selenium, user, navigate_to_ftq, cred_data):
    if user == 'unrestricted_qa_inspector':
        enter_login_username = by_xpath(selenium, LG_USERNAME_FIELD).send_keys(cred_data['QA_INSPECT_USER'])
        enter_login_password = by_xpath(selenium, LG_PASSWORD_FIELD).send_keys(cred_data['PASSWORD'])
        click_login_button = by_xpath(selenium, LG_LOGIN_BUTTON).click()
        wait_clickable(selenium, GL_TOP_MENU)

    elif user == 'restricted_inspector':
        enter_login_username = by_xpath(selenium, LG_USERNAME_FIELD).send_keys(cred_data['RESTRICTED_INSPECT_USER'])
        enter_login_password = by_xpath(selenium, LG_PASSWORD_FIELD).send_keys(cred_data['PASSWORD'])
        click_login_button = by_xpath(selenium, LG_LOGIN_BUTTON).click()
        wait_clickable(selenium, GL_TOP_MENU)

    elif user == 'unrestricted_qa_non-inspect':
        enter_login_username = by_xpath(selenium, LG_USERNAME_FIELD).send_keys(cred_data['QA_NON_INSPECT_USER'])
        enter_login_password = by_xpath(selenium, LG_PASSWORD_FIELD).send_keys(cred_data['PASSWORD'])
        click_login_button = by_xpath(selenium, LG_LOGIN_BUTTON).click()
        wait_clickable(selenium, GL_TOP_MENU)


@then(parsers.parse("I see {page} as my home page"))
def login_as_usertype(selenium, page):
    if page == 'create inspection':
        wait_page_title_exact(selenium, "FTQ360 - Start New Inspection")
        wait_clickable(selenium, SP_SELECT_FIRST_AVAILABLE)

    elif page == 'open item dashboard':
        wait_page_title_exact(selenium, "FTQ360 - Priority Dashboard")
        wait_clickable(selenium, DASH_GRID_ROW)

    elif page == 'deficiencies archive':
        wait_page_title_exact(selenium, "FTQ360 - List All")
        wait_clickable(selenium, GRID_ITEM_NAME)

