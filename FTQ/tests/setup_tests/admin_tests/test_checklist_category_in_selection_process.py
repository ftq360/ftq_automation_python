import pytest
from pytest_bdd import scenario, when, then
from functions import *
from locators.account_management_locators import CHECKLIST_CATEGORY_SETTING


@pytest.mark.xray('DEV-8806')
@scenario("../../../features/setup/admin/checklist_category_in_selection_process.feature",
          "Checklist Category in Selection Process")
def test_checklist_category_in_selection_process():
    """Verify that Checklist Category in Selection Process works as expected"""


@when("I enable 'INSPECTION SELECTION STEPS: Checklist category is included in the selection process.'")
def enable_category_setting(selenium):
    if not selenium.execute_script("return arguments[0].previousSibling.previousElementSibling.checked;",
                                   by_xpath(selenium, CHECKLIST_CATEGORY_SETTING)):
        category_setting = by_xpath(selenium, CHECKLIST_CATEGORY_SETTING)
        scroll_into_view(selenium, category_setting)
        category_setting.click()
    assert selenium.execute_script("return arguments[0].previousSibling.previousElementSibling.checked;",
                                   by_xpath(selenium, CHECKLIST_CATEGORY_SETTING))


@then("I see category in the selection process")
def category_is_in_selection_process(selenium):
    try:
        select_division = by_xpath(selenium, SPECIFIC_SELECTION(selenium, "(Undefined)")).click()
    except:
        pass
    select_project1 = by_xpath(selenium, SPECIFIC_SELECTION(selenium, "Project 1 (1)")).click()
    select_category = by_xpath(selenium, SPECIFIC_SELECTION(selenium, "General (01)")).click()


@when("I disable 'INSPECTION SELECTION STEPS: Checklist category is included in the selection process.'")
def disable_category_setting(selenium):
    if selenium.execute_script("return arguments[0].previousSibling.previousElementSibling.checked;",
                               by_xpath(selenium, CHECKLIST_CATEGORY_SETTING)):
        category_setting = by_xpath(selenium, CHECKLIST_CATEGORY_SETTING)
        scroll_into_view(selenium, category_setting)
        category_setting.click()
    assert not selenium.execute_script("return arguments[0].previousSibling.previousElementSibling.checked;",
                                       by_xpath(selenium, CHECKLIST_CATEGORY_SETTING))
