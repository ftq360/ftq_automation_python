import re
import pytesseract
import xlrd
from PIL import Image
from pdfminer.high_level import extract_text
from pytest_bdd import scenario, then, when, parsers
from functions import *
from locators.account_management_locators import *
from locators.dashboard_locators import *
from locators.inspection_page_locators import *
from locators.listing_grid_locators import *
from locators.old_setup_page_locators import *
from locators.report_locators import *
from locators.setup_locators import *


@pytest.mark.xray('DEV-5418')
@scenario("../../../features/setup/admin/date_formats.feature", "Group Date Formats")
def test_date_formats():
    """test that group date format settings work as expected"""


@when(parsers.parse(r"I change Date Format to {date_format} on account management page"))
def set_date_format(selenium, date_format):
    formatted_date = date_format
    if 'x' in date_format:
        formatted_date = date_format.replace("x", "/")
    company_settings = by_xpath_multiple(selenium, OLD_GRID_SETTING_NAMES)
    settings = [x.text for x in company_settings]
    index = settings.index('Date Format') + 1
    click_date_dropdown = by_xpath(selenium, f"//tr[{index}]//select").click()
    date_format_options = by_xpath_multiple(selenium, f"//tr[{index}]//select/option")
    date_format_options_list = [x.text for x in date_format_options]
    print(date_format_options_list)
    date_index = date_format_options_list.index(formatted_date) + 1
    click_date_option = by_xpath(selenium, f"(//tr[{index}]//select/option)[{date_index}]").click()


@when(parsers.parse("I change Time Format to {time_format} on account management page"))
def set_time_format(selenium, time_format):
    formatted_time = time_format
    if 'x' in time_format:
        formatted_time = time_format.replace("x", "/")
    company_settings = by_xpath_multiple(selenium, OLD_GRID_SETTING_NAMES)
    settings = [x.text for x in company_settings]
    index = settings.index('Time Format') + 1  # skips header row
    click_time_dropdown = by_xpath(selenium, f"//tr[{index}]//select").click()
    time_format_options = by_xpath_multiple(selenium, f"//tr[{index}]//select/option")
    time_format_options_list = [x.text for x in time_format_options]
    print(time_format_options_list)
    time_index = time_format_options_list.index(formatted_time) + 1
    click_time_option = by_xpath(selenium, f"(//tr[{index}]//select/option)[{time_index}]").click()
    save = by_xpath(selenium, SETUP_ALL_SAVE_BUTTONS).click()
    try:
        confirm_save = by_xpath(selenium, POP_UP_CONFIRM).click()
    except:
        pass
    wait_present(selenium, SETUP_ALL_SAVED_CONFIRMATION_MASK)


@then(parsers.parse("dates and times are displayed in {date_format} and {time_format} format for {page}"))
def datetimes_displayed_in_correct_format(selenium, page, cred_data):
    if "Create Inspection" in page:
        create_an_inspection(selenium, cred_data)
        if "Inspection Header" in page:
            inspection_date = by_xpath(selenium, INSP_HEADER_CREATION_DATE).text
            assert re.match(r"\d\d/\d\d/\d\d\d\d", inspection_date)
        elif "Attachment Timestamps" in page:
            add_signature_to_checkpoint(selenium)
            time.sleep(1)
            go_to_attachment_manager = selenium.get(cred_data['BASE_URL'] + cred_data['ATTACHMENT_FILE_MANAGER'])
            wait_clickable(selenium, "//tr")
            get_signature_image = by_xpath(selenium, FILE_MANAGER_FIRST_ATTACHMENT_LINK).click()
            download_wait()
            time.sleep(3)
            img = return_downloaded_jpgs()
            # begin image OCR
            pytesseract.pytesseract.tesseract_cmd = r'C:\\Program Files\\Tesseract-OCR\\tesseract.exe'
            image_text = pytesseract.image_to_string(Image.open(img[-1]))
            image_text_list = image_text.split(" ")
            image_time = image_text_list[1] + " " + image_text_list[2].strip(r"\n")
            image_date = image_text_list[0]
            # assert we have a new signature image
            now = datetime.datetime.now().strftime("%H:%M")
            assert datetime.datetime.strptime(image_text_list[1], "%H:%M") - datetime.datetime.strptime(now, "%H:%M") < datetime.timedelta(minutes=2)
            # assert time in correct format
            assert re.match("(0?[0-9]|1[0-9]|2[0-3]):(0?[0-9]|[1-5][0-9]) [a-zA-Z]+", image_time)
            # assert date in correct format
            assert image_date[3:] == datetime.datetime.now().strftime("%d/%m/%Y")
        elif "PDF" in page:
            sync_insp = by_xpath(selenium, INSP_SAVE_BUTTON).click()
            wait_clickable_long(selenium, INSP_SYNCED_TEXT)
            click_get_pdf_button = by_xpath(selenium, INSP_GET_PDF_BUTTON).click()
            wait_clickable(selenium, INSP_GET_PDF_ICON_DISABLED)
            wait_clickable_long(selenium, INSP_GET_PDF_ICON_ENABLED)
            download_wait()
            pdfs = return_downloaded_pdfs()
            correct_pdf = pdfs[-1]
            pdf_content = extract_text(correct_pdf)
            pdf_content_list = pdf_content.splitlines()
            assert any([re.match(r"\d\d\d\d\.(0?[1-9]|1[0-2])\.(0?[1-9]|[12]\d|3[01])", x) for x in pdf_content_list])
    elif "Queries" in page:
        selenium.get(cred_data['BASE_URL'] + cred_data['RUN_REPORTS_ONLINE'])
        wait_clickable(selenium, REPORT_NAMES)
        time.sleep(2)
        report_names = by_xpath_multiple(selenium, REPORT_NAMES)
        report_names_list = [x.text for x in report_names]
        for x in report_names_list:
            if x == "Q901 - Data Query 901":
                report_index = report_names_list.index(x) + 1
                click_report_radio_button = by_xpath(selenium, f"({REPORT_SELECT})[{report_index}]").click()
                by_xpath(selenium, REPORT_RUN_REPORT_BUTTON).click()
        wait_clickable(selenium, REPORT_HISTORY_REFRESH)
        start_time = time.time()
        while by_xpath(selenium, REPORT_HISTORY_TABLE_FIRST_ROW_STATUS).text != "Completed" and time.time() - start_time < 900:
            refresh_report_history = by_xpath(selenium, REPORT_HISTORY_REFRESH).click()
            wait_clickable(selenium, REPORT_HISTORY_REFRESH)
        assert by_xpath(selenium, REPORT_HISTORY_TABLE_FIRST_ROW_STATUS).text == "Completed"
        download_query = by_xpath(selenium, REPORT_DOWNLOAD).click()
        download_wait()
        queries = return_downloaded_xls()
        correct_query = queries[-1]
        book = xlrd.open_workbook(correct_query)
        sheet = book.sheet_by_index(0)
        query_date = sheet.cell_value(rowx=1, colx=24)
        print(query_date)
        assert re.match(r"[a-zA-Z][a-zA-Z][a-zA-Z] (0?[1-9]|[12]\d|3[01]), \d\d\d\d", query_date)
    elif "ITP Subgrid" in page:
        selenium.get(cred_data['BASE_URL'] + cred_data['PROJECT_SETUP'])
        projects_list = by_xpath_multiple(selenium, SETUP_GRID_NAME)
        project_names_list = [x.text for x in projects_list]
        index = project_names_list.index('Project for Dictionary Updates') + 1
        click_project = by_xpath(selenium, f"({SETUP_GRID_CHECKBOX})[{index}]").click()
        wait_clickable(selenium, PROJECT_GO_TO_DROPDOWN)
        click_navigation_dropdown = by_xpath(selenium, PROJECT_GO_TO_DROPDOWN).click()
        click_go_to_itp = by_xpath(selenium, PROJECT_GO_TO_DROPDOWN_ITP).click()
        wait_clickable(selenium, SETUP_GRID_ROW)
        time.sleep(1)
        try:
            # get date from grid
            itp_grid_start_date = by_xpath(selenium, ITP_GRID_START_DATE).text
        except:
            # add itp start date column to grid
            by_xpath(selenium, SETUP_COG_ICON).click()
            by_xpath(selenium, SETUP_COG_MANAGE_COLUMNS).click()
            wait_clickable(selenium, SETUP_POPUP_FRONT + SETUP_POPUP_HEADER)
            scroll_to_bottom(selenium)
            by_xpath(selenium, MANAGE_COLUMN_ITP_START + MANAGE_COLUMN_VISIBILITY_BUTTON).click()
            by_xpath(selenium, SETUP_POPUP_SAVE).click()
            wait_invisible(selenium, SETUP_POPUP_FRONT)
            time.sleep(1)
            itp_grid_start_date = by_xpath(selenium, ITP_GRID_START_DATE).text
        assert re.match(r"(0?[1-9]|[12]\d|3[01]) [a-zA-Z][a-zA-Z][a-zA-Z] \d\d\d\d", itp_grid_start_date)
    else:
        page_name = text_to_credential(page)
        selenium.get(cred_data['BASE_URL'] + cred_data[page_name])
        if "Recent Inspections" in page:
            get_recent_insp_date = by_xpath(selenium, LISTING_GRID_CREATION_DATE).text
            assert re.match(r"(0?[1-9]|[12]\d|3[01])-(0?[1-9]|1[0-2])-\d\d\d\d", get_recent_insp_date)
        elif "Deficiency Archive" in page:
            get_def_archive_date = by_xpath(selenium, LISTING_GRID_CREATION_DATE).text
            assert re.match(r"(0?[1-9]|[12]\d|3[01])\.(0?[1-9]|1[0-2])\.\d\d\d\d", get_def_archive_date)
        elif "Inspection Activity" in page:
            get_insp_dash_date = by_xpath(selenium, INSPDASH_GRID_DATES).text
            re.match(r"\d\d\d\d-(0?[1-9]|1[0-2])-(0?[1-9]|[12]\d|3[01])", get_insp_dash_date)
