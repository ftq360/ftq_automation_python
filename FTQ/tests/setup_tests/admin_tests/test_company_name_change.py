from pdfminer.high_level import extract_text
from pytest_bdd import scenario, then, given

from functions import *
from locators.account_management_locators import *
from locators.inspection_page_locators import *
from locators.old_setup_page_locators import *


@pytest.mark.xray('DEV-5387')
@scenario("../../../features/setup/admin/company_name_change.feature", "Company Name Change")
def test_company_name_change():
    """test company name change works as expected"""


@given("I change Company Name on account management page")
def change_company_name(selenium, pass_data_to_step):
    new_company_name = f"FTQ Automation Group - {time.strftime('%j%M%S')}"
    company_settings = by_xpath_multiple(selenium, OLD_GRID_SETTING_NAMES)
    settings = [x.text for x in company_settings]
    index = settings.index('Company Name') + 2 # skips header row
    clear_current_company_name = by_xpath(selenium, f"//tr[{index}]//input").clear()
    enter_new_company_name = by_xpath(selenium, f"//tr[{index}]//input").send_keys(new_company_name)
    pass_data_to_step['new_company_name'] = new_company_name
    save_new_name = by_xpath(selenium, SETUP_ALL_SAVE_BUTTONS).click()
    confirm_save = by_xpath(selenium, POP_UP_CONFIRM).click()
    wait_present(selenium, SETUP_ALL_SAVED_CONFIRMATION_MASK)
    time.sleep(3)


@then("company name is changed under user menu")
def company_name_changed_under_user_menu(selenium, pass_data_to_step):
    wait_clickable(selenium, GL_LOGO)
    new_company_name = pass_data_to_step.get('new_company_name')
    time.sleep(2)
    open_user_menu = by_xpath(selenium, USER_MENU_ICON).click()
    try:
        wait_clickable(selenium, USER_MENU_CURRENT_USER_GROUP_NAME)
        get_current_user_group_name = by_xpath(selenium, USER_MENU_CURRENT_USER_GROUP_NAME).text
        assert new_company_name in get_current_user_group_name
    except:
        wait_clickable(selenium, USER_MENU_ONLY_USER_GROUP_NAME)
        get_user_group_name = by_xpath(selenium, USER_MENU_ONLY_USER_GROUP_NAME).text
        assert new_company_name in get_user_group_name


@then("the PDF shows new company name")
def pdf_shows_new_company_name(selenium, pass_data_to_step):
    new_company_name = pass_data_to_step.get('new_company_name')
    wait_clickable_long(selenium, INSP_GET_PDF_ICON_ENABLED)
    download_wait()
    pdfs = return_downloaded_pdfs()
    print(pdfs)
    correct_pdf = pdfs[-1]
    print(correct_pdf)
    pdf_content = extract_text(correct_pdf)
    assert new_company_name in pdf_content


@then("new company name is shown in the email subject line")
def new_company_name_in_email(selenium, pass_data_to_step):
    new_company_name = pass_data_to_step.get('new_company_name')
    get_subject_line = by_xpath(selenium, EMAIL_SUBJECT_LINE).get_attribute('value')
    assert new_company_name in get_subject_line
