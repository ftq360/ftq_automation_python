from pytest_bdd import scenario, then, given, parsers
from functions import *
from locators.account_management_locators import *
from locators.setup_locators import *


@pytest.mark.xray('DEV-5380')
@scenario("../../../features/setup/admin/free_users.feature", "Free Users")
def test_free_users():
    """test that free user counts are work as expected"""


@given("I see how many Users are listed (userNum)")
def user_count(selenium, pass_data_to_step):
    current_user_count = by_xpath(selenium, ACCOUNT_CURRENT_USER_COUNT).text
    pass_data_to_step['current_user_count'] = current_user_count


@given("I see how many Free Users are listed (freeNum)")
def free_user_count(selenium, pass_data_to_step):
    current_free_user_count = by_xpath(selenium, ACCOUNT_CURRENT_FREE_USER_COUNT).text
    pass_data_to_step['current_free_user_count'] = current_free_user_count


@given(parsers.parse("I {add_remove} create permissions for permissions_user"))
def add_remove_permissions(selenium, add_remove):
    click_project_access = by_xpath(selenium, USER_PROJECT_ACCESS).click()
    time.sleep(2)
    select_all_create = by_xpath(selenium, SETUP_POPUP_FRONT + SETUP_ACCESS_CREATE_SELECT_ALL)
    create_status = selenium.execute_script("return arguments[0].children[0].checked;", select_all_create)
    create_intd_status = selenium.execute_script("return arguments[0].children[0].indeterminate;", select_all_create)
    if add_remove == "remove":
        if create_status:
            remove_all_create_permissions = select_all_create.click()
            assert not selenium.execute_script("return arguments[0].children[0].checked;", select_all_create)
        elif create_intd_status:
            give_all_create_permissions = select_all_create.click()
            remove_all_create_permissions = select_all_create.click()
            assert not selenium.execute_script("return arguments[0].children[0].checked;", select_all_create)
    elif add_remove == "add":
        if not create_status and not create_intd_status:
            give_all_create_permissions = select_all_create.click()
            assert selenium.execute_script("return arguments[0].children[0].checked;", select_all_create)

    apply = by_xpath(selenium, SETUP_POPUP_APPLY).click()
    try:
        save = by_xpath(selenium, USER_DETAILS_SAVE).click()
    except:
        pass


@then("the number of users is (userNum - 1)")
def user_count_plus_one(selenium, pass_data_to_step):
    new_user_count = by_xpath(selenium, ACCOUNT_CURRENT_USER_COUNT).text
    assert int(new_user_count) == int(pass_data_to_step.get('current_user_count')) - 1


@then("the number of free users is (freeNum + 1)")
def free_user_count_plus_one(selenium, pass_data_to_step):
    new_free_user_count = by_xpath(selenium, ACCOUNT_CURRENT_FREE_USER_COUNT).text
    assert int(new_free_user_count) == int(pass_data_to_step.get('current_free_user_count')) + 1
