import datetime
import random

from pytest_bdd import scenario, when, then
from functions import *
from locators.setup_locators import *


@pytest.mark.xray('DEV-8807')
@scenario("../../../features/setup/admin/key_metrics_setup.feature", "Key Metrics Setup")
def test_key_metrics_setup():
    """test that Key Metrics Setup is work as expected"""


@when("I click on key metric with code 'KM1'")
def select_km1(selenium):
    all_codes = [x.text for x in by_xpath_multiple(selenium, SETUP_GRID_CODE)]
    index = all_codes.index('KM1') + 1
    select_km = by_xpath(selenium, f"({SETUP_GRID_CHECKBOX})[{index}]").click()


@when("I update the name of the key metric")
def change_km_name(selenium, pass_data_to_step):
    clear_name_field = by_xpath(selenium, KM_DETAILS_PANEL_NAME).clear()
    time_now = datetime.datetime.now().time()
    new_name = f"KM for edits - {time_now}"
    pass_data_to_step['km_name'] = new_name
    enter_new_name = by_xpath(selenium, KM_DETAILS_PANEL_NAME).send_keys(new_name)


@when("I change the key metric default value")
def change_km_default_value(selenium, pass_data_to_step):
    clear_field = by_xpath(selenium, KM_DETAILS_PANEL_DEFAULT_VALUE).clear()
    new_value = str(random.randint(0, 1000))
    pass_data_to_step['km_value'] = new_value
    enter_new_value = by_xpath(selenium, KM_DETAILS_PANEL_DEFAULT_VALUE).send_keys(new_value)


@when("I save changes on key metrics details panel")
def save_km_details_panel(selenium):
    by_xpath(selenium, KM_DETAILS_PANEL_SAVE).click()
    wait_clickable(selenium, TOAST_SUCCESS)


@then("I should see the new KM1 changes on the checkpoint")
def km_changes_shown(selenium, pass_data_to_step):
    km_value = pass_data_to_step.get('km_value')
    km_name = pass_data_to_step.get('km_name')
    checkpoint_xpath = f"({INSP_CHECKPOINT(selenium, 'Punch Item/Deficiency:')}"
    key_metrics = by_xpath_multiple(selenium, CP_KEY_METRICS)
    key_metrics_list = [x.text for x in key_metrics]
    index = key_metrics_list.index(km_name) + 1
    key_metric_xpath = f"/..{CP_KEY_METRICS})[{index}]{CP_KEY_METRIC_INPUT}"
    checkpoint_km_value = by_xpath(selenium, checkpoint_xpath + key_metric_xpath).get_attribute("value")
    assert km_value == checkpoint_km_value
