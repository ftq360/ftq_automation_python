from pytest_bdd import scenario, when, then

from functions import *
from locators.setup_locators import *


@pytest.mark.xray('DEV-8083')
@scenario("../../../features/setup/new_project_setup/duplicate_single_project.feature", "Duplicate Single Project")
def test_duplicate_single_project():
    """test that user can duplicate a single project in details panel as expected"""


@when("I click 'Duplicate'")
def click_duplicate(selenium, pass_data_to_step):
    get_project_names = by_xpath_multiple(selenium, SETUP_GRID_NAME)
    project_names_list = [x.text for x in get_project_names]
    pass_data_to_step['project_count'] = len(project_names_list)
    click_duplicate_button = by_xpath(selenium, SETUP_COG_DUPLICATE).click()


@then("I should see 'Duplicate Project' notification window")
def confirmation_dialog_duplicate_project(selenium):
    assert by_xpath(selenium, SETUP_POPUP_FRONT + SETUP_POPUP_HEADER).text == "Duplicate Project"


@then("I see the duplicated project created in the project grid")
def checklist_action_successful(selenium, pass_data_to_step):
    time.sleep(2)
    wait_clickable(selenium, SETUP_ROW_NEW)
    project_count = pass_data_to_step.get('project_count')
    get_new_project_names = by_xpath_multiple(selenium, SETUP_GRID_NAME)
    new_project_names_list = [x.text for x in get_new_project_names]
    new_project_count = len(new_project_names_list)
    assert any("Duplicate of" in x for x in new_project_names_list)
    assert new_project_count > project_count
