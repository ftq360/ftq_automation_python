from datetime import *

from pytest_bdd import scenario, given, parsers, when

from functions import *
from locators.selection_process_locators import *
from locators.setup_locators import *


@pytest.mark.xray('DEV-6766')
@scenario("../../../features/setup/new_project_setup/create_single_new_project.feature", "Create single new project")
def test_create_single_project():
    """test creating a single new project"""


@given(parsers.parse("I enter '{text}' text in project Name field"))
def enter_project_name(selenium, text):
    keys = text + " " + datetime.datetime.now().strftime(r'%m/%d/%Y %H:%M')
    click_project_name_field = by_xpath(selenium, PROJECT_ADD_NEW_ENTER_NAME).click()
    enter_project_name_in_field = by_xpath(selenium, PROJECT_ADD_NEW_ENTER_NAME).send_keys(keys)


@given("I click Create project button to create the project (project1)")
def click_create_project_button(selenium, pass_data_to_step):
    click_to_create_project = by_xpath(selenium, PROJECT_ADD_NEW_CREATE_BUTTON).click()
    get_project_code = by_xpath(selenium, PROJECT_DETAILS_CODE).get_attribute("value")
    pass_data_to_step['project_code'] = get_project_code


@given("I give my user create permissions in Project Access")
def give_my_user_create_permissions(selenium):
    expand_project_access_section = by_xpath(selenium, PROJECT_DETAILS_ACCESS).click()
    open_inspector_access = by_xpath(selenium, PROJECT_DETAILS_ACCESS_INSPECTOR).click()
    wait_clickable(selenium, PROJECT_DETAILS_INSPECTOR_ACCESS_APPLY)
    all_inspector_names = by_xpath_multiple(selenium, PROJECT_DETAILS_ACCESS_INSPECTOR_NAMES)
    names_list = [x.text for x in all_inspector_names]
    index = names_list.index("Automation, Python") + 1
    click_create_for_my_user = by_xpath(selenium, f"({PROJECT_DETAILS_ACCESS_CREATE})[{index}]").click()
    click_apply = by_xpath(selenium, PROJECT_DETAILS_INSPECTOR_ACCESS_APPLY).click()
    click_save = by_xpath(selenium, PROJECT_DETAILS_SAVE).click()
    # wait_clickable_long(selenium, TOAST_SUCCESS)


@when("I create an inspection with project1")
def create_insp_from_project(selenium, pass_data_to_step):
    dict_loading_modal_appears = wait_present(selenium, SP_DICT_LOADING_MODAL_OPEN)
    dictionaries_load = wait_present(selenium, SP_DICT_LOADING_MODAL_SUCCESS)
    dictionary_loading_complete = wait_present(selenium, SP_DICT_LOADING_MODAL_CLOSED)
    project = pass_data_to_step.get('project_code')
    try:
        select_division = by_xpath(selenium, SPECIFIC_SELECTION(selenium, "(Undefined)")).click()
    except:
        pass
    select_project = by_xpath(selenium, SPECIFIC_SELECTION(selenium, "Project 1 (1)")).click()
    select_checklist = by_xpath(selenium, (SPECIFIC_SELECTION(selenium, "Concrete-General Checklist (CT150)"))).click()
    select_rp = by_xpath(selenium, SPECIFIC_SELECTION(selenium, "FTQ Automation Group (D1)")).click()
