from pytest_bdd import scenario, when, then
from functions import *
from locators.setup_locators import *


@pytest.mark.xray('DEV-8084')
@scenario("../../../features/setup/new_project_setup/duplicate_single_phase.feature", "Duplicate Single Phase")
def test_duplicate_single_phase():
    """test that user can duplicate a single phase in details panel as expected"""


@when("I click 'Duplicate'")
def click_duplicate(selenium, pass_data_to_step):
    get_phase_names = by_xpath_multiple(selenium, SETUP_GRID_NAME)
    phase_names_list = [x.text for x in get_phase_names]
    pass_data_to_step['phase_count'] = len(phase_names_list)
    click_duplicate_button = by_xpath(selenium, SETUP_COG_DUPLICATE).click()


@then("I should see 'Duplicate Phase' notification window")
def confirmation_dialog_duplicate_phase(selenium):
    assert by_xpath(selenium, SETUP_POPUP_FRONT + SETUP_POPUP_HEADER).text == "Duplicate Phase"


@then("I see the duplicated phase created in the phase grid")
def checklist_action_successful(selenium, pass_data_to_step):
    time.sleep(2)
    wait_clickable(selenium, SETUP_GRID_NAME)
    phase_count = pass_data_to_step.get('phase_count')
    get_new_phase_names = by_xpath_multiple(selenium, SETUP_GRID_NAME)
    new_phase_names_list = [x.text for x in get_new_phase_names]
    new_phase_count = len(new_phase_names_list)
    assert any("Duplicate of" in x for x in new_phase_names_list)
    assert new_phase_count > phase_count
