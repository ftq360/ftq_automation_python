import random
from pytest_bdd import scenario, when, then
from functions import *
from locators.setup_locators import *


@pytest.mark.xray('DEV-8082')
@scenario("../../../features/setup/new_project_setup/reassign_equipment.feature", "Reassign Equipment")
def BROKEN_test_reassign_equipment():
    """test that reassigning equipment works as expected"""


@when("I select an equipment")
def select_equipment(selenium, pass_data_to_step):
    equipment_options = [x.text for x in by_xpath_multiple(selenium, SETUP_GRID_CODE)]
    select_random = random.choice(equipment_options)
    index = equipment_options.index(select_random) + 1
    click_equipment = by_xpath(selenium, f"({SETUP_GRID_CHECKBOX})[{index}]").click()
    wait_clickable_long(selenium, DETAILS_PANEL)
    pass_data_to_step['equipment'] = select_random


@when("I click reassign button in details panel")
def click_reassign(selenium):
    click_reassign_button = by_xpath(selenium, REASSIGN_EQUIPMENT).click()
    wait_clickable(selenium, SETUP_POPUP_FRONT + SETUP_POPUP_HEADER)


@when("I change equipment project to 'Project for Selection Process Order (45)'")
def change_equipment_project(selenium):
    click_project_dropdown = by_xpath(selenium, REASSIGN_EQUIPMENT_PROJECT_DROPDOWN).click()
    select_new_project = by_xpath(selenium, (SETUP_POPUP_FRONT + SETUP_POPUP_SEARCH)).send_keys('project for selection')
    time.sleep(1)
    click_project = by_xpath(selenium, SETUP_POPUP_FRONT + SETUP_DROPDOWN_ITEM_SELECTED).click()
    save_changes = by_xpath(selenium, REASSIGN_EQUIPMENT_CONFIRM).click()
    wait_clickable(selenium, TOAST_MESSAGE)


@then("the equipment is no longer listed in the grid")
def equipment_not_present(selenium, pass_data_to_step):
    equipment = pass_data_to_step.get('equipment')
    get_all_equipment_codes = [x.text for x in by_xpath_multiple(selenium, SETUP_GRID_CODE)]
    assert equipment not in get_all_equipment_codes


@then("I see the equipment listed in the grid")
def equipment_present(selenium, pass_data_to_step):
    equipment = pass_data_to_step.get('equipment')
    get_all_equipment_codes = [x.text for x in by_xpath_multiple(selenium, SETUP_GRID_CODE)]
    assert equipment in get_all_equipment_codes
