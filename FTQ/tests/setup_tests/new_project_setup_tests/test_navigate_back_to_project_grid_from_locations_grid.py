from pytest_bdd import scenario, then

from functions import *
from locators.setup_locators import *


@pytest.mark.xray('DEV-6437')
@scenario("../../../features/setup/new_project_setup/navigate_back_to_project_grid_from_locations_grid.feature", "Navigate back to community grid from locations grid")
def test_navigating_back_to_project_grid():
    """test that user can navigate back to the main project grid from sub grids"""


@then("I am brought back to the project grid")
def brought_to_project_grid(selenium):
    wait_clickable(selenium, SETUP_GRID_BODY)
