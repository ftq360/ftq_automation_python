from datetime import *
from functions import *
from pytest_bdd import scenario, given, parsers, then
from selenium.webdriver.common.keys import Keys
from locators.setup_locators import *


@pytest.mark.xray('DEV-6544')
@scenario("../../../features/setup/new_project_setup/create_multiple_new_projects.feature", "Create multiple new projects")
def test_create_multiple_projects():
    """test creating multiple new projects"""


@given(
    parsers.parse("I enter both '{first_project_name}' and '{second_project_name}' text in project Name field"))
def enter_two_project_names(selenium, first_project_name, second_project_name):
    first_project = first_project_name + " " + datetime.datetime.now().strftime('%m/%d/%Y %H:%M')
    second_project = second_project_name + " " + datetime.datetime.now().strftime('%m/%d/%Y %H:%M')
    enter_first_project_name = by_xpath(selenium, PROJECT_ADD_NEW_ENTER_NAME).send_keys(first_project)
    enter = by_xpath(selenium, PROJECT_ADD_NEW_ENTER_NAME).send_keys(Keys.ENTER)
    enter_second_project_name = by_xpath(selenium, PROJECT_ADD_NEW_ENTER_NAME).send_keys(second_project)


@given("I click Create Projects button to create the projects")
def click_create_projects_button(selenium):
    click_to_create_project = by_xpath(selenium, PROJECT_ADD_NEW_CREATE_BUTTON).click()


@then("both projects created successfully")
def verify_projects_created(selenium):
    verify_new_projects(selenium)
