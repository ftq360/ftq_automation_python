import csv
from pytest_bdd import scenario, then, when, given
from functions import *
from locators.old_setup_page_locators import *
from locators.setup_locators import *


@pytest.mark.xray('DEV-6409')
@scenario("../../../features/setup/new_project_setup/phase_inherit_permissions.feature", "Phase Inherit Permissions")
def test_phase_inherit_permissions():
    """test that phases inherit correct permissions when creating new via  manual or import"""


@given("I import a project phase CSV file")
def import_project_phase(selenium, pass_data_to_step):
    # create csv data
    now = datetime.datetime.today().strftime('%m%d%Y%H%M%S')
    pass_data_to_step['imported_phase'] = f"Imported Phase - {now}"
    csv_data = [['CommunityCode', 'LotNumber', 'JobsCode', 'JobsNotes', 'Email', 'ExternKey', 'ExternKeyID', 'Active', 'DisplaySequence', 'PlanName', 'InheritPermissions', 'ImportMode'],
                ['2855361', f'Imported Inherit Phase - {now}', f'2855361-{now}', '', '', '', '', '1', '1', '', '0', '0']]
    with open(r'C:\Workspace\AutomationAttachments\jobs.csv', 'w', newline='') as f:
        f.truncate()
        writer = csv.writer(f)
        writer.writerows(csv_data)
        f.close()
    # upload csv file
    file_input = by_xpath(selenium, IMPORT_FILEINPUT)
    file_input.send_keys(r'C:\Workspace\AutomationAttachments\jobs.csv')
    time.sleep(1)
    click_submit = by_xpath(selenium, IMPORT_SUBMIT).click()
    wait_clickable(selenium, IMPORT_MESSAGE)
    import_message = by_xpath(selenium, IMPORT_MESSAGE).get_attribute('outerText')
    assert "Import process ending" in import_message
    assert "Missing" not in import_message


@when("I create a new phase")
def create_new_phase(selenium, pass_data_to_step):
    click_add_button = by_xpath(selenium, SETUP_ADD_NEW).click()
    phase_name = f"Manually Added Phase for Inherit - {datetime.datetime.now().strftime('%m%d%Y%H%M%S')}"
    add_phase_description = by_xpath(selenium, PHASE_ADD_NEW_ENTER_NAME).send_keys(phase_name)
    save_changes = by_xpath(selenium, PHASE_ADD_NEW_CREATE_BUTTON).click()
    wait_clickable(selenium, TOAST_MESSAGE)
    pass_data_to_step['manual_phase'] = phase_name


@then("permissions for both new phases were inherited correctly")
def phase_permissions_correct(selenium,pass_data_to_step):
    manual_phase = pass_data_to_step.get('manual_phase')
    imported_phase = pass_data_to_step.get('imported_phase')
    try:
        open_project_access = try_by_xpath(selenium, USER_PROJECT_ACCESS).click()
    except:
        try_by_xpath(selenium, USER_ACCESS_COLLAPSEEXPAND).click()
        open_project_access = by_xpath(selenium, USER_PROJECT_ACCESS).click()
    wait_clickable(selenium, SETUP_POPUP_FRONT + SETUP_ACCESS_HEADERS)
    expand_all_projects = [x.click() for x in by_xpath_multiple(selenium, SETUP_CARET)]
    search_manual = by_xpath(selenium, SETUP_POPUP_FRONT + SETUP_ACCESS_SEARCH).send_keys(manual_phase)
    assert selenium.execute_script("return arguments[0].children[0].checked;", by_xpath(selenium, SETUP_POPUP_FRONT + SETUP_ACCESS_VIEW_CHECKBOXES)) == True
    assert selenium.execute_script("return arguments[0].children[0].checked;", by_xpath(selenium, SETUP_POPUP_FRONT + SETUP_ACCESS_EDIT_CHECKBOXES)) == False
    assert selenium.execute_script("return arguments[0].children[0].checked;", by_xpath(selenium, SETUP_POPUP_FRONT + SETUP_ACCESS_CREATE_CHECKBOXES)) == False
    by_xpath(selenium, SETUP_POPUP_FRONT + SETUP_ACCESS_SEARCH).clear()
    search_imported = by_xpath(selenium, SETUP_POPUP_FRONT + SETUP_ACCESS_SEARCH).send_keys(imported_phase)
    assert selenium.execute_script("return arguments[0].children[0].checked;", by_xpath(selenium, SETUP_POPUP_FRONT + SETUP_ACCESS_VIEW_CHECKBOXES)) == True
    assert selenium.execute_script("return arguments[0].children[0].checked;", by_xpath(selenium, SETUP_POPUP_FRONT + SETUP_ACCESS_EDIT_CHECKBOXES)) == False
    assert selenium.execute_script("return arguments[0].children[0].checked;", by_xpath(selenium, SETUP_POPUP_FRONT + SETUP_ACCESS_CREATE_CHECKBOXES)) == False
