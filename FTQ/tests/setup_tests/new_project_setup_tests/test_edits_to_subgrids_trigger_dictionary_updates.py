from datetime import *
from pytest_bdd import scenario, given, parsers
from functions import *
from locators.setup_locators import *


@pytest.mark.xray('DEV-6720')
@scenario("../../../features/setup/new_project_setup/edits_to_subgrids_trigger_dictionary_updates.feature", "Edits To sub_grids Trigger Dictionary Updates")
def BROKEN_test_edits_to_subgrids_trigger_dictionary_update():
    """test that edits to subgrids trigger dictionary updates"""


@given(parsers.parse("I select {item}"))
def select_item(selenium):
    click_item = by_xpath(selenium, SETUP_GRID_CHECKBOX).click()


@given(parsers.parse("I make edits to the {item}"))
def edits_to_item(selenium, item):
    if item == "Location 1":
        clear_current_code = by_xpath(selenium, LOCATION_DETAILS_CODE).clear()
        enter_new_code = by_xpath(selenium, LOCATION_DETAILS_CODE).send_keys(f"{datetime.datetime.now().time()}")
    elif item == "Equipment 1":
        clear_current_code = by_xpath(selenium, EQUIPMENT_DETAILS_CODE).clear()
        enter_new_code = by_xpath(selenium, EQUIPMENT_DETAILS_CODE).send_keys(f"{datetime.datetime.now().time()}")
    elif item == "ITP 1":
        clear_current_code = by_xpath(selenium, ITP_DETAILS_CODE).clear()
        enter_new_code = by_xpath(selenium, ITP_DETAILS_CODE).send_keys(f"{datetime.datetime.now().time()}")


@given(parsers.parse("I save changes on {sub_grid} details panel"))
def save_changes_on_sub_grids(selenium, sub_grid):
    if sub_grid == "locations":
        by_xpath(selenium, LOCATION_DETAILS_SAVE).click()
        wait_invisible(selenium, LOCATION_DETAILS_SAVE)
    elif sub_grid == "equipment":
        by_xpath(selenium, EQUIPMENT_DETAILS_SAVE).click()
        wait_invisible(selenium, EQUIPMENT_DETAILS_SAVE)
    elif sub_grid == "itp":
        by_xpath(selenium, ITP_DETAILS_SAVE).click()
        wait_invisible(selenium, ITP_DETAILS_SAVE)
