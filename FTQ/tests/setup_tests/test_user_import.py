from locators.setup_locators import *
from locators.old_setup_page_locators import *
from pytest_bdd import scenario, when, then
from functions import *
import csv


@pytest.mark.xray('DEV-5382')
@scenario("../../features/setup/user_import.feature", "User Import")
def test_user_import():
    """test importing user works as expected"""


@when("I import a user CSV file")
def import_equipment_csv(selenium, pass_data_to_step):
    # create csv data
    now = time.strftime('%j%M%S')
    pass_data_to_step['imported_user'] = f'imported_user_{now}'
    csv_data = [['VendorCode', 'CrewCode', 'CrewNotes', 'Email', 'FTQInspectorTypeID', 'ExternKey', 'ExternKeyID', 'UserName', 'UserFirstName', 'UserLastName', 'CrewActive', 'UserActive', 'ImportMode'],
               ['D1', f'D1-{now}', '', 'testing+imported_user@ftq360.com', '12', '', '', f'imported_user_{now}', 'imported', 'user', '0', '1', '0']]
    with open(r'C:\Workspace\AutomationAttachments\crew_users.csv', 'w', newline='') as f:
        f.truncate()
        writer = csv.writer(f)
        writer.writerows(csv_data)
        f.close()
    # upload csv file
    file_input = by_xpath(selenium, IMPORT_FILEINPUT)
    file_input.send_keys(r'C:\Workspace\AutomationAttachments\crew_users.csv')
    time.sleep(1)
    click_submit = by_xpath(selenium, IMPORT_SUBMIT).click()


@then("I should see the import present on user setup grid")
def imported_user_shown(selenium, pass_data_to_step):
    get_usernames = by_xpath_multiple(selenium, USER_USERNAMES)
    username_list = [x.text for x in get_usernames]
    imported_user = pass_data_to_step.get('imported_user')
    assert imported_user in username_list
