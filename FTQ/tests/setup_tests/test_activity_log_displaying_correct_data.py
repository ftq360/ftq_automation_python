from pytest_bdd import scenario, then, when
from functions import *
from locators.old_setup_page_locators import *


@pytest.mark.skip('Unstable')
@pytest.mark.xray('DEV-6726')
@scenario("../../features/setup/activity_log_displaying_correct_data.feature", "Activity Log Displaying Correct Data")
def test_activity_log_displaying_correct_data():
    """test that Activity Log is Displaying Data as expected"""


@when("I view activity log table")
def view_activity_log(selenium, pass_data_to_step):
    time.sleep(5)
    selenium.refresh()
    activity_log_rows = by_xpath_multiple(selenium, ACTIVITY_LOG_DETAIL_COLUMN)
    log_text = [x.text for x in activity_log_rows]
    pass_data_to_step['activity_data'] = log_text


@then("I see updates made from user and checklist setup pages")
def updates_shown_on_activity_log(selenium, pass_data_to_step):
    activity_data = pass_data_to_step.get('activity_data')
    username = pass_data_to_step.get('username')
    expected_user_data = f'Created User {username}'
    expected_checklist_data = 'Modified Checklist CT457'
    assert expected_user_data in activity_data
    assert expected_checklist_data in activity_data
