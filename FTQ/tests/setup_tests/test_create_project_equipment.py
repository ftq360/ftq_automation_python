from datetime import datetime

from pytest_bdd import scenario, given, then, when

from functions import *
from locators.selection_process_locators import *
from locators.setup_locators import *


#OBSOLETE
@pytest.mark.skip('Unstable')
@pytest.mark.xray('DEV-5378')
@scenario("../../features/setup/create_project_equipment.feature", "Create Project Equipment")
def test_create_project_equipment():
    """test a user creating project equipment"""


@given("I add a new equipment")
def add_equipment(selenium, pass_data_to_step):
    click_add_button = by_xpath(selenium, SETUP_ADD_NEW).click()
    equipment_name = f"Equipment {datetime.datetime.now().strftime('%m/%d/%Y %H:%M')}"
    add_equipment_description = by_xpath(selenium, EQUIPMENT_ADD_NEW_NAME).send_keys(equipment_name)
    save_changes = by_xpath(selenium, SETUP_POPUP_SAVE).click()
    wait_clickable(selenium, TOAST_MESSAGE)
    code_for_new_equipment = by_xpath(selenium, EQUIPMENT_DETAILS_CODE).get_attribute("value")
    pass_data_to_step['equipment_name'] = equipment_name
    pass_data_to_step['equipment_code'] = code_for_new_equipment


@when("I get to equipment selection page")
def click_through_to_equipment(selenium):
    select_phase = by_xpath(selenium, SP_SELECT_FIRST_AVAILABLE).click()
    select_location = by_xpath(selenium, SP_SELECT_FIRST_AVAILABLE).click()
    # select_rp = by_xpath(selenium, SP_SELECT_FIRST_AVAILABLE).click()


@then("my new equipment is listed to select from")
def new_equipment_is_listed(selenium, pass_data_to_step):
    equipment_name = pass_data_to_step.get('equipment_name') + " (" + pass_data_to_step.get('equipment_code') + ")"
    all_selections = by_xpath_multiple(selenium, SP_LIST)
    titles_list = [txt.text for txt in all_selections]
    assert equipment_name in titles_list


@then("I am given the option to select No Equipment")
def no_equipment_option_available(selenium):
    all_selections = by_xpath_multiple(selenium, SP_LIST)
    titles_list = [txt.text for txt in all_selections]
    assert "No Equipment" in titles_list

