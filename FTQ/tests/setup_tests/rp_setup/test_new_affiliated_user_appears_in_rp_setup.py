from pytest_bdd import scenario, when, then

from functions import *
from locators.setup_locators import *


@pytest.mark.xray('DEV-8080')
@scenario("../../../features/setup/rp_setup/new_affiliated_user_appears_in_rp_setup.feature", "New Affiliated User Appears in RP Setup")
def test_new_affiliated_user_appears_in_rp_setup():
    """test that a New Affiliated User Appears in RP Setup as expected"""


@when("I set the Responsible Party affiliation for the user to 'Responsible Party 2'")
def set_user_rp_affiliation(selenium):
    click_rp_dropdown = by_xpath(selenium, USER_DETAILS_RP_AFFILIATE_DROPDOWN).click()
    rp_options = by_xpath_multiple(selenium, USER_DETAILS_RP_AFFILIATE_OPTIONS)
    rp_list = [x.text for x in rp_options]
    index = rp_list.index('Responsible Party 2 (V1)') + 1
    click_rp = by_xpath(selenium, f"({USER_DETAILS_RP_AFFILIATE_OPTIONS})[{index}]").click()
    try:
        click_save = try_by_xpath(selenium, USER_DETAILS_SAVE).click()
        wait_invisible(selenium, USER_DETAILS_SAVE)
    except:
        pass


@then("I should see the new user listed in the Users grid for the RP")
def new_user_listed_in_rp_user_grid(selenium, pass_data_to_step):
    users_in_grid = by_xpath_multiple(selenium, RP_USER_GRID_NAME)
    user_list = [x.text for x in users_in_grid]
    assert pass_data_to_step.get('username') in user_list
