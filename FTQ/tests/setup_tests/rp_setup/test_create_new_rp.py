from pytest_bdd import scenario, when, then
from functions import *
from locators.setup_locators import *


@pytest.mark.xray('DEV-5099')
@scenario("../../../features/setup/rp_setup/create_new_rp.feature", "Create New Responsible Party")
def test_create_new_responsible_party():
    """test creating a new responsible party works as expected"""


@when("I enter 'RP' text in Checkpoint Name field")
def enter_rp_name(selenium, pass_data_to_step):
    rp_name = "RP " + datetime.datetime.now().strftime('%m/%d/%Y %H:%M')
    enter_rp_name_in_field = by_xpath(selenium, RP_ADD_NEW_NAME).send_keys(rp_name)
    pass_data_to_step['rp_name'] = rp_name


@when("I click Create RP button to create the rp (rp1)")
def click_create_rp(selenium, pass_data_to_step):
    click_to_create_rp = by_xpath(selenium, RP_ADD_NEW_CONFRIM).click()


@then("I see the new RP created successfully")
def rp_created_successfully(selenium, pass_data_to_step):
    wait_clickable(selenium, RP_DETAILS_PANEL)
    get_rp_name = by_xpath(selenium, RP_DETAILS_NAME).get_attribute("value")
    assert get_rp_name == pass_data_to_step.get('rp_name')