from pytest_bdd import scenario, when, then

from functions import *
from locators.setup_locators import *


@pytest.mark.xray('DEV-8079')
@scenario("../../../features/setup/rp_setup/delete_rp.feature", "Delete Responsible Party")
def test_delete_responsible_party():
    """test editing responsible party works as expected"""


@when("I click 'Delete'")
def click_delete(selenium):
    by_xpath(selenium, SETUP_COG_DELETE).click()


@then("I should see the RP deleted from the grid")
def rp_deleted(selenium, pass_data_to_step):
    time.sleep(2)
    get_rp_names = by_xpath_multiple(selenium, SETUP_GRID_NAME)
    rp_name_list = [x.text for x in get_rp_names]
    get_rp_codes = by_xpath_multiple(selenium, SETUP_GRID_CODE)
    rp_code_list = [y.text for y in get_rp_codes]
    assert pass_data_to_step.get('original_code') not in rp_code_list
    assert pass_data_to_step.get('rp_name') not in rp_name_list
