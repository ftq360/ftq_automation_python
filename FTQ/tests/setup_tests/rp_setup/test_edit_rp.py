from pytest_bdd import scenario, when, then
from functions import *
from locators.setup_locators import *


@pytest.mark.xray('DEV-8081')
@scenario("../../../features/setup/rp_setup/edit_rp.feature", "Edit Responsible Party")
def test_edit_responsible_party():
    """test editing responsible party works as expected"""


@when("I edit responsible party code")
def edit_rp_code(selenium, pass_data_to_step):
    rp_name = pass_data_to_step.get('rp_name')
    original_code = pass_data_to_step.get('original_code')
    wait_clickable(selenium, RP_DETAILS_PANEL)
    clear_code = by_xpath(selenium, RP_DETAILS_CODE).clear()
    now = datetime.datetime.now().strftime('%m-%d-%Y-%H-%M')
    enter_new_code = by_xpath(selenium, RP_DETAILS_CODE).send_keys(now)
    time.sleep(2)
    get_rp_code = by_xpath(selenium, RP_DETAILS_CODE).get_attribute("value")
    pass_data_to_step['new_code'] = get_rp_code


@when("I save changes on responsible party details panel")
def save_rp_details_panel(selenium):
    by_xpath(selenium, RP_DETAILS_SAVE).click()
    wait_invisible(selenium, RP_DETAILS_SAVE)


@then("code change is reflected on the responsible party setup grid")
def code_change_reflected_on_rp_grid(selenium, pass_data_to_step):
    time.sleep(2)
    get_rp_codes = by_xpath_multiple(selenium, SETUP_GRID_CODE)
    rp_code_list = [x.text for x in get_rp_codes]
    assert pass_data_to_step.get('new_code') in rp_code_list


@when("I select multiple responsible parties")
def select_multiple_rp(selenium):
    pass


@when("I change the emails of all selected responsible parties")
def change_emails_of_multiple_rp(selenium):
    pass


@when("I save changes on responsible party multi-edit details panel")
def save_multiedit_rp_details(selenium):
    pass


@then("email changes are reflected on the responsible party setup grid")
def email_changes_reflected_on_rp_grid(selenium):
    pass
