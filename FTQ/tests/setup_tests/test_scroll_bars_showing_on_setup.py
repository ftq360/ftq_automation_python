from pytest_bdd import scenario, when, given, then, parsers
from selenium.webdriver.common.keys import Keys
from functions import *
from locators.setup_locators import *


@pytest.mark.xray('DEV-7213')
@scenario("../../features/setup/scroll_bars_showing_on_setup.feature", "Scroll bars showing on setup")
def test_scroll_bars_showing_on_setup():
    """test that scroll bars are showing on setup text fields as expected"""


@when(parsers.parse("I navigate to {setup_page}"))
def navigate_to_page(selenium, setup_page, cred_data, pass_data_to_step):
    page = text_to_credential(setup_page)
    selenium.get(cred_data['BASE_URL'] + cred_data[page])
    assert selenium.current_url == cred_data['BASE_URL'] + cred_data[page]
    pass_data_to_step['setup_page'] = setup_page


@given("I add 10+ items into the text box")
def add_10_items_to_text_box(selenium, pass_data_to_step):
    setup_page = pass_data_to_step.get('setup_page')
    for x in range(20):
        if setup_page == 'checklist setup':
            by_xpath(selenium, CHECKLIST_ADD_NEW_ENTER_NAME).send_keys('new line', Keys.ENTER)
        elif setup_page == 'project setup':
            by_xpath(selenium, PROJECT_ADD_NEW_ENTER_NAME).send_keys('new line', Keys.ENTER)


@then("I should see a scroll bar in the text field")
def scroll_bar(selenium, pass_data_to_step):
    setup_page = pass_data_to_step.get('setup_page')
    if setup_page == 'checklist setup':
        window_max_height = by_xpath(selenium, CHECKLIST_ADD_NEW_ENTER_NAME).get_attribute("style")
        assert 'max-height: 203.998px' in window_max_height
    elif setup_page == 'project setup':
        window_max_height = by_xpath(selenium, PROJECT_ADD_NEW_ENTER_NAME).get_attribute("style")
        assert 'max-height: 203.998px' in window_max_height
