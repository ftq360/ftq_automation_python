from datetime import datetime

from pytest_bdd import scenario, when, then

from functions import *
from locators.setup_locators import *


#OBSOLETE
@pytest.mark.skip('Unstable')
@pytest.mark.xray('DEV-5823')
@scenario("../../../../../features/setup/new_checklist_setup/checkpoint_grid/details_panel/discard_checkpoint_detail_panel_changes.feature",
          "Discard checkpoint details panel changes")
def test_discard_checkpoint_details_changes():
    """test editing info in the details panel and discarding changes"""


@when("I edit checkpoint code")
def edit_checkpoint_title(selenium, pass_data_to_step):
    wait_clickable(selenium, DETAILS_PANEL)
    clear_code = by_xpath(selenium, CHECKPOINT_DETAILS_CODE).clear()
    enter_new_code = by_xpath(selenium, CHECKPOINT_DETAILS_CODE).send_keys(f"{datetime.datetime.now().time()}")
    get_checkpoint_code = by_xpath(selenium, CHECKPOINT_DETAILS_CODE).get_attribute("value")
    pass_data_to_step['new_code'] = get_checkpoint_code


@when("I click on the main checkpoint grid")
def click_on_main_checkpoint_grid(selenium):
    by_xpath(selenium, CHECKPOINTS_GO_BACK_TO_CHECKLIST_BREADCRUMB).click()


@then("a checkpoint details confirmation pop up appears")
def checkpoint_discard_changes_confirmation(selenium):
    wait_clickable(selenium, CHECKLIST_UNSAVED_CHANGES_POPUP)


@when("I click cancel on checkpoint details confirmation pop up")
def cancel_checkpoint_discard_changes_confirmation(selenium):
    by_xpath(selenium, SETUP_POPUP_FRONT + SETUP_POPUP_CANCEL).click()


@then("my changes are still present in checkpoint details panel but not saved")
def confirmation_canceled(selenium, pass_data_to_step):
    new_code = pass_data_to_step.get('new_code')
    actual_checkpoint_code = by_xpath(selenium, CHECKPOINT_DETAILS_CODE).get_attribute("value")
    assert new_code == actual_checkpoint_code


@when("I click discard changes button on bottom of checkpoint details panel")
def discard_changes(selenium):
    by_xpath(selenium, CHECKPOINT_DETAILS_DISCARD_CHANGES).click()
    wait_invisible(selenium, CHECKPOINT_DETAILS_DISCARD_CHANGES)


@then("my changes checkpoint details panel changes are discarded")
def changes_discarded(selenium, pass_data_to_step):
    checkpoint_code_originally = pass_data_to_step.get('original_code')
    checkpoint_code_in_grid = by_xpath(selenium, CHECKPOINTS_SELECTED + CHECKPOINT_GRID_CODE).text
    assert checkpoint_code_in_grid == checkpoint_code_originally
