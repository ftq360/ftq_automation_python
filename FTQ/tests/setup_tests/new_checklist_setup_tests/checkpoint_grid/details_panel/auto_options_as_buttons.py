from pytest_bdd import scenario, when, then
from selenium.webdriver.common.keys import Keys

from functions import *
from locators.inspection_page_locators import *
from locators.setup_locators import *


#OBSOLETE
@scenario("../../../../../features/setup/new_checklist_setup/checkpoint_grid/details_panel/auto_options_as_buttons.feature", "Auto Options as Buttons")
def auto_options_as_buttons():
    """test that auto options as buttons works as expected"""


@when("I set all checkpoints to inactive")
def set_other_checkpoints_inactive(selenium):
    try:
        try_by_xpath_multiple(selenium, CHECKPOINT_GRID_NAME)
        by_xpath(selenium, SETUP_SELECT_ALL).click()
        while not by_xpath(selenium, SETUP_SELECT_ALL).get_attribute("checked"):
            by_xpath(selenium, SETUP_SELECT_ALL).click()
            time.sleep(1)
        by_xpath(selenium, SETUP_MULTIEDIT_ACTIVE_TOGGLE).click()
        by_xpath(selenium, SETUP_MULTIEDIT_SAVE).click()
        wait_invisible(selenium, SETUP_MULTIEDIT_SAVE)
    except:
        pass


@when("I add a new checkpoint")
def add_new_checkpoint(selenium, pass_data_to_step):
    by_xpath(selenium, CHECKPOINT_ADD_NEW).click()
    by_xpath(selenium, CHECKPOINT_ADD_NEW_ENTER_NAME).send_keys(
        f"Auto Option Checkpoint {datetime.datetime.today().date()}-{datetime.datetime.now().time()}")
    by_xpath(selenium, SETUP_POPUP_SAVE).click()
    wait_clickable(selenium, CHECKPOINT_GRID)
    pass_data_to_step['checkpoint name'] = by_xpath(selenium, CHECKPOINT_DETAILS_NAME).get_attribute("value")


@when("I expand 'Note Options' in checkpoint details panel")
def expand_ref_in_details(selenium):
    expand_notes_options = by_xpath(selenium, CHECKPOINT_DETAILS_EXPAND_NOTES).click()


@when("I add 3 options in the Options text box")
def add_three_options(selenium):
    section_options = by_xpath_multiple(selenium, CHECKPOINT_DETAILS_SECTION)
    options = [x.text for x in section_options]
    index = options.index('Options') + 1
    by_xpath(selenium, f"({CHECKPOINT_DETAILS_SECTION})[{index}]/../textarea").send_keys("Option 1", Keys.ENTER, "Option 2", Keys.ENTER, "Option 3")


@when("I select the auto-options button toggle")
def select_auto_options_as_buttons(selenium):
    by_xpath(selenium, CHECKPOINT_AUTO_OPTIONS_AS_BUTTONS_TOGGLE).click()


@when("I click save button on checkpoint details panel")
def click_save_checkpoint_details_panel(selenium):
    by_xpath(selenium, CHECKPOINT_DETAILS_SAVE).click()
    wait_invisible(selenium, CHECKPOINT_DETAILS_SAVE)


@when("I select an option button for my newly created checkpoint")
def select_auto_option_button(selenium, pass_data_to_step):
    checkpoint = pass_data_to_step.get('checkpoint name')
    auto_options = by_xpath_multiple(selenium, (INSP_CHECKPOINT(selenium, checkpoint) + CP_AUTO_OPTIONS))
    auto_options_list = [x.text for x in auto_options]
    index = auto_options_list.index('Option 2') + 1
    by_xpath(selenium, f"({INSP_CHECKPOINT(selenium, checkpoint) + CP_AUTO_OPTIONS})[{index}]").click()
    while True:
        try:
            try_by_xpath(selenium, (INSP_CHECKPOINT(selenium, checkpoint) + CP_AUTO_OPTIONS))
        except:
            break


@then("I should see the text from the selection populated in the checkpoint notes")
def auto_option_text_populated(selenium, pass_data_to_step):
    checkpoint = pass_data_to_step.get('checkpoint name')
    assert by_xpath(selenium, INSP_CHECKPOINT(selenium, checkpoint) + CP_NOTE).text == "Option 2"
