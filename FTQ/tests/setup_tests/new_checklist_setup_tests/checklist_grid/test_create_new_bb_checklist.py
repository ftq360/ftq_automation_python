import pytest
from pytest_bdd import scenario, then
from functions import verify_new_checklists


@pytest.mark.xray('DEV-8099')
@scenario("../../../../features/setup/new_checklist_setup/checklist_grid/create_new_bb_checklist.feature", "Create New Building Block Checklist")
def test_create_new_bb_checklist():
    """test creating a building block checklist works as expected"""


@then("the checklist is created successfully")
def verify_checklist_created(selenium):
    assert verify_new_checklists(selenium) == 1
