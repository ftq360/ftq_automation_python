import random

from pytest_bdd import scenario, when, then
from functions import *
from locators.setup_locators import *


@pytest.mark.xray('DEV-8409')
@scenario("../../../../features/setup/new_checklist_setup/checklist_grid/sharing_library.feature", "Sharing Library")
def test_sharing_library():
    """test creating checklist via sharing library works as expected"""


@when("I click the dropdown on Add New button on checklist setup page")
def click_add_new_dropdown(selenium):
    click_dropdown = by_xpath(selenium, SETUP_ADD_NEW_DROPDOWN).click()


@when("I click 'Browse Library' option on the dropdown")
def click_browse_library(selenium):
    time.sleep(1)
    click_browse = by_xpath(selenium, SETUP_ADD_NEW_DROPDOWN_BROWSE).click()
    wait_clickable(selenium, SETUP_POPUP_FRONT)


@when("I select a checklist from the options in the sharing library")
def select_checklist_from_sharing_library(selenium, pass_data_to_step):
    checklist_options = [x for x in by_xpath_multiple(selenium, CHECKLIST_SHARING_LIBRARY_CHECKLISTS)]
    index = random.randrange(1, 16) #choose a random checklist
    select_checklist = by_xpath(selenium, f"({CHECKLIST_SHARING_LIBRARY_CHECKLISTS})[{index}]").click()
    pass_data_to_step['checklist_name'] = by_xpath(selenium, CHECKLIST_ADD_NEW_FROM_LIBRARY_NAME).get_attribute('value')


@when("I click 'Add Checklist' button")
def click_add_checklist_from_sharing_library(selenium):
    click_add_checklist = by_xpath(selenium, SETUP_POPUP_FRONT + CHECKLIST_ADD_NEW_FROM_LIBRARY).click()


@then("I should see the new checklist added to the checklist grid")
def new_checklist_added_to_grid(selenium, pass_data_to_step):
    name_of_checklist_in_details_panel = by_xpath(selenium, CHECKLIST_DETAILS_NAME).get_attribute('value')
    library_checklist_name = pass_data_to_step.get('checklist_name')
    assert name_of_checklist_in_details_panel == library_checklist_name
