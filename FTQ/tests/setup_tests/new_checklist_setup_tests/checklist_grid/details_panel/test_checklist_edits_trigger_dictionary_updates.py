import random

from pytest_bdd import scenario, when
from functions import *
from locators.setup_locators import *


@pytest.mark.xray('DEV-6740')
@scenario("../../../../../features/setup/new_checklist_setup/checklist_grid/details_panel/checklist_edits_trigger_dictionary_update.feature",
          "Checklist Edits Trigger Dictionary Update")
def BROKEN_test_checklist_updates_trigger_dictionary_update():
    """test that making edits to existing checklists trigger a dictionary update"""


@when("I change the checklist category and save")
def change_checklist_category(selenium):
    click_category_dropdown = by_xpath(selenium, CHECKLIST_DETAILS_CATEGORY).click()
    time.sleep(1)
    category_options = [x.text for x in by_xpath_multiple(selenium, VISIBLE_MENU + LIST_ITEM)]
    index = random.randrange(1, len(category_options))
    print(index)
    select_new_category = by_xpath(selenium, f"({VISIBLE_MENU}{LIST_ITEM})[{index}]").click()
    save = by_xpath(selenium, CHECKLIST_DETAILS_SAVE).click()
    wait_clickable(selenium, TOAST_MESSAGE)


