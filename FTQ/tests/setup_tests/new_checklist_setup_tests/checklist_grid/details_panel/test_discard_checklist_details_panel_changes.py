from pytest_bdd import scenario, when, then
from functions import *
from locators.setup_locators import *


@pytest.mark.xray('DEV-5833')
@scenario("../../../../../features/setup/new_checklist_setup/checklist_grid/details_panel/discard_checklist_details_panel_changes.feature",
          "Discard checklist details panel changes")
def test_discard_checklist_details_changes():
    """test editing info in the details panel and discarding changes"""


@when("I click on the main checklist grid")
def click_on_main_grid(selenium):
    by_xpath(selenium, CHECKLIST_GO_TO_CHECKPOINTS).click()


@then("a checklist details confirmation pop up appears")
def checklist_discard_changes_confirmation(selenium):
    wait_clickable(selenium, CHECKLIST_UNSAVED_CHANGES_POPUP)


@when("I click cancel on checklist details confirmation pop up")
def cancel_checklist_discard_changes_confirmation(selenium):
    by_xpath(selenium, CHECKLIST_UNSAVED_CHANGES_POPUP + SETUP_POPUP_CANCEL).click()


@then("my changes are still present in checklist details panel but not saved")
def confirmation_canceled(selenium, pass_data_to_step):
    new_code = pass_data_to_step.get('new_code')
    actual_checklist_code = by_xpath(selenium, CHECKLIST_DETAILS_CODE).get_attribute("value")
    assert new_code == actual_checklist_code


@when("I click discard changes button on bottom of checklist details panel")
def discard_changes(selenium):
    wait_clickable(selenium, CHECKLIST_DETAILS_DISCARD_CHANGES)
    by_xpath(selenium, CHECKLIST_DETAILS_DISCARD_CHANGES).click()
    wait_invisible(selenium, CHECKLIST_DETAILS_DISCARD_CHANGES)


@then("my changes checklist details panel changes are discarded")
def changes_discarded(selenium, pass_data_to_step):
    checklist_code_originally = pass_data_to_step.get('original_code')
    checklist_code_in_grid = by_xpath(selenium, SETUP_GRID_ROW_SELECTED + SETUP_GRID_CODE).text
    assert checklist_code_in_grid == checklist_code_originally
