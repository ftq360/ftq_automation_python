import pytest
from pytest_bdd import scenario


@pytest.mark.skip('Unstable')
@pytest.mark.xray('DEV-5783')
@scenario("../../../../../features/setup/new_checklist_setup/checklist_grid/details_panel/duplicate_deactivate_delete_multiple_checklists.feature", "Duplicate, deactivate, delete multiple checklists")
def test_duplicate_deactivate_delete_multiple_checklists():
    """test that user can duplicate, deactivate and delete multiple checklists in details panel as expected"""