from pytest_bdd import scenario, parsers, when, then

from functions import *
from locators.setup_locators import *


@pytest.mark.xray('DEV-5752')
@scenario("../../../../../features/setup/new_checklist_setup/checklist_grid/details_panel/duplicate_deactivate_delete_single_checklist.feature", "Duplicate, deactivate and delete a single checklist")
def test_duplicate_deactivate_delete_single_checklist():
    """test that user can duplicate, deactivate and delete a single checklist in details panel as expected"""


@when("I click the '...' button in checklist details panel")
def click_options_button_in_details_panel(selenium, pass_data_to_step):
    pass_data_to_step['checklist_name'] = by_xpath(selenium, CHECKLIST_DETAILS_NAME).get_attribute("value")
    pass_data_to_step['checklist_code'] = by_xpath(selenium, CHECKLIST_DETAILS_CODE).text
    click_button = by_xpath(selenium, CHECKLIST_DETAIL_THREEDOT).click()


@when(parsers.parse("I click '{button}' in '...' checklist details panel options"))
def click_button_in_options(selenium, button, pass_data_to_step):
    if button == "duplicate":
        get_checklist_names = by_xpath_multiple(selenium, SETUP_GRID_NAME)
        checklist_names_list = [x.text for x in get_checklist_names]
        checklist_name = pass_data_to_step.get('checklist_name')
        pass_data_to_step['checklist_count'] = checklist_names_list.count(checklist_name)
        click_button = by_xpath(selenium, CHECKLIST_DETAIL_THREEDOT_DUPLICATE).click()
    elif button == "deactivate":
        click_button = by_xpath(selenium, CHECKLIST_DETAIL_THREEDOT_DEACTIVATE).click()
        save_changes = by_xpath(selenium, CHECKLIST_DETAILS_SAVE).click()
    elif button == "delete":
        click_button = by_xpath(selenium, CHECKLIST_DETAIL_THREEDOT_DELETE).click()
        confirm = by_xpath(selenium, SETUP_POPUP_CONFRIM).click()


@then(parsers.parse("the checklist is successfully {action}"))
def checklist_action_successful(selenium, action, pass_data_to_step):
    checklist_name = pass_data_to_step.get('checklist_name')
    checklist_code = pass_data_to_step.get('checklist_code')
    checklist_count = pass_data_to_step.get('checklist_count')
    wait_clickable(selenium, SETUP_GRID)
    time.sleep(5)
    if action == "duplicated":
        get_checklist_names = by_xpath_multiple(selenium, SETUP_GRID_NAME)
        checklist_names_list = [x.text for x in get_checklist_names]
        assert checklist_names_list.count(checklist_name) == checklist_count + 1
    elif action == "deactivated":
        wait_clickable(selenium, SETUP_GRID_INACTIVE_ICON)
    elif action == "deleted":
        get_checklist_codes = by_xpath_multiple(selenium, SETUP_GRID_CODE)
        checklist_codes_list = [x.text for x in get_checklist_codes]
        assert checklist_code not in checklist_codes_list
