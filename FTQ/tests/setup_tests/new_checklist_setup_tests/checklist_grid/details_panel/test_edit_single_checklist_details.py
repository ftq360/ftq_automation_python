from pytest_bdd import scenario

from functions import *


# @pytest.fixture()
@pytest.mark.xray('DEV-5827')
@scenario("../../../../../features/setup/new_checklist_setup/checklist_grid/details_panel/edit_single_checklist_details.feature", "Edit single checklist details")
def test_edit_single_checklist_details():
    """test editing info in the details panel for a single selected checklist"""
