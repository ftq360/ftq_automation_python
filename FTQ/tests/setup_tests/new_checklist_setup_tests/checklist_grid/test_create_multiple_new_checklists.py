from pytest_bdd import scenario, parsers, then, when
from selenium.webdriver.common.keys import Keys
from functions import *
from locators.setup_locators import *
from datetime import datetime


@pytest.mark.xray('DEV-5854')
@scenario("../../../../features/setup/new_checklist_setup/checklist_grid/create_multiple_new_checklists.feature", "Create multiple new checklists")
def test_create_multiple_checklists():
    """test creating multiple new checklists"""


@when(
    parsers.parse("I enter both '{first_checklist_name}' and '{second_checklist_name}' text in Checklist Name field"))
def enter_two_checklist_names(selenium, first_checklist_name, second_checklist_name):
    enter_checklist_names_with_line_break = by_xpath(selenium, CHECKLIST_ADD_NEW_ENTER_NAME).send_keys(
        first_checklist_name + " " + datetime.now().strftime('%m/%d/%Y %H:%M') + Keys.ENTER + second_checklist_name +
        " " + datetime.now().strftime('%m/%d/%Y %H:%M'))


@then("both checklists created successfully")
def verify_checklists_created(selenium):
    assert verify_new_checklists(selenium) == 2
