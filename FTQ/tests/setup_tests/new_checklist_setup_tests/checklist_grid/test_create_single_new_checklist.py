from functions import *
from pytest_bdd import scenario, parsers, when
from locators.setup_locators import *
from locators.selection_process_locators import *


@pytest.mark.xray('DEV-5759')
@scenario("../../../../features/setup/new_checklist_setup/checklist_grid/create_single_new_checklist.feature", "Create single new checklist")
def test_create_single_checklist():
    """test creating a single new checklist"""


@when(parsers.parse("I enter '{text}' text in Checkpoint Name field"))
def enter_checklist_name(selenium, text):
    enter_checklist_name_in_field = by_xpath(selenium, CHECKPOINT_ADD_NEW_ENTER_NAME).send_keys(text)


@when("I click Create Checkpoint button to create the checkpoint (checkpoint1)")
def click_create_checkpoint_button(selenium):
    click_to_create_checkpoint = by_xpath(selenium, CHECKPOINT_CREATE_BUTTON).click()


@when("I create an inspection with checklist1")
def create_insp_from_checklist(selenium, pass_data_to_step):
    dict_loading_modal_appears = wait_present(selenium, SP_DICT_LOADING_MODAL_OPEN)
    dictionaries_load = wait_present(selenium, SP_DICT_LOADING_MODAL_SUCCESS)
    dictionary_loading_complete = wait_present(selenium, SP_DICT_LOADING_MODAL_CLOSED)
    try:
        select_division = by_xpath(selenium, SPECIFIC_SELECTION(selenium, "(Undefined)")).click()
    except:
        pass
    select_project = select_project1 = by_xpath(selenium,
                                                SPECIFIC_SELECTION(selenium, "Project 1 (1)")).click()
    checklist = pass_data_to_step.get('checklist_code')
    select_specific_checklist = by_xpath(selenium, (SPECIFIC_SELECTION(selenium, checklist))).click()
    select_rp = by_xpath(selenium, SPECIFIC_SELECTION(selenium, "FTQ Automation Group (D1)")).click()
