import csv

from pytest_bdd import scenario, when, then

from functions import *
from locators.old_setup_page_locators import *
from locators.setup_locators import *


@pytest.mark.xray('DEV-6407')
@scenario("../../features/setup/equipment_import.feature", "Equipment Import")
def test_equipment_import():
    """test importing project equipment works as expected"""


@when("I import a project equipment CSV file")
def import_equipment_csv(selenium, pass_data_to_step):
    # create csv data
    now = time.strftime('%M%S')
    pass_data_to_step['imported_equipment'] = now
    csv_data = [['CommunityCode', 'JobsCode', 'LocationCode', 'ObjectCode', 'ObjectDescription', 'ObjectSequence', 'FTQObjectTypeID', 'ExternKey', 'ExternKeyID', 'Active', 'ImportMode'],
                ['3', '', '', f'3-{now}', f'Imported equipment {now}', '1', '1', '', '', '1', '0']]
    with open(r'C:\Workspace\AutomationAttachments\objects.csv', 'w', newline='') as f:
        f.truncate()
        writer = csv.writer(f)
        writer.writerows(csv_data)
        f.close()
    # upload csv file
    file_input = by_xpath(selenium, IMPORT_FILEINPUT)
    file_input.send_keys(r'C:\Workspace\AutomationAttachments\objects.csv')
    time.sleep(1)
    click_submit = by_xpath(selenium, IMPORT_SUBMIT).click()


@then("the import should be finalized without any errors")
def import_successful(selenium):
    wait_clickable(selenium, IMPORT_MESSAGE)
    import_message = by_xpath(selenium, IMPORT_MESSAGE).get_attribute('outerText')
    assert "Import process ending" in import_message


@then("I should see the newly imported equipment listed")
def imported_equipment_shown(selenium, pass_data_to_step):
    get_equipment_names = by_xpath_multiple(selenium, SETUP_GRID_NAME)
    equipment_list = [x.text for x in get_equipment_names]
    imported_equipment = pass_data_to_step.get('imported_equipment')
    new_equipment = [equipment_name for equipment_name in equipment_list if imported_equipment in equipment_name]
    assert len(new_equipment) == 1
