from pytest_bdd import scenario, when, then, parsers

from functions import *
from locators.setup_locators import *


#OBSOLETE
@pytest.mark.skip('Unstable')
@pytest.mark.xray('DEV-6383')
@scenario("../../features/setup/manage_columns.feature", "Manage Columns")
def test_manage_columns():
    """test that managing columns for setup grids works as expected"""


@when("I select Manage Columns")
def click_manage_columns(selenium):
    by_xpath(selenium, SETUP_COG_MANAGE_COLUMNS).click()


@then("the Manage Columns dialog appears")
def manage_columns_dialog(selenium):
    assert by_xpath(selenium, SETUP_POPUP_FRONT + SETUP_POPUP_HEADER).text == "Manage Columns' Visibility and Order"


@when(parsers.parse("I drag and drop column '{drag_column}' to first position"))
def drag_specific_column(selenium, drag_column, pass_data_to_step):
    column = []
    if drag_column == "Code":
        column.append(by_xpath(selenium, MANAGE_COLUMN_CODE + "/td"))
    elif drag_column == "Project Name":
        column.append(by_xpath(selenium, MANAGE_COLUMN_PROJECT_NAME + "/td"))
    drag_the_column = ActionChains(selenium).click_and_hold(column[0]).drag_and_drop(column[0], by_xpath(selenium, MANAGE_COLUMN_TABLE + "//tr[1]")).perform()
    pass_data_to_step['drag_column'] = drag_column


@when(parsers.parse("I toggle Sequence column {visibility}"))
def activate_specific_column(selenium, visibility, pass_data_to_step):
    current_visibility = by_xpath(selenium, MANAGE_COLUMN_SEQUENCE + MANAGE_COLUMN_VISIBILITY_BUTTON).get_attribute("class")
    time.sleep(2)
    if visibility == "on" and "circular-mark-disabled" in current_visibility:
        by_xpath(selenium, MANAGE_COLUMN_SEQUENCE + MANAGE_COLUMN_VISIBILITY_BUTTON).click()
        assert "circular-mark-enabled" in by_xpath(selenium, MANAGE_COLUMN_SEQUENCE + MANAGE_COLUMN_VISIBILITY_BUTTON).get_attribute("class")
    elif visibility == "off" and "circular-mark-enabled" in current_visibility:
        by_xpath(selenium, MANAGE_COLUMN_SEQUENCE + MANAGE_COLUMN_VISIBILITY_BUTTON).click()
        assert "circular-mark-disabled" in by_xpath(selenium, MANAGE_COLUMN_SEQUENCE + MANAGE_COLUMN_VISIBILITY_BUTTON).get_attribute("class")
    pass_data_to_step['visibility'] = visibility


@when("I click Save on manage columns dialog")
def save_manage_columns(selenium):
    by_xpath(selenium, SETUP_POPUP_SAVE).click()
    wait_invisible(selenium, SETUP_POPUP_FRONT)
    time.sleep(1)


@then("I should see new column settings in the grid")
def new_columns_settings_shown_in_grid(selenium, pass_data_to_step):
    visibility = pass_data_to_step.get('visibility')
    drag_column = pass_data_to_step.get('drag_column')
    get_columns = by_xpath_multiple(selenium, SETUP_GRID_HEADERS)
    column_names = [x.text for x in get_columns]
    assert column_names[1] == drag_column
    if visibility == "on":
        assert "Sequence" in column_names
    elif visibility == "off":
        assert "Sequence" not in column_names
