from functions import *
from pytest_bdd import scenario, when, then
from locators.setup_locators import *


@pytest.mark.xray('DEV-6747')
@scenario("../../features/setup/details_expanded_sticky.feature", "Details Panel Expanded Sections are Sticky")
def test_details_expanded_sticky():
    """test that details panel expanded sections are working as expected"""


@when("I expand the references section")
def expand_references(selenium):
    by_xpath(selenium, SETUP_REFERENCE_EXPAND).click()
    wait_clickable(selenium, SETUP_REFERENCE_ATTACHMENT_OPTIONS)


@then("references section is shown expanded")
def references_section_shown_expanded(selenium):
    wait_clickable(selenium, SETUP_REFERENCE_ATTACHMENT_OPTIONS)
