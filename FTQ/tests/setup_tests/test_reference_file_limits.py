from pytest_bdd import scenario, when
from functions import *
from locators.setup_locators import *


@pytest.mark.xray('DEV-6411')
@scenario("../../features/setup/reference_file_limits.feature", "Reference File Limits")
def test_reference_file_limits():
    """test that uploading references has limits applied as expected"""


@when("I add a reference attachment to the project that is over 305mb")
def add_reference_over_305mb(selenium):
    by_xpath(selenium, SETUP_ADD_REFERENCE_BUTTON).click()
    by_xpath(selenium, PROJECT_ADD_REFERENCE_FILE).click()
    selenium.execute_script("$('[type=file]').css('display', 'block')")
    file_input = by_xpath_simple(selenium, "(//input[@type='file'])")
    file_input.send_keys(r'C:\Workspace\AutomationAttachments\stockvideo.mov')
    by_xpath(selenium, SETUP_POPUP_SAVE).click()
    wait_invisible(selenium, SETUP_REFERENCE_MODAL)
