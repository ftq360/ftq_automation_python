import csv
from pytest_bdd import scenario, then, when
from functions import *
from locators.setup_locators import SETUP_COG_DOWNLOAD


@pytest.mark.xray('DEV-5389')
@scenario("../../features/setup/export.feature", "Export Community")
def test_project_setup_export():
    """test use of exporting on community/project setup page"""


@when("I select Download CSV")
def click_download_csv(selenium):
    by_xpath(selenium, SETUP_COG_DOWNLOAD).click()
    time.sleep(2)
    download_wait()


@then("exported data matches project setup page")
def open_csv_file(selenium):
    csvs = return_downloaded_csvs()
    correct_csv = csvs[-1]
    with open(correct_csv) as csv_data_file:
        csv_reader = csv.reader(csv_data_file)
        project_list = [x for x in csv_reader]
        print(project_list)
        assert "Project 1" in project_list[1]
