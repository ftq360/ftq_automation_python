from pytest_bdd import scenario, then

from functions import *
from locators.setup_locators import *


@pytest.mark.xray('DEV-6742')
@scenario("../../features/setup/references.feature", "Uploading References")
def test_upload_references():
    """test that uploading references works as expected"""


@then("I can add a reference attachment to the checklist")
def add_reference_to_checklist(selenium):
    by_xpath(selenium, SETUP_ADD_REFERENCE_BUTTON).click()
    by_xpath(selenium, CHECKLIST_ADD_REFERENCE_FILE).click()
    selenium.execute_script("$('[type=file]').css('display', 'block')")
    file_input = by_xpath_simple(selenium, "(//input[@type='file'])")
    file_input.send_keys(r'C:\Workspace\AutomationAttachments\FTQ-square.png')
    by_xpath(selenium, SETUP_POPUP_SAVE).click()
    wait_invisible(selenium, SETUP_REFERENCE_MODAL)


@then("I can add a reference attachment to the project")
def add_reference_to_project(selenium):
    by_xpath(selenium, SETUP_ADD_REFERENCE_BUTTON).click()
    by_xpath(selenium, PROJECT_ADD_REFERENCE_FILE).click()
    selenium.execute_script("$('[type=file]').css('display', 'block')")
    file_input = by_xpath_simple(selenium, "(//input[@type='file'])")
    file_input.send_keys(r'C:\Workspace\AutomationAttachments\FTQ-square.png')
    by_xpath(selenium, SETUP_POPUP_SAVE).click()
    wait_invisible(selenium, SETUP_REFERENCE_MODAL)
