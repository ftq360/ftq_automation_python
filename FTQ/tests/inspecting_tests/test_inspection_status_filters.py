from pytest_bdd import scenario, when, then, parsers
from functions import *
from locators.inspection_page_locators import *


@pytest.mark.xray('DEV-5113')
@scenario("../../features/inspecting/inspection_status_filters.feature", "Inspection Status Filters")
def BROKEN_test_inspection_status_filters():
    """Verify that inspection status filters work as expected"""


@when("I click on the inspection status filter")
def click_on_status_filter(selenium):
    time.sleep(1)
    open_filters = by_xpath(selenium, INSP_TOP_FILTERS).click()
    click_status_filters = by_xpath(selenium, INSP_TOP_STATUS_FILTER).click()
    wait_clickable(selenium, INSP_TOP_STATUS_FILTER_OPTIONS)


@when(parsers.parse("I click on '{status}' status in filter"))
def click_open_status_filter(selenium, status):
    filter_options = by_xpath_multiple(selenium, INSP_TOP_STATUS_FILTER_OPTIONS)
    filter_options_list = [x.text for x in filter_options]
    index = filter_options_list.index(status) + 1
    click_filter = by_xpath(selenium, f"({INSP_TOP_STATUS_FILTER_OPTIONS})[{index}]").click()
    click_done = by_xpath(selenium, INSP_TOP_FILTER_DONE).click()
    wait_invisible(selenium, INSP_TOP_STATUS_FILTER_OPTIONS)


@then(parsers.parse("I should only see checkpoints with {status} statuses"))
def only_see_OPN_checkpoints(selenium, status):
    ftq_checkpoint_statuses = by_xpath_multiple(selenium, CP_FTQ_STATUS_FROM_CHECK_NAME)
    qc_checkpoint_statuses = by_xpath_multiple(selenium, CP_QC_STATUS_FROM_CHECK_NAME)
    opn_checkpoint_statuses = by_xpath_multiple(selenium, CP_OPN_STATUS_FROM_CHECK_NAME)
    na_checkpoint_statuses = by_xpath_multiple(selenium, CP_NA_STATUS_FROM_CHECK_NAME)
    if status == "OPN":
        opn_checkpoint_statuses_list = [selenium.execute_script("return arguments[0].childNodes[1].checked;", x) for x in opn_checkpoint_statuses]
        for x in opn_checkpoint_statuses_list:
            assert x == True
    elif status == "QC":
        qc_checkpoint_statuses_list = [selenium.execute_script("return arguments[0].childNodes[1].checked;", x) for x in qc_checkpoint_statuses]
        for x in qc_checkpoint_statuses_list:
            assert x == True
    elif status == "FTQ":
        ftq_checkpoint_statuses_list = [selenium.execute_script("return arguments[0].childNodes[1].checked;", x) for x in ftq_checkpoint_statuses]
        for x in ftq_checkpoint_statuses_list:
            assert x == True
    else:
        ftq_checkpoint_statuses_list = [selenium.execute_script("return arguments[0].childNodes[1].checked;", x) for x in ftq_checkpoint_statuses]
        qc_checkpoint_statuses_list = [selenium.execute_script("return arguments[0].childNodes[1].checked;", x) for x in qc_checkpoint_statuses]
        opn_checkpoint_statuses_list = [selenium.execute_script("return arguments[0].childNodes[1].checked;", x) for x in opn_checkpoint_statuses]
        na_checkpoint_statuses_list = [selenium.execute_script("return arguments[0].childNodes[1].checked;", x) for x in na_checkpoint_statuses]
        checkpoint_statuses_list = ftq_checkpoint_statuses_list+qc_checkpoint_statuses_list+opn_checkpoint_statuses_list+na_checkpoint_statuses_list
        for x in checkpoint_statuses_list:
            assert x == False
