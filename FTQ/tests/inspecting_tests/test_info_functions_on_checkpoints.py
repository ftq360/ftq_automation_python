from pytest_bdd import scenario, parsers, then
from functions import *
from locators.inspection_page_locators import *


@pytest.mark.xray('DEV-7176')
@scenario("../../features/inspecting/info_functions_on_checkpoints.feature", "Info Functions On Checkpoints")
def test_info_functions_on_checkpoints():
    """test that Info Functions On Checkpoints are working as expexted"""


@then(parsers.parse("I should see '{info_formula}' info formula checkpoint display '{formula_response}'"))
def info_formula_response(selenium, info_formula, formula_response):
    checkpoint_text = by_xpath(selenium, INSP_CHECKPOINT(selenium, f"{info_formula} Formula Checkpoint") + CP_NOTE).text
    assert checkpoint_text == formula_response
