from pytest_bdd import scenario, parsers, when, then

from functions import *
from locators.inspection_page_locators import CP_NOTE


@pytest.mark.xray('DEV-5363')
@scenario("../../features/inspecting/html_editor.feature", "HTML Editor")
def BROKEN_test_html_editor():
    """test html editor works as expected"""


@when(parsers.parse("I select '{style}' HTML button on checkpoint '{checkpoint}'"))
def html_style(selenium, style, checkpoint):
    click_note_area = by_xpath(selenium, INSP_CHECKPOINT(selenium, checkpoint) + CP_NOTE).click()
    time.sleep(1)
    if style == 'bold':
        click_note_area = by_xpath(selenium, INSP_CHECKPOINT(selenium, checkpoint) + CP_NOTE).click()
        time.sleep(1)
        by_xpath(selenium, CP_NOTE_HTML_BOLD).click()
    elif style == 'italic':
        by_xpath(selenium, CP_NOTE_HTML_ITALIC).click()
    elif style == 'underline':
        by_xpath(selenium, CP_NOTE_HTML_UNDERLINE).click()


@then(parsers.parse("'{checkpoint}' checkpoint note text is {stylized}"))
def text_is_stylized(selenium, checkpoint, stylized, pass_data_to_step):
    selenium.refresh()
    checkpoint_html = by_xpath(selenium, INSP_CHECKPOINT(selenium, checkpoint) + CP_NOTE).get_attribute("innerHTML")
    if stylized == 'bold':
        assert checkpoint_html == '<b>note</b>'
    elif stylized == 'italic':
        assert checkpoint_html == '<i>note</i>'
    elif stylized == 'underline':
        assert checkpoint_html == '<u>note</u>'


@when(parsers.parse("I select '{style_1}' and '{style_2}' HTML button on checkpoint '{checkpoint}'"))
def html_style_double(selenium, style_1, style_2, checkpoint):
    scroll_to_bottom(selenium)
    click_note_area = by_xpath(selenium, INSP_CHECKPOINT(selenium, checkpoint) + CP_NOTE).click()
    time.sleep(1)
    click_note_area = by_xpath(selenium, INSP_CHECKPOINT(selenium, checkpoint) + CP_NOTE).click()
    if style_1 == 'bold' and style_2 == 'italic':
        by_xpath(selenium, CP_NOTE_HTML_BOLD).click()
        by_xpath(selenium, CP_NOTE_HTML_ITALIC).click()
    elif style_1 == 'bold' and style_2 == 'underline':
        by_xpath(selenium, CP_NOTE_HTML_BOLD).click()
        by_xpath(selenium, CP_NOTE_HTML_UNDERLINE).click()
    elif style_1 == 'underline' and style_2 == 'italic':
        by_xpath(selenium, CP_NOTE_HTML_UNDERLINE).click()
        by_xpath(selenium, CP_NOTE_HTML_ITALIC).click()


@then(parsers.parse("'{checkpoint}' checkpoint note text is {style_1} and {style_2}"))
def text_is_stylized_double(selenium, checkpoint, style_1, style_2):
    checkpoint_html = by_xpath(selenium, INSP_CHECKPOINT(selenium, checkpoint) + CP_NOTE).get_attribute("innerHTML")
    if style_1 == 'bold' and style_2 == 'italic':
        assert checkpoint_html == '<b><i>note</i></b>'
    elif style_1 == 'bold' and style_2 == 'underline':
        assert checkpoint_html == '<b><u>note</u></b>'
    elif style_1 == 'underline' and style_2 == 'italic':
        assert checkpoint_html == '<i><u>note</u></i>'


@when("I select 'bold' and 'underline' and 'italic' HTML button on checkpoint 'Bold Underline Italic'")
def html_style_triple(selenium):
    click_note_area = by_xpath(selenium, INSP_CHECKPOINT(selenium, "Bold Underline Italic") + CP_NOTE).click()
    time.sleep(1)
    click_note_area = by_xpath(selenium, INSP_CHECKPOINT(selenium, "Bold Underline Italic") + CP_NOTE).click()
    by_xpath(selenium, CP_NOTE_HTML_ITALIC).click()
    by_xpath(selenium, CP_NOTE_HTML_UNDERLINE).click()


@then("'Bold Underline Italic' checkpoint note text is bold, underline and italic")
def text_is_stylized_triple(selenium):
    checkpoint_html = by_xpath(selenium, INSP_CHECKPOINT(selenium, "Bold Underline Italic") + CP_NOTE).get_attribute("innerHTML")
    assert checkpoint_html == '<b><i><u>note</u></i></b>'
