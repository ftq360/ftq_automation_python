from pytest_bdd import scenario, when, then, parsers
from selenium.webdriver.common.keys import Keys

from functions import *
from locators.inspection_page_locators import *


@pytest.mark.xray('DEV-5077')
@scenario("../../../features/inspecting/dynamic_items/dynamic_matrix_formulas.feature", "Dynamic matrix formulas")
def test_dynamic_matrix_formulas():
    """Verify that dynamic matrix formulas are working as expected"""


@when(parsers.parse("I enter '{enter_data}' to cell 'Enter Data'"))
def enter_data_in_cell(selenium, pass_data_to_step, enter_data):
    index = pass_data_to_step.get('dynamic_index')
    selenium.refresh() # without this step, I'm getting stale element for following steps
    wait_clickable(selenium, CP_DYNAMIC_ADD_ROW_NAMES)
    first_cell = by_xpath(selenium, f"(({CP_DYNAMIC_ADD_ROW_BUTTON})[{index}])/parent::div/parent::div/preceding-sibling::horizontal-matrix[1]//td[1]")
    scroll_to_bottom(selenium)
    scroll_into_view(selenium, first_cell)
    click_first_cell = first_cell.click()
    enter_text_in_first_cell = by_xpath(selenium, "//textarea").send_keys(enter_data)
    time.sleep(1) # dynamic wait would be more complicated


@when(parsers.parse("I enter '{more_data}' to cell 'More Data'"))
def enter_more_data_in_cell(selenium, more_data):
    by_xpath(selenium, "//textarea").send_keys(Keys.TAB)
    time.sleep(1) # dynamic wait would be more complicated
    by_xpath(selenium, "//textarea").send_keys(more_data)
    click_save = by_xpath(selenium, INSP_SAVE_BUTTON).click()
    wait_clickable(selenium, INSP_SYNCED_TEXT)


@then(parsers.parse("cell 'Formula' is '{formula}'"))
def formula_correct_output(selenium, pass_data_to_step, formula):
    index = pass_data_to_step.get('dynamic_index')
    get_formula_result = by_xpath(selenium, f"(({CP_DYNAMIC_ADD_ROW_BUTTON})[{index}])/parent::div/parent::div/preceding-sibling::horizontal-matrix[1]//td[3]").text
    assert get_formula_result == formula
