from functions import *
from pytest_bdd import scenario, then
from locators.inspection_page_locators import *


@pytest.mark.xray('DEV-5075')
@scenario("../../../features/inspecting/dynamic_items/attachments_to_dynamic_checkpoints.feature", "Attachments to dynamic checkpoints")
def test_attachments_to_dynamic_checkpoints():
    """Verify that Attachments to dynamic checkpoints work as expected"""


@then("I should see attached file on 'Dynamic Quality Checkpoint' checkpoint on inspection page")
def attached_file_still_present_on_dynamic(selenium):
    wait_clickable(selenium, INSP_CHECKPOINT(selenium, 'Dynamic Quality Checkpoint') + CP_ATTACHMENT)
