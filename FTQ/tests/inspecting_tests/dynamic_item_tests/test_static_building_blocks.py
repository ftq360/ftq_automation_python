import pytest
from pytest_bdd import scenario, when, then


@pytest.mark.xray('DEV-8464')
@scenario("../../../features/inspecting/dynamic_items/static_building_blocks.feature", "Static Building Blocks")
def test_static_building_blocks():
    """Verify that Static Building Blocks are working as expected"""

# all steps in conftest