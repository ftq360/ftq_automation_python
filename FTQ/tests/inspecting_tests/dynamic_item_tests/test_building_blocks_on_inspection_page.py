from pytest_bdd import scenario, then
from functions import *
from locators.inspection_page_locators import *


@pytest.mark.xray('DEV-7207')
@scenario("../../../features/inspecting/dynamic_items/building_blocks_on_inspection_page.feature", "Building Blocks On Inspection Screen")
def test_bb_on_insp():
    """Verify that building blocks are work as expected on the inspection screen"""


@then("I see building block checkpoint")
def bb_shown_on_insp(selenium):
    wait_clickable(selenium, INSP_CHECKPOINT(selenium, 'Dynamic Building Block - Grouped'))
    wait_clickable(selenium, INSP_CHECKPOINT(selenium, 'Dynamic Building Block - Separate'))


@then("I should see the building block added to the inspection")
def bb_added(selenium):
    wait_clickable(selenium, INSP_CHECKPOINT(selenium, 'Quality Checkpoint - Building Block'))
    wait_clickable(selenium, INSP_CHECKPOINT(selenium, 'Safety Checkpoint - Building Block'))
    cp_names = by_xpath_multiple(selenium, CP_NAMES)
    names = [x.text for x in cp_names]
    bb_checkpoints_added = [x for x in names if 'Building Block' in x]
    assert len(bb_checkpoints_added) == 6
