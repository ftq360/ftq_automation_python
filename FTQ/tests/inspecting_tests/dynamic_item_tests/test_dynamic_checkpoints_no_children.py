from pytest_bdd import scenario, when
from functions import *
from locators.inspection_page_locators import *


@pytest.mark.xray('DEV-5388')
@scenario("../../../features/inspecting/dynamic_items/dynamic_checkpoints_no_children.feature", "Dynamic checkpoint with no children")
def test_dynamic_checkpoint_with_no_children():
    """Verify that a dynamic checkpoint with
    no children does not break inspection page"""


@when("I press dynamic button title 'Dynamic Checkpoint with No Children'")
def click_matrix_button(selenium):
    get_matrix_buttons = by_xpath_multiple(selenium, CP_DYNAMIC_ADD_ROW_NAMES)
    matrix_list = [x.text for x in get_matrix_buttons]
    index = matrix_list.index("Dynamic Checkpoint with No Children") + 1
    scroll_into_view(selenium, get_matrix_buttons[index])
    click_button = by_xpath(selenium, f"({CP_DYNAMIC_ADD_ROW_BUTTON})[{index}]").click()
