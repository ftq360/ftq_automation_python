from pytest_bdd import scenario, when, then

from functions import *
from locators.inspection_page_locators import *


@pytest.mark.skip('Unstable')
@pytest.mark.xray('DEV-5066')
@scenario("../../../features/inspecting/dynamic_items/dynamic_checkpoints.feature", "Dynamic Checkpoints")
def test_dynamic_checkpoints():
    """Verify that a dynamic checkpoints work as expected"""


@then("I should see a dynamic checkpoint added to the inspection")
def dynamic_checkpoint_added(selenium):
    wait_clickable(selenium, INSP_CHECKPOINT(selenium, 'Dynamic Quality Checkpoint'))


@when("I click on the 'Undo' button")
def click_dynamic_undo(selenium):
    undo = by_xpath(selenium, CP_DYNAMIC_UNDO)
    scroll_into_view(selenium, undo)
    undo.click()
    time.sleep(1)


@then("I should see the dynamic checkpoint disappear")
def dynamic_checkpoint_removed(selenium):
    try:
        try_by_xpath(selenium, INSP_CHECKPOINT(selenium, 'Dynamic Quality Checkpoint'))
        pytest.fail("Dynamic checkpoint not removed")
    except:
        pass
