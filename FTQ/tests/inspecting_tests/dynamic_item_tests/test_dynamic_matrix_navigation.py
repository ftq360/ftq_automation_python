from pytest_bdd import scenario, when, then
from selenium.webdriver.common.keys import Keys

from functions import *
from locators.inspection_page_locators import *


@pytest.mark.xray('DEV-5115')
@scenario("../../../features/inspecting/dynamic_items/dynamic_matrix_navigation.feature", "Dynamic matrix navigation")
def test_dynamic_matrix_navigation():
    """Verify that dynamic matrix navigation is working as expected"""


@when("I enter 'data' to cell 'Enter Data'")
def enter_data_in_cell(selenium, pass_data_to_step):
    index = pass_data_to_step.get('dynamic_index')
    selenium.refresh()  # without this step, I'm getting stale element for following steps
    wait_clickable(selenium, CP_DYNAMIC_ADD_ROW_NAMES)
    first_cell = by_xpath(selenium, f"({CP_DYNAMIC_ADD_ROW_BUTTON})[{index}]/parent::div/parent::div/preceding-sibling::horizontal-matrix[1]//td[1]")
    scroll_to_bottom(selenium)
    scroll_into_view(selenium, first_cell)
    click_first_cell = first_cell.click()
    time.sleep(1)  # dynamic wait would be more complicated


@when("I tab through the matrix to create a new line")
def tab_through_matrix(selenium):
    for x in range(3):
        by_xpath(selenium, "//textarea").send_keys(Keys.TAB)
        time.sleep(1)


@then("a new matrix row is created")
def new_matrix_row_created(selenium, pass_data_to_step):
    index = pass_data_to_step.get('dynamic_index')
    second_row_exists = wait_clickable(selenium, f"({CP_DYNAMIC_ADD_ROW_BUTTON})[{index}]/parent::div/parent::div/preceding-sibling::horizontal-matrix[1]//tr[2]")
