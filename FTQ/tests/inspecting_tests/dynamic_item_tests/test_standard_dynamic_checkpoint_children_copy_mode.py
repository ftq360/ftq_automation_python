from pytest_bdd import scenario, then
from functions import *


@pytest.mark.skip('Unstable')
@pytest.mark.xray('DEV-6744')
@scenario("../../../features/inspecting/dynamic_items/standard_dynamic_checkpoint_children_copy_mode.feature", "Standard Dynamic Checkpoint Children Copy Mode")
def test_standard_dynamic_checkpoint_children_copy_mode():
    """Verify that Standard Dynamic Checkpoint Children Copy Mode is working as expected"""


@then("I see standard dynamic checkpoint children copied over correctly")
def dynamic_matrix_children_copied_settings(selenium):
    # verify Dynamic Quality Checkpoint copied everything over
    wait_clickable(selenium, INSP_CHECKPOINT(selenium, 'Dynamic Quality Checkpoint') + CP_ATTACHMENT)
    assert selenium.execute_script("return arguments[0].childNodes[1].checked;", by_xpath(selenium, INSP_CHECKPOINT(selenium, 'Dynamic Quality Checkpoint') + CHECKPOINT_STATUS()['FTQ'])) == True
    assert by_xpath(selenium, INSP_CHECKPOINT(selenium, 'Dynamic Quality Checkpoint') + CP_NOTE).text != "Observations"

    # verify Dynamic Quality Checkpoint 2 copied everything except attachments
    try:
        wait_clickable(selenium, INSP_CHECKPOINT(selenium, 'Dynamic Quality Checkpoint 2') + CP_ATTACHMENT)
        pytest.fail('Attachment copied over for Dynamic Quality Checkpoint 2')
    except:
        assert selenium.execute_script("return arguments[0].childNodes[1].checked;", by_xpath(selenium, INSP_CHECKPOINT(selenium, 'Dynamic Quality Checkpoint 2') + CHECKPOINT_STATUS()['FTQ'])) == True
        assert by_xpath(selenium, INSP_CHECKPOINT(selenium, 'Dynamic Quality Checkpoint 2') + CP_NOTE).text != "Observations"

    # verify Dynamic Quality Checkpoint 3 copied nothing over
    try:
        wait_clickable(selenium, INSP_CHECKPOINT(selenium, 'Dynamic Quality Checkpoint 3') + CP_ATTACHMENT)
        pytest.fail('Attachment copied over for Dynamic Quality Checkpoint 2')
    except:
        assert selenium.execute_script("return arguments[0].childNodes[1].checked;", by_xpath(selenium, INSP_CHECKPOINT(selenium, 'Dynamic Quality Checkpoint 3') + CHECKPOINT_STATUS()['FTQ'])) == False
        assert by_xpath(selenium, INSP_CHECKPOINT(selenium, 'Dynamic Quality Checkpoint 3') + CP_NOTE).text == "Observations"
