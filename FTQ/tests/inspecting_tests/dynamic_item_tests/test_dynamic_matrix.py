from pdfminer.high_level import extract_text
from pytest_bdd import scenario, when, then

from functions import *
from locators.inspection_page_locators import *


@pytest.mark.xray('DEV-5067')
@scenario("../../../features/inspecting/dynamic_items/dynamic_matrices.feature", "Dynamic matrix")
def test_dynamic_matrix():
    """Verify that dynamic matrix checkpoints are working as expected"""


@when("I add text to all 3 cells")
def add_text_to_cells(selenium, pass_data_to_step):
    matrix_table_xpath = pass_data_to_step.get('matrix table xpath')
    matrix_cells = by_xpath_multiple(selenium, matrix_table_xpath + CP_DYNAMIC_TABLE_CELLS)
    for cell in matrix_cells:
        cell.click()
        time.sleep(1)
        by_xpath(selenium, CP_DYNAMIC_TABLE_CELL_TEXTAREA).send_keys("data 1")
        by_xpath(selenium, INSP_SAVE_BUTTON).click()


@when("I add new data to 2 of the cells")
def add_new_text_to_cells(selenium, pass_data_to_step):
    matrix_table_xpath = pass_data_to_step.get('matrix table xpath')
    matrix_cells = by_xpath_multiple(selenium, matrix_table_xpath + CP_DYNAMIC_TABLE_CELLS)
    for cell in matrix_cells[1:3]:
        cell.click()
        time.sleep(1)
        by_xpath(selenium, CP_DYNAMIC_TABLE_CELL_TEXTAREA).send_keys("data 2")
        by_xpath(selenium, INSP_SAVE_BUTTON).click()
        wait_clickable_long(selenium, INSP_SYNCED_TEXT)


@then("the PDF shows the new data and empty cell")
def pdf_shows_new_data(selenium):
    wait_clickable_long(selenium, INSP_GET_PDF_ICON_ENABLED)
    inspection_id = by_xpath(selenium, INSP_ID).text
    pdfs = return_downloaded_pdfs()
    correct_pdf = pdfs[-1]
    pdf_content = extract_text(correct_pdf)
    assert inspection_id in pdf_content
    assert "data 2" in pdf_content
    assert "data 1" not in pdf_content
