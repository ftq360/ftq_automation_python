from pytest_bdd import scenario, then

from functions import *
from locators.inspection_page_locators import *


@pytest.mark.xray('DEV-7243')
@scenario("../../../features/inspecting/dynamic_items/groupable_buttons_with_visibility_formulas.feature", "Groupable Buttons With Visibility Formulas")
def test_groupable_buttons_with_visibility_formulas():
    """Verify that groupable buttons are working as expected with visibility formulas"""


@then("I do not see 'Dynamic Checkpoint1 - Grouped' grouped button checkpoint visible on inspection creation")
def item_not_visible_on_insp_creation(selenium):
    cp_names = by_xpath_multiple(selenium, CP_NAMES)
    names = [x.text for x in cp_names]
    assert 'Dynamic Checkpoint1 - Grouped' not in names


@then("I do see 'Dynamic Checkpoint2 - Grouped' grouped button checkpoint visible on inspection creation")
def item_visible_on_insp_creation(selenium):
    cp_names = by_xpath_multiple(selenium, CP_NAMES)
    names = [x.text for x in cp_names]
    assert 'Dynamic Checkpoint2 - Grouped' in names


@then("I see 'Dynamic Checkpoint1 - Grouped' groupable button checkpoint become visible")
def grouped_button_added(selenium):
    names = wait_checkpoint_visibile(selenium, 'Dynamic Checkpoint1 - Grouped')
    assert 'Dynamic Checkpoint1 - Grouped' in names
