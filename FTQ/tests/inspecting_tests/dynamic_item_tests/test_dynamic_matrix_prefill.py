import random

from pdfminer.high_level import extract_text
from pytest_bdd import scenario, then, parsers, when

from functions import *
from locators.inspection_page_locators import *


@pytest.mark.xray('DEV-5394')
@scenario("../../../features/inspecting/dynamic_items/dynamic_matrix_prefill.feature", "Dynamic matrix prefill")
def test_dynamic_matrix_prefill():
    """Verify that dynamic matrix prefill is working as expected"""


@then(
    parsers.parse("dynamic matrix button titled '{dynamic_matrix_name}' is automatically populated with rows and data"))
def dynamic_matrix_is_prefilled(selenium, dynamic_matrix_name, pass_data_to_step):
    get_matrix_buttons = by_xpath_multiple(selenium, CP_DYNAMIC_ADD_ROW_NAMES)
    index = [x.text for x in get_matrix_buttons].index(dynamic_matrix_name) + 1
    matrix_prefill_table = f"({CP_DYNAMIC_ADD_ROW_BUTTON})[{index}]{CP_DYNAMIC_TABLE_CONNECTED_TO_BUTTON}"
    pass_data_to_step['matrix table xpath'] = matrix_prefill_table
    matrix_prefill_table_data = by_xpath(selenium, matrix_prefill_table).text
    list_prefill_data = matrix_prefill_table_data.splitlines()
    check_data_count = list_prefill_data.count("prefill data")
    assert check_data_count == 3


@when("I add new data to some of the cells")
def add_new_text_to_some_cells(selenium, pass_data_to_step):
    matrix_table_xpath = pass_data_to_step.get('matrix table xpath')
    matrix_cells = by_xpath_multiple(selenium, matrix_table_xpath + CP_DYNAMIC_TABLE_CELLS)
    for cell in random.choices(matrix_cells, k=3):
        cell.click()
        time.sleep(1)
        by_xpath(selenium, CP_DYNAMIC_TABLE_CELL_TEXTAREA).send_keys("new data")
        by_xpath(selenium, INSP_SAVE_BUTTON).click()
        wait_clickable(selenium, INSP_SYNCED_TEXT)


@then("the PDF shows the new data in my prefilled dynamic matrix")
def pdf_shows_new_data(selenium):
    wait_clickable_long(selenium, INSP_GET_PDF_ICON_ENABLED)
    inspection_id = by_xpath(selenium, INSP_ID).text
    pdfs = return_downloaded_pdfs()
    correct_pdf = pdfs[-1]
    pdf_content = extract_text(correct_pdf)
    assert inspection_id in pdf_content
    assert "new data" in pdf_content
    assert "prefill data" not in pdf_content
