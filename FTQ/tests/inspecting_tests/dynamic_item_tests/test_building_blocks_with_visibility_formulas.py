from pytest_bdd import scenario, then, parsers
from functions import *
from locators.inspection_page_locators import *


@pytest.mark.xray('DEV-7210')
@scenario("../../../features/inspecting/dynamic_items/building_blocks_with_visibility_formulas.feature", "Building Blocks With Visibility Formulas")
def test_bb_with_visibility_formulas():
    """Verify that building blocks are working as expected with visiblity formulas"""


@then(parsers.parse("I do not see '{checkpoint}' building block checkpoint visible on inspection creation"))
def bb_no_visible_on_insp_creation(selenium, checkpoint):
    cp_names = by_xpath_multiple(selenium, CP_NAMES)
    names = [x.text for x in cp_names]
    bb_checkpoints = [x for x in names if 'Building Block' in x]
    assert len(bb_checkpoints) == 0
    assert checkpoint not in names


@then(parsers.parse("I see '{checkpoint}' building block checkpoint become visible"))
def bb_added(selenium, checkpoint):
    names = []
    start_time = time.time()
    while (checkpoint not in names) and time.time() - start_time < 60:
        cp_names_new = by_xpath_multiple(selenium, CP_NAMES)
        get_names = [x.text for x in cp_names_new]
        names.extend(get_names)
    bb_checkpoints = [x for x in names if 'Building Block' in x]
    assert len(bb_checkpoints) != 0
    assert checkpoint in names


@then("I should see the building block added to the inspection")
def bb_added(selenium):
    names = []
    start_time = time.time()
    while "Quality Checkpoint1 - Building Block" not in names and time.time() - start_time < 60:
        cp_names_new = by_xpath_multiple(selenium, CP_NAMES)
        get_names = [x.text for x in cp_names_new]
        names.extend(get_names)
    assert "Quality Checkpoint1 - Building Block" in names


@then("I should see 'Quality Checkpoint2 - Building Block' become visible")
def bb_added(selenium):
    wait_clickable(selenium, INSP_CHECKPOINT(selenium, "Quality Checkpoint2 - Building Block"))
