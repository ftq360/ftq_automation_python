from pytest_bdd import scenario, then
from functions import *


@pytest.mark.xray('DEV-6743')
@scenario("../../../features/inspecting/dynamic_items/dynamic_matrix_children_copy_mode.feature", "Dynamic Matrix Children Copy Mode")
def test_dynamic_matrix_children_copy_mode():
    """Verify that Dynamic Matrix Children Copy Mode ia working as expected"""


@then("I see dynamic matrix children copied over correctly")
def dynamic_matrix_children_copied_settings(selenium, pass_data_to_step):
    dynamic_index = pass_data_to_step.get('dynamic_index')
    wait_clickable(selenium, CP_DYNAMIC_ADD_ROW_NAMES)
    get_cell_data = by_xpath_multiple(selenium, CP_DYNAMIC_TABLE_CELLS)
    cell_data = [x.text for x in get_cell_data]
    assert "automation data added cell3 row1" not in cell_data
    assert "automation data added cell3 row2" not in cell_data
