from pytest_bdd import scenario, then
from functions import *
from locators.inspection_page_locators import *


@pytest.mark.xray('DEV-7242')
@scenario("../../../features/inspecting/dynamic_items/groupable_dynamic_buttons_on_inspection_page.feature", "Groupable Buttons On Inspection Screen")
def test_groupable_items():
    """Verify that a groupable buttons are work as expected on the inspection screen"""


@then("I see dynamic items grouped")
def dynamic_checkpoint_added(selenium):
    grouped_items = by_xpath_multiple(selenium, CP_GROUPED_DYNAMIC_ITEM)
    assert len([x for x in grouped_items]) == 3


@then("I should see a dynamic checkpoint added to the inspection")
def dynamic_checkpoint_added(selenium):
    wait_clickable(selenium, INSP_CHECKPOINT(selenium, 'Dynamic Child Checkpoint'))
