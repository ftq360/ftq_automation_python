from pytest_bdd import scenario, when, then, parsers

from functions import *
from locators.inspection_page_locators import *


@pytest.mark.xray('DEV-5364')
@scenario("../../../features/inspecting/dynamic_items/dynamic_matrix_calculations.feature", "Dynamic Matrix Calculations")
def test_dynamic_matrix_calculations():
    """Verify that dynamic matrix calculations are working as expected"""


@when(parsers.parse("I enter '{data}' to cell '{cell}', row {row_number}"))
def enter_data_to_cell_row(selenium, pass_data_to_step, data, cell, row_number):
    dynamic_index = pass_data_to_step.get('dynamic_index')
    wait_clickable(selenium, CP_DYNAMIC_ADD_ROW_NAMES)
    cell_index = []
    if cell == "Sum":
        cell_index.clear()
        cell_index.append(1)
    elif cell == "Average":
        cell_index.clear()
        cell_index.append(2)
    elif cell == "Count Not Empty":
        cell_index.clear()
        cell_index.append(3)
    cell_element = by_xpath(selenium, f"({CP_DYNAMIC_ADD_ROW_BUTTON})[{dynamic_index}]/parent::div/parent::div/preceding-sibling::horizontal-matrix[1]"
                                      f"//tr[{row_number}]/td{cell_index}")
    scroll_to_bottom(selenium)
    scroll_into_view(selenium, cell_element)
    click_cell = cell_element.click()
    time.sleep(1)  # dynamic wait would be more complicated
    enter_text = by_xpath(selenium, "//textarea").send_keys(data)
    click_save = by_xpath(selenium, INSP_SAVE_BUTTON).click()


@then(parsers.parse("'{calculation}' calculation is '{output}'"))
def calculation_is_correct(selenium, calculation, output):
    footer_data = by_xpath_multiple(selenium, CP_DYNAMIC_FOOTER_CELL)
    footer_data_text = [x.text for x in footer_data]
    for x in footer_data_text:
        if calculation in x:
            assert output in x
