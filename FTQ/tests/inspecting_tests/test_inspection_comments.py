from pytest_bdd import scenario, when, then

from functions import *
from locators.inspection_page_locators import *


@pytest.mark.xray('DEV-7048')
@scenario("../../features/inspecting/inspection_comments.feature", "Inspection Comments")
def test_inspection_comments():
    """Verify that inspection comments works as expected"""


@when("I click on Add Comment in the checkpoint options menu")
def click_add_comment(selenium):
    by_xpath(selenium, CP_OPTIONS_ADD_COMMENT).click()


@then("a comments section appears below the checkpoint")
def comments_section_appears(selenium, pass_data_to_step):
    comment_index = pass_data_to_step.get('checkpoint_index') + 1
    comment_xpath = f"//div[contains(@data-bind, 'checkpoints.visible')]/div[{comment_index}]"
    wait_clickable(selenium, comment_xpath)
    pass_data_to_step['comment_xpath'] = comment_xpath


@when("I add a comment")
def type_a_comment(selenium):
    note = "comment"
    try:
        try_by_xpath(selenium, CP_ANY_NOTE_ENTER).send_keys(note)
    except:
        ActionChains(selenium).send_keys(note).perform()
    assert by_xpath(selenium, CP_ANY_NOTE_ENTER).text != ""


@when("I edit my comment")
def edit_comment(selenium, pass_data_to_step):
    comment_xpath = pass_data_to_step.get('comment_xpath')
    by_xpath(selenium, comment_xpath + COMMENT_TEXTAREA).click()
    time.sleep(2)


@when("I type '@us' to tag a user in my comment")
def tag_user_via_typing(selenium):
    note = " @us"
    try:
        try_by_xpath(selenium, CP_ANY_NOTE_ENTER).send_keys(note)
    except:
        ActionChains(selenium).send_keys(note).perform()


@then("I see a list of users available to tag")
def list_of_users_shown_to_tag(selenium):
    assert selenium.execute_script("return arguments[0].style.display;", by_xpath(selenium, COMMENT_TAG_USERS_LIST)) == "block"


@when("I select a user to tag in the comment")
def select_user_to_tag_in_comment(selenium, pass_data_to_step):
    comment_xpath = pass_data_to_step.get('comment_xpath')
    taggable_users = by_xpath_multiple(selenium, COMMENT_TAG_USERS_LIST_ITEMS)
    taggable_users_list = [x.text for x in taggable_users]
    index = taggable_users_list.index('user, standard') + 1
    by_xpath(selenium, f"({COMMENT_TAG_USERS_LIST_ITEMS})[{index}]").click()
    assert by_xpath(selenium, comment_xpath + "//p").text == "comment @user, standard "
