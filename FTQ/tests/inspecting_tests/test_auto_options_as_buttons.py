import random
from pytest_bdd import scenario, when, then
from functions import *
from locators.inspection_page_locators import *


@pytest.mark.xray('DEV-5362')
@scenario("../../features/inspecting/auto_options_as_buttons.feature", "Auto Options As Buttons")
def test_auto_options_as_buttons():
    """test that Auto Options As Buttons work as expected"""


@when("I select an option button for 'Auto Option Checkpoint' checkpoint")
def select_option_button(selenium, pass_data_to_step):
    option_buttons = by_xpath_multiple(selenium, INSP_CHECKPOINT(selenium, "Auto Option Checkpoint") + AUTO_OPTIONS_FROM_CP)
    options_list = [x.text for x in option_buttons]
    option_text = random.choice(options_list)
    index = options_list.index(option_text) + 1
    click_option = by_xpath(selenium, f"({INSP_CHECKPOINT(selenium, 'Auto Option Checkpoint')}{AUTO_OPTIONS_FROM_CP})[{index}]").click()
    time.sleep(1)
    try:
        try_by_xpath_multiple(selenium, INSP_CHECKPOINT(selenium, "Auto Option Checkpoint") + AUTO_OPTIONS_FROM_CP)
    except:
        pass
    pass_data_to_step['option_text'] = option_text


@then("I should see the text from the selected option populated in checkpoint notes")
def option_populated_in_cp_notes(selenium, pass_data_to_step):
    option_text = pass_data_to_step.get('option_text')
    get_cp_note = by_xpath(selenium, INSP_CHECKPOINT(selenium, "Auto Option Checkpoint") + CP_NOTE).get_attribute("outerText")
    assert get_cp_note == option_text
