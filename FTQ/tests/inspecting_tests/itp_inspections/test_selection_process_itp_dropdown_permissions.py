from pytest_bdd import scenario, when, then

from functions import *


#OBSOLETE
@pytest.mark.skip('Unstable')
@pytest.mark.xray('DEV-6748')
@scenario("../../../features/inspecting/itp_inspections/selection_process_itp_dropdown_permissions.feature", "Selection Process ITP Dropdown Permissions")
def test_selection_process_itp_dropdown_permissions():
    """test that plan mode works as expected"""


@when("I click dropdown arrow for plan item '[4-4]'")
def click_dropdown_for_plan_item(selenium):
    wait_clickable(selenium, SP_PLAN_ITEM_DROPDOWN)
    plan_items = [x.text for x in by_xpath_multiple(selenium, SP_LIST)]
    print(plan_items)
    index = plan_items.index('ITP Equipment (4-1), General-Punch List Deficiency (CT147), [4-4]') + 1
    expand_plan_item_data = by_xpath(selenium, f"({SP_PLAN_ITEM_DROPDOWN})[{index}]").click()
    try_dictionary_loading(selenium)
    try:
        select_division = by_xpath(selenium, SPECIFIC_SELECTION(selenium, "(Undefined)")).click()
    except:
        pass
    wait_clickable(selenium, SP_PLAN_ITEM_DROPDOWN_TABLE)


@then("I see a message that I do not have permission to view the inspection")
def no_permissions_to_view(selenium):
    try:
        find_table_rows = try_by_xpath(selenium, SP_PLAN_ITEM_DROPDOWN_ITEMS)
        pytest.fail('User can view inspection they do not have permission to')
    except:
        text_shown = by_xpath(selenium, SP_PLAN_ITEM_DROPDOWN_NO_PERMISSION).text
        assert "inspection not displayed because the inspection was made on another phase" in text_shown
