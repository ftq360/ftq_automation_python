import pytest
from pytest_bdd import scenario, then
from functions import by_xpath
from locators.inspection_page_locators import *


@pytest.mark.xray('DEV-7495')
@scenario("../../../features/inspecting/itp_inspections/non-equipment_checklist_with_assigned_equipment_ITP.feature", "Non-Equipment Checklist With Assigned Equipment ITP")
def test_nonequipment_checklist_with_assigned_equipment_ITP():
    """Verify that Non-Equipment Checklist With Assigned Equipment ITP have correct data populated in inspection header"""


@then("I see correct plan item selected in ITP plan field on the inspection page")
def correct_itp_data_in_plan_field(selenium):
    itp_inspection = by_xpath(selenium, INSP_HEADER_ITP)
    assert itp_inspection.get_attribute("value") == "[4-4], Responsible Party 2 (V1)"


@then("I see the project equipment listed on the ITP inspection")
def equipment_data_in_insp_header(selenium):
    equipment = by_xpath(selenium, INSP_HEADER_EQUIPMENT)
    assert equipment.text == "Equipment: ITP Equipment (4-1)"


