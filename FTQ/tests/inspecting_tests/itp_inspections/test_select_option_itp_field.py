import pytest
from pytest_bdd import scenario, then

from functions import by_xpath
from locators.inspection_page_locators import *


@pytest.mark.xray('DEV-5111')
@scenario("../../../features/inspecting/itp_inspections/select_option_itp_field.feature", "Select Option in ITP field")
def test_select_option_itp_field():
    """Verify that ITP inspections have correct data populated in the plan field"""


@then("I see 'Select Option' in ITP plan field on the inspection page")
def correct_itp_data_in_plan_field(selenium):
    itp_inspection = by_xpath(selenium, INSP_HEADER_ITP)
    assert itp_inspection.get_attribute("value") == "Select option"
