import pytest
from pytest_bdd import scenario, then

from functions import by_xpath
from locators.inspection_page_locators import *


@pytest.mark.xray('DEV-5112')
@scenario("../../../features/inspecting/itp_inspections/itp_inspections.feature", "ITP Inspections have correct data in plan field")
def test_correct_itp_data_in_inspection_plan_field():
    """Verify that ITP inspections have correct data populated in the plan field"""


@then("I see correct plan item selected in ITP plan field on the inspection page")
def correct_itp_data_in_plan_field(selenium):
    itp_inspection = by_xpath(selenium, INSP_HEADER_ITP)
    assert itp_inspection.get_attribute("value") == "[4-4], Responsible Party 2 (V1)"
