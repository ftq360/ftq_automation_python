import pytest
from pytest_bdd import scenario, then
from selenium.webdriver.common.by import By


@pytest.mark.xray('DEV-5393')
@scenario("../../../features/inspecting/itp_inspections/plan_mode_restricted.feature", "Plan mode restricted")
def test_plan_mode_required():
    """test that plan mode restricted permission works as expected"""


@then("the plan mode checkbox cannot but unchecked")
def plan_mode_is_restricted(selenium):
    checkbox_status = selenium.find_element(By.NAME, "example").get_attribute("disabled")
    assert checkbox_status == "true"
