from pytest_bdd import scenario, then

from functions import *
from locators.selection_process_locators import *


@pytest.mark.xray('DEV-5392')
@scenario("../../../features/inspecting/itp_inspections/plan_mode_works_in_selection_process.feature", "Plan mode checkbox filters the selection Checklists ITP")
def test_plan_mode_works():
    """test that plan mode works as expected"""


@then("only plan items are shown in selection process")
def plan_items_only_to_select(selenium):
    select_equipment = by_xpath(selenium, SPECIFIC_SELECTION(selenium, "ITP Equipment (4-1)")).click()
    all_available_selections = by_xpath_multiple(selenium, SP_LIST)
    checklist_names_list = [txt.text for txt in all_available_selections]
    itps_for_group = ['Concrete-General Checklist (CT150), [4-1]',
                      'Concrete-General Checklist (CT150), [4-2]',
                      'ITP Equipment (4-1), General-Punch List Deficiency (CT147), [4-4]']
    assert checklist_names_list == itps_for_group
