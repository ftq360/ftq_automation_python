from pytest_bdd import scenario, then
from functions import *
from locators.inspection_page_locators import *


@pytest.mark.xray('DEV-5447')
@scenario("../../features/inspecting/standard_checkpoint_formulas.feature", "Standard Checkpoint Formulas")
def test_standard_checkpoint_formulas():
    """test standard checkpoint formulas work as expected"""


@then("I see my formula checkpoint")
def formula_checkpoint_shown(selenium):
    wait_clickable(selenium, INSP_CHECKPOINT(selenium, "Formula Checkpoint"))


@then("the formula checkpoint shows '10' in the observation notes field")
def formula_checkpoint_shows_correct_data(selenium):
    get_notes = by_xpath(selenium, INSP_CHECKPOINT(selenium, "Formula Checkpoint") + CP_NOTE).text
    assert get_notes == "10"
