from pytest_bdd import scenario, then, when, parsers
from selenium.webdriver.common.keys import Keys

from functions import *


@pytest.mark.xray('DEV-7226')
@scenario("../../features/inspecting/search_on_inspection.feature", "Search On Inspection")
def test_search_on_inspection():
    """test that Search On Inspection works as expected"""


@when(parsers.parse("I enter '{text}' in the inspection search bar"))
def search_inspection(selenium, text):
    inspection_search = by_xpath(selenium, INSPECTION_SEARCH)
    inspection_search.click()
    inspection_search.clear()
    inspection_search.send_keys(text, Keys.ENTER)
    time.sleep(2)


@then(parsers.parse("I should see '{checkpoints}' checkpoint(s) on the inspection page"))
def inspection_filtered_by_search(selenium, checkpoints):
    expected_checkpoints = checkpoints.split(", ")
    get_all_shown_checkpoints = by_xpath_multiple(selenium, CP_NAMES)
    checkpoint_list = [txt.text for txt in get_all_shown_checkpoints]
    assert checkpoint_list == expected_checkpoints
