from pytest_bdd import scenario, then, parsers

from functions import *


@pytest.mark.xray('DEV-7227')
@scenario("../../features/inspecting/note_empty_visibility_formulas.feature", "Note Empty/Not Empty Visibility Formulas")
def test_note_empty_visibility_formulas():
    """Verify that Note Empty/Not Empty Visibility Formulas are working as expected"""


@then(parsers.parse("checkpoint '{checkpoint}' is '{visibility}'"))
def checkpoint_visibility(selenium, checkpoint, visibility):
    time.sleep(3)
    try:
        by_xpath(selenium, INSP_CHECKPOINT(selenium, checkpoint))
        if visibility != "visible":
            pytest.fail("Checkpoint is visible when it should not be")
    except ValueError:
        if visibility != "not visible":
            pytest.fail("Checkpoint not visible when it should be")
