from pytest_bdd import scenario, when, parsers

from functions import *
from locators.inspection_page_locators import *


@pytest.mark.xray('DEV-5143')
@scenario("../../features/inspecting/hidden_characters.feature", "Hidden characters pasted to inspection")
def test_hidden_characters_on_inspection():
    """test pasting hidden characters to an inspection does not break anything"""


@when(parsers.parse("I enter hidden characters in '{checkpoint}' checkpoint observation field"))
def enter_note_in_checkpoint_observations_notes(selenium, checkpoint):
    by_xpath(selenium, INSP_CHECKPOINT(selenium, checkpoint) + CP_NOTE).click()
    note = "\x02\x60\x20\x53\x70\x65\x63\x69\x61\x6C\x20\x0C\x0C\x7F\x00\x20\x43\x68\x61\x72\x61\x63\x74\x65\x72\x73\x20\x7E\x12"
    try:
        try_by_xpath(selenium, CP_ANY_NOTE_ENTER).send_keys(note)
    except:
        ActionChains(selenium).send_keys(note).perform()
    assert by_xpath(selenium, CP_ANY_NOTE_ENTER).text != ""
