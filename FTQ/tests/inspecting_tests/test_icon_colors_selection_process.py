from pytest_bdd import scenario, given, then, parsers

from functions import *
from locators.selection_process_locators import *


@pytest.mark.xray('DEV-5089')
@scenario("../../features/inspecting/icon_colors_selection_process.feature", "Icon Colors (Selection Process)")
def BROKEN_test_icon_colors_selection_process():
    """test icon colors in selection process are correct"""


@given("I select project 1 on selection page")
def select_project(selenium):
    select_project1 = by_xpath(selenium, SPECIFIC_SELECTION(selenium, "Project 1 (1)")).click()


@then(parsers.parse("I should see {icon_color} icon for checklist 'CT135'"))
def override_status(selenium,
                    icon_color):
    get_icon_color = by_xpath(selenium, (SPECIFIC_SELECTION(selenium, 'CT135') + SP_ICON_COLORS)).get_attribute("src")
    if icon_color == "grey":
        assert "Icon_TaskSupplier_2.png" in get_icon_color
    elif icon_color == "yellow":
        assert "Icon_TaskSupplier_3.png" in get_icon_color
    elif icon_color == "red filled":
        assert "Icon_TaskSupplier_4.png" in get_icon_color
    elif icon_color == "green":
        assert "Icon_TskSupplier_8.png" or "Icon_TaskSupplier_5.png" in get_icon_color
    elif icon_color == "red":
        assert "Icon_TaskSupplier_9.png" in get_icon_color


