from pytest_bdd import scenario, when, then, parsers

from functions import *
from locators.inspection_page_locators import *
from locators.listing_grid_locators import *


@pytest.mark.xray('DEV-6752')
@scenario("../../../features/inspecting/user_types/restricted_user_has_restricted_edit.feature", "Restricted User has Restricted Edit")
def test_restricted_user_has_restricted_edit():
    """test that restricted users have restricted edit"""


@when(parsers.parse("I open {inspection_number}"))
def open_inspection_number(selenium, inspection_number, pass_data_to_step):
    get_insp_ids = by_xpath_multiple(selenium, GRID_ITEM_NAME)
    index = []
    if inspection_number == "inspection1":
        insp_1_id = pass_data_to_step.get('inspection1')
        insp1_index = [get_insp_ids.index(x) + 1 for x in get_insp_ids if x.text == insp_1_id]
        index.append(insp1_index[0])
    elif inspection_number == "inspection2":
        insp_2_id = pass_data_to_step.get('inspection2')
        insp2_index = [get_insp_ids.index(y) + 1 for y in get_insp_ids if y.text == insp_2_id]
        index.append(insp2_index[0])
    click_recent_insp_link = by_xpath(selenium, f"({GRID_ITEM_NAME}){index}").click()
    try_dictionary_loading(selenium)
    wait_clickable(selenium, INSP_HEADER)


@then(parsers.parse("I {restriction} make edits to the inspection"))
def restricted_user_edit_permission(selenium, restriction):
    scroll_to_checkpoint = scroll_into_view(selenium, by_xpath(selenium, INSP_CHECKPOINT(selenium, "Quality Checkpoint")))
    checkpoint_status = by_xpath(selenium, (INSP_CHECKPOINT(selenium, "Quality Checkpoint") + CHECKPOINT_STATUS()['FTQ']))
    checkpoint_status.click()
    time.sleep(2)
    if restriction == "can":
        assert selenium.execute_script("return arguments[0].childNodes[1].checked;", checkpoint_status)
        try:
            try_by_xpath(selenium, "//*[contains(@data-bind, 'pikaday')]")
        except:
            pytest.fail("user cannot edit checkpoints")
    elif restriction == "cannot":
        assert not selenium.execute_script("return arguments[0].childNodes[1].checked;", checkpoint_status)
        try:
            try_by_xpath(selenium, "//*[contains(@data-bind, 'pikaday')]")
            pytest.fail("user can edit checkpoints")
        except:
            pass
