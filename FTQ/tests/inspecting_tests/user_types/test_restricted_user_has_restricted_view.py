from pytest_bdd import scenario, then

from functions import *
from locators.listing_grid_locators import *
from locators.setup_locators import *


@pytest.mark.xray('DEV-5356')
@scenario("../../../features/inspecting/user_types/restricted_user_has_restricted_view.feature", "Restricted user has restricted view")
def test_restricted_user_view():
    """test that restricted users have restricted view"""


@then("I see only inspections from Project 2 (2) in Recent Inspections Listing")
def recent_insp_filtered_correctly(selenium):
    wait_clickable(selenium, LISTING_GRID_PROJECT_COLUMN)
    get_projects_column = by_xpath_multiple(selenium, LISTING_GRID_PROJECT_COLUMN)
    projects_listed = [x.text for x in get_projects_column]
    for x in projects_listed:
        assert "Project 2 (2)" in x


@then("I only see Project 2 in page filter options")
def recent_insp_filter(selenium):
    ActionChains(selenium).double_click(by_xpath(selenium, GRID_PAGE_PROJECT_FILTER)).perform()
    filter_options = by_xpath_multiple(selenium, GRID_PAGE_PROJECT_FILTER_OPTIONS)
    filter_options_list = [x.text for x in filter_options]
    assert len(filter_options_list) == 1
    for x in filter_options_list:
        assert "Project 2" in x
