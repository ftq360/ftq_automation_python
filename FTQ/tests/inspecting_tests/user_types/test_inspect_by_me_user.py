import pytest
from pytest_bdd import scenario, then
from functions import by_xpath_multiple


from locators.listing_grid_locators import LISTING_GRID_CREATED_BY


@pytest.mark.xray('DEV-5204')
@scenario("../../../features/inspecting/user_types/inspect_by_me_user.feature", "Inspect By Me User")
def test_inspect_by_me_user():
    """Verify that Inspect By Me users are working as expected"""


@then("I can only see inspections created by me")
def only_inspections_by_me(selenium):
    inspectors_on_page = by_xpath_multiple(selenium, LISTING_GRID_CREATED_BY)
    inspectors_list = [x.text for x in inspectors_on_page]
    for x in inspectors_list:
        assert x == "user, inspectbyme"
