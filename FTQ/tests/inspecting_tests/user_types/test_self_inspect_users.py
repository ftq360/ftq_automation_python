from pytest_bdd import scenario, when, then
from selenium.webdriver.common.keys import Keys
from functions import *
from locators.inspection_page_locators import *
from locators.listing_grid_locators import *
from locators.selection_process_locators import *


@pytest.mark.xray('DEV-5357')
@scenario("../../../features/inspecting/user_types/self_inspect_user.feature", "Self Inspect user can login and inspect")
def test_self_inspect_user_can_inspect():
    """test logging in and inspecting with a self inspect user"""


@when("I create an inspection as a self inspect user")
def create_inspection_self_inspect_user(selenium, cred_data):
    go_to_create_insp = selenium.get(cred_data['BASE_URL'] + cred_data['CREATE_INSPECTION'])
    dict_loading_modal_appears = wait_present(selenium, SP_DICT_LOADING_MODAL_OPEN)
    dictionaries_load = wait_present(selenium, SP_DICT_LOADING_MODAL_SUCCESS)
    dictionary_loading_complete = wait_present(selenium, SP_DICT_LOADING_MODAL_CLOSED)
    select_checklist_general = by_xpath(selenium,
                                        SPECIFIC_SELECTION(selenium, "General-Punch List Deficiency (CT147)")).click()


@then("only Project 1 is available to me")
def project_1_only_available(selenium):
    get_inspection_project = by_xpath(selenium, INSP_HEADER_PROJECT).text
    assert get_inspection_project == "Project 1 (1)"


@then("my Responsible Party is automatically selected")
def rp_automatically_selected(selenium):
    get_inspection_rp = by_xpath(selenium, INSP_HEADER_RP).get_attribute("value")
    assert get_inspection_rp == "FTQ Automation Group (D1)"


@then("I can see Project 1 and Project 2")
def can_see_proj_1_and_2(selenium):
    search_for_proj1 = by_xpath(selenium, GRID_SEARCH).send_keys("Project 1 (1)")
    enter_search_for_proj1 = by_xpath(selenium, GRID_SEARCH).send_keys(Keys.ENTER)
    get_rows = by_xpath_multiple(selenium, LISTING_GRID_ROWS)
    rows_list = [x for x in get_rows]
    assert len(rows_list) > 1
    clear_search_field = by_xpath(selenium, GRID_SEARCH).clear()
    search_for_proj2 = by_xpath(selenium, GRID_SEARCH).send_keys("Project 2 (2)")
    enter_search_for_proj2 = by_xpath(selenium, GRID_SEARCH).send_keys(Keys.ENTER)
    get_rows = by_xpath_multiple(selenium, LISTING_GRID_ROWS)
    rows_list = [x for x in get_rows]
    assert len(rows_list) > 1


@then("I cannot see 'Project for Data Creation'")
def cannot_see_proj_3(selenium):
    search_for_data_proj = by_xpath(selenium, GRID_SEARCH).send_keys("Project for Data Creation (3)")
    enter_search_for_proj2 = by_xpath(selenium, GRID_SEARCH).send_keys(Keys.ENTER)
    time.sleep(2)
    try:
        get_rows = by_xpath_multiple(selenium, LISTING_GRID_ROWS)
        pytest.fail("Project for Data Creation showing data")
    except:
        pass
