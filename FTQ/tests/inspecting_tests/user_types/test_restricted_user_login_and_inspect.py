import pytest
from pytest_bdd import scenario


@pytest.mark.xray('DEV-5373')
@scenario("../../../features/inspecting/user_types/restricted_user_login_and_inspect.feature", "Restricted user can login and inspect")
def test_restricted_user_inspect():
    """test that restricted users can login and inspect"""


# all steps are common steps found in conftest file
