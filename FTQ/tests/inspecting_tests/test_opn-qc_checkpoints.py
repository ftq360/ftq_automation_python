from pytest_bdd import scenario, parsers, when, then
from functions import *
from locators.inspection_page_locators import *


@pytest.mark.xray('DEV-5124')
@scenario("../../features/inspecting/opn-qc_checkpoints.feature", "OPN/QC checkpoints work as expected")
def test_opn_qc_checkpoints():
    """Verify that OPN/QC checkpoints are working as expected"""


@when(parsers.parse("I fill 'Corrective Action Notes' field for '{checkpoint}' checkpoint with '{text}'"))
def fill_deficiency_checkpoint_fields_CA(selenium, checkpoint, text):
    by_xpath(selenium, INSP_CHECKPOINT(selenium, checkpoint) + CP_CA_NOTE).click()
    time.sleep(3)
    try:
        try_by_xpath(selenium, CP_ANY_NOTE_ENTER).send_keys(f"{text}")
    except:
        ActionChains(selenium).send_keys(f"{text}").perform()
    try:
        try_by_xpath(selenium, INSP_SAVE_BUTTON).click()
    except:
        print("already saved")
    wait_clickable(selenium, CP_CA_NOTE + "/p")


@when(parsers.parse("I fill 'Responsible Party' field for '{checkpoint}' checkpoint with '{rp_to_select}'"))
def fill_deficiency_checkpoint_fields_RP(selenium, checkpoint, rp_to_select):
    scroll_to_bottom(selenium)
    click_rp_dropdown = by_xpath(selenium, INSP_CHECKPOINT(selenium, checkpoint) + CP_RP_DROPDOWN).click()
    type_rp_name = by_xpath(selenium, CP_RP_SEARCH).send_keys(rp_to_select)
    click_rp = by_xpath(selenium, CP_DROPDOWN_FIRST_RP_IN_LIST).click()
    wait_present(selenium, INSP_SYNCED_TEXT)
    true_rp_selected = by_xpath(selenium, INSP_CHECKPOINT(selenium, checkpoint) + CP_RP_DROPDOWN).get_attribute("value")
    assert true_rp_selected == rp_to_select


@then(parsers.parse("I should see changes made to fields in '{checkpoint}' checkpoint on inspection page"))
def value_still_present(selenium, checkpoint):
    true_ca_note = by_xpath(selenium, INSP_CHECKPOINT(selenium, checkpoint) + CP_CA_NOTE).text
    assert true_ca_note == "actionNote"
    true_rp_selected = by_xpath(selenium, INSP_CHECKPOINT(selenium, checkpoint) + CP_RP_DROPDOWN).get_attribute("value")
    assert true_rp_selected == "Responsible Party 2 (V1)"
