from pytest_bdd import scenario, when, then

from functions import *
from locators.inspection_page_locators import *


@pytest.mark.xray('DEV-5078')
@scenario("../../../features/inspecting/attachments/attachment_notes.feature", "Attachment Notes")
def test_attachments_notes():
    """Verify that attachments notes on inspections are working as expected"""


@when("I add notes to the attached file")
def add_notes_to_attached_file(selenium, pass_data_to_step):
    checkpoint_name = pass_data_to_step.get('checkpoint_name')
    checkpoint_note = by_xpath(selenium, INSP_CHECKPOINT(selenium, checkpoint_name) + CP_ATTACHMENT + CP_ATTACHMENT_NOTES)
    checkpoint_note.send_keys("automated note")
    while checkpoint_note.get_attribute("value") != "automated note":
        time.sleep(0)


@then("my notes are still present on the attachment")
def notes_still_present_on_attachment(selenium, pass_data_to_step):
    checkpoint_name = pass_data_to_step.get('checkpoint_name')
    checkpoint_note = by_xpath(selenium, INSP_CHECKPOINT(selenium, checkpoint_name) + CP_ATTACHMENT + CP_ATTACHMENT_NOTES)
    assert checkpoint_note.get_attribute("value") == "automated note"
