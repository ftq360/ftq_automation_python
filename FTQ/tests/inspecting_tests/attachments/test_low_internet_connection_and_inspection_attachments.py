from pytest_bdd import scenario, then, given
from functions import *
from locators.inspection_page_locators import *


@pytest.mark.xray('DEV-5410')
@scenario("../../../features/inspecting/attachments/low_internet_connection_and_inspection_attachments.feature", "Low Internet Connection and Inspection Attachments")
def test_low_internet_connection_and_inspection_attachments():
    """Verify that Low Internet Connection and Inspection Attachment are working as expected"""


@given("I set network to 'Slow 3g' network throttling")
def set_network_throttling(selenium):
    selenium.set_network_conditions(
        offline=False,
        latency=2000,  # additional latency (ms)
        download_throughput=400 * 1024,  # maximal throughput
        upload_throughput=400 * 1024
    )  # maximal throughput


@then("attachments are displayed correctly")
def attachments_displayed_correctly(selenium, pass_data_to_step):
    checkpoint_name = pass_data_to_step.get('checkpoint_name')
    attachment_number = pass_data_to_step.get('attachment_number')
    get_number_of_attachments = by_xpath_multiple(selenium, INSP_CHECKPOINT(selenium, checkpoint_name) + CP_ATTACHMENT)
    attachment_list = [x for x in get_number_of_attachments]
    assert len(attachment_list) == int(attachment_number)