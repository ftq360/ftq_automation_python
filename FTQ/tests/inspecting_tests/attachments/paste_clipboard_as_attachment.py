from PIL import Image
from pytest_bdd import scenario, when
from six import BytesIO

from functions import *


@pytest.mark.skip('Unstable')
@pytest.mark.xray('DEV-5555')
@scenario("../../../features/inspecting/attachments/paste_clipboard_as_attachment.feature", "Paste clipboard as attachment")
def test_paste_clipboard_as_attachment():
    """Verify that pasting from windows clipboard on chrome browser works as expected as an inspection attachment"""


@when('I press Print Scr key')
def press_print_scr(selenium):
    output = BytesIO()
    Image.open(r"C:\Workspace\AutomationAttachments\image.bmp")
    data = output.getvalue()[14:]
    output.close()
    send_to_clipboard(win32clipboard.CF_DIB, data)

    # win32clipboard.SetClipboardData(r"C:\Workspace\AutomationAttachments\image.bmp")
    # winclip32.set_clipboard_data("bitmapinfo_std_structure", r"C:\Workspace\AutomationAttachments\image.bmp")
    # mem_file = BytesIO()
    # screen = ImageGrab.grab()
    # screen.save(mem_file, 'png')
    # data = mem_file.getvalue()
    # Image.open(BytesIO(data))
    # send_to_clipboard(data)
    # win32clipboard.SetClipboardData(, r"C:\Workspace\AutomationAttachments\FTQ-square.png")






