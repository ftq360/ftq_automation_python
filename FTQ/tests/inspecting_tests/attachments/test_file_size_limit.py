from pytest_bdd import scenario, when, then, parsers
from functions import *
from locators.inspection_page_locators import *


@pytest.mark.xray('DEV-5076')
@scenario("../../../features/inspecting/attachments/limit_file_size.feature", "Limit File Size")
def test_limit_file_size():
    """Verify that file limits are working as expected on inspection page"""


@when(parsers.parse("I attach large PDF file to '{checkpoint_name}' checkpoint on inspection page"))
def attach_large_pdf(selenium, checkpoint_name):
    click_camera_button = by_xpath(selenium, (INSP_CHECKPOINT(selenium, checkpoint_name) + CP_CAMERA)).click()
    selenium.execute_script("$('.cm-file').css('display', 'block')")
    file_input = by_xpath_simple(selenium, "//input[@class='cm-file']")
    file_input.send_keys(r'C:\Workspace\AutomationAttachments\large_pdf.pdf')


@then("I should see 'Upload limit exceeded' popup")
def upload_limit_exceeded(selenium):
    wait_clickable(selenium, POPUP)
    assert by_xpath(selenium, POPUP + POPUP_MESSAGE).get_attribute("outerText") ==\
           "Up to 10 files can be uploaded at one time. Each file may be up to 100 MB. Please try again."


@when("I press OK")
def confirm_popup(selenium):
    by_xpath(selenium, POPUP_OK_BUTTON).click()


@then("large PDF file is not uploaded")
def large_pdf_not_uploaded(selenium):
    try:
        wait_clickable_short(selenium, CP_ATTACHMENT)
        pytest.fail("PDF uploaded")
    except:
        pass
