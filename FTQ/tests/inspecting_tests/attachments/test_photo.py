from pytest_bdd import scenario, when
from functions import *
from locators.inspection_page_locators import *


@pytest.mark.xray('DEV-5079')
@scenario("../../../features/inspecting/attachments/photo.feature", "Take photo on inspection")
def test_photo():
    """Verify that user can take photo on inspection as expected"""


@when("I take a picture")
def take_picture(selenium):
    by_xpath(selenium, TAKE_PICTURE).click()
