from pytest_bdd import scenario, then, when, parsers
from functions import *
from locators.inspection_page_locators import *
from locators.selection_process_locators import *


@pytest.mark.xray('DEV-5070')
@scenario("../../../features/inspecting/attachments/references.feature", "References on Inspection Page")
def test_references_on_inspection_page():
    """test that references work as expected on inspection page"""


@when(parsers.parse("I create an inspection via Project for References, checklist '{checklist}'"))
def create_insp_by_reference_project(selenium, cred_data, checklist):
    go_to_create_insp = selenium.get(cred_data['BASE_URL'] + cred_data['CREATE_INSPECTION'])
    try_dictionary_loading(selenium)
    try:
        select_division = by_xpath(selenium, SPECIFIC_SELECTION(selenium, "(Undefined)")).click()
    except:
        pass
    try:
        select_project1 = by_xpath(selenium, SPECIFIC_SELECTION(selenium, "Project for References (26)")).click()
    except:
        pass

    try:
        try_by_xpath(selenium, SP_PLAN_MODE_CHECKED).click()
        try:
            try_by_xpath(selenium, SP_PLAN_MODE_CHECKED).click()
        except:
            print("plan mode removed")
    except:
        print("plan mode not checked")

    select_specific_checklist = by_xpath(selenium, (SPECIFIC_SELECTION(selenium, checklist))).click()
    select_rp = by_xpath(selenium, SPECIFIC_SELECTION(selenium, "Responsible Party for References (V2)")).click()
    check_inspection_created_successfully = wait_clickable(selenium, INSP_HEADER)
    assert "Inspection" in selenium.title 
    click_save_btn = by_xpath(selenium, INSP_SAVE_BUTTON).click()
    wait_clickable(selenium, INSP_SYNCED_TEXT)


@then("checklist references are shown")
def checklist_references_show(selenium):
    scroll_into_view(selenium, by_xpath(selenium, INSP_HEADER_CHECKLIST_REFERENCE))
    by_xpath(selenium, INSP_HEADER_CHECKLIST_REFERENCE + REFERENCE_EXPAND).click()
    wait_clickable(selenium, INSP_HEADER_CHECKLIST_REFERENCE + REFERENCE_ATTACHMENT)


@then("project references are shown")
def project_references_show(selenium):
    scroll_into_view(selenium, by_xpath(selenium, INSP_HEADER_PROJECT_REFERENCE))
    by_xpath(selenium, INSP_HEADER_PROJECT_REFERENCE + REFERENCE_EXPAND).click()
    wait_clickable(selenium, INSP_HEADER_PROJECT_REFERENCE + REFERENCE_ATTACHMENT)


@then("responsible party references are shown")
def rp_references_show(selenium):
    scroll_into_view(selenium, by_xpath(selenium, INSP_HEADER_RP_REFERENCE))
    by_xpath(selenium, INSP_HEADER_RP_REFERENCE + REFERENCE_EXPAND).click()
    wait_clickable(selenium, INSP_HEADER_RP_REFERENCE + REFERENCE_ATTACHMENT)
