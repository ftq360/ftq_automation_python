from pytest_bdd import scenario, then

from functions import *
from locators.inspection_page_locators import *


@pytest.mark.xray('DEV-5086')
@scenario("../../../features/inspecting/attachments/inspection_attachments.feature", "Inspection Attachments")
def test_inspection_attachments():
    """Verify that inspection attachments are working as expected"""


@then("attachments are displayed correctly")
def attachments_displayed_correctly(selenium, pass_data_to_step):
    checkpoint_name = pass_data_to_step.get('checkpoint_name')
    attachment_number = pass_data_to_step.get('attachment_number')
    get_number_of_attachments = by_xpath_multiple(selenium, INSP_CHECKPOINT(selenium, checkpoint_name) + CP_ATTACHMENT)
    attachment_list = [x for x in get_number_of_attachments]
    assert len(attachment_list) == int(attachment_number)
