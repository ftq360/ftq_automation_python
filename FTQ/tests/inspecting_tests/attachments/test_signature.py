from pytest_bdd import scenario, when, then

from functions import *
from locators.inspection_page_locators import *


@pytest.mark.xray('DEV-5084')
@scenario("../../../features/inspecting/attachments/signature.feature", "Signature")
def test_signature():
    """test the user can add signature to a checkpoint on an inspection"""


@then("I should see drawing board")
def signature_box_pops_up(selenium):
    verify_canvas = wait_clickable(selenium, CP_SIGNATURE_CANVAS)


@when("I draw signature on drawing board")
def draw_signature(selenium):
    canvas = by_xpath(selenium, CP_SIGNATURE_CANVAS)
    draw = ActionChains(selenium).click_and_hold(canvas) \
        .move_by_offset(100, 100).release().perform()
