from pytest_bdd import scenario, when, then

from functions import *


@pytest.mark.xray('DEV-8407')
@scenario("../../features/inspecting/equipment_selector_on_checkpoint.feature", "Equipment Selector On Checkpoint")
def test_equipment_selector_on_checkpoint():
    """Test that Equipment Selector On Checkpoint is working as expected"""


@then("I see 'Equipment Checkpoint' has equipment field populated")
def equipment_field_populated(selenium, pass_data_to_step):
    equipment_locator = by_xpath(selenium, INSP_CHECKPOINT(selenium, 'Equipment Checkpoint') + CP_EQUIPMENT_SELECTOR)
    equipment_displayed = selenium.execute_script("return arguments[0].childNodes[1].childNodes[3].childNodes[1].value;", equipment_locator)
    assert equipment_displayed != ""
    pass_data_to_step['equipment_displayed'] = equipment_displayed


@when("I change equipment selected")
def change_equipment_on_checkpoint(selenium, pass_data_to_step):
    scroll_to_bottom(selenium)
    by_xpath(selenium, INSP_CHECKPOINT(selenium, 'Equipment Checkpoint') + CP_EQUIPMENT_SELECTOR).click()
    equipment_options = by_xpath_multiple(selenium, CP_EQUIPMENT_SELECTOR_OPTIONS)
    equipment_list = [x.text for x in equipment_options]
    index = equipment_list.index('equipment 2 (2-2)') + 1
    by_xpath(selenium, f"({CP_EQUIPMENT_SELECTOR_OPTIONS})[{index}]").click()
    time.sleep(1)
    equipment_locator = by_xpath(selenium, INSP_CHECKPOINT(selenium, 'Equipment Checkpoint') + CP_EQUIPMENT_SELECTOR)
    new_equipment_displayed = selenium.execute_script(
        "return arguments[0].childNodes[1].childNodes[3].childNodes[1].value;", equipment_locator)
    original_equipment = pass_data_to_step.get('equipment_displayed')
    assert new_equipment_displayed != original_equipment


@then("I see equipment change recorded in inspection activity log")
def equipment_change_recorded_on_activity_log(selenium):
    by_xpath(selenium, ACTIVITY_LOG).click()
    activity_log_messages = by_xpath_multiple(selenium, ACTIVITY_LOG_MESSAGE)
    activity_log_messages_text = [x.text for x in activity_log_messages]
    assert any('changed the Equipment of Checkpoint' in x for x in activity_log_messages_text)
