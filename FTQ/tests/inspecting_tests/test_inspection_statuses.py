from pytest_bdd import scenario, then, parsers

from functions import *
from locators.inspection_page_locators import *


@pytest.mark.xray('DEV-5108')
@scenario("../../features/inspecting/inspection_statuses.feature", "Inspection Statuses")
def test_inspection_statuses():
    """Verify that inspection statuses working as expected"""


@then(parsers.parse("inspection status is '{inspection_status}'"))
def inspection_status_is(selenium, inspection_status):
    scroll_into_view(selenium, by_xpath(selenium, INSP_HEADER_STATUS))
    current_inspection_status = by_xpath(selenium, INSP_HEADER_STATUS).get_attribute("value")
    assert current_inspection_status == inspection_status
