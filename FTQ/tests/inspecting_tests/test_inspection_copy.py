from pytest_bdd import scenario, then

from functions import *
from locators.inspection_page_locators import *


@pytest.mark.xray('DEV-5085')
@scenario("../../features/inspecting/inspection_copy.feature", "Inspection Copy")
def BROKEN_test_inspection_copy():
    """Verify that inspection copy works as expected"""


@then("checkpoint statuses were copied over correctly")
def checkpoint_statuses_copied(selenium):
    ftq_checked = by_xpath(selenium, INSP_CHECKPOINT(selenium, "Footer depth and width per plan and code.")
                       + CP_FTQ_STATUS_FROM_CHECK_NAME)
    ftq_check_status = selenium.execute_script("return arguments[0].childNodes[1].checked;", ftq_checked)
    assert ftq_check_status == True

    opn_checked = by_xpath(selenium, INSP_CHECKPOINT(selenium, "Fall of waste lines verified with level, supported with backfill.")
                       + CP_OPN_STATUS_FROM_CHECK_NAME)
    opn_check_status = selenium.execute_script("return arguments[0].childNodes[1].checked;", opn_checked)
    assert opn_check_status == True
