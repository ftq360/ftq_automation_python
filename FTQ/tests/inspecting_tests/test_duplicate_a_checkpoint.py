from pdfminer.high_level import extract_text
from pytest_bdd import scenario, when, then

from functions import *


@pytest.mark.xray('DEV-7427')
@scenario("../../features/inspecting/duplicate_a_checkpoint.feature", "Duplicate A Checkpoint")
def BROKEN_test_duplicate_a_checkpoint():
    """test duplicating a checkpoint on the inspection screen is working as expected"""


@when("I click Duplicate button")
def click_duplicate_checkpoint(selenium):
    by_xpath(selenium, CP_OPTIONS_DUPLICATE).click()
    time.sleep(3)


@then("I should see '{checkpoint}' duplicated")
def checkpoint_duplicated(selenium, pass_data_to_step):
    all_checkpoints = by_xpath_multiple(selenium, CP_NAMES)
    list_checkpoints = [txt.text for txt in all_checkpoints]
    pass_data_to_step['list_checkpoints'] = list_checkpoints
    assert list_checkpoints.count('Quality Checkpoint') > 1


@then("I should see duplicated checkpoint has no notes, status or attachments")
def duplicated_checkpoint_clean(selenium, pass_data_to_step):
    checkpoints = pass_data_to_step.get('list_checkpoints')
    index = [i for i, n in enumerate(checkpoints) if n == 'Quality Checkpoint']
    try:
        try_by_xpath(selenium, f"//div[contains(@data-bind, 'checkpoints.visible')]/div[{index[-1]+1}]" + CP_ATTACHMENT)
        pytest.fail()
    except:
        pass
    assert by_xpath(selenium, f"//div[contains(@data-bind, 'checkpoints.visible')]/div[{index[-1]+1}]" + CP_NOTE).text == "Observations"


@then("I see duplicated checkpoint on report")
def duplicated_checkpoint_on_pdf(selenium):
    wait_clickable_long(selenium, INSP_GET_PDF_ICON_ENABLED)
    pdfs = return_downloaded_pdfs()
    correct_pdf = pdfs[-1]
    pdf_content = extract_text(correct_pdf)
    split = pdf_content.splitlines()
    cp_list = [x for x in split if 'Quality Checkpoint' in x]
    assert len(cp_list) == 2
