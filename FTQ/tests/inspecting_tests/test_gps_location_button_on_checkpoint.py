from pytest_bdd import scenario, when, then
from functions import *


@pytest.mark.xray('DEV-8411')
@scenario("../../features/inspecting/gps_location_button_on_checkpoint.feature", "GPS Location Button on Checkpoint")
def test_gps_location_button_on_checkpoint():
    """Test that GPS Location Button on Checkpoint is working as expected"""


@when("I select 'Add GPS Position'")
def select_cp_gps_button(selenium):
    by_xpath(selenium, CP_OPTIONS_GPS).click()


@then("I should see the GPS coordinates populate on the checkpoint")
def gps_coordinate_added_to_cp(selenium):
    gps_coord = by_xpath(selenium, CP_GPS_INSERTED).get_attribute("value")
