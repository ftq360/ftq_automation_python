from pytest_bdd import then, when, parsers
from selenium.webdriver.common.keys import Keys

from functions import *
from locators.inspection_page_locators import *


# Inspection Header and Footer Steps
@pytest.fixture
@then(parsers.parse("the Inspection ID is {inspection_number}"))
def inspection_id_numerate(selenium, inspection_number, pass_data_to_step):
    if inspection_number == "inspection1":
        pass_data_to_step['inspection1'] = pass_data_to_step.get('insp_ID')
    elif inspection_number == "inspection2":
        pass_data_to_step['inspection2'] = pass_data_to_step.get('insp_ID')


@pytest.fixture
@when(parsers.parse("I click on Options Menu for '{checkpoint}' checkpoint on inspection page"))
def click_options_hamburger_menu(selenium, checkpoint, pass_data_to_step):
    pass_data_to_step['checkpoint_index'] = INSP_CHECKPOINT_INDEX(selenium, checkpoint)
    scroll_into_view(selenium, by_xpath(selenium, INSP_CHECKPOINT(selenium, checkpoint)))
    by_xpath(selenium, INSP_CHECKPOINT(selenium, checkpoint) + CP_OPTIONS_MENU).click()


# Inspection Attachment Steps
@pytest.fixture
@when(parsers.parse("I click on camera button in '{checkpoint}' checkpoint on inspection page"))
def click_camera_icon(selenium, checkpoint):
    camera = by_xpath(selenium, INSP_CHECKPOINT(selenium, checkpoint) + CP_CAMERA)
    scroll_into_view(selenium, camera)
    click_camera = camera.click()


@pytest.fixture
@when(parsers.parse("I select '{attachment_option}' option in camera dropdown on inspection page"))
def select_signature_icon(selenium, attachment_option):
    if attachment_option == "Signature":
        click_signature = by_xpath(selenium, CP_SIGNATURE).click()
    elif attachment_option == "Photo":
        click_photo = by_xpath(selenium, CP_PHOTO).click()
    elif attachment_option == "Clipboard":
        pass


@pytest.fixture
@when("I press save on the image editor")
def save_image_editor(selenium):
    by_xpath(selenium, CP_IMAGE_EDITOR_SAVE).click()


@pytest.fixture
@then(parsers.parse("I should see attached file to '{checkpoint}' checkpoint on inspection page"))
def see_signature_attached(selenium, checkpoint):
    wait_clickable(selenium, (INSP_CHECKPOINT(selenium, checkpoint) + CP_ATTACHMENT))


@pytest.fixture
@when(parsers.parse("I set '{key_metric}' key metric to '{metric_number}"))
def set_key_metric(selenium, key_metric, metric_number):
    checkpoint_xpath = f"({INSP_CHECKPOINT(selenium, 'Punch Item/Deficiency:')}"
    key_metrics = by_xpath_multiple(selenium, CP_KEY_METRICS)
    key_metrics_list = [x.text for x in key_metrics]
    index = key_metrics_list.index(key_metric) + 1
    key_metric_xpath = f"/..{CP_KEY_METRICS})[{index}]{CP_KEY_METRIC_INPUT}"
    by_xpath(selenium, checkpoint_xpath + key_metric_xpath).send_keys(Keys.CONTROL, 'a')
    by_xpath(selenium, checkpoint_xpath + key_metric_xpath).send_keys(metric_number)
    try:
        by_xpath(selenium, INSP_SAVE_BUTTON).click()
    except:
        pass


@pytest.fixture
@then(parsers.parse("I see '{key_metric}' key metric is set to '{metric_number}'"))
def key_metric_set_to(selenium, key_metric, metric_number):
    checkpoint_xpath = f"({INSP_CHECKPOINT(selenium, 'Punch Item/Deficiency:')}"
    key_metrics = by_xpath_multiple(selenium, CP_KEY_METRICS)
    key_metrics_list = [x.text for x in key_metrics]
    index = key_metrics_list.index(key_metric) + 1
    key_metric_xpath = f"/..{CP_KEY_METRICS})[{index}]{CP_KEY_METRIC_INPUT}"
    assert by_xpath(selenium, checkpoint_xpath + key_metric_xpath).get_attribute("value") == metric_number


@pytest.fixture
@when(parsers.parse("I clear notes field for '{checkpoint}' checkpoint"))
def clear_notes_field(selenium, checkpoint):
    by_xpath(selenium, INSP_CHECKPOINT(selenium, checkpoint) + CP_NOTE).click()
    by_xpath(selenium, CP_ANY_NOTE_EDIT).clear()
    try:
        try_by_xpath(selenium, INSP_SAVE_BUTTON).click()
    except:
        by_xpath(selenium, INSP_SYNCED_TEXT).click()


# Dynamic Checkpoint Steps
@pytest.fixture
@when(parsers.parse("I press dynamic button title '{dynamic_name}'"))
def click_dynamic_button(selenium, dynamic_name, pass_data_to_step):
    try:
        get_dynamic_buttons1 = by_xpath_multiple(selenium, CP_DYNAMIC_ADD_ROW_NAMES)
        dynamic_button_list1 = [x.text for x in get_dynamic_buttons1]
        index1 = dynamic_button_list1.index(dynamic_name)
        scroll_to_bottom(selenium)
        scroll_into_view(selenium, get_dynamic_buttons1[index1])
        click_button = by_xpath(selenium, f"({CP_DYNAMIC_ADD_ROW_BUTTON})[{index1 + 1}]").click()
        pass_data_to_step['dynamic_index'] = index1 + 1
    except:
        get_dynamic_buttons2 = by_xpath_multiple(selenium, CP_GROUPED_DYNAMIC_ITEM)
        dynamic_button_list2 = [x.text for x in get_dynamic_buttons2]
        index2 = dynamic_button_list2.index(dynamic_name)
        scroll_to_bottom(selenium)
        scroll_into_view(selenium, get_dynamic_buttons2[index2])
        click_button = by_xpath(selenium, f"({CP_GROUPED_DYNAMIC_ITEM})[{index2 + 1}]").click()
        pass_data_to_step['dynamic_index'] = index2 + 1
    time.sleep(1)
    try:
        matrix_table_xpath = f"(({CP_DYNAMIC_ADD_ROW_BUTTON})[{+1}])/parent::div/parent::div/preceding-sibling::horizontal-matrix[1]"
        wait_clickable_short(selenium, matrix_table_xpath)
        pass_data_to_step['matrix table xpath'] = matrix_table_xpath
    except:
        pass


@pytest.fixture
@when("I clear text from all cells")
def clear_cells(selenium, pass_data_to_step):
    matrix_table_xpath = pass_data_to_step.get('matrix table xpath')
    matrix_cells = by_xpath_multiple(selenium, matrix_table_xpath + CP_DYNAMIC_TABLE_CELLS)
    for cell in matrix_cells:
        if cell.text != "":
            cell.click()
            time.sleep(1)
            by_xpath(selenium, CP_DYNAMIC_TABLE_CELL_TEXTAREA).send_keys(Keys.CONTROL, 'A', Keys.BACKSPACE)
            by_xpath(selenium, INSP_SAVE_BUTTON).click()


@pytest.fixture
@when(parsers.parse("I enter '{data}' to cell '{cell}', row {row_number}"))
def enter_data_to_cell_row(selenium, pass_data_to_step, data, cell, row_number):
    dynamic_index = pass_data_to_step.get('dynamic_index')
    wait_clickable(selenium, CP_DYNAMIC_ADD_ROW_NAMES)
    get_cell_headers = by_xpath_multiple(selenium, f"({CP_DYNAMIC_ADD_ROW_BUTTON})[{dynamic_index}]" + CP_DYNAMIC_TABLE_CONNECTED_TO_BUTTON + CP_DYNAMIC_ROW_HEADERS)
    cell_headers = [x.text for x in get_cell_headers]
    cell_index = cell_headers.index(cell) + 1
    cell_element = by_xpath(selenium, f"({CP_DYNAMIC_ADD_ROW_BUTTON})[{dynamic_index}]/parent::div/parent::div/preceding-sibling::horizontal-matrix[1]"
                                      f"//tr[{row_number}]/td[{cell_index}]")
    scroll_into_view(selenium, cell_element)
    click_cell = cell_element.click()
    time.sleep(1)  # dynamic wait would be more complicated
    enter_text = by_xpath(selenium, "//textarea").send_keys(data)
    click_save = by_xpath(selenium, INSP_SAVE_BUTTON).click()


@pytest.fixture
@when("I click copy button on inspection page")
def click_copy_insp(selenium):
    copy_button = by_xpath(selenium, INSP_COPY_BUTTON).click()
    confirm_pop_up = by_xpath(selenium, POPUP_OK_BUTTON).click()
    wait_clickable_long(selenium, INSP_HEADER)


@pytest.fixture
@then("I am brought to a new copied inspection")
def new_copied_inspection(selenium, pass_data_to_step):
    try:
        by_xpath(selenium, INSP_SAVE_BUTTON).click()
    except:
        pass
    wait_clickable(selenium, INSP_SYNCED_TEXT)
    wait_clickable(selenium, INSP_ID)
    original_id = pass_data_to_step.get('insp_ID')
    assert "copy" in by_xpath(selenium, TOAST_MESSAGE).text
    assert by_xpath(selenium, INSP_ID).text != original_id