from pytest_bdd import scenario, when, then
from functions import *


@pytest.mark.xray('DEV-8410')
@scenario("../../features/inspecting/reason_corrective_codes.feature", "Reason and Corrective Codes")
def test_reason_corrective_codes():
    """Verify that Reason and Corrective codes are working as expected"""


@when('I click the reason code button on a checkpoint')
def click_reason_code(selenium):
    click_button = by_xpath(selenium, CP_REASON_CODE_BUTTON).click()
    wait_clickable(selenium, POPUP)


@when('I select a reason code from the dialog')
def select_reason_code_option(selenium):
    search_item = by_xpath(selenium, POPUP_SEARCH).send_keys('Debris')
    time.sleep(1)
    click_result = by_xpath(selenium, POPUP_SELECTION).click()


@then('I should see the reason code populate for the checkpoint')
def reason_code_populates(selenium):
    text_from_reason_code = by_xpath(selenium, CP_REASON_CODE_POPULATED).text
    assert "Debris" in text_from_reason_code


@when('I click the corrective action code button on a checkpoint')
def click_corrective_action_code(selenium):
    click_button = by_xpath(selenium, CP_CORRECTIVE_ACTION_CODE_BUTTON).click()
    wait_clickable(selenium, POPUP)


@when('I select a corrective action code from the dialog')
def select_corrective_action_code_option(selenium):
    search_item = by_xpath(selenium, POPUP_SEARCH).send_keys('Cleanup')
    time.sleep(1)
    click_result = by_xpath(selenium, POPUP_SELECTION).click()


@then('I should see the corrective action code populate for the checkpoint')
def corrective_action_code_populates(selenium):
    text_from_corrective_code = by_xpath(selenium, CP_CORRECTIVE_ACTION_CODE_POPULATED).text
    assert "Cleanup" in text_from_corrective_code
