from pytest_bdd import scenario, when, then

from functions import *
from locators.listing_grid_locators import *


@pytest.mark.xray('DEV-6408')
@scenario("../../features/inspecting/refresh_inspection_from_server.feature", "Refresh Inspection From Server")
def test_refresh_inspection_from_server():
    """Verify that Refreshing Inspection From Server works as expected"""


@when("I open the inspection from recent inspections listing grid")
def open_insp(selenium, pass_data_to_step):
    insp_ID = pass_data_to_step.get('insp_ID')
    inspection_links = by_xpath_multiple(selenium, GRID_ITEM_NAME)
    inspection_list = [x.text for x in inspection_links]
    index = inspection_list.index(insp_ID) + 1
    click_recent_insp_link = by_xpath(selenium, f"({GRID_ITEM_NAME})[{index}]").click()
    wait_clickable(selenium, INSP_HEADER)
    try_dictionary_loading(selenium)


@then("refresh from server icon is orange to show changes")
def refresh_icon_is_orange(selenium):
    wait_clickable(selenium, INSP_REFRESH_FROM_SERVER_FOOTER)


@when("I refresh inspection from server using the button on footer")
def refresh_insp_from_server_footer_button(selenium):
    by_xpath(selenium, INSP_REFRESH_FROM_SERVER_FOOTER).click()
    by_xpath(selenium, POPUP_OK_BUTTON).click()
    wait_clickable(selenium, PROCESSING_MASK)
    try_dictionary_loading(selenium)


@then("inspection changes are shown")
def inspection_changes_shown(selenium):
    assert by_xpath(selenium, INSP_HEADER_NOTE).text == "inspection change"
