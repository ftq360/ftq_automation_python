from datetime import datetime

from pytest_bdd import scenario, when, given, then, parsers
from selenium.webdriver.common.keys import Keys

from functions import *
from locators.selection_process_locators import *
from locators.setup_locators import *


@pytest.mark.xray('DEV-5359')
@scenario("../../features/inspecting/if_then.feature", "If-Then")
def test_if_then_visibility_formulas():
    """Verify that if-then visibility formulas are working as expected"""


@when("I create a new checklist(checklist1)")
def create_new_checklist(selenium, pass_data_to_step):
    click_add_new = by_xpath(selenium, SETUP_ADD_NEW).click()
    enter_checklist_name_in_field = by_xpath(selenium, CHECKLIST_ADD_NEW_ENTER_NAME).send_keys(
        "checklist1" + " " + datetime.datetime.now().strftime('%m/%d/%Y %H:%M'))
    click_to_create_checklist = by_xpath(selenium, CHECKLIST_CREATE_BUTTON).click()
    get_checklist_code = by_xpath(selenium, CHECKLIST_DETAILS_CODE).get_attribute("value")
    pass_data_to_step['checklist_code'] = get_checklist_code


@when("I add 4 quality checkpoints (checkpoint1, checkpoint2, checkpoint3, checkpoint4)")
def add_quality_checkpoints(selenium):
    click_go_to_checkpoints = by_xpath(selenium, CHECKLIST_GO_TO_CHECKPOINTS).click()
    loopnumber = 0
    for x in range(7):
        loopnumber += 1
        enter_checkpoint1_name_in_field = by_xpath(selenium, CHECKPOINT_ADD_NEW_ENTER_NAME).send_keys(f"checkpoint{loopnumber}" + Keys.ENTER)
    click_to_create_checkpoint = by_xpath(selenium, CHECKPOINT_CREATE_BUTTON).click()


@when("I add 1 safety checkpoint(checkpoint5)")
def add_safety_checkpoint(selenium):
    deselect_checkpoints = by_xpath(selenium, SETUP_SELECT_ALL).click()
    time.sleep(1)
    select_checkpoint5 = by_xpath(selenium, CHECKPOINT_ROW(selenium, "checkpoint5")).click()
    click_type_dropdown = by_xpath(selenium, CHECKPOINT_DETAILS_TYPE).click()
    change_checkpoint_type = by_xpath(selenium, "//*[@data-value='6']").click()
    save_changes = by_xpath(selenium, CHECKPOINT_DETAILS_SAVE).click()


@when("I add 1 data checkpoint(checkpoint6)")
def add_data_checkpoint(selenium):
    time.sleep(1)
    select_checkpoint6 = by_xpath(selenium, CHECKPOINT_ROW(selenium, "checkpoint6")).click()
    time.sleep(1)
    click_type_dropdown = by_xpath(selenium, CHECKPOINT_DETAILS_TYPE).click()
    change_checkpoint_type = by_xpath(selenium, "//*[@data-value='3']").click()
    save_changes = by_xpath(selenium, CHECKPOINT_DETAILS_SAVE).click()


@when("I add 1 image checkpoint(checkpoint7)")
def add_image_checkpoint(selenium):
    select_checkpoint7 = by_xpath(selenium, CHECKPOINT_ROW(selenium, "checkpoint7")).click()
    time.sleep(1)
    click_type_dropdown = by_xpath(selenium, CHECKPOINT_DETAILS_TYPE).click()
    change_checkpoint_type = by_xpath(selenium, "//*[@data-value='4']").click()
    save_changes = by_xpath(selenium, CHECKPOINT_DETAILS_SAVE).click()


@when("I set checkpoint2 visibility status to rely on checkpoint1 status is FTQ")
def set_checkpoint2_visibility(selenium):
    expand_behavior_details = by_xpath(selenium, CHECKPOINT_DETAILS_BEHAVIOR).click()
    click_create_visibility = by_xpath(selenium, CHECKPOINT_DETAILS_BEHAVIOR_VISIBILITY_CREATE).click()


@when("I set checkpoint3 visibility status to rely on checkpoint2 note is 'no'")
def set_checkpoint3_visibility(selenium):
    pass


@when("I add 3 auto-options to checkpoint3(option1, option2, option3)")
def add_auto_options(selenium):
    pass


@when("I set checkpoint4 visibility status to rely on checkpoint3 auto-option is option1")
def set_checkpoint4_visibility(selenium):
    pass


@when("I set checkpoint6 visibility status to rely on checkpoint5 status is not FAILED")
def set_checkpoint6_visibility(selenium):
    pass


@when("I set checkpoint7 visibility status to rely on checkpoint6 note is not 'no'")
def set_checkpoint7_visibility(selenium):
    pass


@given("I create a new inspection with visibility checklist(checklist1)")
def new_inspection_with_checklist1(selenium, pass_data_to_step):
    dict_loading_modal_appears = wait_present(selenium, SP_DICT_LOADING_MODAL_OPEN)
    dictionaries_load = wait_present(selenium, SP_DICT_LOADING_MODAL_SUCCESS)
    dictionary_loading_complete = wait_present(selenium, SP_DICT_LOADING_MODAL_CLOSED)
    try:
        select_division = by_xpath(selenium, SPECIFIC_SELECTION(selenium, "(Undefined)")).click()
    except:
        pass
    select_project = select_project1 = by_xpath(selenium,
                                                SPECIFIC_SELECTION(selenium, "Project 1 (1)")).click()
    checklist = pass_data_to_step.get('checklist_code')
    select_specific_checklist = by_xpath(selenium, (SPECIFIC_SELECTION(selenium, checklist))).click()
    select_rp = by_xpath(selenium, SPECIFIC_SELECTION(selenium, "FTQ Automation Group (D1)")).click()


@when(parsers.parse("I add '{text}' to the notes field of {checkpoint}"))
def add_notes_to_checkpoint2(selenium, text, checkpoint):
    pass


@when(parsers.parse("I select auto-option {option} for checkpoint3"))
def set_checkpoint3_auto_option(selenium, option):
    pass


@then(parsers.parse("{checkpoint} should be {visibility}"))
def checkpoint7_not_visible(selenium, checkpoint, visibility):
    pass


@then("the PDF should show only visible checkpoints")
def pdf_shows_correct_checkpoints(selenium):
    pass
