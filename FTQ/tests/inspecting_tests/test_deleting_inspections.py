from pytest_bdd import scenario, when, then
from selenium.webdriver.common.keys import Keys
from functions import *
from locators.inspection_page_locators import *
from locators.listing_grid_locators import *


@pytest.mark.skip('Unstable')
@pytest.mark.xray('DEV-5396')
@scenario("../../features/inspecting/delete_inspections.feature", "Deleting Inspections")
def test_delete_inspections():
    """Verify that deleting inspections is working as expected"""


@when("I open an inspection")
def open_inspection(selenium, pass_data_to_step):
    search_for_proj1 = by_xpath(selenium, GRID_SEARCH).send_keys("Project 1 (1)")
    enter_search_for_proj1 = by_xpath(selenium, GRID_SEARCH).send_keys(Keys.ENTER)
    time.sleep(1)
    click_insp_id = by_xpath(selenium, GRID_ITEM_NAME).click()
    wait_clickable(selenium, INSP_HEADER)
    try:
        assert by_xpath(selenium, INSP_HEADER_PROJECT).text == "Project 1 (1)"
    except AssertionError:
        selenium.back()
        wait_page_title_exact(selenium, "FTQ360 - List Recent Inspections")
        clear_search = by_xpath(selenium, GRID_SEARCH).send_keys(Keys.CONTROL + "a", Keys.DELETE)
        search_for_proj1 = by_xpath(selenium, GRID_SEARCH).send_keys("Project 1 (1)")
        enter_search_for_proj1 = by_xpath(selenium, GRID_SEARCH).send_keys(Keys.ENTER)
    finally:
        save_insp_id = by_xpath(selenium, INSP_ID).text
        pass_data_to_step['inspection1'] = save_insp_id


@when("I delete the inspection on the inspection page")
def delete_inspection_from_insp_page(selenium):
    click_delete_button = by_xpath(selenium, INSP_DELETE_BUTTON).click()
    wait_for_modal = wait_clickable(selenium, INSP_DELETE_CONFIRMATION_WINDOW)
    confirm_deletion = by_xpath(selenium, INSP_DELETE_ACCEPT_CONFIRMATION).click()


@then("I am redirected to Recent Inspections Listing page")
def redirected_to_listing_grid(selenium):
    wait_page_title_exact(selenium, "FTQ360 - List Recent Inspections")
    wait_clickable(selenium, GRID_ITEM_NAME)


@then("the deleted inspection is not listed")
def deleted_inspection_not_listed(selenium, pass_data_to_step):
    deleted_inspection = pass_data_to_step.get('inspection1')
    search_deleted_id = by_xpath_multiple(selenium, GRID_ITEM_NAME)
    list_of_ids = [x.text for x in search_deleted_id]
    assert deleted_inspection not in list_of_ids
