from pytest_bdd import scenario, when, then
from functions import *
from locators.inspection_page_locators import *
from locators.listing_grid_locators import *


@pytest.mark.xray('DEV-5117')
@scenario("../../features/inspecting/inspection_creation_date_change.feature", "Inspection Creation Date Change")
def test_inspection_creation_date_change():
    """Verify that users can change inspection creation dates as expected"""


@when("I click on the inspection creation date")
def click_on_inspection_creation_date(selenium):
    by_xpath(selenium, INSP_HEADER_CREATION_DATE).click()


@then("I can change the inspection creation date")
def change_inspection_creation_date(selenium, pass_data_to_step):
    get_current_date = by_xpath(selenium, INSP_HEADER_CREATION_DATE).text
    by_xpath(selenium, INSP_HEADER_CREATION_DATE_CALENDAR).click()
    by_xpath(selenium, INSP_HEADER_CREATION_DATE_CALENDAR_NEXT_MONTH).click()
    by_xpath(selenium, INSP_HEADER_CREATION_DATE_CALENDAR_LAST_DAY).click()
    get_new_date = by_xpath(selenium, INSP_HEADER_CREATION_DATE).text
    pass_data_to_step['insp_date'] = get_new_date


@then("the changed date is reflected in Recent Inspection Listing Grid")
def changed_inspection_date_is_reflected_in_grid(selenium, pass_data_to_step):
    insp_date = pass_data_to_step.get('insp_date')
    insp_ID = pass_data_to_step.get('insp_ID')
    get_insp_ids = by_xpath_multiple(selenium, GRID_ITEM_NAME)
    insp_id_list = [x.text for x in get_insp_ids]
    index = insp_id_list.index(insp_ID) + 1
    assert by_xpath(selenium, f"({LISTING_GRID_CREATION_DATE})[{index}]").text == insp_date


@then("I cannot change the inspection creation date")
def cannot_change_inspection_date(selenium):
    try:
        wait_clickable_short(selenium, INSP_HEADER_CREATION_DATE_CALENDAR)
        pytest.fail("user can illegally change inspection creation date")
    except:
        pass
