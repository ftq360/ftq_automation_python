from pytest_bdd import scenario, when, then, parsers
from functions import *
from locators.inspection_page_locators import *


@pytest.mark.xray('DEV-5365')
@scenario("../../features/inspecting/html_data_inserts.feature", "HTML Data Inserts")
def BROKEN_test_html_data_inserts():
    """test that the html data inserts on checkpoint notes work as expected"""


@when("I click to enter notes on any checkpoint")
def click_to_enter_notes(selenium):
    click_notes_field_first_checkpoint = by_xpath(selenium, f"({CP_NOTE})[1]").click()
    wait_clickable(selenium, CP_ANY_NOTE_EDIT)
    click_text_box = by_xpath(selenium, CP_ANY_NOTE_EDIT).click()


@when(parsers.parse("I click the {html_button}"))
def click_html_button(selenium, html_button):
    if html_button == "GPS":
        selenium.switch_to_frame()
        # selenium.switch_to_default_content()
        click_gps_button = by_xpath(selenium, f"{CP_GPS_INSERT}/i").click()
        click_gps_button = by_xpath(selenium, CP_GPS_INSERT).click()
        time.sleep(2)
    elif html_button == "Weather":
        click_weather_button = by_xpath(selenium, CP_WEATHER_INSERT).click()
        click_weather_button = by_xpath(selenium, CP_WEATHER_INSERT).click()
        time.sleep(2)
    elif html_button == "Date/Time":
        click_date_time_button = by_xpath(selenium, CP_DATE_TIME_INSERT).click()
        click_date_time_button = by_xpath(selenium, CP_DATE_TIME_INSERT).click()
        time.sleep(2)
        click_save_btn = by_xpath(selenium, INSP_SAVE_BUTTON).click()


@then(parsers.parse("{html_data} is automatically inserted into the notes field"))
def html_data_inserted(selenium, html_data):
    if html_data == "GPS location":
        get_checkpoint_text = by_xpath(selenium, f"({CP_NOTE})[1]").get_attribute("outerText")
        print(get_checkpoint_text)
    elif html_data == "NOAA weather report":
        get_checkpoint_text = by_xpath(selenium, f"({CP_NOTE})[1]").get_attribute("outerText")
        print(get_checkpoint_text)
    elif html_data == "the current date and time":
        get_checkpoint_text = by_xpath(selenium, f"({CP_NOTE})[1]").get_attribute("outerText")
        print(get_checkpoint_text)
