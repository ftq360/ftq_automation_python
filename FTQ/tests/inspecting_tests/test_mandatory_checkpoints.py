from locators.listing_grid_locators import *
from pytest_bdd import scenario, when, then, parsers
from functions import *
from locators.core_locators import *


@pytest.mark.xray('DEV-5361')
@scenario("../../features/inspecting/mandatory_checkpoints.feature", "Mandatory Checkpoints")
def test_mandatory_checkpoints():
    """Verify that mandatory checkpoints are working as expected"""


@when("I navigate away from the inspection")
def navigate_away_from_inspection_fail(selenium):
    click_logo = by_xpath(selenium, GL_LOGO).click()
    try:
        wait_clickable_short(selenium, LISTING_GRID_ROWS)
        pytest.fail("Navigated away from inspection successfully without fulfilling mandatory items")
    except:
        pass


@then(parsers.parse("I see popup with text '{message}'"))
def mandatory_pop_up(selenium, message):
    wait_clickable(selenium, POPUP)
    get_text = by_xpath(selenium, POPUP + POPUP_MESSAGE).text
    assert get_text == message


@when("I click 'cancel' on the popup")
def popup_cancel(selenium):
    click_cancel = by_xpath(selenium, POPUP_CANCEL).click()
    wait_invisible(selenium, POPUP)


@then("I am able to navigate away from the inspection")
def navigate_away_from_inspection_success(selenium):
    click_logo = by_xpath(selenium, GL_LOGO).click()
    wait_clickable(selenium, LISTING_GRID_ROWS)
