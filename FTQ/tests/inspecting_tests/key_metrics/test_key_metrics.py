import pytest
from pytest_bdd import scenario


@pytest.mark.xray('DEV-5071')
@scenario("../../../features/inspecting/key_metrics/key_metrics.feature", "Key Metrics")
def test_key_metrics():
    """Verify that key metrics are working as expected"""

# all steps are common steps
