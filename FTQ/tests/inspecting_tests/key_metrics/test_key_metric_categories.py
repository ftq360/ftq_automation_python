import pytest
from pytest_bdd import scenario


@pytest.mark.xray('DEV-5118')
@scenario("../../../features/inspecting/key_metrics/key_metric_categories.feature", "Key Metric Categories")
def test_key_metric_categories():
    """Verify that key metric categories are working as expected"""

# all steps are common steps
