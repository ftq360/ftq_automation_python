from pytest_bdd import scenario, when, then, parsers

from functions import *


@pytest.mark.xray('DEV-6770')
@scenario("../../features/inspecting/approval_checkpoint.feature", "Approval Checkpoint")
def test_approval_checkpoint():
    """test that approval checkpoints work as expected"""


@when("I click on 'NA' checkbox on the Approval Checkpoint")
def click_approval_status(selenium):
    scroll_to_checkpoint = scroll_into_view(selenium, by_xpath(selenium, INSP_CHECKPOINT(selenium, "Approval checkpoint")))
    checkpoint_status_select = by_xpath(selenium, (INSP_CHECKPOINT(selenium, "Approval checkpoint") + CHECKPOINT_STATUS()['NA'])).click()


@then("I am given a warning that I need to assign an approver")
def warn_need_to_assign_approver(selenium):
    wait_clickable(selenium, POPUP)
    assert by_xpath(selenium, POPUP + POPUP_MESSAGE).get_attribute('outerText') == \
           "You are not the assigned approver. First, you must be assigned as the approver, then proceed with your approval."
    by_xpath(selenium, POP_UP_CONFIRM).click()


@when(parsers.parse("I assign '{user}' user as the checkpoint approver"))
def assign_approver(selenium, user):
    click_approver_dropdown = by_xpath(selenium, INSP_CHECKPOINT(selenium, "Approval checkpoint") + CP_APPROVER_DROPDOWN).click()
    type_user_name = by_xpath(selenium, CP_RP_SEARCH).send_keys(user)
    click_user = by_xpath(selenium, CP_DROPDOWN_FIRST_RP_IN_LIST).click()
    wait_present(selenium, INSP_SYNCED_TEXT)
    if user == 'Automation, Python':
        assert selenium.execute_script(
            "return arguments[0].childNodes[1].children[1].children[0].value;", by_xpath(selenium, INSP_CHECKPOINT(selenium, "Approval checkpoint") + CP_APPROVER_DROPDOWN)
            ) == user
    else:
        pass


@then("I am given a warning that this will reset the checkpoint status")
def warn_reset_checkpoint_status(selenium):
    assert by_xpath(selenium, POPUP + POPUP_MESSAGE).get_attribute('outerText') == \
           "The assigned approver has already set an approval status. Reassigning the approver will reset the approval " \
           "status. Continue anyway?"


@then("checkpoint status is cleared")
def status_cleared(selenium):
    assert selenium.execute_script("return arguments[0].childNodes[1].checked;", by_xpath(selenium, INSP_CHECKPOINT(selenium, "Approval checkpoint"))) is None
    assert selenium.execute_script(
        "return arguments[0].childNodes[1].children[1].children[0].value;", by_xpath(selenium, INSP_CHECKPOINT(selenium, "Approval checkpoint") + CP_APPROVER_DROPDOWN)
    ) == 'Inspector, Restricted'
