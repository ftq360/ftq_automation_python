from pytest_bdd import scenario, when, then
from functions import *


@pytest.mark.xray('DEV-5372')
@scenario("../features/delete_all_data.feature", "Delete All Data")
def test_delete_all_data():
    """test user can successfully delete all data in indexedDB"""


@when("I look in the indexedDB there is FTQ360 data")
def data_in_indexeddb(selenium):
    current_idb = (selenium.execute_script("return window.indexedDB.databases();"))
    name_of_idb = (current_idb[0]['name'])
    assert name_of_idb == 'FTQ360DB'


@when("I click on 'Delete all Data' button on the FTQ screen")
def click_delete_all_data(selenium):
    by_xpath(selenium, DELETE_DATA_BUTTON).click()
    by_xpath(selenium, POPUP_GREEN_OK).click()
    time.sleep(1)


@then("I look in the indexedDB and there is no FTQ360 data")
def no_data_in_indexeddb(selenium):
    new_idb = (selenium.execute_script("return window.indexedDB.databases();"))
    assert len(new_idb) == 0
