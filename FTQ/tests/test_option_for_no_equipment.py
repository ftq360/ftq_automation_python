from pytest_bdd import scenario, when, then

from functions import *


@pytest.mark.xray('DEV-6410')
@scenario("../features/option_for_no_equipment.feature", "Option for no equipment")
def test_option_for_no_equipment():
    """test that there is an option for no equipment"""


@when("I select 'ITP Project' in the selection process")
def select_proj_2(selenium):
    try_dictionary_loading(selenium)
    try:
        select_division = by_xpath(selenium, SPECIFIC_SELECTION(selenium, "(Undefined)")).click()
    except:
        pass
    select_project = by_xpath(selenium, SPECIFIC_SELECTION(selenium, 'ITP Project (4)')).click()
    select_phase = by_xpath(selenium, SPECIFIC_SELECTION(selenium, 'ITP Phase 1 (4-3)')).click()
    try:
        wait_clickable_short(selenium, SP_PLAN_MODE_CHECKED)
    except:
        by_xpath(selenium, SP_PLAN_MODE_CHECKBOX).click()
        wait_clickable(selenium, SP_PLAN_MODE_CHECKED)


@then("I should see a 'No equipment' option in the selection process")
def no_equip_option_shown(selenium):
    wait_clickable(selenium, SPECIFIC_SELECTION(selenium, "No Equipment"))
    # by_xpath(selenium, SPECIFIC_SELECTION(selenium, "No Equipment"))
