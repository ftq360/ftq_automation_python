from pytest_bdd import when, given, parsers

from functions import *
from locators.old_setup_page_locators import *
from locators.report_locators import *


# Report Steps
@pytest.fixture
@when("I wait 6 minutes for reporting tables to update")
def wait_for_reporting_tables(selenium):
    time.sleep(360)


@pytest.fixture
@given(parsers.parse("I select query '{query}'"))
def select_query(selenium, query, pass_data_to_step):
    pass_data_to_step['query'] = query
    time.sleep(5)
    wait_clickable(selenium, REPORT_NAMES)
    report_names = by_xpath_multiple(selenium, REPORT_NAMES)
    report_names_list = [x.text for x in report_names]
    report_index = report_names_list.index(query) + 1
    click_report_radio_button = by_xpath(selenium, f"({REPORT_SELECT})[{report_index}]").click()
    wait_clickable(selenium, REPORT_RUN_REPORT_BUTTON)
    # for x in report_names_list:
    #     if query in x:
    #         print(x)
    #         report_index = report_names_list.index(x) + 1
    #         click_report_radio_button = by_xpath(selenium, f"({REPORT_SELECT})[{report_index}]").click()
    #         wait_clickable(selenium, REPORT_RUN_REPORT_BUTTON)


@pytest.fixture
@given(parsers.parse("I select date range '{date_range}'"))
def report_date_range(selenium, date_range, pass_data_to_step):
    report = pass_data_to_step.get('report')
    if '405' in report:
        pass
    else:
        by_xpath(selenium, REPORT_DATE_RANGE_TEXT).click()
        get_date_range_options = by_xpath_multiple(selenium, REPORT_DATE_RANGE_SELECT)
        date_range_options = [x.text for x in get_date_range_options]
        index = date_range_options.index(date_range) + 1
        click_date_range_option = by_xpath(selenium, f"({REPORT_DATE_RANGE_SELECT})[{index}]").click()


@pytest.fixture
@when("I download the query")
def download_report_query(selenium):
    click_first_download_option = by_xpath(selenium, REPORT_DOWNLOAD).click()
    time.sleep(2)
    download_wait()



