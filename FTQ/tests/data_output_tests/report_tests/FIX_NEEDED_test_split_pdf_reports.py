import re
import zipfile

from pytest_bdd import scenario, when, then, given, parsers
from selenium.webdriver.support.ui import Select

from functions import *
from locators.old_setup_page_locators import *
from locators.report_locators import *


@pytest.mark.skip('Unstable')
@pytest.mark.xray('DEV-5370')
@scenario("../../../features/data_output/reports/split_pdf_reports.feature", "Split PDF Reports")
def test_split_pdf_reports():
    """Verify that Split PDF Reports run as expected"""


@given(parsers.parse("I change filename for split inspection to {split_report_setting}"))
def change_split_report_setting(selenium, split_report_setting):
    report_settings = by_xpath_multiple(selenium, RP_SETTING_NAMES)
    settings = [x.get_attribute('outerText') for x in report_settings]
    index = []
    for x in settings:
        if 'split' in x:
            index.append(settings.index(x) + 1)
    Select(by_xpath(selenium, f"({RP_SETTING_NAMES}){index}{RP_SETTING_SELECT}")).select_by_visible_text(split_report_setting)


@given("I save changes on Email Customizations page")
def save_changes_on_email_customizations_page(selenium):
    by_xpath(selenium, SETUP_ALL_SAVE_BUTTONS).click()
    wait_present(selenium, SETUP_ALL_SAVED_CONFIRMATION_MASK)


@when("I run report 401ax with split report selected")
def run_split_report(selenium):
    time.sleep(5)
    wait_clickable(selenium, REPORT_NAMES)
    report_names = by_xpath_multiple(selenium, REPORT_NAMES)
    report_names_list = [x.text for x in report_names]
    report_index = report_names_list.index('FTQ-401ax - Inspection Reports') + 1
    click_report_radio_button = by_xpath(selenium, f"({REPORT_SELECT})[{report_index}]").click()
    wait_clickable(selenium, REPORT_RUN_REPORT_BUTTON)
    click_split_report_option = by_xpath(selenium, SPLIT_REPORT).click()
    wait_clickable(selenium, SPLIT_REPORT_ENABLED)
    click_run_report_button = by_xpath(selenium, REPORT_RUN_REPORT_BUTTON).click()


@when("I open the zipped report")
def open_zipped_report(selenium, pass_data_to_step):
    download_reports = by_xpath(selenium, REPORT_DOWNLOAD).click()
    time.sleep(2)
    download_wait()
    time.sleep(1)
    zips = return_downloaded_zips()
    correct_zip = zips[-1]
    zip = zipfile.ZipFile(correct_zip)
    file_list = zip.namelist()
    pass_data_to_step['zip_names'] = file_list


@then("each inspection is in a separate PDF file")
def report_pdfs_split(selenium, pass_data_to_step):
    zip_names = pass_data_to_step.get('zip_names')
    assert len(zip_names) > 1


@then(parsers.parse("the title of each file is in {split_report_setting} format"))
def file_in_split_report_setting(selenium, pass_data_to_step, split_report_setting):
    zip_names = pass_data_to_step.get('zip_names')
    if "only" in split_report_setting:
        assert re.match("(\d+).pdf", zip_names[0])
    elif "Standard" in split_report_setting:
        projects_with_phases = bool(re.match(".+-.+-\d\d_\d\d_\d\d\d\d-\d+.pdf", zip_names[0]))
        projects_without_phases = bool(re.match(".+-\d\d_\d\d_\d\d\d\d-\d+.pdf", zip_names[0]))
        bool_list = [projects_with_phases, projects_without_phases]
        assert any(bool_list)
