import xlrd
from pytest_bdd import scenario, parsers, given, then
from functions import *
from locators.old_setup_page_locators import *


@pytest.mark.skip_production_test
@pytest.mark.xray('DEV-5263')
@scenario("../../../features/data_output/reports/custom_BHP_query.feature", "Custom BHP Query")
def BROKEN_test_custom_BHP_query():
    """Verify that Custom BHP Query returns data as expected"""


@given(parsers.parse("I import '{file}'"))
def import_BHP_data(selenium, file):
    file_input = by_xpath(selenium, IMPORT_FILEINPUT)
    if file == 'BHP All Checklists File':
        file_input.send_keys(r'C:\Workspace\AutomationAttachments\ct-timemile checklists-updated feb 2022\tasks (10_8_2021 9_15_52 AM).csv')
        time.sleep(1)
    elif file == 'BHP Checklist CT15 File':
        file_input.send_keys(r'C:\Workspace\AutomationAttachments\ct-timemile checklists-updated feb 2022\task_checkpoints (CT17) 6_2_2021 11_51_00 AM.csv')
        time.sleep(1)
    elif file == 'BHP Checklist CT16 File':
        file_input.send_keys(r'C:\Workspace\AutomationAttachments\ct-timemile checklists-updated feb 2022\task_checkpoints (CT16) 6_2_2021 11_50_09 AM.csv')
        time.sleep(1)
    elif file == 'BHP Checklist CT17 File':
        file_input.send_keys(r'C:\Workspace\AutomationAttachments\ct-timemile checklists-updated feb 2022\task_checkpoints (CT15) 6_2_2021 11_48_33 AM.csv')
        time.sleep(1)
    click_submit = by_xpath(selenium, IMPORT_SUBMIT).click()


@then(parsers.parse("I create a BHP inspection via checklist '{checklist}'"))
def create_BHP_inspection(selenium, cred_data, checklist):
    go_to_create_insp = selenium.get(cred_data['BASE_URL'] + cred_data['CREATE_INSPECTION'])
    try_dictionary_loading(selenium)
    select_region = by_xpath(selenium, SPECIFIC_SELECTION(selenium, "Australia")).click()
    try:
        select_well = try_by_xpath(selenium, SPECIFIC_SELECTION(selenium, "Pyrenees Phase 4 FEED (AUS-PP4-FEED)")).click()
    except:
        pass
    select_phase = by_xpath(selenium, SPECIFIC_SELECTION(selenium, "Incoming (IN)")).click()
    select_checklist = by_xpath(selenium, SPECIFIC_SELECTION(selenium, checklist)).click()
    select_rp = by_xpath(selenium, SPECIFIC_SELECTION(selenium, "51 Oil Corporation (51OIL)")).click()
    check_inspection_created_successfully = wait_clickable(selenium, INSP_HEADER)


@then("data returned for query 1031 is correct")
def query_1031_data_correct(selenium):
    queries = return_downloaded_xls()
    correct_query = queries[-1]
    book = xlrd.open_workbook(correct_query)
    sheet = book.sheet_by_index(0)
    query_list = []
    for x in range(0, sheet.nrows):
        query_data = sheet.row_values(rowx=x, start_colx=1, end_colx=sheet.ncols)
        query_list.append(query_data)

    assert_cell_has_data_list = []
    for x in query_list[1][9:17]:
        assert_cell_has_data_list.append(x)
    row_2_index_data_list = [9, 10, 11, 13, 16]
    for x in row_2_index_data_list:
        assert_cell_has_data_list.append(query_list[2][x])
    row_3_index_data_list = [9, 11, 12, 15]
    for x in row_3_index_data_list:
        assert_cell_has_data_list.append(query_list[3][x])
    row_5_index_data_list = [10, 14]
    for x in row_5_index_data_list:
        assert_cell_has_data_list.append(query_list[5][x])
    for x in assert_cell_has_data_list:
        assert x == "automation added data 123"

    assert_cell_has_no_data_list = []
    for x in query_list[4][9:]:
        assert_cell_has_no_data_list.append(x)
    row_3_index_no_data_list = [10, 13, 14]
    row_5_index_no_data_list = [9, 11, 12, 13, 15]
    for x in row_3_index_no_data_list:
        assert_cell_has_no_data_list.append(query_list[3][x])
    for x in row_5_index_no_data_list:
        assert_cell_has_no_data_list.append(query_list[5][x])
    for x in assert_cell_has_no_data_list:
        assert x == ""
