import re

from pytest_bdd import scenario, then, given

from functions import *
from locators.report_locators import *


@pytest.mark.skip("listing grid used -- need to update test")
@pytest.mark.skip_production_test
@pytest.mark.xray('DEV-5395')
@scenario("../../../features/data_output/reports/inspection_email_subject_and_file_name_settings_BHP_custom.feature", "Inspection Email Subject and File Name Settings - BHP Custom")
def test_inspection_email_subject_and_file_name_settings_BHP_custom():
    """Verify that inspection email subject and file name settings feature work as expected for custom BHP format"""


@given("I change 'Filename template for Immediate Email Notifications (Inspection Checklist Screen)' setting to BHP Custom Format")
def change_filename_setting(selenium):
    email_settings = by_xpath_multiple(selenium, ACTIVATE_RP_EMAIL_SETTING_NAMES)
    settings = [x.text for x in email_settings]
    index = settings.index('Filename template for Immediate Email Notifications (Inspection Checklist Screen)') + 1
    click_filename_dropdown = by_xpath(selenium, f"//tr[{index}]//select").click()
    filename_options = by_xpath_multiple(selenium, f"//tr[{index}]//select/option")
    filename_options_list = [x.text for x in filename_options]
    filename_index = filename_options_list.index('BHP Custom Format') + 1
    click_filename_option = by_xpath(selenium, f"(//tr[{index}]//select/option)[{filename_index}]").click()


@given("I change 'Subject line template for Immediate Email Notifications (Inspection Checklist Screen)' setting to BHP Custom Format")
def change_subject_setting(selenium):
    email_settings = by_xpath_multiple(selenium, ACTIVATE_RP_EMAIL_SETTING_NAMES)
    settings = [x.text for x in email_settings]
    index = settings.index('Subject line template for Immediate Email Notifications (Inspection Checklist Screen)') + 1
    click_subject_dropdown = by_xpath(selenium, f"//tr[{index}]//select").click()
    subject_options = by_xpath_multiple(selenium, f"//tr[{index}]//select/option")
    subject_options_list = [x.text for x in subject_options]
    subject_index = subject_options_list.index('BHP Custom Format') + 1
    click_subject_option = by_xpath(selenium, f"(//tr[{index}]//select/option)[{subject_index}]").click()


@then("the email subject line is in BHP Custom Format and matches current data")
def email_subject_in_correct_format(cred_data, pass_data_to_step):
    insp_ID = pass_data_to_step.get('insp_ID')
    email_recipient = pass_data_to_step.get('email_recipient')
    get_email_data = get_email_by_recipient(cred_data, email_recipient)
    returned_subject = get_email_data['returned_subject']
    split_subject = returned_subject.splitlines()
    subject_one_line = ''.join(split_subject)
    pass_data_to_step['returned_attachment_filename'] = get_email_data['returned_attachment_filename']
    print(subject_one_line)
    todays_date = datetime.datetime.today().strftime("%y%m%d")
    assert re.match(r".+_.+_.+_.+_.+_[a-zA-Z]+,(\s|_)[a-zA-Z]+_"+re.escape(insp_ID)+r"_\d\d-\d\d-\d\d_[a-zA-Z]+_"+re.escape(
        todays_date), subject_one_line)


@then("the email filename is in BHP Custom Format and matches current data")
def email_filename_in_correct_format(pass_data_to_step):
    insp_ID = pass_data_to_step.get('insp_ID')
    returned_attachment_filename = pass_data_to_step.get('returned_attachment_filename')
    print(returned_attachment_filename)
    todays_date = datetime.datetime.today().strftime("%y%m%d")
    assert re.match(r".+_.+_.+_.+_.+_[a-zA-Z]+__[a-zA-Z]+_"+re.escape(insp_ID)+r"_\d\d-\d\d-\d\d_[a-zA-Z]+_"+re.escape(
        todays_date)+r"\.pdf", returned_attachment_filename)
