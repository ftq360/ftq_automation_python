import pdfplumber
import xlrd
from pytest_bdd import scenario, then, parsers

from functions import *
from locators.report_locators import *


@pytest.mark.skip('Unstable')
@pytest.mark.xray('DEV-6953')
@scenario("../../../features/data_output/reports/common_reports_return_correct_data.feature", "Common Reports Return Correct Data")
def test_common_reports_return_correct_data():
    """Verify that common reports are returning correct data"""


@then(parsers.parse("I should be able to download the {report}"))
def download_report(selenium):
    click_download_report = by_xpath(selenium, REPORT_DOWNLOAD).click()
    time.sleep(2)
    download_wait()
    time.sleep(1)


@then(parsers.parse("I should be able to see the correct data for the  {report}"))
def correct_report_data(selenium, report, pass_data_to_step):
    note = pass_data_to_step.get('note')
    insp_ID = pass_data_to_step.get('insp_ID')
    if "FTQ-" in report:
        pdfs = return_downloaded_pdfs()
        correct_pdf = pdfs[-1]
        pdf_obj = pdfplumber.open(correct_pdf)
        pages = pdf_obj.pages
        page_text = pages[0].extract_text()
        pdf_text_data = "".join([page_text.replace("\n", " ")])
        try:
            assert note and insp_ID in pdf_text_data
        except:
            pytest.fail(pdf_text_data)
    else:
        xls = return_downloaded_xls()
        correct_query = xls[-1]
        book = xlrd.open_workbook(correct_query)
        sheet = book.sheet_by_index(0)
        query_list = []
        for x in range(sheet.nrows):
            query_data = sheet.row_values(rowx=x, start_colx=1, end_colx=sheet.ncols)
            if note or f"{insp_ID}.0" in query_data:
                query_list.append(True)
        assert any(query_list)
