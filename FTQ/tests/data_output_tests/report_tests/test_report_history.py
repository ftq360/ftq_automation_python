from pytest_bdd import scenario, then, when, parsers

from functions import *
from locators.report_locators import *


@pytest.mark.xray('DEV-5402')
@scenario("../../../features/data_output/reports/report_history.feature", "Report History")
def test_report_history():
    """Verify that report history page features work as expected"""

#
# @given(parsers.parse("I select '{report}' report"))
# def select_report(selenium, report, pass_data_to_step):
#     wait_clickable(selenium, REPORT_NAMES)
#     report_names = by_xpath_multiple(selenium, REPORT_NAMES)
#     report_names_list = [x.text for x in report_names]
#     print(report_names_list)
#     for x in report_names_list:
#         if report in x:
#             report_index = report_names_list.index(x) + 1
#             click_report_radio_button = by_xpath(selenium, f"({REPORT_SELECT})[{report_index}]").click().click()
#     pass_data_to_step['report'] = report
#
# @then("I should see the report on Report History page")
# def report_shown_on_reports_history(selenium, pass_data_to_step):
#     report = pass_data_to_step.get('report')
#     wait_clickable(selenium, REPORT_HISTORY_REFRESH)
#     if "FTQ-" in report:
#         report_number = report.strip("FTQ-")
#         assert report_number in by_xpath(selenium, REPORT_HISTORY_TABLE_FIRST_ROW_NAME).text
#     else:
#         assert report in by_xpath(selenium, REPORT_HISTORY_TABLE_FIRST_ROW_NAME).text
#
#     # get report data for assertions
#     most_recent_report = by_xpath(selenium, "//tr[1]" + REPORT_REQUEST_DATE).text
#     get_all_request_dates = by_xpath_multiple(selenium, REPORT_REQUEST_DATE)
#     request_date_list = [x.text for x in get_all_request_dates]
#     pass_data_to_step['report_list_len'] = request_date_list.count(most_recent_report)
#     pass_data_to_step['report_request_date'] = most_recent_report


@when("I click on the 'x' button for a processing report")
def click_x_process_report(selenium):
    cancel_report_in_first_row = by_xpath(selenium, "//tr[1]" + REPORT_CANCEL).click()


@then(parsers.parse("I see the report status '{status}'"))
def report_status(selenium, status):
    report_status_in_first_row = by_xpath(selenium, "//tr[1]" + REPORT_STATUS).text
    assert status == report_status_in_first_row


@when(parsers.parse("I click the '{button}' button in the report row"))
def click_button_in_report_row(selenium, button):
    if button == "email":
        email_button_in_first_row = by_xpath(selenium, "//tr[1]" + REPORT_EMAIL).click()
    elif button == "trashcan":
        trashcan_button_in_first_row = by_xpath(selenium, "//tr[1]" + REPORT_DELETE).click()
        wait_invisible(selenium, PROCESSING_MASK)


@then("I see an email dialog")
def email_dialog(selenium):
    assert by_xpath(selenium, REPORT_EMAIL_DIALOG_HEADER).text == "Send Print Out"


@when("I send report email")
def send_report_email(selenium):
    select_recipient = by_xpath(selenium, REPORT_EMAIL_DIALOG_MAIN_RECIPIENT + REPORT_EMAIL_FIRST_OPTION).click()
    select_cc_field = by_xpath(selenium, REPORT_EMAIL_DIALOG_CC_RECIPIENT).click()
    select_cc_recipient = by_xpath(selenium, REPORT_EMAIL_DIALOG_CC_RECIPIENT + REPORT_EMAIL_FIRST_OPTION).click()
    by_xpath(selenium, POP_UP_CONFIRM).click()
    wait_invisible(selenium, PROCESSING_MASK)


@then("the report is deleted")
def report_deleted(selenium, pass_data_to_step):
    most_recent_report = pass_data_to_step.get('report_request_date')
    report_list_len = pass_data_to_step.get('report_list_len') - 1
    get_all_request_dates = by_xpath_multiple(selenium, REPORT_REQUEST_DATE)
    request_date_list = [x.text for x in get_all_request_dates]
    assert report_list_len == len(request_date_list)
