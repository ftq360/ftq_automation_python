import re
from pytest_bdd import scenario, then, given, parsers
from selenium.webdriver.support.select import Select

from functions import *
from locators.report_locators import *


@pytest.mark.xray('DEV-5395')
@scenario("../../../features/data_output/reports/inspection_email_subject_and_file_name_settings.feature",
          "Inspection Email Subject and File Name Settings")
def test_inspection_email_subject_and_file_name_settings():
    """Verify that inspection email subject and file name settings feature work as expected"""


@given(parsers.parse(
    "I change 'Filename template for Immediate Email Notifications (Inspection Checklist Screen.)' setting to {filename_setting}"))
def change_filename_setting(selenium, filename_setting):
    email_settings = by_xpath_multiple(selenium, ACTIVATE_RP_EMAIL_SETTING_NAMES)
    settings = [x.text for x in email_settings]
    index = [i for i, n in enumerate(settings) if
             'INSPECTION > SEND NOTIFICATION: Filename template for Immediate Email Notifications' in n]
    print(index[0])
    print(settings[index[0]])
    select_subject = Select(
        by_xpath(selenium, f"(//tr/td/*[not(@id)])[{index[0]}]")).select_by_visible_text(filename_setting)
    # click_filename_dropdown = by_xpath(selenium, f"(//tr/td/*[not(@id)])[{index[0]}]").click()
    # filename_options = by_xpath_multiple(selenium, f"(//tr/td/*[not(@id)])[{index[0]}]/option")
    # filename_options_list = [x.text for x in filename_options]
    # print("filename: ", filename_options_list)
    # filename_index = filename_options_list.index(filename_setting) + 1
    # click_filename_option = by_xpath(selenium,
    #                                  f"((//tr/td/*[not(@id)])[{index[-1] + 1}]/option)[{filename_index}]").click()


@given(parsers.parse(
    "I change 'Subject line template for Immediate Email Notifications (Inspection Checklist Screen.)' setting to {subject_setting}"))
def change_subject_setting(selenium, subject_setting):
    email_settings = by_xpath_multiple(selenium, ACTIVATE_RP_EMAIL_SETTING_NAMES)
    settings = [x.text for x in email_settings]
    print(settings)
    subject_index = [i for i, n in enumerate(settings) if
                     'INSPECTION > SEND NOTIFICATION: Subject line template for Immediate Email Notifications' in n]
    print(subject_index[0])
    print(settings[subject_index[0]])
    select_subject = Select(
        by_xpath(selenium, f"(//tr/td/*[not(@id)])[{subject_index[0]}]")).select_by_visible_text(subject_setting)
    # subject_options = by_xpath_multiple(selenium, f"(//tr/td/*[not(@id)])[{subject_index[-1] + 1}]/option")
    # subject_options_list = [x.text for x in subject_options]
    # print("subject: ", subject_options_list)
    # print(subject_setting)
    # subject_index = subject_options_list.index(subject_setting) + 1
    # click_subject_option = by_xpath(selenium, f"((//tr/td/*[not(@id)])[{subject_index[-1] + 1}]/option)[{subject_index}]").click()


@then(parsers.parse("the email subject line is in {subject_setting} format"))
def email_subject_in_correct_format(cred_data, subject_setting, pass_data_to_step):
    insp_ID = pass_data_to_step.get('insp_ID')
    email_recipient = pass_data_to_step.get('email_recipient')
    get_email_data = get_email_by_recipient(cred_data, email_recipient)
    returned_subject = get_email_data['returned_subject']
    pass_data_to_step['returned_attachment_filename'] = get_email_data['returned_attachment_filename']

    # assert subject correct
    if subject_setting == "Standard (Company Name, Project and Phase)":
        assert re.match(r"FTQ Automation Group - \d+ - Project 1 \(1\)", returned_subject)
    elif subject_setting == "Company Name, Project, Phase, Checklist and Inspection ID":
        subject_split = returned_subject.splitlines()
        subject_one_line = subject_split[0] + subject_split[1]
        assert re.match(r"FTQ Automation Group - \d+ - Project .+ - Punch List Deficiency - {}".format(insp_ID),
                        subject_one_line)


@then(parsers.parse("the email filename is in {filename_setting} format"))
def email_filename_in_correct_format(filename_setting, pass_data_to_step):
    insp_ID = pass_data_to_step.get('insp_ID')
    returned_attachment_filename = pass_data_to_step.get('returned_attachment_filename')

    if "Standard" in filename_setting:
        assert re.match(r"Project_.+_\d\d_\d\d\d\d-" + re.escape(insp_ID) + r"\.pdf", returned_attachment_filename)
    elif filename_setting == "Inspection ID only":
        assert returned_attachment_filename == insp_ID + ".pdf"
    elif filename_setting == "Inspection ID and Inspection Date":
        assert re.match(re.escape(insp_ID) + r"-\d\d_\d\d_\d\d\d\d\.pdf", returned_attachment_filename)
