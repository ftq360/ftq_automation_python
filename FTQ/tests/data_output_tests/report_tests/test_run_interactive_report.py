from pytest_bdd import scenario, given, then

from functions import *
from locators.report_locators import *


@pytest.mark.xray('DEV-5097')
@scenario("../../../features/data_output/reports/run_interactive_report.feature", "Run Interactive Report")
def run_interactive_report():
    """Verify that interactive reports run as expected"""


@given("I select Inspections on interactive reports page")
def select_interactive_report(selenium):
    interactive_reports = by_xpath_multiple(selenium, INTERACTIVE_REPORT_TITLE)
    report_titles = [x.text for x in interactive_reports]
    index = report_titles.index('Inspections') + 1
    by_xpath(selenium, f"({INTERACTIVE_REPORT_TITLE})[{index}]").click()
    wait_clickable(selenium, OLD_REPORT_RUN_REPORT_BUTTON)


@given("I select Select All until I reach the Report Runner")
def select_all_until_report_runner(selenium):
    while element_boolean(selenium, INTERACTIVE_REPORT_CHART):
        try:
            by_xpath(selenium, OLD_REPORT_RUN_REPORT_BUTTON).click()
            time.sleep(1)
        except:
            break
    selenium.switch_to_frame("report-frame")
    scroll_to_bottom(selenium)


@then("I should see report 401ax on Report History page")
def report_on_report_history_page(selenium):
    assert "401ax" in by_xpath(selenium, REPORT_HISTORY_TABLE_FIRST_ROW_NAME).text
