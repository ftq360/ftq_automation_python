from pytest_bdd import scenario, when, then

from functions import *


@pytest.mark.xray('DEV-7180')
@scenario("../../../features/data_output/reports/r4r_checkpoints_trigger_open_def_report_on_insp_page.feature", "R4R Checkpoints Trigger Open Deficiency Report on Inspection Page")
def test_r4r_checkpoints_trigger_open_def_report_on_insp_page():
    """Verify that R4R Checkpoints Trigger Open Deficiency Report on Inspection Page as expected"""


@when("I check R4R checkbox in 'Punch Item/Deficiency:' checkpoint")
def check_r4r(selenium):
    by_xpath(selenium, INSP_CHECKPOINT(selenium, "Punch Item/Deficiency:") + CP_R4R).click()
    time.sleep(1)
    assert selenium.execute_script("return arguments[0].previousSibling.checked;",
                                   by_xpath(selenium, INSP_CHECKPOINT(selenium, "Punch Item/Deficiency:") + CP_R4R))


@then("I am able to email an Open Deficiency Report")
def open_def_report_available(selenium):
    time.sleep(2)
    by_xpath(selenium, INSP_FOOTER_EMAIL_BUTTON).click()
    by_xpath(selenium, EMAIL_OPEN_DEF_BUTTON).click()
    time.sleep(1)
    assert selenium.execute_script("return arguments[0].previousElementSibling.checked;",
                                   by_xpath(selenium, EMAIL_OPEN_DEF_BUTTON))
