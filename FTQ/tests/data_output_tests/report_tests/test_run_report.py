from pytest_bdd import scenario

from functions import *


@pytest.mark.xray('DEV-5091')
@scenario("../../../features/data_output/reports/run_report.feature", "Run Report")
def test_run_report():
    """Verify that reports run as expected"""

# all steps in conftest