import pytest
from pytest_bdd import scenario


@pytest.mark.xray('DEV-7179')
@scenario("../../../features/data_output/reports/favorited_user_affiliate_report.feature", "Favorited User Affiliated Report")
def test_favorited_user_affiliate_report():
    """Verify that Favorited User Affiliated Report are running as expected"""

# all steps in conftest
