from pytest_bdd import scenario, given, then
from functions import *
from locators.report_locators import *


@pytest.mark.xray('DEV-5114')
@scenario("../../../features/data_output/reports/run_query.feature", "Run Query")
def test_run_query():
    """Verify that queries run as expected"""


@given("I select query 'Q901'")
def select_query(selenium):
    wait_clickable(selenium, REPORT_NAMES)
    report_names = by_xpath_multiple(selenium, REPORT_NAMES)
    report_names_list = [x.text for x in report_names]
    print(report_names_list)
    for x in report_names_list:
        if 'Q901' in x:
            report_index = report_names_list.index(x) + 1
            click_report_radio_button = by_xpath(selenium, f"({REPORT_SELECT})[{report_index}]").click().click()


@then("I should see Q901 on Report History page")
def report_history_page_correct_query(selenium):
    wait_clickable(selenium, REPORT_HISTORY_REFRESH)
    assert "Q901" in by_xpath(selenium, REPORT_HISTORY_TABLE_FIRST_ROW_NAME).text
