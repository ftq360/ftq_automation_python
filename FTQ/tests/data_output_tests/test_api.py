import json
from json import JSONDecodeError
from pytest_bdd import scenario, given, then
import requests.api
from functions import *
from locators.old_setup_page_locators import *
from locators.report_locators import OLD_REPORT_RUN_REPORT_BUTTON


@pytest.mark.xray('DEV-5403')
@scenario("../../features/data_output/api.feature", "APIs")
def test_api_historical_data():
    """Verify that APIs returning data as expected"""


@given("I have an API set up with my FTQ360 admin user")
def api_is_set_up(selenium, pass_data_to_step):
    # give time for api to catch insp created
    time.sleep(30)
    api_key = ""
    try:
        my_key = try_by_xpath(selenium, API_KEY).text
        api_key += my_key
    except:
        enable = by_xpath(selenium, OLD_REPORT_RUN_REPORT_BUTTON)
        my_key = try_by_xpath(selenium, API_KEY).text
        api_key += my_key
    pass_data_to_step['api_key'] = api_key.strip()


@then("APIs are returning data as expected")
def api_returning_correct_data(selenium, pass_data_to_step, cred_data):
    # give time for api to catch insp created
    time.sleep(30)

    api_key = pass_data_to_step.get('api_key')
    api_request_1025 = requests.api.get(url=f"{cred_data['BASE_URL']}/WebApi/RunDataQuery?q=1025", headers=({'x-api-key': api_key}))
    api_request_1027 = requests.api.get(url=f"{cred_data['BASE_URL']}/WebApi/RunDataQuery?q=1027", headers=({'x-api-key': api_key}))
    api_request_1029 = requests.api.get(url=f"{cred_data['BASE_URL']}/WebApi/RunDataQuery?q=1029", headers=({'x-api-key': api_key}))

    api_requests = {'1025': api_request_1025, '1027': api_request_1027, '1029': api_request_1029}
    for r in api_requests:
        try:
            json_body = json.loads(api_requests[r].text)
            json_data = json_body[-1]['InspectionID']
            print(json_data)
            assert json_data != ""
            assert api_requests[r].status_code == 200
        except JSONDecodeError:
            pytest.fail(f"Data for query {r} failed:\n{api_requests[r].text}")
