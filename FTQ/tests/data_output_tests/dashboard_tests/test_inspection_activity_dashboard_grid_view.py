import random

from pytest_bdd import scenario, when, then
from selenium.webdriver.common.keys import Keys
from functions import *
from locators.inspection_page_locators import *
from locators.dashboard_locators import *


@pytest.mark.xray('DEV-5407')
@scenario("../../../features/data_output/dashboards/inspection_activity_dashboard_grid_view.feature", "Inspection Activity Dashboard - Grid View")
def test_inspection_activity_dash_grid_view():
    """Verify that inspection activity dashboard grid view works as expected"""


@when("I search for an inspection in the grid")
def search_insp_date(selenium, pass_data_to_step):
    get_insp_IDs = by_xpath_multiple(selenium, INSPDASH_INSP_LINKS)
    insp_ID_list = [x.text for x in get_insp_IDs]
    insp_ID_to_search = random.choice(insp_ID_list)
    pass_data_to_step['insp_ID_to_search'] = insp_ID_to_search
    search_for_ID = by_xpath(selenium, DASH_GRID_SEARCH).send_keys(insp_ID_to_search, Keys.ENTER)
    wait_invisible(selenium, PROCESSING_MASK)


@then("I see search filtered the grid list correctly")
def grid_filtered_for_search(selenium, pass_data_to_step):
    insp_ID_to_search = pass_data_to_step.get('insp_ID_to_search')
    get_all_items_in_grid = by_xpath_multiple(selenium, INSPDASH_INSP_LINKS)
    new_insp_ID_list = [x.text for x in get_all_items_in_grid]
    assert [insp_ID_to_search] == new_insp_ID_list


@when("I change an inspection status from dashboard grid")
def change_inspection_status_in_grid(selenium, pass_data_to_step):
    open_status_options = by_xpath(selenium, INSPDASH_INSP_STATUS).click()
    time.sleep(2)
    get_status_options = by_xpath_multiple(selenium, INSPDASH_GRID_STATUS_OPTIONS)
    status_options = [x.text for x in get_status_options]
    new_status = random.choice(status_options)
    pass_data_to_step['new_status'] = new_status
    index = status_options.index(new_status) + 1
    select_new_status = by_xpath(selenium, f'({INSPDASH_GRID_STATUS_OPTIONS})[{index}]').click()
    wait_invisible(selenium, PROCESSING_MASK)
    new_status_in_grid = selenium.execute_script("return arguments[0].children[1].outerText;", by_xpath(selenium, INSPDASH_GRID_STATUS_NAME))
    assert new_status == new_status_in_grid


@when("I open the inspection from dashboard grid")
def open_insp_from_grid(selenium):
    by_xpath(selenium, INSPDASH_INSP_LINKS).click()
    loading_data_modal_appears_and_loads(selenium)
    wait_clickable_long(selenium, INSP_HEADER)


@then("I see inspection status is changed")
def insp_status_changed(selenium, pass_data_to_step):
    insp_ID = pass_data_to_step.get('insp_ID_to_search')
    confirm_correct_insp_opened = by_xpath(selenium, INSP_ID).text
    assert insp_ID == confirm_correct_insp_opened

    new_status = pass_data_to_step.get('new_status')
    confirm_correct_status = by_xpath(selenium, INSP_HEADER_STATUS).get_attribute('value')
    assert new_status == confirm_correct_status
