from pytest_bdd import scenario, when, then
from functions import *
from locators.dashboard_locators import *


@pytest.mark.xray('DEV-7496')
@scenario("../../../features/data_output/dashboards/days_to_fix_dashboards.feature", "Days To Fix Dashboards")
def test_days_to_fix_dashboards():
    """Verify that Days To Fix dashboards working as expected"""


@when("I select 'Project 1' in 'Project' dashboard panel")
def select_dash_panel(selenium, pass_data_to_step):
    wait_clickable(selenium, DASH_GRID)
    # get data of panels prior to any changes
    panel_title_list = [x.text for x in by_xpath_multiple(selenium, ALLDASH_PANEL_TITLE)]
    panel_to_edit = panel_title_list.index('Project') + 1
    stats_panel_titles = [x.text for x in by_xpath_multiple(selenium, ALLDASH_STATS_TITLES)]
    stat_to_check = stats_panel_titles.index('Total Days Open') + 1
    stats_data_before_filter = int(by_xpath(selenium, f'({ALLDASH_STATS_TITLES})[{stat_to_check}]' + ALLDASH_STATS_VALUES_FROM_TITLE).text)

    # make changes
    items_in_panel_to_edit = by_xpath_multiple(selenium, f'({ALLDASH_PANEL_TITLE})[{panel_to_edit}]' + ALLDASH_PANEL_ITEM_TITLE)
    panel_to_edit_item_list = [x.text for x in items_in_panel_to_edit]
    item_index = panel_to_edit_item_list.index('Project 1') + 1
    click_item_in_panel = by_xpath(selenium, f'(({ALLDASH_PANEL_TITLE})[{panel_to_edit}]' + f'{ALLDASH_PANEL_ITEM_TITLE})[{item_index}]').click()
    wait_invisible(selenium, PROCESSING_MASK)

    pass_data_to_step['stats_data_before_filter'] = stats_data_before_filter
    pass_data_to_step['stat_to_check'] = stat_to_check


@then("the dashboard stats are filtered accordingly")
def stats_panel_filtered(selenium, pass_data_to_step):
    stats_data_before_filter = pass_data_to_step.get('stats_data_before_filter')
    stat_to_check = pass_data_to_step.get('stat_to_check')

    stats_data_after_filter = int(by_xpath(selenium, f'({ALLDASH_STATS_TITLES})[{stat_to_check}]' + ALLDASH_STATS_VALUES_FROM_TITLE).text)
    assert stats_data_before_filter > stats_data_after_filter
