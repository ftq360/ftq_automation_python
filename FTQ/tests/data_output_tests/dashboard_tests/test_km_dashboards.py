import re

from pytest_bdd import scenario, when, then

from functions import *
from locators.dashboard_locators import *
from locators.listing_grid_locators import *


@pytest.mark.skip('Unstable')
@pytest.mark.xray('DEV-8403')
@scenario("../../../features/data_output/dashboards/km_dashboards.feature", "Key Metric Dashboards")
def test_km_dashboards():
    """Verify that Key Metric Dashboards working as expected"""


@when("I click on the 'Show Deficiencies' button")
def click_show_defs(selenium, pass_data_to_step):
    page_name = pass_data_to_step['page_name']
    original_window = selenium.current_window_handle
    click_link = by_xpath(selenium, KM_DASH_SHOW_DEFICIENCIES).click()

    # switch to new tab
    new_window_handles = selenium.window_handles
    new_window_handles.remove(original_window)
    selenium.switch_to_window(new_window_handles[0])

    # verify page
    wait_clickable(selenium, LISTING_GRID_TITLE)
    dashboard_title = page_name.replace(" Dashboard", "")
    assert dashboard_title in by_xpath(selenium, LISTING_GRID_TITLE).text


@then("the filtered deficiency grid is opened")
def filtered_def_grid_opened(selenium, pass_data_to_step):
    panel_data = pass_data_to_step['panel_data_after_filter']
    filtered_panel = pass_data_to_step['filtered_panel']
    panel_data_list = [data[0] for data in panel_data]
    if filtered_panel == 'Checkpoint':
        grid_checkpoints_with_code = [a.text for a in by_xpath_multiple(selenium, LISTING_GRID_CP_COLUMN)]
        grid_checkpoints = [re.sub(r" \(.*\)", "", x) for x in grid_checkpoints_with_code]
        assert all(x in panel_data_list for x in grid_checkpoints)
    elif filtered_panel == 'Project':
        grid_projects_with_code = [b.text for b in by_xpath_multiple(selenium, LISTING_GRID_PROJECT_COLUMN)]
        grid_projects = [re.sub(r" \(.*\)", "", x) for x in grid_projects_with_code]
        assert all(x in panel_data_list for x in grid_projects)
    else:
        grid_rps_with_code = [c.text for c in by_xpath_multiple(selenium, LISTING_GRID_RP_COLUMN)]
        grid_rps = [re.sub(r" \(.*\)", "", x) for x in grid_rps_with_code]
        assert all(x in panel_data_list for x in grid_rps)
