import csv

from pytest_bdd import scenario, when, then, parsers

from functions import *
from locators.dashboard_locators import *


@pytest.mark.xray('DEV-5408')
@scenario("../../../features/data_output/dashboards/rp_performance_dashboard.feature", "RP Performance Dashboard")
def test_rp_performance_dashboard():
    """Verify that RP performance dashboard is working as expected"""


@when(parsers.parse("I clear filtering on '{dash_panel}' dashboard panel"))
def clear_dashboard_panel_filtering(selenium, dash_panel):
    panel_titles = by_xpath_multiple(selenium, ALLDASH_PANEL_TITLE)
    panel_title_list = [x.text for x in panel_titles]
    panel_index = panel_title_list.index(dash_panel) + 1
    click_clear_filters = by_xpath(selenium, f'({ALLDASH_PANEL_TITLE})[{panel_index}]' + ALLDASH_CLEAR_PANEL_FILTER).click()
    wait_invisible(selenium, PROCESSING_MASK)


@when(parsers.parse("I sort '{dash_panel}' dashboard panel by '{sort_by}'"))
def sort_dashboard_panel(selenium, dash_panel, sort_by):
    panel_titles = by_xpath_multiple(selenium, ALLDASH_PANEL_TITLE)
    panel_title_list = [x.text for x in panel_titles]
    panel_index = panel_title_list.index(dash_panel) + 1
    click_sort = by_xpath(selenium, f'({ALLDASH_PANEL_TITLE})[{panel_index}]' + ALLDASH_PANEL_SORT).click()
    sort_options = by_xpath_multiple(selenium, f'({ALLDASH_PANEL_TITLE})[{panel_index}]' + ALLDASH_PANEL_SORT_OPTIONS)
    sort_options_list = [x.text for x in sort_options]
    index = sort_options_list.index(sort_by) + 1
    click_sort_option = by_xpath(selenium, f'({ALLDASH_PANEL_TITLE})[{panel_index}]' + f'{ALLDASH_PANEL_SORT_OPTIONS}[{index}]').click()
    time.sleep(1)


@then("data on 'Recurring Deficiencies By Checkpoint' panel should be listed by 'Title - Ascending'")
def dash_panel_sorted(selenium):
    panel_titles = by_xpath_multiple(selenium, ALLDASH_PANEL_TITLE)
    panel_title_list = [x.text for x in panel_titles]
    panel_index = panel_title_list.index('Recurring Deficiencies By Checkpoint') + 1
    get_items = by_xpath_multiple(selenium, f'({ALLDASH_PANEL_TITLE})[{panel_index}]' + ALLDASH_PANEL_ITEM_TITLE)
    panel_item_list = [x.text for x in get_items]
    python_sorted_list = sorted(panel_item_list, key=str.lower)
    assert panel_item_list == python_sorted_list


@when(parsers.parse("I download '{dash_panel}' dashboard panel data"))
def download_dashboard_panel_data(selenium, dash_panel):
    panel_titles = by_xpath_multiple(selenium, ALLDASH_PANEL_TITLE)
    panel_title_list = [x.text for x in panel_titles]
    panel_index = panel_title_list.index(dash_panel) + 1
    click_download = by_xpath(selenium, f'({ALLDASH_PANEL_TITLE})[{panel_index}]' + ALLDASH_PANEL_DOWNLOAD).click()
    time.sleep(2)
    download_wait()


@then(parsers.parse("data in the downloaded csv should match sorting order on '{dash_panel}' panel"))
def data_in_csv_matches_sorting_on_dashboard(selenium, dash_panel):
    # get csv data
    downloaded_panel_data = return_downloaded_csvs()[-1]
    with open(downloaded_panel_data) as csv_data_file:
        csv_reader = csv.reader(csv_data_file)
        csv_data = [x for x in csv_reader]

    # get panel data
    panel_titles = by_xpath_multiple(selenium, ALLDASH_PANEL_TITLE)
    panel_title_list = [x.text for x in panel_titles]
    panel_index = panel_title_list.index(dash_panel) + 1
    get_items = by_xpath_multiple(selenium, f'({ALLDASH_PANEL_TITLE})[{panel_index}]' + ALLDASH_PANEL_ITEM)
    panel_item_list = [x.text.splitlines() for x in get_items]

    assert csv_data[1:] == panel_item_list
