import random

from pytest_bdd import scenario, when, then, parsers

from functions import *
from locators.dashboard_locators import *


@pytest.mark.recently_fixed
#@pytest.mark.unstable
@pytest.mark.xray('DEV-5409')
@scenario("../../../features/data_output/dashboards/dashboard_landing_page.feature", "Dashboard Favorites")
def test_dashboard_favorites():
    """Verify that favoriting dashboards works as expected"""


@when("I remove all dashboard stars")
def remove_all_stars(selenium):
    try:
        find_all_favorited = by_xpath_multiple(selenium, ALLDASH_SELECTED_STARS)
        for star in find_all_favorited:
            time.sleep(1)
            star.click()
    except:  # skip step if no stars found
        pass


@then("dashboard menu shows the default options")
def dashboard_menu_shows_default(selenium):
    click_dashboard_menu_dropdown = by_xpath(selenium, DASHBOARD_MENU_DROPDOWN).click()
    get_dashboard_menu_options = by_xpath_multiple(selenium, DASHBOARD_MENU_OPTIONS)
    dashboard_menu_list = [x.text for x in get_dashboard_menu_options]
    assert dashboard_menu_list == DEFAULT_DASHBOARD_MENU_ITEMS_LIST


@when("I star some dashboards")
def star_some_dashboards(selenium, pass_data_to_step):
    all_dashboard_stars = by_xpath_multiple(selenium, ALLDASH_ALL_STARS)
    for star in random.sample(all_dashboard_stars, k=6):
        time.sleep(1)
        star.click()
    wait_clickable(selenium, ALLDASH_ALL_STARS)
    find_all_favorited = by_xpath_multiple(selenium, ALLDASH_SELECTED_STARS + ALLDASH_DASHNAME_FROM_STAR)
    favorited_dashboards = [x.text for x in find_all_favorited]
    pass_data_to_step['favorited_dashboards'] = favorited_dashboards


@then("dashboard menu shows my starred dashboards")
def dashboard_menu_shows_favorites(selenium, pass_data_to_step):
    click_dashboard_menu_dropdown = by_xpath(selenium, DASHBOARD_MENU_DROPDOWN).click()
    get_dashboard_menu_options = by_xpath_multiple(selenium, DASHBOARD_MENU_OPTIONS)
    dashboard_menu_list = [x.text for x in get_dashboard_menu_options]
    favorited_dashboards = pass_data_to_step.get('favorited_dashboards')
    assert sorted(dashboard_menu_list[:-1]) == sorted(favorited_dashboards)


@when(parsers.parse("I navigate to '{tab_name}' dashboard tab"))
def dashboard_filter_tab(selenium, tab_name):
    filter_tabs = by_xpath_multiple(selenium, ALL_DASH_FILTER_BUTTONS)
    filter_options = [txt.text for txt in filter_tabs]
    index = filter_options.index(tab_name) + 1
    click_filter_tab = by_xpath(selenium, f"({ALL_DASH_FILTER_BUTTONS})[{index}]").click()


@then("only my starred dashboards are shown")
def favorites_tab_filtered(selenium, pass_data_to_step):
    dashboards_shown = by_xpath_multiple(selenium, ALLDASH_DASHNAME)
    dashboard_names = [x.text for x in dashboards_shown]
    favorited_dashboards = pass_data_to_step.get('favorited_dashboards')
    assert sorted(dashboard_names) == sorted(favorited_dashboards)
