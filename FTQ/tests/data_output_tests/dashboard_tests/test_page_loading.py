from pytest_bdd import scenario, then, given, parsers
from functions import *
from locators.dashboard_locators import *


@pytest.mark.xray('DEV-5093')
@scenario("../../../features/data_output/dashboards/page_loading.feature", "Dashboard Page Loading")
def test_dashboards_page_loading():
    """Verify that dashboards load as expected"""


@given(parsers.parse("I navigate to {dashboard}"))
def navigate_to_dash(selenium, cred_data, dashboard):
    page = text_to_credential(dashboard)
    selenium.get(cred_data['BASE_URL'] + cred_data[page])
    assert selenium.current_url == cred_data['BASE_URL'] + cred_data[page]


@then("dashboard loads as expected")
def dashboards_load(selenium):
    try:
        wait_clickable_long(selenium, DASH_GRID_ROW)
    except:
        selenium.refresh()
        wait_clickable_long(selenium, DASH_GRID_ROW)

