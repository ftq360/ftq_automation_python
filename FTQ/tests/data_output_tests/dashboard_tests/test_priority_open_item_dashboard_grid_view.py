import random

from pytest_bdd import scenario, when, then
from selenium.webdriver.common.keys import Keys

from functions import *
from locators.dashboard_locators import *


# feature deprecated
@pytest.mark.xray('DEV-5406')
@scenario("../../../features/data_output/dashboards/priority_open_item_dashboard_grid_view.feature", "Priority Open Item Dashboard - Grid View")
def priority_open_item_dash_grid_view():
    """Verify that priority open item dashboard grid view works as expected"""


@when("I search for a due date in the grid")
def search_due_date(selenium, pass_data_to_step):
    get_due_dates = by_xpath_multiple(selenium, PRIORITYDASH_GRID_DUEDATES)
    due_date_list = [x.text for x in get_due_dates]
    due_date_to_search = random.choice(due_date_list)
    pass_data_to_step['due_date_to_search'] = due_date_to_search
    search_for_due_date = by_xpath(selenium, DASH_GRID_SEARCH).send_keys(due_date_to_search, Keys.ENTER)
    wait_invisible(selenium, PROCESSING_MASK)


@then("I see search filtered the grid list correctly")
def grid_filtered_for_search(selenium, pass_data_to_step):
    due_date_to_search = pass_data_to_step.get('due_date_to_search')
    get_all_items_in_grid = by_xpath_multiple(selenium, PRIORITYDASH_GRID_DUEDATES)
    new_due_date_list = [x.text for x in get_all_items_in_grid]
    for y in new_due_date_list[1:]:
        assert due_date_to_search == y
