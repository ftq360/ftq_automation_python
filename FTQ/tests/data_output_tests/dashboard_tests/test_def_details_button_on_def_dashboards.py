from pytest_bdd import scenario, when, then, parsers

from functions import *
from locators.listing_grid_locators import *


@pytest.mark.unstable
@pytest.mark.skip("Unstable")
@pytest.mark.xray('DEV-9314')
@scenario("../../../features/data_output/dashboards/def_details_button_on_def_dashboards.feature", "Deficiency Details Button on Deficiency Dashboards")
def def_details_button_on_def_dashboards():
    """Test that Deficiency Details Button on Deficiency Dashboards is working as expected"""


@when("I click on 'Show Deficiencies' button")
def click_show_def_button(selenium, pass_data_to_step):
    original_window = selenium.current_window_handle
    pass_data_to_step['original_window'] = original_window
    time.sleep(1)
    details_button = by_xpath(selenium, "//*[@class='filters']")
    selenium.execute_script("arguments[0].firstElementChild.click();", details_button)
    # ActionChains(selenium).click(details_button).perform()
    # details_button_location = details_button.location
    # action = ActionBuilder(selenium)
    # action.pointer_action.move_to_location(x=details_button_location['x'], y=details_button_location['y'])
    # action.pointer_action.click()
    # action.perform()

    # action = ActionChains(selenium)
    # action.move_to_element_with_offset(to_element=details_button, xoffset=-1, yoffset=0).click().perform()


@then("I should see a new tab open")
def new_show_def_tab_opens(selenium, pass_data_to_step):
    page_name = pass_data_to_step['page_name']
    original_window = pass_data_to_step['original_window']
    # switch to new tab
    new_window_handles = selenium.window_handles
    new_window_handles.remove(original_window)
    selenium.switch_to_window(new_window_handles[0])

    # verify page
    wait_clickable(selenium, LISTING_GRID_TITLE)
    dashboard_title = page_name.replace(" Dashboard", "")
    listing_grid_title_shown = by_xpath(selenium, LISTING_GRID_TITLE).text
    assert dashboard_title in listing_grid_title_shown


@then(parsers.parse("I should see the same deficiencies filtered on the dashboard for {project}, {phase}, and {inspector}"))
def def_listing_is_filtered_by_dash(selenium, project, phase, inspector):
    if 'All' in project:
        pass
    else:
        projects_shown = [x.text for x in by_xpath_multiple(selenium, LISTING_GRID_PROJECT_COLUMN)]
        print(projects_shown[0])
    if 'All' in phase:
        pass
    else:
        phases_shown = [x.text for x in by_xpath_multiple(selenium, LISTING_GRID_PHASE_COLUMN)]
        print(phases_shown[0])
    if 'All' in inspector:
        pass
    else:
        inspectors_shown = [x.text for x in by_xpath_multiple(selenium, LISTING_GRID_CREATED_BY)]
        print(inspectors_shown[0])
