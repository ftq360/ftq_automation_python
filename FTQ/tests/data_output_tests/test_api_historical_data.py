import zipfile

from pytest_bdd import scenario, when, then, parsers
from selenium.common.exceptions import ElementClickInterceptedException
from selenium.webdriver.support.select import Select

from functions import *
from locators.listing_grid_locators import *
from locators.old_setup_page_locators import *
from locators.report_locators import OLD_REPORT_RUN_REPORT_BUTTON


#OBSOLETE
@pytest.mark.skip('Unstable')
@pytest.mark.xray('DEV-5404')
@scenario("../../features/data_output/api_historical_data.feature", "API Historical Data")
def test_api_historical_data():
    """Verify that API Historical Data returning correct data"""


@when("I search for the current year in Last Activity Date column")
def search_year_activity(selenium):
    by_xpath(selenium, INSP_ARCHIVE_LAST_ACTIVITY_DATE_SEARCH).send_keys(datetime.datetime.now().strftime('%Y'))
    by_xpath(selenium, LISTING_GRID_TITLE).click()
    wait_clickable(selenium, GRID_ITEM_NAME)


@when(parsers.parse("I sort Last Activity Date column by {direction}"))
def sort_activity_date_column(selenium, direction):
    wait_clickable(selenium, INSP_ARCHIVE_LAST_ACTIVITY_COLUMN_HEAD)
    time.sleep(1)
    by_xpath(selenium, INSP_ARCHIVE_LAST_ACTIVITY_COLUMN_HEAD).click()
    if direction == 'descending':
        try:
            by_xpath(selenium, INSP_ARCHIVE_COLUMN_ASCENDING).click()
            wait_clickable(selenium, INSP_ARCHIVE_LAST_ACTIVITY_COLUMN_HEAD + INSP_ARCHIVE_COLUMN_DESCENDING)
        except ElementClickInterceptedException:
            time.sleep(5)
            by_xpath(selenium, INSP_ARCHIVE_COLUMN_ASCENDING).click()
            wait_clickable(selenium, INSP_ARCHIVE_LAST_ACTIVITY_COLUMN_HEAD + INSP_ARCHIVE_COLUMN_DESCENDING)
    elif direction == 'ascending':
        wait_clickable(selenium, INSP_ARCHIVE_LAST_ACTIVITY_COLUMN_HEAD + INSP_ARCHIVE_COLUMN_ASCENDING)
    wait_clickable_long(selenium, GRID_ITEM_NAME)
    time.sleep(1)


@then(parsers.parse("I see the inspection ID of the first inspection listed in {direction} ({inspection})"))
def get_sorted_insp_id(selenium, direction, inspection, pass_data_to_step):
    time.sleep(2)
    inspection_id = by_xpath(selenium, GRID_ITEM_NAME).text
    if direction == 'descending':
        pass_data_to_step[inspection] = inspection_id
    elif direction == 'ascending':
        pass_data_to_step['insp2'] = inspection_id


@when("I downloaded API query 1025")
def download_api_query(selenium):
    Select(by_xpath(selenium, SELECT_API_QUERY)).select_by_value('1025')
    by_xpath(selenium, OLD_REPORT_RUN_REPORT_BUTTON).click()
    wait_present(selenium, PROCESSING_MASK)
    wait_invisible_long(selenium, PROCESSING_MASK)
    download_wait()
    time.sleep(15)


@then("both inspection IDs are present in the API query data")
def insp_id_present_in_api_query(selenium, pass_data_to_step):
    insp1 = pass_data_to_step['insp1']
    insp2 = pass_data_to_step['insp2']
    zips = return_downloaded_zips()
    zip = zipfile.ZipFile(zips[-1])
    file_list = zip.namelist()
    with zipfile.ZipFile(zips[-1], 'r') as myzip:
        with myzip.open(file_list[0]) as myfile:
            readit = myfile.read()
            assert insp1 in str(readit)
            assert insp2 in str(readit)
