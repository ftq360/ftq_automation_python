from pytest_bdd import scenario, when, then, parsers
from selenium.webdriver.common.keys import Keys

from functions import *
from locators.listing_grid_locators import *


@pytest.mark.xray('DEV-5400')
@scenario("../../../features/data_output/listing_grids/itp_listing_filtering.feature", "ITP Listing Filtering")
def test_itp_listing_filtering():
    """Verify that ITP Listing Filtering  works as expected"""


@when(parsers.parse("I enter '{text}' in '{column}' search field"))
def enter_text_in_column_search(selenium, column, text, pass_data_to_step):
    pass_data_to_step['column'] = column
    pass_data_to_step['text'] = text
    wait_clickable(selenium, ITP_LISTING_CODES)
    if column == 'code':
        by_xpath(selenium, ITP_LISTING_CODE_SEARCH).send_keys(text, Keys.ENTER)
    elif column == 'checklist':
        by_xpath(selenium, ITP_LISTING_CHECKLIST_SEARCH).send_keys(text, Keys.ENTER)
    elif column == 'project':
        by_xpath(selenium, ITP_LISTING_PROJECT_SEARCH).send_keys(text, Keys.ENTER)
    elif column == 'equipment':
        by_xpath(selenium, ITP_LISTING_EQUIPMENT_SEARCH).send_keys(text, Keys.ENTER)
    elif column == 'responsible party':
        by_xpath(selenium, ITP_LISTING_RP_SEARCH).send_keys(text, Keys.ENTER)
    wait_clickable(selenium, ITP_LISTING_CODES)


@then("the ITP Listing grid is filtered accordingly")
def itp_grid_filtered(selenium, pass_data_to_step):
    column = pass_data_to_step.get('column')
    text = pass_data_to_step.get('text')
    listing_grid_data = []
    time.sleep(2)
    if column == 'code':
        codes_list = [x.text for x in by_xpath_multiple(selenium, ITP_LISTING_CODES)]
        listing_grid_data.append(codes_list)
    elif column == 'checklist':
        checklist_list = [x.text for x in by_xpath_multiple(selenium, ITP_LISTING_CHECKLISTS)]
        listing_grid_data.append(checklist_list)
    elif column == 'project':
        project_list = [x.text for x in by_xpath_multiple(selenium, ITP_LISTING_PROJECTS)]
        listing_grid_data.append(project_list)
    elif column == 'equipment':
        equipment_list = [x.text for x in by_xpath_multiple(selenium, ITP_LISTING_EQUIPMENT)]
        listing_grid_data.append(equipment_list)
    elif column == 'responsible party':
        rp_list = [x.text for x in by_xpath_multiple(selenium, ITP_LISTING_RPS)]
        listing_grid_data.append(rp_list)
    for y in listing_grid_data[0]:
        assert text in y

