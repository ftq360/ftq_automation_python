from pytest_bdd import scenario, parsers, then
from functions import *
from locators.listing_grid_locators import *


@pytest.mark.xray('DEV-5157')
@scenario("../../../features/data_output/listing_grids/inspections_listed_under_correct_plan_item.feature", "Inspections are listed under correct plan item")
def test_inspections_listed_under_correct_plan_item():
    """Verify that ITP inspections show under correct plan items on ITP Progress Grid"""


@then(parsers.parse("the Inspection ID is {inspection_number}"))
def inspection_id_numerate(selenium, inspection_number, pass_data_to_step):
    if inspection_number == "inspection1":
        pass_data_to_step['inspection1'] = pass_data_to_step.get('insp_ID')
    elif inspection_number == "inspection2":
        pass_data_to_step['inspection2'] = pass_data_to_step.get('insp_ID')


@then(parsers.parse("{inspection} is listed under plan item '{plan_item}'"))
def inspection_listed_under_correct_plan_item(selenium, plan_item, inspection, pass_data_to_step):
    inspection1 = pass_data_to_step.get('inspection1')
    inspection2 = pass_data_to_step.get('inspection2')
    plan_items = by_xpath_multiple(selenium, ITPPROG_PLAN_ITEM_ROW)
    plan_items_list = [x.get_attribute('outerText') for x in plan_items]
    index = []
    for x in plan_items_list:
        if plan_item in x:
            index.append(plan_items_list.index(x) + 1)
    expand_plan_item_data = by_xpath(selenium, f"({ITPPROG_PLAN_ITEM_ROW}){index}" + ITPPROG_EXPAND_DATA).click()
    wait_clickable(selenium, f"({ITPPROG_PLAN_ITEM_ROW}){index}/.." + ITPPROG_GRID)
    if inspection == "inspection1":
        listed_inspection = by_xpath(selenium, f"({ITPPROG_PLAN_ITEM_ROW}){index}/.." + ITPPROG_GRID + f"{ITPPROG_GRID_LINK}[1]").text
        assert inspection1 == listed_inspection
    if inspection == "inspection2":
        listed_inspection = by_xpath(selenium, f"({ITPPROG_PLAN_ITEM_ROW}){index}/.." + ITPPROG_GRID + f"{ITPPROG_GRID_LINK}[1]").text
        assert inspection2 == listed_inspection
