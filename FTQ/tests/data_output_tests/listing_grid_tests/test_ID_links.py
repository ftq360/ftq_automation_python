from pytest_bdd import scenario, then, when, parsers, given

from functions import *
from locators.inspection_page_locators import *
from locators.listing_grid_locators import *


@pytest.mark.xray('DEV-5083')
@scenario("../../../features/data_output/listing_grids/ID_links.feature", "ID links on Recent Listing Grids")
def test_ID_links_on_recent_listing_grids():
    """Verify that ID links are working correctly on listing grids"""


@given(parsers.parse("I navigate to {listing_grid_page}"))
def navigate_to_specific_page(selenium, listing_grid_page, cred_data, pass_data_to_step):
    page = text_to_credential(listing_grid_page)
    selenium.get(cred_data['BASE_URL'] + cred_data[page])
    assert selenium.current_url == cred_data['BASE_URL'] + cred_data[page]
    pass_data_to_step['listing_grid_page'] = listing_grid_page


@when(parsers.parse("I click on the {ID_link} for my inspection"))
def click_ID_link(selenium, pass_data_to_step, ID_link):
    listing_grid_page = pass_data_to_step.get('listing_grid_page')
    insp_ID = pass_data_to_step.get('insp_ID')
    get_insp_ids = by_xpath_multiple(selenium, GRID_ITEM_NAME)
    insp_id_list = [x.text for x in get_insp_ids]
    index = insp_id_list.index(insp_ID) + 1
    clear_console_log = selenium.get_log('browser')
    if ID_link == "deficiencyId":
        click_recent_deficiency_link = by_xpath(selenium, f"({GRID_RECENT_CHECKPOINTID_LINKS})[{index}]").click()
        wait_clickable(selenium, INSP_HEADER)
    elif ID_link == "inspId":
        click_recent_insp_link = by_xpath(selenium, f"({GRID_ITEM_NAME})[{index}]").click()
        wait_clickable(selenium, INSP_HEADER)


@then(parsers.parse("I should be directed to {inspection_open_on}"))
def ID_link_opens_to_correct_place(selenium, inspection_open_on):
    time.sleep(3)
    scroll_position = selenium.execute_script("return window.scrollY")
    if inspection_open_on == "insp header":
        assert scroll_position == 0
    elif inspection_open_on == "checkpoint":
        assert scroll_position != 0
