from pytest_bdd import scenario, then, when, parsers

from functions import *
from locators.inspection_page_locators import *
from locators.listing_grid_locators import *


@pytest.mark.skip('Unstable')
@pytest.mark.xray('DEV-5398')
@scenario("../../../features/data_output/listing_grids/itp_progress_grid.feature", "Icon colors (ITP progress)")
def test_itp_progress_grid_icon_colors():
    """Verify that icon colors are correct for ITP Progress Grid plan items"""


@when(parsers.parse("I click on plan item '{plan_item}'"))
def click_plan_item(selenium, plan_item):
    get_all_plan_items = by_xpath_multiple(selenium, GRID_ITEM_NAME)
    plan_item_list = [x.text for x in get_all_plan_items]
    index = plan_item_list.index(plan_item) + 1
    click_plan_item = by_xpath(selenium, f"({ITPPROG_PLAN_ITEM_LINK})[{index}]").click()


@when("I select a Responsible Party to create the plan item with")
def select_rp_for_plan_item(selenium):
    select_rp = by_xpath(selenium, SPECIFIC_SELECTION(selenium, "FTQ Automation Group (D1)")).click()


@then("ITP inspection is successfully created")
def itp_inspection_created_successfully(selenium):
    itp_inspection = by_xpath(selenium, INSP_HEADER_ITP)
    assert "Inspection" in selenium.title 
    assert itp_inspection.get_attribute("value") == "[1-1]"


@then(parsers.parse("I should see {icon_color} icon for plan item '{plan_item}'"))
def filter_itp_project(selenium, icon_color, plan_item):
    get_all_plan_items = by_xpath_multiple(selenium, GRID_ITEM_NAME)
    plan_item_list = [x.text for x in get_all_plan_items]
    index = plan_item_list.index(plan_item) + 1
    get_icon_color = by_xpath(selenium, f"({ITPPROG_PLAN_ITEM_ICON})[{index}]").get_attribute("src")
    if icon_color == "grey":
        assert "Icon_TaskSupplier_2.png" in get_icon_color
    elif icon_color == "yellow":
        assert "Icon_TaskSupplier_3.png" in get_icon_color
    elif icon_color == "red filled":
        assert "Icon_TaskSupplier_4.png" in get_icon_color
    elif icon_color == "green":
        assert "Icon_TskSupplier_8.png" or "Icon_TaskSupplier_5.png" in get_icon_color
    elif icon_color == "red":
        assert "Icon_TaskSupplier_9.png" in get_icon_color
