import re

from pytest_bdd import scenario, parsers, given, when, then
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.select import Select

from functions import *
from locators.account_management_locators import *
from locators.listing_grid_locators import *
from locators.old_setup_page_locators import *


@pytest.mark.xray('DEV-8085')
@scenario("../../../features/data_output/listing_grids/searching_for_dates_on_recent_listings.feature", "Searching for Dates on Recent Listing Grids")
def test_searching_for_dates_on_recent_listings():
    """Verify that Searching for Dates on Recent Listing Grids is working as expected"""


@given(parsers.parse("I navigate to listing {listing_page_name} page"))
def navigate_to_specific_page(selenium, listing_page_name, cred_data):
    page = text_to_credential(listing_page_name)
    selenium.get(cred_data['BASE_URL'] + cred_data[page])
    assert selenium.current_url == cred_data['BASE_URL'] + cred_data[page]


@given(parsers.parse("I set date format to '{date_format}'"))
def set_date_format(selenium, date_format):
    formatted_date = date_format
    if 'x' in date_format:
        formatted_date = date_format.replace("x", "/")
    Select(by_xpath(selenium, "(//td//select)[1]")).select_by_visible_text(formatted_date)
    save_new_name = by_xpath(selenium, SETUP_ALL_SAVE_BUTTONS).click()
    confirm_save = by_xpath(selenium, POP_UP_CONFIRM).click()
    wait_present(selenium, SETUP_ALL_SAVED_CONFIRMATION_MASK)


@when(parsers.parse("I search the page in {date_format}"))
def search_page_in_date_format(selenium, date_format):
    time.sleep(2)
    wait_clickable(selenium, GRID_ITEM_NAME)
    formatted_date = date_format
    if 'x' in date_format:
        formatted_date = date_format.replace("x", "/")

    # assert testing correct date format
    grid_date = [x.text for x in by_xpath_multiple(selenium, LISTING_GRID_ANY_DATE)]
    formatted_for_assertion = formatted_date.replace("MM", "[0-9]+").replace("MON", "[A-Za-z]+").replace("DD", "[0-9]+").replace("YYYY", "[0-9]+").replace("(abbreviated month name)", "").strip()
    assert re.match(formatted_for_assertion, grid_date[0])

    last_month = datetime.datetime.today() - datetime.timedelta(weeks=4)
    if "MON" in formatted_date:
        print(last_month.strftime("%b %Y"))
        by_xpath(selenium, GRID_SEARCH).send_keys(last_month.strftime("%b %Y"), Keys.ENTER)
    else:
        print(last_month.strftime("%m %Y"))
        by_xpath(selenium, GRID_SEARCH).send_keys(last_month.strftime("%m %Y"), Keys.ENTER)


@then(parsers.parse("I should see the {listing_page_name} filter for my input"))
def listing_grid_filtered(selenium, pass_data_to_step, listing_page_name):
    listing_page_name = pass_data_to_step['page_name']
    time.sleep(2)
    wait_clickable(selenium, LISTING_GRID_ANY_DATE)
    assert len([x.text for x in by_xpath_multiple(selenium, LISTING_GRID_ANY_DATE)]) > 0

