from pytest_bdd import scenario, then, when, parsers
from functions import *
from locators.listing_grid_locators import *
from locators.selection_process_locators import *


@pytest.mark.xray('DEV-5367')
@scenario("../../../features/data_output/listing_grids/recent_inspections_between_linked_accounts.feature", "Recent Inspections Between Linked Accounts")
def test_recent_inspections_between_linked_accounts():
    """Verify that Recent Inspections Between Linked Accounts works as expected"""


@then(parsers.parse("I see only inspections on '{grid_name}' from my automation group"))
def inspections_only_from_automation_group(selenium, grid_name):
    inspectors_list = []
    if grid_name == 'Show Recent Insp':
        get_data = by_xpath_multiple(selenium, SHOW_RECENT_INSP)
        show_recent_inspectors_list = [x.text for x in get_data]
        inspectors_list.extend(show_recent_inspectors_list)
    elif grid_name == 'Recent Listing Grid':
        get_data = by_xpath_multiple(selenium, LISTING_GRID_CREATED_BY)
        recent_insp_grid_inspectors_list = [x.text for x in get_data]
        inspectors_list.extend(recent_insp_grid_inspectors_list)
    for y in inspectors_list:
        assert "4628" not in y


@when("I expand Show Recent Inspections data")
def expand_show_recent_inspection_data(selenium):
    by_xpath(selenium, SP_RECENT_INSP_EXPAND).click()
    try:
        modal_appears = wait_clickable_short(selenium, SP_DICT_LOADING_MODAL_OPEN)
        loading_complete = wait_present(selenium, SP_DICT_LOADING_MODAL_CLOSED)
    except:
        pass
    wait_clickable(selenium, SP_SHOW_RECENT_GRID)
