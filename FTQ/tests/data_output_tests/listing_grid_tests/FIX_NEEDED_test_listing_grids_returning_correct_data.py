from pytest_bdd import scenario, given, then, parsers

from functions import *
from locators.listing_grid_locators import *


#OBSOLETE
@pytest.mark.skip('Unstable')
@pytest.mark.xray('DEV-6990')
@scenario("../../../features/data_output/listing_grids/listing_grids_returning_correct_data.feature", "Listing Grids Returning Correct Data")
def test_listing_grids_returning_correct_data():
    """Verify that Listing Grids Returning Correct Data as expected"""


@given(parsers.parse("I navigate to {listing_grids} page"))
def navigate_to_listing(selenium, listing_grids, cred_data):
    page = text_to_credential(listing_grids)
    selenium.get(cred_data['BASE_URL'] + cred_data[page])
    assert selenium.current_url == cred_data['BASE_URL'] + cred_data[page]


@then("I see the correct notes on grid")
def correct_notes_shown_on_grid(selenium, listing_grids, pass_data_to_step):
    header_note = pass_data_to_step.get('header_note')
    checkpoint_note = pass_data_to_step.get('note')
    try:
        assert checkpoint_note in [x.text for x in try_by_xpath_multiple(selenium, LISTING_GRID_CP_NOTE)]
    except:
        assert header_note in [x.text for x in by_xpath_multiple(selenium, LISTING_GRID_INSP_NOTE)]
