from pytest_bdd import scenario, then
from functions import *
from locators.listing_grid_locators import *


@pytest.mark.xray('DEV-5062')
@scenario("../../../features/data_output/listing_grids/users_should_see_group_inspections_from_other_users.feature", "Users should see group inspections from other users")
def test_users_see_inspections_from_other_users_in_group():
    """Verify that users can see inspections created by other users in the same group"""


@then("I should see Inspection ID for the newly created inspection")
def see_insp_id_for_new_inspection(selenium, pass_data_to_step):
    insp_ID = pass_data_to_step.get('insp_ID')
    get_insp_ids = by_xpath_multiple(selenium, GRID_ITEM_NAME)
    insp_id_list = [x.text for x in get_insp_ids]
    assert insp_ID in insp_id_list
