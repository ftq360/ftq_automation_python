import random
from pytest_bdd import scenario, then, when
from functions import *
from locators.inspection_page_locators import *
from locators.listing_grid_locators import *


@pytest.mark.xray('DEV-5369')
@scenario("../../../features/data_output/listing_grids/itp_progress_grid.feature", "Drop-down data (ITP Progress)")
def test_itp_progress_grid_drop_down_data():
    """Verify that dropdown data is correct for ITP Progress Grid plan items"""


@then("the ITP status bar at the top of the page is correct calculation")
def itp_calc_correct(selenium):
    # receive and format page calculation into a floating decimal number from page text
    page_total_calc = float(by_xpath(selenium, ITPPROG_PAGE_TOTAL_CALC).text.split('%')[0])
    # receive calcs from all plan items
    item_calcs = by_xpath_multiple(selenium, ITPPROG_PLAN_ITEM_CALC)
    item_calc_list = [x.text.split('%') for x in item_calcs]
    item_calc_numbers = [float(y[0]) for y in item_calc_list]
    # get the average of the calculations from plan items
    true_calc = sum(item_calc_numbers) / len(item_calc_numbers)
    print("true calc: ", true_calc)
    print("actual shown: ", page_total_calc)
    # verify calculations match
    assert str(true_calc) == str(page_total_calc)


@when("I expand the drop down for plan item '[4-2]'")
def expand_ITP_details(selenium):
    plan_items = by_xpath_multiple(selenium, ITPPROG_PLAN_ITEM_NAME)
    plan_item_names = [x.text for x in plan_items]
    for x in plan_item_names:
        if '[4-2]' in x:
            index = plan_item_names.index(x) + 1
            by_xpath(selenium, f"({ITPPROG_EXPAND_DATA})[{index}]").click()
    wait_clickable_long(selenium, ITPPROG_GRID)


@when("I open an inspection from the ITP Progress page")
def opem_itp_insp_from_grid(selenium, pass_data_to_step):
    # receive inspection ids listed and select one at random
    inspection_ids = by_xpath_multiple(selenium, ITPPROG_INSP_ID_LINK)
    inspection_ids_list = [x.text for x in inspection_ids]
    insp_id = random.choice(inspection_ids_list)
    pass_data_to_step['insp_id'] = insp_id
    index = inspection_ids_list.index(insp_id) + 1
    click_insp_id_link = by_xpath(selenium, f"({ITPPROG_INSP_ID_LINK})[{index}]").click()
    wait_clickable_long(selenium, INSP_HEADER)


@then("the inspection shows new inspection status on the ITP Progress page")
def insp_shows_new_status(selenium, pass_data_to_step):
    insp_id = pass_data_to_step.get('insp_id')
    get_insp_row = by_xpath_multiple(selenium, ITPPROG_INSP_ID_LINK)
    inspection_ids_list = [x.text for x in get_insp_row]
    index = inspection_ids_list.index(insp_id) + 1
    get_status_for_insp_id = by_xpath(selenium, f"({ITPPROG_INSP_ID_STATUS})[{index}]").text
    assert get_status_for_insp_id == 'Fail (final)'
