import pytest
from pytest_bdd import scenario, when, then, parsers


@pytest.mark.xray('DEV-9491')
@scenario("../features/home_page_loading_correct_data.feature", "Home Page Loading Correct Data")
def test_home_page_loading_correct_data():
    """test that the Home Page is Loading Correct Data and working as expected"""


@when("I click on the home icon")
def click_home_icon(selenium):
    pass


@then("I am brought to my homepage dashboard")
def my_homepage_dashboard(selenium):
    pass


@then(parsers.parse("I should see the correct info loading on {panel}"))
def correct_homepage_dashboard_info(selenium):
    pass


@then(parsers.parse("I should see the new inspection info added to {panel}"))
def new_insp_info_on_homepage_dashboard(selenium):
    pass
