import shlex
import sys

from pytest_bdd import scenario, given, parsers

from functions import *
from locators.listing_grid_locators import *
from locators.old_setup_page_locators import *
from locators.setup_locators import *


@pytest.mark.skipif("teardown" not in str(sys.argv), reason="Not running data teardown")
@scenario("../../features/my_test_admin/teardown.feature", "Teardown")
def test_teardown_data():
    """data teardown"""


@given("I delete inspections from inspections archive listing grid")
def delete_inspections(selenium):
    pages = by_xpath(selenium, ARCHIVE_GRID_PAGE_NUMBERING).text
    if int(pages) > 1:
        for x in range(int(pages) - 1):
            by_xpath(selenium, INSPARCHIVE_SELECTALL_CHECKBOX).click()
            by_xpath(selenium, INSPARCHIVE_DELETE_BUTTON).click()
            by_xpath(selenium, POPUP_OK_BUTTON).click()
            wait_select_state_xpath(selenium, INSPARCHIVE_CHECKBOXES, False)
        print(f"{(int(pages) - 1)} Pages of Inspections Deleted")
    else:
        print("Not Enough Inspections To Delete")


@given("I delete project data")
def delete_proj_data(selenium, pass_data_to_step):
    # close dialog and wait for next step if 'add new' dialog pops up-- means empty grid and nothing to delete
    try:
        try_by_xpath(selenium, SETUP_POPUP_FRONT + SETUP_POPUP_CANCEL).click()
    except Exception:
        # set grid filter to 'all' so we don't miss any items
        click_filter_dropdown = by_xpath(selenium, SETUP_GRID_FILTER_DROPDOWN).click()
        filter_options = by_xpath_multiple(selenium, SETUP_GRID_FILTER_DROPDOWN_OPTIONS)
        filter_list = [x.text for x in filter_options]
        index = filter_list.index('All') + 1
        click_filter = by_xpath(selenium, f"({SETUP_GRID_FILTER_DROPDOWN_OPTIONS})[{index}]").click()
        wait_clickable(selenium, SETUP_GRID_BODY)

        # attempt to delete all project data
        select_all = by_xpath(selenium, SETUP_GRID_SELECT_ALL).click()
        click_cog_icon = by_xpath(selenium, SETUP_COG_ICON).click()
        try:
            click_delete = try_by_xpath(selenium, SETUP_COG_DELETE).click()
        except Exception:
            # if nothing selected, the clicking the delete button will fail
            sub_grid = pass_data_to_step.get('sub_grid')
            print(f"No Project {sub_grid} Data To Delete")

        # deselect any items preventing deletion
        multiple_items = [] # workaround for not having to duplicate the 'for' statement in code below
        single_item = []
        if by_xpath(selenium, SETUP_POPUP_FRONT + SETUP_POPUP_HEADER).text == "Unable to delete":
            try: # if multiple items to delete they are in ul
                get_undeletable_items = try_by_xpath_multiple(selenium, SETUP_POPUP_FRONT + SETUP_POPUP_CONTENT_LIST)
                undeleteable_names = [x.text for x in get_undeletable_items]
                multiple_items.append(undeleteable_names)
            except Exception: # if list of items not found, there is single item to delete and item name will be between quotes
                get_undeletable_item = try_by_xpath(selenium, SETUP_POPUP_FRONT + SETUP_POPUP_CONTENT).text
                undeleteable_name = shlex.split(get_undeletable_item)
                single_item.append(undeleteable_name[2]) #undeletable item name is always index 2 in the list.
            click_to_dismiss = by_xpath(selenium, SETUP_POPUP_FRONT + SETUP_POPUP_CONFRIM).click()
            get_all_names_in_grid = by_xpath_multiple(selenium, SETUP_GRID_NAME)
            names_in_grid_list = [x.text for x in get_all_names_in_grid]
            for x in multiple_items or single_item:
                index = names_in_grid_list.index(x) + 1
                scroll_into_view(selenium, f"({SETUP_GRID_CHECKBOX})[{index}]")
                deselect_undeletable = by_xpath(selenium, f"({SETUP_GRID_CHECKBOX})[{index}]").click()
                wait_select_state_xpath(selenium, f"({SETUP_GRID_CHECKBOX})[{index}]", False)
            click_cog_icon = by_xpath(selenium, SETUP_COG_ICON).click()
            try:
                click_delete = try_by_xpath(selenium, SETUP_COG_DELETE).click()
            except Exception:
                # if nothing selected, the clicking the delete button will fail
                sub_grid = pass_data_to_step.get('sub_grid')
                print(f"No Project {sub_grid} Data To Delete")
        else:
            # confirm delete
            click_confirm = by_xpath(selenium, SETUP_POPUP_SAVE).click()
            wait_clickable(selenium, SETUP_GRID_BODY)


@given("I navigate back to project grid")
def go_to_back_to_project_grid(selenium):
    by_xpath(selenium, SETUP_BACK_TO_PARENT_GRID_BUTTON).click()
    wait_clickable(selenium, SETUP_GRID_BODY)


@given("I delete all automation created projects")
def delete_automation_projects(selenium, items_in_use):
    # reset grid
    selenium.refresh()

    # select all projects and deselect the ones I actually use
    select_all = by_xpath(selenium, SETUP_GRID_SELECT_ALL).click()
    get_all_project_names = by_xpath_multiple(selenium, SETUP_GRID_NAME)
    all_project_names = [x.text for x in get_all_project_names]
    my_projects = items_in_use[1]
    for x in my_projects:
        index = all_project_names.index(x) + 1
        project = by_xpath(selenium, f"({SETUP_GRID_CHECKBOX})[{index}]")
        ActionChains(selenium).move_to_element(project).click().perform()
        wait_select_state_xpath(selenium, f"({SETUP_GRID_CHECKBOX})[{index}]", False)
    click_cog_icon = by_xpath(selenium, SETUP_COG_ICON).click()
    try:
        click_delete = try_by_xpath(selenium, SETUP_COG_DELETE).click()
    except Exception:
        # if nothing selected, the clicking the delete button will fail
        print("No Projects To Delete")
    else:
        # deselect any items preventing deletion
        multiple_items = [] # workaround for not having to duplicate the 'for' statement in code below
        single_item = []
        if by_xpath(selenium, SETUP_POPUP_FRONT + SETUP_POPUP_HEADER).text == "Unable to delete":
            try: # if multiple items to delete they are in ul
                get_undeletable_items = try_by_xpath_multiple(selenium, SETUP_POPUP_FRONT + SETUP_POPUP_CONTENT_LIST)
                for x in get_undeletable_items:
                    multiple_items.append(x.text)
            except Exception: # if list of items not found, there is single item to delete and item name will be between quotes
                get_undeletable_item = try_by_xpath(selenium, SETUP_POPUP_FRONT + SETUP_POPUP_CONTENT).text
                undeleteable_name = shlex.split(get_undeletable_item)
                single_item.append(undeleteable_name[2])
            click_to_dismiss = by_xpath(selenium, SETUP_POPUP_FRONT + SETUP_POPUP_CONFRIM).click()
            get_all_names_in_grid = by_xpath_multiple(selenium, SETUP_GRID_NAME)
            names_in_grid_list = [x.text for x in get_all_names_in_grid]
            for x in multiple_items or single_item:
                index = names_in_grid_list.index(x) + 1
                project = by_xpath(selenium, f"({SETUP_GRID_CHECKBOX})[{index}]")
                ActionChains(selenium).move_to_element(project).click().perform()
                wait_select_state_xpath(selenium, f"({SETUP_GRID_CHECKBOX})[{index}]", False)
            click_cog_icon = by_xpath(selenium, SETUP_COG_ICON).click()
            try:
                click_delete = try_by_xpath(selenium, SETUP_COG_DELETE).click()
            except Exception:
                # if nothing selected, the clicking the delete button will fail
                print("No Projects To Delete")
            else:
                click_confirm = by_xpath(selenium, SETUP_POPUP_SAVE).click()
                wait_clickable(selenium, SETUP_GRID_BODY)
        else:
            click_confirm = by_xpath(selenium, SETUP_POPUP_SAVE).click()
            wait_clickable(selenium, SETUP_GRID_BODY)


@given("I delete all automation created and downloaded checklists")
def delete_automation_checklists(selenium, items_in_use):
    # select all checklists and deselect the ones I actually use
    select_all = by_xpath(selenium, SETUP_GRID_SELECT_ALL).click()
    get_all_checklist_codes = by_xpath_multiple(selenium, SETUP_GRID_CODE)
    all_checklist_codes = [x.text for x in get_all_checklist_codes]
    my_checklists = items_in_use[0]
    for x in my_checklists:
        index = all_checklist_codes.index(x) + 1
        checklist = by_xpath(selenium, f"({SETUP_GRID_CHECKBOX})[{index}]")
        ActionChains(selenium).move_to_element(checklist).click().perform()
        wait_select_state_xpath(selenium, f"({SETUP_GRID_CHECKBOX})[{index}]", False)
    click_cog_icon = by_xpath(selenium, SETUP_COG_ICON).click()
    try:
        click_delete = try_by_xpath(selenium, SETUP_COG_DELETE).click()
    except Exception:
        # if nothing selected, the clicking the delete button will fail
        print("No Checklists To Delete")

    else:
        # deselect any items preventing deletion
        multiple_items = [] # workaround for not having to duplicate the 'for' statement in code below
        single_item = []
        if by_xpath(selenium, SETUP_POPUP_FRONT + SETUP_POPUP_HEADER).text == "Unable to delete":
            try: # if multiple items to delete they are in ul
                get_undeletable_items = try_by_xpath_multiple(selenium, SETUP_POPUP_FRONT + SETUP_POPUP_CONTENT_LIST)
                for x in get_undeletable_items:
                    multiple_items.append(x.text)
            except Exception: # if list of items not found, there is single item to delete and item name will be between quotes
                get_undeletable_item = try_by_xpath(selenium, SETUP_POPUP_FRONT + SETUP_POPUP_CONTENT).text
                undeleteable_name = shlex.split(get_undeletable_item)
                single_item.append(undeleteable_name[2])
            click_to_dismiss = by_xpath(selenium, SETUP_POPUP_FRONT + SETUP_POPUP_CONFRIM).click()
            get_all_names_in_grid = by_xpath_multiple(selenium, SETUP_GRID_NAME)
            names_in_grid_list = [x.text for x in get_all_names_in_grid]
            for x in multiple_items or single_item:
                index = names_in_grid_list.index(x)
                checklist = by_xpath(selenium, f"({SETUP_GRID_CHECKBOX})[{index}]")
                ActionChains(selenium).move_to_element(checklist).click().perform()
                wait_select_state_xpath(selenium, f"({SETUP_GRID_CHECKBOX})[{index}]", False)
            click_cog_icon = by_xpath(selenium, SETUP_COG_ICON).click()
            try:
                click_delete = try_by_xpath(selenium, SETUP_COG_DELETE).click()
            except Exception:
                # if nothing selected, the clicking the delete button will fail
                print("No Checklists To Delete")
            else:
                click_confirm = by_xpath(selenium, SETUP_POPUP_SAVE).click()
                wait_clickable(selenium, SETUP_GRID_BODY)
        else:
            click_confirm = by_xpath(selenium, SETUP_POPUP_SAVE).click()
            wait_clickable(selenium, SETUP_GRID_BODY)


@given(parsers.parse("I expand References in {grid_name} details panel"))
def expand_ref_in_details(selenium, grid_name):
    if grid_name == "project":
        expand_references = by_xpath(selenium, PROJECT_DETAILS_REFERENCES).click()
    elif grid_name == "checklist":
        expand_references = by_xpath(selenium, CHECKLIST_DETAILS_REFERENCES).click()


@given("I delete all all reference attachments")
def delete_all_reference_attachments(selenium):
    get_all_reference_attachments = by_xpath_multiple(selenium, SETUP_REFERENCE_ATTACHMENT_OPTIONS)
    for x in get_all_reference_attachments[:-1]:
        x.click()
        by_xpath(selenium, SETUP_REFERENCE_ATTACHMENT_DELETE).click()
        by_xpath(selenium, SETUP_POPUP_CONFRIM).click()
        wait_clickable(selenium, TOAST_MESSAGE)
        assert "deleted" in by_xpath(selenium, TOAST_MESSAGE).text
    print(f"{len(get_all_reference_attachments)} reference attachments deleted")


@given("I delete all responsible parties added by automation")
def delete_rps(selenium, items_in_use):
    trashcan_icons = by_xpath_multiple(selenium, RP_DELETE_ICON)
    trashcan_icon_list = [x for x in trashcan_icons]
    for x in trashcan_icon_list:
        x.click()
        time.sleep(1)
    my_responsible_parties = items_in_use[2]
    for x in my_responsible_parties:
        index = trashcan_icon_list.index(x) + 1
        by_xpath(selenium, f"({RP_DELETE_ICON})[{index}]").click()
    save_changes = by_xpath(selenium, SETUP_ALL_SAVE_BUTTONS).click()
    try:
        wait_clickable(selenium, SETUP_ALL_SAVED_CONFIRMATION_MASK)
    except Exception:
       pytest.skip("No responsible parties to delete")

