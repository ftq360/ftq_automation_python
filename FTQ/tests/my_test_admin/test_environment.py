from urllib.parse import urlparse

from pytest_bdd import scenario, given

from functions import *

# CHANGE THIS TEXT FOR EACH NEW RELEASE
VERSION_BASE_DEV = "v20"
VERSION_BASE_PROD = "v20"

# Universal definitions
NOW = datetime.datetime.now()
PATH_TO_DWN_FOLDER = r"C:\Workspace\VersionTestFiles"
FILE_TYPE = "\*txt"


@pytest.mark.xray('DEV-6765')
@scenario("../../features/my_test_admin/environment_version_control.feature", "environment control")
def test_environment_version_page():
    """test that the environment is set up correctly via info on version control page"""


@given("I run environmental version control test")
def environment_version_page(selenium):
    # Gather environment data
    url = urlparse(selenium.current_url).hostname
    parsed_url = url.split('.')
    ENV = parsed_url[0]

    # Gather version data
    version_page_data = by_xpath(selenium, ENV_DATA).text
    version_list = version_page_data.splitlines()
    version_number = version_list[0]
    built_from = version_list[1]
    database = version_list[2]
    environment = version_list[3]
    base_url = version_list[4]
    base_folder = version_list[5]

    # Record version number in txt file only if it is new
    get_all_files = glob.glob(PATH_TO_DWN_FOLDER + FILE_TYPE)
    most_recent_file = get_all_files[-1]
    f = open(most_recent_file, "r")
    last_version_number = f.read()
    version_number_split_list = version_number.split(":")
    if last_version_number != version_number_split_list[1]:
        f = open(f'C:\\Workspace\\VersionTestFiles\\{ENV}-version-number_{NOW.year}-{NOW.month}-{NOW.day}_{NOW.hour}-{NOW.minute}.txt',
                 'w')
        f.write(version_number_split_list[1])
        f.close()

    # Check that the version number begins with the release version and current year
    # add f".{NOW.year}" for further assertion later in the year
    if any(x.isdigit() for x in ENV):
        assert VERSION_BASE_DEV.strip("v") in version_number
    else:
        assert VERSION_BASE_PROD.strip("v") in version_number

    # Check that it is built from the version number's master branch
    if ENV == "devprod" or ENV == "www":
        assert "devprod" in built_from
    else:
        assert ENV in built_from

    # Check that the database it is pointing to is correct (should be FTQ_ and the version number)
    if ENV == "devprod" or ENV == "www":
        assert "FTQ" in database
    elif ENV == "devtest":
        assert "FTQ_TEST" in database
    else:
        assert "FTQ_" + text_to_credential(ENV) in database

    # Check that the base url matches what it should be
    assert url in base_url

    # Check that it is pointed correctly to either a dev or production environment
    if ENV == "www":
        assert "prod" in environment
    else:
        assert "dev" in environment

    # Check that the correct version number is pulled in base folder
    if ENV == "www":
        assert "Prod" in base_folder
    else:
        assert ENV in base_folder
