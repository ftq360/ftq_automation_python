from pytest_bdd import scenario, then, given

from functions import *
from locators.inspection_page_locators import *


@pytest.mark.xray('DEV-5371')
@scenario("../../features/offline/sync_inspections_saved_to_device.feature", "Sync 5 inspections saved to device")
def test_sync_5_inspections_saved_to_device():
    """test syncing 5 inspections to server that were
    created in offline mode and saved to device"""


@given("I create 5 inspections")
def create_5_inspections(selenium, cred_data):
    for x in range(0, 5):
        go_to_create_insp = selenium.get(cred_data['BASE_URL'] + cred_data['CREATE_INSPECTION'])
        try:
            select_division = by_xpath(selenium, SPECIFIC_SELECTION(selenium, "(Undefined)")).click()
        except:
            pass
        select_project1 = by_xpath(selenium, SPECIFIC_SELECTION(selenium, "Project 1 (1)")).click()
        select_checklist_general = by_xpath(selenium,
                                            SPECIFIC_SELECTION(selenium,
                                                               "General-Punch List Deficiency (CT147)")).click()
        select_rp = by_xpath(selenium, SPECIFIC_SELECTION(selenium, "FTQ Automation Group (D1)")).click()
        check_inspection_created_successfully = wait_clickable(selenium, INSP_HEADER)
        assert "Inspection" in selenium.title 
        click_save_btn = by_xpath(selenium, INSP_SAVE_BUTTON).click()
        wait_clickable(selenium, INSP_SAVED_TO_DEVICE_TEXT)
    number_on_signal_icon = by_xpath(selenium, SIGNAL_ICON_INSP_STATUS_UNSYNCED_NUMBER).get_attribute("outerText")
    assert number_on_signal_icon == "5"


@then("all 5 inspections automatically sync to server")
def create_5_inspections(selenium):
    wait_clickable_long(selenium, SIGNAL_ICON_INSP_STATUS_GREEN_CHECK)
