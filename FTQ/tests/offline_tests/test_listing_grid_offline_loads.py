from pytest_bdd import scenario, when, then
from functions import *
import datetime
from locators.inspection_page_locators import *
from locators.selection_process_locators import *


@pytest.mark.xray('DEV-5059')
@scenario("../../features/offline/listing_grids_load_offline.feature", "Show Recent Inspections Loads Offline")
def show_recent_inspections_offline():
    """test that show recent inspections listing grid works as expected when offline"""


@when("I enter dynamic note {todays_date} in the Notes field in the inspection header")
def enter_note_in_inspection_header(selenium, pass_data_to_step):
    by_xpath(selenium, INSP_HEADER_NOTE).click()
    note = str(datetime.datetime.now())
    by_xpath(selenium, INSP_HEADER_NOTE_INPUT).send_keys(note)
    pass_data_to_step['note'] = note


@when("I click on 'Show Recent Inspections' button on Create Inspection page")
def click_show_recent_inspections(selenium):
    try:
        select_division = by_xpath(selenium, SPECIFIC_SELECTION(selenium, "(Undefined)")).click()
    except:
        pass
    select_project1 = by_xpath(selenium, SPECIFIC_SELECTION(selenium, "Project 1 (1)")).click()
    select_recent_insp_dropdown = by_xpath(selenium, SP_SHOW_RECENT_DROPDOWN).click()
    wait_clickable(selenium, SP_SHOW_RECENT_GRID)


@then("I should see inspections my inspection with the note listed")
def inspection_listed_with_note(selenium, pass_data_to_step):
    note = pass_data_to_step.get('note')
    assert note == by_xpath(selenium, SP_SHOW_RECENT_GRID_FIRST_ROW + SP_SHOW_RECENT_GRID_NOTE).text


@then("my offline inspection sync to server successfully")
def offline_inspection_syncs_to_server(selenium):
    selenium.refresh()
    wait_clickable_long(selenium, SIGNAL_ICON_GREEN)
