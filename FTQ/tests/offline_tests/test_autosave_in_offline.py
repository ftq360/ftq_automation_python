from pytest_bdd import scenario, then, when

from functions import *
from locators.inspection_page_locators import *


@pytest.mark.skip('Unstable')
@pytest.mark.xray('DEV-5068')
@scenario("../../features/offline/autosave_in_offline.feature", "Autosave In Offline")
def test_autosave_in_offline():
    """test autosave in offline mode works as expected"""


@when("I enter 'note' in the Location/Ref field in the inspection header")
def enter_location_note_in_inspection_header(selenium):
    by_xpath(selenium, INSP_HEADER_LOCATION_NOTE).click()
    by_xpath(selenium, INSP_HEADER_LOCATION_NOTE).send_keys("note")


@then("data on inspection should still be present")
def data_still_present(selenium):
    assert by_xpath(selenium, INSP_HEADER_NOTE).text == "note"
    assert by_xpath(selenium, INSP_HEADER_LOCATION_NOTE).get_attribute("value") == "note"
    assert by_xpath(selenium, INSP_HEADER_STATUS).get_attribute("value") == "Fail (final)"
    checkpoint = INSP_CHECKPOINT(selenium, 'Footer depth and width per plan and code.')
    scroll_into_view(selenium, by_xpath(selenium, checkpoint))
    wait_clickable(selenium, checkpoint + CP_ATTACHMENT)
    opn_status = by_xpath(selenium, checkpoint + CP_OPN_STATUS_FROM_CHECK_NAME)
    assert selenium.execute_script("return arguments[0].childNodes[1].checked;", opn_status) == True
