from pytest_bdd import scenario, when, then

from functions import *
from locators.inspection_page_locators import *
from locators.selection_process_locators import *


@pytest.mark.skip('Unstable')
@pytest.mark.xray('DEV-5074')
@scenario("../../features/offline/offline_equipment_selection.feature", "Offline equipment selection")
def test_offline_equipment_selection():
    """test use of selecting equipment in offline mode"""


@when("I create an inspection via project 2, any equipment checklist")
def create_equipment_inspection(selenium):
    try:
        select_division = by_xpath(selenium, SPECIFIC_SELECTION(selenium, "(Undefined)")).click()
    except:
        pass
    select_project2 = by_xpath(selenium, SPECIFIC_SELECTION(selenium, "Project 2 (2)")).click()
    select_equipment = by_xpath(selenium, SPECIFIC_SELECTION(selenium, "equipment (2-1)")).click()
    select_equipment_checklist = by_xpath(selenium, SP_SELECT_SECOND_AVAILABLE).click()
    select_rp = by_xpath(selenium, SPECIFIC_SELECTION(selenium, "FTQ Automation Group (D1)")).click()
    check_inspection_created_successfully = wait_clickable(selenium, INSP_HEADER)
    assert "Inspection" in selenium.title 


@then("equipment is listed on inspection header")
def equipment_listed_on_insp(selenium):
    equipment_listed = by_xpath(selenium, INSP_HEADER_EQUIPMENT)
    assert equipment_listed.text == "Equipment: equipment (2-1)"
