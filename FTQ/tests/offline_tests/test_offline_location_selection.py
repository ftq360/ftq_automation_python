import pytest
from pytest_bdd import scenario, then

from functions import by_xpath
from locators.inspection_page_locators import INSP_HEADER_LOCATION


#OBSOLETE
@pytest.mark.skip('Unstable')
@pytest.mark.xray('DEV-5092')
@scenario("../../features/offline/offline_location_selection.feature", "Offline location selection")
def test_offline_location_selection():
    """test use of selecting location in offline mode"""


@then("location is listed on inspection header")
def location_listed_on_insp(selenium):
    location_listed = by_xpath(selenium, INSP_HEADER_LOCATION)
    assert location_listed.text == "Location"
