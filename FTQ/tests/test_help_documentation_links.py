from pytest_bdd import scenario, when, then, parsers

from functions import *


#OBSOLETE
# @pytest.mark.uc_Chrome
@pytest.mark.skip('Unstable')
@pytest.mark.xray('DEV-7058')
@scenario("../features/help_documentation_links.feature", "Help Documentation Links Correct")
def test_help_documentation_links():
    """test that the help documentation links are correct and work as expected"""


@when(parsers.parse("I navigate to {ftq_page} page via menu"))
def find_page_preference(selenium, ftq_page, pass_data_to_step):
    original_window = selenium.current_window_handle
    pass_data_to_step['original_window'] = original_window
    time.sleep(2)
    menu_item = click_via_menu(selenium, ftq_page)
    menu_item.click()
    try:
        modal_appears = wait_clickable_short(selenium, SP_DICT_LOADING_MODAL_OPEN)
        loading_complete = wait_present(selenium, SP_DICT_LOADING_MODAL_CLOSED)
    except:
        pass
    page_title = by_xpath(selenium, "//title")
    assert ftq_page in page_title.text


@when('I click the help documentation link')
def step_when_click_help_documentation_link(selenium):
    click_help_menu = by_xpath(selenium, GL_HELP_ICON).click()
    click_help_link = by_xpath(selenium, GL_HELP_LINK).click()


@then(parsers.parse('I should be brought to the correct help {documentation_page}'))
def step_then_brought_to_correct_help_page(selenium, documentation_page, pass_data_to_step):
    original_window = pass_data_to_step.get('original_window')
    new_window_handles = selenium.window_handles
    new_window_handles.remove(original_window)
    selenium.switch_to_window(new_window_handles[0])
    documentation_page_url = selenium.current_url
    assert documentation_page in documentation_page_url


    # help_title_text = by_xpath(selenium, "//h1").text
    # assert help_title_text == documentation_page
