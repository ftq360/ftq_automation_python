Feature: Create Project Data

  @p2
  Scenario: Create Project Location
    Given I login as data creation user
    And I navigate to Project Setup page
    And I select 'Project for Data Creation' project in new project setup grid
    And I navigate to project locations grid
    And I add a new location
    And I navigate to Create Inspection page
    Then I should see 'Loading data' modal
    And I should wait until 'Loading data' modal will be closed
    When I get to location selection page
    Then my new location is listed to select from
