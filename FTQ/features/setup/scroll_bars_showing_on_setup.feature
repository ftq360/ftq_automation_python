Feature: Setup text fields

  @p2
  Scenario Outline: Scroll bars showing on setup
	  Given I login as admin user
	  When I navigate to <setup_page>
	  Given I click Add New on the setup page
	  And I add 10+ items into the text box
	  Then I should see a scroll bar in the text field
	  Examples:
		| setup_page              |
		| checklist setup         |
		| project setup           |
