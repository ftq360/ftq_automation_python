Feature: Import

  @p2
  Scenario: User Import
	  Given I login as admin user
	  And I navigate to Import page
	  When I import a user CSV file
	  Then the import should be finalized without any errors
	  Given I navigate to user setup page
	  Then I should see the import present on user setup grid
