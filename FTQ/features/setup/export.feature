Feature: Export

  @p2
  Scenario: Export Community
    Given I login as admin user
    And I navigate to Project Setup page
    When I click on the Settings cog
    And I select Download CSV
    Then exported data matches project setup page
