Feature: Create Project Data

  @p2
  Scenario: Create Project Equipment
    Given I login as data creation user
    And I navigate to Project Setup page
    And I select 'Project for Data Creation' project in new project setup grid
    And I navigate to project equipment grid
    And I add a new equipment
    And I navigate to Create Inspection page
    Then I should see 'Loading data' modal
    And I should wait until 'Loading data' modal will be closed
    When I get to equipment selection page
    Then my new equipment is listed to select from
    And I am given the option to select No Equipment
