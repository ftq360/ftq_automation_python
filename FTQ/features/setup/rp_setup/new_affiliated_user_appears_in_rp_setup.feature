Feature: Responsible Party Setup

  	@p1
	Scenario: New Affiliated User Appears in RP Setup
	  Given I login as admin user
	  And I navigate to User Setup page
	  When I add a new user
	  And I set the Responsible Party affiliation for the user to 'Responsible Party 2'
	  Given I navigate to Responsible Party Setup page
	  And I select Responsible Party 'Responsible Party 2' in Responsible Party setup grid
	  And I navigate to Users grid for the RP
	  And I set grid filter to 'All'
	  Then I should see the new user listed in the Users grid for the RP