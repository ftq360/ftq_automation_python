Feature: Responsible Party Setup

  	@p1
	Scenario: Create New Responsible Party
	  Given I login as admin user
	  And I navigate to Responsible Party Setup page
	  And I click Add New on Responsible Party setup page
	  When I enter 'RP' text in Checkpoint Name field
	  And I click Create RP button to create the rp (rp1)
	  Then I see the new RP created successfully
