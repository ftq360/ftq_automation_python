Feature: Responsible Party Setup

  	@p1
	Scenario: Edit Responsible Party
	  Given I login as admin user
	  And I navigate to Responsible Party Setup page
	  When I select a responsible party
	  And I edit responsible party code
	  And I save changes on responsible party details panel
	  Then code change is reflected on the responsible party setup grid
	  When I select multiple responsible parties
	  And I change the emails of all selected responsible parties
	  And I save changes on responsible party multi-edit details panel
	  Then email changes are reflected on the responsible party setup grid