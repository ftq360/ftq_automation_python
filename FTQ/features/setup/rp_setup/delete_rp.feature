Feature: Responsible Party Setup

  	@p1
	Scenario: Delete Responsible Party
	  Given I login as admin user
	  And I navigate to Responsible Party Setup page
	  When I select a responsible party
	  When I click on the Settings cog
	  And I click 'Delete'
	  Then I should see 'Delete Responsible Party' notification window
	  When I click in popup to confirm
	  Then I should see the RP deleted from the grid