Feature: Auto Options

  @checklist_setup @p1
  Scenario: Auto Options as Buttons
    Given I login as admin user
    And I navigate to checklist setup page
    When I select 'Auto Options Checklist' checklist
    Given I click Go To Checkpoints button
    When I set all checkpoints to inactive
    And I add a new checkpoint
    And I expand 'Note Options' in checkpoint details panel
    And I add 3 options in the Options text box
    And I select the auto-options button toggle
    And I click save button on checkpoint details panel
    And I create an inspection via project 1, checklist 'CT125'
    And I select an option button for my newly created checkpoint
    Then I should see the text from the selection populated in the checkpoint notes
    And I should see 'Synced to server' inspection status