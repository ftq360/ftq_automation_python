Feature: Duplicate, deactivate, delete checkpoints

  @checklist_setup @p1
  Scenario: Duplicate, deactivate, delete multiple checkpoints
    Given I login as admin user
    And I navigate to checklist setup page
    When I select a checklist
    And I go to checkpoints
    And I select multiple checkpoints
    And I click the '...' button in checkpoint details panel
    And I click 'Duplicate' in '...' checkpoint details panel options
    Then the checkpoints are successfully duplicated
    When I click '...' button in checkpoint details panel
    And I click 'Deactivate' in '...' checkpoint details panel options
    Then the checkpoints are successfully deactivated
    When I click '...' button in checkpoint details panel
    And I click 'Delete' in '...' checkpoint details panel options
    Then the checkpoints are successfully deleted
