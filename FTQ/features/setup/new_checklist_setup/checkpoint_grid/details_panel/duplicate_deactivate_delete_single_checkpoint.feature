Feature: Checklist Setup Details Panel

  @checklist_setup @p1
  Scenario: Duplicate, deactivate and delete a single checkpoint in details panel
    Given I login as admin user
    And I navigate to checklist setup page
    When I select a checklist
    And I go to checkpoints
    And I select a checkpoint
    And I click the <button_option> button in <button_location>
    Then the checklist is <button_action>
