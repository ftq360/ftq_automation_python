Feature: Checklist Setup Details Panel

  @checklist_setup @p2
  Scenario: Discard checkpoint details panel changes
    Given I login as admin user
    And I navigate to checklist setup page
    When I select a checklist
    And I go to checkpoints
    And I select a checkpoint
    When I edit checkpoint code
    And I click on the main checkpoint grid
    Then a checkpoint details confirmation pop up appears
    When I click cancel on checkpoint details confirmation pop up
    Then my changes are still present in checkpoint details panel but not saved
    When I click discard changes button on bottom of checkpoint details panel
    Then my changes checkpoint details panel changes are discarded
