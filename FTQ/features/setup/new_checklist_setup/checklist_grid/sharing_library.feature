Feature: FTQ360 Sharing Library

  @p2
  Scenario: Sharing Library
      Given I login as admin user
      And I navigate to checklist setup page
      When I click the dropdown on Add New button on checklist setup page
      And I click 'Browse Library' option on the dropdown
      And I select a checklist from the options in the sharing library
      And I click 'Add Checklist' button
      Then I should see the new checklist added to the checklist grid
