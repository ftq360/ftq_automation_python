Feature: Duplicate, deactivate, delete checklists

  @checklist_setup @p1
  Scenario Outline: Duplicate, deactivate, delete multiple checklists
    Given I login as admin user
    And I navigate to checklist setup page
    When I select multiple checklists
    And I click the '...' button in checklist multi-edit details panel
    And I click '<details_button>' in '...' checklist multi-edit details panel options
    Then the checklists are successfully <button_action>
    Examples:
      | details_button | button_action |
      | duplicate      | duplicated    |
      | deactivate     | deactivated   |
      | delete         | deleted       |
