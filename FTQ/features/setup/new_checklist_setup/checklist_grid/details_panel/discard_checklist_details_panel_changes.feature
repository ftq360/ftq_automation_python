Feature: Checklist Setup Details Panel

  @checklist_setup @p1
  Scenario: Discard checklist details panel changes
    Given I login as admin user
    And I navigate to checklist setup page
    When I select a checklist
    And I edit checklist code
    And I click on the main checklist grid
    Then a checklist details confirmation pop up appears
    When I click cancel on checklist details confirmation pop up
    Then my changes are still present in checklist details panel but not saved
    When I click discard changes button on bottom of checklist details panel
    Then my changes checklist details panel changes are discarded
