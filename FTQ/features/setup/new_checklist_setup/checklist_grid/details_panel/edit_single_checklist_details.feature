Feature: Checklist Setup Details Panel

  @checklist_setup @p1
  Scenario: Edit single checklist details
        Given I login as admin user
        And I navigate to checklist setup page
        When I select checklist code 'CT457'
        And I edit checklist title
        And I save changes on checklist details panel
        Then changes are reflected on the checklist setup grid
