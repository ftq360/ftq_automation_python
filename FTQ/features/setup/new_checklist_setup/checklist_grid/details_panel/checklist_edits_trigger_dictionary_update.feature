Feature: Checklist Setup

  @p2
  Scenario: Checklist Edits Trigger Dictionary Update
    Given I login as admin user
    And I navigate to checklist setup page
    When I select 'Checklist for dictionary updates' checklist
    And I change the checklist category and save
    And I save changes on checklist details panel
    Given I navigate to Create Inspection page
    Then I should see 'Loading data' modal
    And I should wait until 'Loading data' modal will be closed