Feature: Checklist Setup Details Panel

  @checklist_setup @p1
  Scenario: Duplicate, deactivate and delete a single checklist
    Given I login as admin user
    And I navigate to checklist setup page
    When I select checklist code 'CT457'
    And I click the '...' button in checklist details panel
    And I click 'duplicate' in '...' checklist details panel options
    Then the checklist is successfully duplicated
    When I click the '...' button in checklist details panel
    And I click 'deactivate' in '...' checklist details panel options
    Then the checklist is successfully deactivated
    When I click the '...' button in checklist details panel
    And I click 'delete' in '...' checklist details panel options
    Then the checklist is successfully deleted
