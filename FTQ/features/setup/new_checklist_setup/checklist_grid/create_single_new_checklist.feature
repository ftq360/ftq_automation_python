Feature: New Checklist Setup - Creating New

  @checklist_setup @p1
  Scenario: Create single new checklist
    Given I login as admin user
    And I navigate to checklist setup page
    And I click Add New on checklist setup page
    When I enter 'Automation Checklist' text in Checklist Name field
    And I change checklist type to 'Equipment'
    And I click Create Checklist button to create the checklist (checklist1)
    Given I click Go To Checkpoints button
    When I enter 'Quality Checkpoint' text in Checkpoint Name field
    And I click Create Checkpoint button to create the checkpoint (checkpoint1)
    Given I navigate to Create Inspection page
    When I create an inspection with checklist1
    Then inspection created successfully
