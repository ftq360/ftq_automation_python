Feature: New Checklist Setup - Creating New

  @checklist_setup @p1
  Scenario: Create multiple new checklists
    Given I login as admin user
    And I navigate to checklist setup page
    And I click Add New on checklist setup page
    When I enter both 'Automation Checklist1' and 'Automation Checklist2' text in Checklist Name field
    And I click Create Checklist button to create the checklists
    Then both checklists created successfully
