Feature: New Checklist Setup

  @checklist_setup @p1
  Scenario: Create New Building Block Checklist
    Given I login as admin user
    And I navigate to checklist setup page
    And I click Add New on checklist setup page
    When I change checklist type to 'Building Block'
    And I enter 'Automation BB Checklist' text in Checklist Name field
    And I click Create Checklist button to create the checklist
    Then the checklist is created successfully