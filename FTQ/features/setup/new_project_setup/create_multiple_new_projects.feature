Feature: New project Setup

  @project_setup @p1
  Scenario: Create multiple new projects
    Given I login as admin user
    And I navigate to project setup page
    And I click Add New on project setup page
    And I enter both 'z_Automation Project1' and 'Automation Project2' text in Project Name field
    And I click Create Projects button to create the projects
    Then both projects created successfully
