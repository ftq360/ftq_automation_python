Feature: Project Setup

  @p2
  Scenario Outline: Edits To sub_grids Trigger Dictionary Updates
	  Given I login as admin user
	  And I navigate to project setup page
	  And I select 'Project for Dictionary Updates' project in new project setup grid
	  And I navigate to project <sub_grid> grid
	  And I select <item>
	  And I make edits to the <item>
	  And I save changes on <sub_grid> details panel
	  And I navigate to Create Inspection page
	  Then I should see 'Loading data' modal
	  And I should wait until 'Loading data' modal will be closed
	  Examples:
		| sub_grid  | item        |
		| locations | Location 1  |
		| equipment | Equipment 1 |
		| itp       | ITP 1       |
