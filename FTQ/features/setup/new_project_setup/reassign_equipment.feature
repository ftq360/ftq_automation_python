Feature: Project Equipment

  @p2
  Scenario: Reassign Equipment
	  Given I login as admin user
	  And I navigate to project setup page
	  And I select 'Project for Data Creation' project in new project setup grid
	  And I navigate to project equipment grid
	  When I select an equipment
	  And I click reassign button in details panel
	  And I change equipment project to 'Project for Selection Process Order (45)'
	  Then the equipment is no longer listed in the grid
	  When I go back to parent grid
	  And I deselect all in setup grid
	  Given I select 'Project for Selection Process Order' project in project setup grid
	  And I navigate to project equipment grid
	  Then I see the equipment listed in the grid