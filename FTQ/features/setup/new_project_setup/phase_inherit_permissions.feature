Feature: Project Setup

  @p2
  Scenario: Phase Inherit Permissions
      Given I login as admin user
      And I navigate to Import page
      And I import a project phase CSV file
      And I navigate to Project Setup page
      And I select 'Project for permissions' project in project setup grid
      And I navigate to project phases grid
      When I create a new phase
      Given I navigate to User Setup Page
      When I select 'standard_user' user on User Setup page
      Then permissions for both new phases were inherited correctly
