Feature: Project Setup Details Panel

  @project_setup @p1
  Scenario: Duplicate Single Project
    Given I login as admin user
    And I navigate to project setup page
    And I select 'Project for duplication' project in project setup grid
    When I click on the Settings cog
    And I click 'Duplicate'
    Then I should see 'Duplicate Project' notification window
    When I click in popup to confirm
    Then I see the duplicated project created in the project grid
