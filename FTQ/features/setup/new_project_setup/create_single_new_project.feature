Feature: New project Setup


  @project_setup @p1
  Scenario: Create single new project
    Given I login as admin user
    And I navigate to project setup page
    And I click Add New on project setup page
    And I enter 'z_Automation Project' text in project Name field
    And I click Create project button to create the project (project1)
    And I give my user create permissions in Project Access
    And I navigate to Create Inspection page
    When I create an inspection with project1
    Then inspection created successfully
