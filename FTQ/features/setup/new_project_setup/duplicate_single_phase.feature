Feature: Project Setup

  @project_setup @p1
  Scenario: Duplicate Single Phase
    Given I login as admin user
    And I navigate to project setup page
    And I select 'Project for duplication' project in project setup grid
    And I navigate to project phases grid
    And I select 'Phase for duplication' phase in phase setup grid
    When I click on the Settings cog
    And I click 'Duplicate'
    Then I should see 'Duplicate Phase' notification window
    When I click in popup to confirm
    Then I see the duplicated phase created in the phase grid
