Feature: New project Setup


  @project_setup @p1
  Scenario Outline: Navigate back to community grid from locations grid
    Given I login as admin user
    And I navigate to project setup page
    And I select 'Project 2' project in project setup grid
    And I navigate to project locations grid
    When I click on back to <back_to_grid>
    Then I am brought back to the project grid
    Examples:
      | back_to_grid |
      | back button  |
      | breadcrumb   |