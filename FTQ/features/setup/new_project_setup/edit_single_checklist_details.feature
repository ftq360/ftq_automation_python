Feature: Checklist Setup Details Panel

  @checklist_setup
  Scenario: Edit single checklist details
    Given I login as admin user
    And I navigate to checklist setup page
    When I select a checklist
    And I edit checklist code
    And I save changes on checklist details panel
    Then changes are reflected on the checklist setup grid
