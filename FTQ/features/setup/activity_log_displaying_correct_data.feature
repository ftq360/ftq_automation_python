Feature: Setup Activity Log

  @p2
  Scenario: Activity Log Displaying Correct Data
	Given I login as admin user
	And I navigate to checklist setup page
	When I select checklist code 'CT457'
	And I edit checklist title
	And I save changes on checklist details panel
	Then changes are reflected on the checklist setup grid
	Given I navigate to User Setup page
	When I add a new user
	Given I navigate to Activity Log page
	When I view activity log table
	Then I see updates made from user and checklist setup pages
