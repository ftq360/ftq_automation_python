Feature: Setup Breadcrumb Navigation

  @p2
  Scenario Outline: Item code showing in breadcrumb
	Given I login as admin user
	When I navigate to <setup_page>
	And I select an item
	And I then navigate to <subgrid>
	Then I should see the code of <item> in the breadcrumbs title
	Examples:
	  | setup_page 	    | subgrid     | item      |
	  | checklist setup | checkpoints | checklist |
	  | project setup   | phases      | project   |

