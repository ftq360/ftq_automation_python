Feature: Manage Columns

  @p2
  Scenario Outline: Manage Columns
    Given I login as admin user
    And I navigate to project setup page
    When I click on the Settings cog
    And I select Manage Columns
    Then the Manage Columns dialog appears
    When I drag and drop column '<drag_column>' to first position
    And I toggle Sequence column <visibility>
    And I click Save on manage columns dialog
    Then I should see new column settings in the grid
    Given I navigate to Create Inspection page
    And I navigate to project setup page
    Then I should see new column settings in the grid
    Examples:
      | drag_column  | visibility |
      | Code         | on         |
      | Project Name | off        |
