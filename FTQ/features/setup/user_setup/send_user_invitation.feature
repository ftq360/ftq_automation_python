Feature: User Setup

  @p1
  Scenario: Send User Invitation
	Given I login as admin user
	And I navigate to User Setup page
	When I select 'standard_user' user on User Setup page
	And I click 'Send Invitation'
	Then I see toast notification with 'Invitation sent' text
	Then user 'standard_user' receives an invitation email
