Feature: User Setup

  @p1
  Scenario: Create Users
	Given I login as admin user
	And I navigate to User Setup page
	When I add a new user
	And I set user password
	Then that user can login successfully
