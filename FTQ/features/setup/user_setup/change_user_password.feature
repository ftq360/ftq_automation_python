Feature: User Setup

  @p1
  Scenario: Change User Password
	Given I login as admin user
	And I navigate to User Setup page
	When I select 'password_user' user on User Setup page
	And I change user password
	Given I log out
	Then when user 'password_user' logs back in, only the new password works
