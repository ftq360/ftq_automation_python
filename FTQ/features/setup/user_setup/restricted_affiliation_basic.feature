Feature: User Permissions

  @p2 @user_permissions
  Scenario: Restricted Affiliation Basic
	Given I login as admin user
	When I create an inspection via project 1, checklist 'CT150'
	And I click on 'NA' checkbox in 'Quality Checkpoint' checkpoint on inspection page
	Then Inspection syncs to server successfully
	Given I login as view permissions user
	And I navigate to Recent Inspections Listing page
	And I open the inspection
	When I click on 'FTQ' checkbox in 'Quality Checkpoint' checkpoint on inspection page
	Then I should be able to 'view' the inspection
	And Inspection syncs to server successfully
	Given I login as edit permissions user
	And I navigate to Recent Inspections Listing page
	And I open the inspection
	When I click on 'FTQ' checkbox in 'Quality Checkpoint' checkpoint on inspection page
	Then I should be able to 'edit' the inspection
	And Inspection syncs to server successfully
	Given I login as standard user
	And I navigate to Recent Inspections Listing page
	And I open the inspection
	When I click on 'QC' checkbox in 'Quality Checkpoint' checkpoint on inspection page
	Then I should be able to 'edit' the inspection
	And Inspection syncs to server successfully
	Given I login as no permissions user
	And I navigate to Recent Inspections Listing page
	And I open the inspection
	Then I should be able to 'not view' the inspection
