Feature: Account Management

  @p2
 Scenario Outline: Home Pages
    When I log in as <user>
    Then I see <page> as my home page
   Examples:
     | user                        | page                 |
     | unrestricted_qa_inspector   | create inspection    |
     | restricted_inspector        | open item dashboard  |
#     | unrestricted_qa_non-inspect | deficiencies archive |
