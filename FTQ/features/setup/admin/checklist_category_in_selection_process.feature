Feature: Checklist Category in Selection Process

  @p1
  Scenario: Checklist Category in Selection Process
    Given I login as admin user
    And I navigate to Account Management page
    And I navigate to 'Additional System Settings' process menu tab
    When I enable 'INSPECTION SELECTION STEPS: Checklist category is included in the selection process.'
    Given I save changes on 'Account Management' page
    And I log out
    And I login as admin user
    And I navigate to Create Inspection page
    Then I see category in the selection process
    Given I navigate to Account Management page
    And I navigate to 'Additional System Settings' process menu tab
    When I disable 'INSPECTION SELECTION STEPS: Checklist category is included in the selection process.'
    Given I save changes on 'Account Management' page
