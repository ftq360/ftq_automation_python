Feature: Date Formats

  @p2
  Scenario Outline: Group Date Formats
      Given I login as admin user
      And I navigate to Account Management page
      And I navigate to 'Additional System Settings' process menu tab
      When I change Date Format to <date_format> on account management page
      And I change Time Format to <time_format> on account management page
      Given I log out
      And I login as demoed user
      Then dates and times are displayed in <date_format> and <time_format> format for <page>
      Examples:
        | date_format                           | page                                      | time_format     |
        | MMxDDxYYYY                            | Create Inspection - Inspection Header     | 12-hour (amxpm) |
        | DDxMMxYYYY                            | Create Inspection - Attachment Timestamps | 12-hour (amxpm) |
        | YYYY.MM.DD                            | Create Inspection - Get PDF               | 24-hour         |
  #      | DD-MM-YYYY                            | Recent Inspections Listing                | 12-hour (am/pm) |
  #      | DD.MM.YYYY                            | Deficiency Archive Listing                | 12-hour (am/pm) |
        | YYYY-MM-DD                            | Inspection Activity Dashboard             | 12-hour (amxpm) |
        | MON DD, YYYY (abbreviated month name) | Queries                                   | 24-hour         |
        | DD MON YYYY (abbreviated month name)  | ITP Subgrid                               | 24-hour         |