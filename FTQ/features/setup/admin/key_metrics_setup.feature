Feature: Key Metrics Setup

  @p1
  Scenario: Key Metrics Setup
    Given I login as admin user
    And I navigate to Key Metric Setup page
    When I click on key metric with code 'KM1'
    And I update the name of the key metric
    And I change the key metric default value
    And I save changes on key metrics details panel
    And I create an inspection
    Then I should see the new KM1 changes on the checkpoint
