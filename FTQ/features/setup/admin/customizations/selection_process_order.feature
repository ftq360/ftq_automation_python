Feature: Account Customizations

  @p2
  Scenario Outline: Selection Process Order
  	Given I login as admin user
	And I navigate to Customization Preferences page
	And I set 'Setting for inspecting (without ITP)' to <insp_sequence>
	And I save changes on Customization Preferences page
	And I set 'Setting for inspecting by plan (ITP)' to <itp_sequence>
	And I save changes on Customization Preferences page
	And I log out
	And I login as admin user
	And I navigate to Create Inspection page
	Then I should see 'Loading data' modal
	And I should wait until 'Loading data' modal will be closed
	And I create an inspection with <insp_sequence>
	Given I navigate to Create Inspection page
	Then I create an inspection with <itp_sequence>
	Examples:
	  | insp_sequence | itp_sequence |
	  | 1        	  | 5            |
	  | 5             | 4            |
	  | 3             | 1            |
	  | 4             | 3            |
	  | 2             | 2            |
