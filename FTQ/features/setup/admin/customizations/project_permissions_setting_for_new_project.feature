Feature: Customization Preference Setting for Project Permissions

  @p2 @project_setup @customization_preferences @project_permissions
  Scenario: Project Permissions Setting for New Project
    Given I login as admin user
    And I navigate to Project Setup page
    When I add a new project
    And I view project permissions
    Then I see no users have any permissions for the new project