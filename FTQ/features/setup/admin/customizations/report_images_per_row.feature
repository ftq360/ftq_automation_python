Feature: Account Customizations

  @p2
  Scenario Outline: Report Images Per Row
      Given I login as admin user
      And I navigate to Customization Preferences page
      And I set 'Number of images per row in Inspection and Deficiency reports' to <imagePerRow>
      And I save changes on Customization Preferences page
      And I log out
      And I login as admin user
      When I create an inspection
      And I attach '4' files to 'Punch Item/Deficiency:' checkpoint on inspection page
      Then I should see 'Synced to server' inspection status
      When I click get PDF on the inspection page
      Then the PDF shows <imagePerRow> images per row
      Examples:
        | imagePerRow |
        | 2           |
        | 4           |
