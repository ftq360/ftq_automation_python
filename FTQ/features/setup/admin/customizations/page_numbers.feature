Feature: Account Customizations

  @p2
  Scenario Outline: Page Numbers
    Given I login as admin user
    And I navigate to Customization Preferences page
    And I find preference titled 'Setting for selectable number of rows on listing grids'
    And I change the page numbering to <page_number_option>
    And I save changes on Customization Preferences page
    And I log out
    And I login as admin user
    When I navigate to <page> page
    Then page number options shown are <page_number_option>
    Examples:
      | page_number_option | page                       |
      | b                  | recent inspections listing |
      | 50,100,200         | old project setup          |
      | 5,8,10             | checklist sharing library  |
