Feature: Customization Preference Setting for Project Permissions

  @p2 @user_setup @customization_preferences @project_permissions
  Scenario Outline: Project Permissions Setting for New User
      Given I login as admin user
      And I navigate to Customization Preferences page
      And I set 'New User automatic setting of ACTIVE checkbox on existing projects' to <setting_number>
      And I log out
      And I login as admin user
      And I navigate to User Setup page
      When I add a new user
      Then user is created with <permission> for all projects
      Examples:
        | setting_number | permission           |
        | 1             | view, edit and create |
        | 0             | no permissions        |