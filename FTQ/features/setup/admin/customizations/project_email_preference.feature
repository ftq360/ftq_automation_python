Feature: Preference for project email on inspection screen

  @p2 @customization_preferences @inspection_page @email
  Scenario Outline: Project Email Preference
      Given I login as admin user
      And I navigate to Account Management page
      And I navigate to 'Additional System Settings' process menu tab
      And I set style setting 'INSPECTION SCREEN EMAIL NOTIFICATION: The \"Send to\" email addresses will default to:' to <value>
      And I save changes on Customization Preferences page
      And I log out
      And I login as admin user
      When I create an inspection
      And I sync inspection to server
      And I click email button on inspection page
      Then I should see the recipient as <recipient>
      Examples:
        | value | recipient                   |
        | 0     | Emails                      |
        | 1     | automation+RP@ftq360.com    |
        | 2     | testing+project1@ftq360.com |
