Feature: Company Name Change

  @p2
  Scenario: Company Name Change
    Given I login as admin user
    And I navigate to Account Management page
    And I navigate to 'Company Details' process menu tab
    And I change Company Name on account management page
    And I log out
    And I login as admin user
    When I refresh page
    And I create an inspection
    And I click on 'Save' button on inspection page
    Then company name is changed under user menu
    When I click get PDF on the inspection page
    Then the PDF shows new company name
    When I click email button on inspection page
    Then new company name is shown in the email subject line
