Feature: Company Emails

  @p2 @account_management @email
  Scenario: BCC Company Address
	  Given I login as admin user
	  And I navigate to Account Management page
	  And I navigate to 'Company Details' process menu tab
	  When I add a new email address into the BCC email address field
	  Given I log out
	  And I login as demoed user
	  And I navigate to Run Reports Online page
	  And I select 'FTQ-401ax - Inspection Reports' report
	  When I click on 'Run report' button
	  Then I should see the report on Report History page
	  And report should complete processing
	  When I send the report to an invalid email address
	  Then the email is sent to the BCC email address
