Feature: Free Users

  @p2
  Scenario: Free Users
    Given I login as admin user
    Given I navigate to User Setup page
    When I select 'permissions_user' user on User Setup page
    Given I add create permissions for permissions_user
    And I navigate to Account Management page
    And I see how many Users are listed (userNum)
    And I see how many Free Users are listed (freeNum)
    And I navigate to User Setup page
    When I select 'permissions_user' user on User Setup page
    Given I remove create permissions for permissions_user
    And I navigate to Account Management page
    Then the number of users is (userNum - 1)
    And the number of free users is (freeNum + 1)
