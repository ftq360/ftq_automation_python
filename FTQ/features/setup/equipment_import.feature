Feature: Import

  @p1
  Scenario: Equipment Import
    Given I login as admin user
    And I navigate to Import page
    When I import a project equipment CSV file
    Then the import should be finalized without any errors
    Given I navigate to project setup page
    And I select 'Project for Data Creation' project in new project setup grid
    And I navigate to project equipment grid
    Then I should see the newly imported equipment listed
