Feature: References

  @p2
  Scenario: Reference File Limits
	Given I login as admin user
	And I navigate to project setup page
	And I select 'Project for References' project in new project setup grid
	When I add a reference attachment to the project that is over 305mb
	Then I see toast notification with 'stockvideo.mov exceeds the file size limit.' text
