Feature: References

  @p2
  Scenario: Uploading References
	Given I login as admin user
	And I navigate to Checklist Setup page
	When I select 'Checklist for References' checklist
	Then I can add a reference attachment to the checklist
	Given I navigate to project setup page
	And I select 'Project for References' project in new project setup grid
	Then I can add a reference attachment to the project
#	Given I navigate to Responsible Party Setup page
#	When I select 'Responsible Party for References' responsible party
#	Then I can add a reference attachment to the responsible party
