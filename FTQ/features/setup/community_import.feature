Feature: Import

  @p2
  Scenario: Community Import
    Given I login as admin user
    And I navigate to Import page
    When I import a project CSV file
    Then the import should be finalized without any errors
    Given I navigate to project setup page
    Then I should see the import present on project setup grid
