Feature: Details Panel

  @p2
  Scenario: Details Panel Expanded Sections are Sticky
	Given I login as admin user
	And I navigate to Checklist Setup page
	When I select 'Checklist for References' checklist
	And I expand the references section
	And I select 'Punch List Deficiency' checklist
	And I select 'Checklist for References' checklist
	Then references section is shown expanded
