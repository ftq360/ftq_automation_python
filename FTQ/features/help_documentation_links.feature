Feature: Help Documentation

  @p2
  Scenario Outline: Help Documentation Links Correct
        Given I login as admin user
        When I navigate to <ftq_page> page via menu
        When I click the help documentation link
        Then I should be brought to the correct help <documentation_page>
        Examples:
          | ftq_page                      | documentation_page                                 |
          | Start New Inspection          | Enter-a-New-Inspection                             |
          | List Recent Inspections       | View-Recent-Inspections-List-                      |
#          | Run Reports Online            | REPORTS                                            |
#          | Responsible Party Setup       | Add-Edit-or-Delete-Responsible-Party-Subcontractor |