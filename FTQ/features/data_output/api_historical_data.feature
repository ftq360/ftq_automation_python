Feature: API Historical Data

  @p2 @api @superadmin
  Scenario: API Historical Data
    Given I login as demoed user
    And I navigate to Inspection Archive Listing page
    When I search for the current year in Last Activity Date column
    And I sort Last Activity Date column by descending
    Then I see the inspection ID of the first inspection listed in descending (insp1)
    When I sort Last Activity Date column by ascending
    Then I see the inspection ID of the first inspection listed in ascending (insp2)
    Given I navigate to API Setup page
    And I navigate to 'API Initalization Data' process menu tab
    When I downloaded API query 1025
    Then both inspection IDs are present in the API query data
