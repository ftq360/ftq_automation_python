Feature: Favorited Reports

  @p2
  Scenario: Favorited User Affiliated Report
    Given I login as demoed user
	And I navigate to Run Reports Online page
	And I select 'FTQ-401ax - 401ax Favorited Inspector Affiliate Report' report
	When I click on 'Run report' button
	Then I should see the report on Report History page
