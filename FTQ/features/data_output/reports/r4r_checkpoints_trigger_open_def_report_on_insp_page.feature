Feature: R4R Checkpoints Trigger Open Deficiency Report on Inspection Page

  @p2
  Scenario: R4R Checkpoints Trigger Open Deficiency Report on Inspection Page
    Given I login as admin user
    When I create an inspection
    And I check R4R checkbox in 'Punch Item/Deficiency:' checkpoint
    Then Inspection syncs to server successfully
    And I am able to email an Open Deficiency Report
