Feature: Email Customizations and Additional Settings

  @p2
  Scenario: Inspection Email Subject and File Name Settings - BHP Custom
    Given I login as BHP user
    And I navigate to Activate Reports page
    And I navigate to 'Email Customization & Additional Settings' process menu tab
    And I change 'Filename template for Immediate Email Notifications (Inspection Checklist Screen)' setting to BHP Custom Format
    And I change 'Subject line template for Immediate Email Notifications (Inspection Checklist Screen)' setting to BHP Custom Format
    And I save changes on Activate Reports page
    And I log out
    And I login as BHP user
    And I navigate to Recent Inspections Listing page
    When I open a recent inspection
    Then I wait until Inspection ID value appears on inspection page
    When I email the inspection to the testing email address with a custom number to identify the email
    Then the email subject line is in BHP Custom Format and matches current data
    And the email filename is in BHP Custom Format and matches current data
