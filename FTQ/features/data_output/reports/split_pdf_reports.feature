Feature: Reports

  @p1
  Scenario Outline: Split PDF Reports
      Given I login as demoed user
      And I navigate to Account Management page
      And I navigate to 'Report Email Customization and Additional Settings' process menu tab
      And I change filename for split inspection to <split_report_setting>
      And I save changes on Email Customizations page
      Given I log out
      And I login as admin user
      And I navigate to Run Reports Online page
      When I run report 401ax with split report selected
      Then report should complete processing
      When I open the zipped report
      Then each inspection is in a separate PDF file
      And the title of each file is in <split_report_setting> format
    Examples:
      | split_report_setting                                               |
      | Inspection ID only                                                 |
      | Standard Printout (Project, Phase, current date and Inspection ID) |
