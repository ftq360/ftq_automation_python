Feature: Report Runner

  @p2
  Scenario Outline: Common Reports Return Correct Data
    Given I login as demoed user
    When I create an inspection
    And I enter 'automation note and current time' note in 'Punch Item/Deficiency:' checkpoint observation field
    And I attach file to 'Punch Item/Deficiency:' checkpoint on inspection page
    And I sync inspection to server
    And I wait 6 minutes for reporting tables to update
    Given I navigate to Run Reports Online page
    And I select a <report>
    And I select date range 'Today (until now)'
    When I click on 'Run report' button
    Then I should see the <report> on Report History page
    And report should complete processing
    And I should be able to download the <report>
    And I should be able to see the correct data for the  <report>
    Examples:
      | report                         |
      | FTQ-401ax - Inspection Reports |
      | Q1009 - Data Query 1009        |
      | Q1025 - Data Query 1025        |
	#	| FTQ-405ax - OL-Open and Ready for Review Deficiency Detail Report by Project |
