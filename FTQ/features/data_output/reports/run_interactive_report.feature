Feature: Report Runner

  @p1
  Scenario: Run Interactive Report
	Given I login as admin user
	And I navigate to Interactive Reports page
	And I select Inspections on interactive reports page
	And I select Select All until I reach the Report Runner
	When I click on 'Run report' button
	Then I should see report 401ax on Report History page
	And report should complete processing