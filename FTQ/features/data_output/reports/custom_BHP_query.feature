Feature: Queries

  @p2
  Scenario: Custom BHP Query
    Given I login as BHP user
    And I navigate to Import page
    And I import 'BHP All Checklists File'
    And I import 'BHP Checklist CT15 File'
    And I import 'BHP Checklist CT16 File'
    And I import 'BHP Checklist CT17 File'
    Then I create a BHP inspection via checklist 'Audits-Checklist1-CT TEST (CT15-TEST)'
    When I enter 'automation added data 123' note in 'ct-rate-1' checkpoint observation field
    And I press dynamic button title 'ct-timemile-1'
    And I press dynamic button title 'ct-timemile-1'
    And I enter 'automation added data 123' to cell 'ct-date-1', row 1
    And I enter 'automation added data 123' to cell 'ct-hrs-travel-1', row 1
    And I enter 'automation added data 123' to cell 'ct-hrs-onlocation-1', row 1
    And I enter 'automation added data 123' to cell 'ct-hrs-reporting-1', row 1
    And I enter 'automation added data 123' to cell 'ct-mileagert-1', row 1
    And I enter 'automation added data 123' to cell 'ct-rotation-1', row 1
    And I enter 'automation added data 123' to cell 'ct-notes-1', row 1
    And I enter 'automation added data 123' to cell 'ct-date-1', row 2
    And I enter 'automation added data 123' to cell 'ct-hrs-travel-1', row 2
    And I enter 'automation added data 123' to cell 'ct-hrs-reporting-1', row 2
    And I enter 'automation added data 123' to cell 'ct-notes-1', row 2
    And I sync inspection to server
    Then I create a BHP inspection via checklist 'Audits-Checklist2-CT TEST (CT16-TEST)'
    When I press dynamic button title 'ct-timemile-2'
    And I press dynamic button title 'ct-timemile-2'
    And I enter 'automation added data 123' to cell 'ct-rotation-2', row 1
    And I enter 'automation added data 123' to cell 'ct-hrs-travel-2', row 1
    And I enter 'automation added data 123' to cell 'ct-hrs-onlocation-2', row 1
    And I enter 'automation added data 123' to cell 'ct-date-2', row 1
    And I sync inspection to server
    Then I create a BHP inspection via checklist 'Audits-Checklist3-CT TEST (CT17-TEST)'
    When I enter 'automation added data 123' note in 'ct-rate-3' checkpoint observation field
    And I press dynamic button title 'ct-timemile-3'
    And I enter 'automation added data 123' to cell 'ct-mileagert-3', row 1
    And I sync inspection to server
    And I wait 6 minutes for reporting tables to update
    Given I navigate to Run Reports Online page
    And I select query 'Q1031'
    And I navigate to 'Dates' process menu tab
    And I select date range 'Today (until now)'
    When I click on 'Run report' button
    Then query should complete processing
    When I download the query
    Then data returned for query 1031 is correct