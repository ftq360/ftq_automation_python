Feature: Email Customizations and Additional Settings

  @p2
  Scenario Outline: Inspection Email Subject and File Name Settings
    Given I login as admin user
    And I navigate to Account Management page
    And I navigate to 'Additional System Settings' process menu tab
    And I change 'Filename template for Immediate Email Notifications (Inspection Checklist Screen.)' setting to <filename_setting>
    And I change 'Subject line template for Immediate Email Notifications (Inspection Checklist Screen.)' setting to <subject_setting>
    And I save changes on 'Account Management' page
    And I log out
    And I login as admin user
    When I create an inspection
    Then I wait until Inspection ID value appears on inspection page
    When I email the inspection to the testing email address with a custom number to identify the email
    Then the email subject line is in <subject_setting> format
    And the email filename is in <filename_setting> format
    Examples:
      | filename_setting                                                   | subject_setting                                           |
      | Standard (Project, Phase, Inspection Date and Inspection ID)       | Standard (Company Name, Project and Phase)                |
      | Inspection ID only                                                 | Standard (Company Name, Project and Phase)                |
      | Inspection ID and Inspection Date                                  | Company Name, Project, Phase, Checklist and Inspection ID |
      | Standard Printout (Project, Phase, current date and Inspection ID) | Company Name, Project, Phase, Checklist and Inspection ID |