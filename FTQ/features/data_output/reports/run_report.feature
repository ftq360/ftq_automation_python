Feature: Report Runner

  @p1
  Scenario Outline: Run Report
	Given I login as demoed user
	And I navigate to Run Reports Online page
	And I select a <report>
	When I click on 'Run report' button
	Then I should see the <report> on Report History page
	And report should complete processing
	Examples:
	| report                                                        |
	| FTQ-324b - WCC-324 FTQ Performance by Project (last 12 weeks) |
	| FTQ-401ax - Inspection Reports                                |
	| FTQ-485a - OL-ITP Status Report                               |
