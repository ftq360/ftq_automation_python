Feature: Report History

  @p2
  Scenario: Report History
      Given I login as demoed user
      And I navigate to Run Reports Online page
      And I select 'FTQ-401ax - Inspection Reports' report
      When I click on 'Run report' button
      Then I should see the report on Report History page
      When I click on the 'x' button for a processing report
      And I click in popup to confirm
      Then I see the report status 'Cancelled'
      Given I navigate to Run Reports Online page
      And I select 'FTQ-401ax - Inspection Reports' report
      When I click on 'Run report' button
      Then I should see the report on Report History page
      And report should complete processing
      When I click the 'email' button in the report row
      Then I see an email dialog
      When I send report email
      Then I see toast notification with 'Email sent' text
      When I click the 'trashcan' button in the report row
      And I click in popup to confirm
      Then the report is deleted
