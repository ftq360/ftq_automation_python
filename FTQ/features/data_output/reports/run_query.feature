Feature: Report Runner

  @p1
  Scenario: Run Query
	Given I login as demoed user
	And I navigate to Run Reports Online page
	And I select query 'Q901 - Data Query 901'
	When I click on 'Run report' button
	Then I should see Q901 on Report History page
	And report should complete processing