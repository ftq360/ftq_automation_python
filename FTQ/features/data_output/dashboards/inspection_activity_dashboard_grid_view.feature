Feature: Inspection Activity Dashboard

  @p2
  Scenario: Inspection Activity Dashboard - Grid View
	Given I login as admin user
	And I navigate to Inspection Activity Dashboard page
	When I filter by 'Last 365 days (until now)' in 'date range' filter dropdown on Insp Activity Dashboard, which filters the dashboard accordingly
	And I select 'Automation, Python' in 'Inspector' dashboard panel, which filters 'Responsible Party' panel
	And I search for an inspection in the grid
	Then I see search filtered the grid list correctly
	When I change an inspection status from dashboard grid
	And I open the inspection from dashboard grid
	Then I see inspection status is changed
