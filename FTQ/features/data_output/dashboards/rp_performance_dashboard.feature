Feature: Dashboards

  @p2
  Scenario: RP Performance Dashboard
	Given I login as admin user
	And I navigate to RP Performance Dashboard page
	When I filter by 'All Projects' in 'project' filter dropdown on RP Dashboard, which filters the dashboard accordingly
	And I select 'QA' in '% FTQ Checkpoints By Category' dashboard panel, which filters '# Deficiencies By Responsible Party' panel
	And I clear filtering on '% FTQ Checkpoints By Category' dashboard panel
	And I select 'Punch Item/Deficiency:' in 'Recurring Deficiencies By Checkpoint' dashboard panel, which filters '% FTQ Checkpoints By Category' panel
	And I clear filtering on 'Recurring Deficiencies By Checkpoint' dashboard panel
	And I filter by 'Last 365 days (until now)' in 'date range' filter dropdown on RP Dashboard, which filters the dashboard accordingly
	And I filter by 'All Projects' in 'project' filter dropdown on RP Dashboard, which filters the dashboard accordingly
	And I sort 'Recurring Deficiencies By Checkpoint' dashboard panel by 'Sort by title'
	Then data on 'Recurring Deficiencies By Checkpoint' panel should be listed by 'Title - Ascending'
	When I sort '% FTQ Checkpoints By Category' dashboard panel by 'Sort by quantity'
	And I download '% FTQ Checkpoints By Category' dashboard panel data
	Then data in the downloaded csv should match sorting order on '% FTQ Checkpoints By Category' panel
