Feature: Dashboards

  @p2 @dashboards @key_metrics @listing_grids
  Scenario Outline: Key Metric Dashboards
    Given I login as admin user
    And I navigate to <page_name> page
    When I filter by 'Last 365 days (until now)' in 'date range' filter dropdown
    And I select '<dash_data>' in '<dash_panel>' dashboard panel, which filters '<filtered_panel>' panel
    And I click on the 'Show Deficiencies' button
    Then the filtered deficiency grid is opened
    Examples:
      | page_name             | dash_data             | dash_panel                   | filtered_panel               |
      | Risk Factor Dashboard | Inspector, Restricted | Checkpoint Inspector         | Inspection Responsible Party |
      | Cost Level Dashboard  | Responsible Party 2   | Checkpoint Responsible Party | Project                      |
      | Money Spent Dashboard | ITP Project           | Project                      | Checkpoint                   |
