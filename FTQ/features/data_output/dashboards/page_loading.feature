Feature: Dashboard Page Loading

  @p1
  Scenario Outline: Dashboard Page Loading
	Given I login as admin user
	And I navigate to <dashboard>
	Then dashboard loads as expected
	Examples:
	  | dashboard                                |
	  | priority open item dashboard             |
	  | inspection activity dashboard            |
	  | aging open item dashboard                |
	  | deficiency by checklist dashboard        |
	  | deficiency by rp dashboard       		 |
	  | inspection activity by project dashboard |
	  | itp completion dashboard                 |
	  | itp progress dashboard                   |
	  | key metric dashboard                     |
      | key metric trend dashboard               |
	  | monthly vendor dpk dashboard             |
	  | number insp by checklist dashboard       |
	  | deficiency trend dashboard               |
	  | percent insp by checklist dashboard      |
	  | deficiency by reason code dashboard      |
	  | trend by reason code dashboard           |
	  | rp performance dashboard                 |
