Feature: Deficiency Details Button on Deficiency Dashboards

  @p1
  Scenario Outline: Deficiency Details Button on Deficiency Dashboards
    Given I login as admin user
    And I navigate to <page_name> page
    When I filter by '<project>' in 'project' filter dropdown
    And I filter by '<time_range>' in 'date range' filter dropdown
    And I select '<phase>' in 'Phase' dashboard panel, which filters 'Inspector' panel
    And I select '<inspector>' in 'Inspector' dashboard panel, which filters 'Phase' panel
    And I click on 'Show Deficiencies' button
    Then I should see a new tab open
    When I click on the Settings cog
    And I use manage columns to make 'Entered By Inspector' column visible
    Then I should see the same deficiencies filtered on the dashboard for <project>, <phase>, and <inspector>

    Examples:
      | page_name                    | project         | time_range                | phase       | inspector       |
      | Priority Open Item Dashboard | All Projects    | Last 365 days (until now) | All phases  | User, Plan Mode |
      | Priority Open Item Dashboard | ITP Project (4) | Prior 3 months (not this) | ITP Phase 2 | All inspectors  |
      | Aging Open Item Dashboard    | All Projects    | Last 365 days (until now) | All phases  | User, Plan Mode |
      | Aging Open Item Dashboard    | ITP Project (4) | Prior 3 months (not this) | ITP Phase 2 | All inspectors  |
