Feature: Dashboards

  @p2 @dashboards
  Scenario Outline: Days To Fix Dashboards
	  Given I login as admin user
	  And I navigate to <page_name> page
	  When I filter by 'Last 365 days (until now)' in 'date range' filter dropdown
	  And I select 'Project 1' in 'Project' dashboard panel
	  Then the dashboard stats are filtered accordingly
	Examples:
	  | page_name			        |
	  | Total Days to Fix Dashboard |
	  | Avg Days to Fix Dashboard   |
