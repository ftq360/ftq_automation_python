Feature: Dashboard Landing Page

  @p2 @WIP
  Scenario: Dashboard Favorites
    Given I login as admin user
    And I navigate to Dashboard Landing page
    When I remove all dashboard stars
    And I refresh page
    Then dashboard menu shows the default options
    When I star some dashboards
    And I refresh page
    Then dashboard menu shows my starred dashboards
    When I navigate to 'My Favorites' dashboard tab
    Then only my starred dashboards are shown
