Feature: Priority Open Item

  @p2
  Scenario: Priority Open Item Dashboard - Grid View
	Given I login as admin user
	And I navigate to Priority Open Item Dashboard page
	When I filter by 'Last 365 days (until now)' in 'date range' filter dropdown on Priority Open Item Dashboard, which filters the dashboard accordingly
	And I select 'Automation, Python' in 'Inspector' dashboard panel, which filters 'Inspection Responsible Party/User' panel
	And I search for a due date in the grid
	Then I see search filtered the grid list correctly
