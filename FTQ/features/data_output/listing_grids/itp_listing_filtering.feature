Feature: ITP Listing

  @p2
  Scenario Outline: ITP Listing Filtering
      Given I login as admin user
      And I navigate to ITP Listing page
      When I enter '<text>' in '<column>' search field
      Then the ITP Listing grid is filtered accordingly
      Examples:
        | text             | column            |
        | 4-1              | code              |
        | Concrete-General | checklist         |
        | ITP Project      | project           |
        | ITP              | equipment         |
        | FTQ              | responsible party |
