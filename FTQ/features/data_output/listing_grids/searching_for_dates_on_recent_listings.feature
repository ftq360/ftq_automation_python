Feature: Listing Grids

  @p2 @listing_grids @account_management @date_format
  Scenario Outline: Searching for Dates on Recent Listing Grids
        Given I login as admin user
        And I navigate to Account Management page
        And I navigate to 'Additional System Settings' process menu tab
        And I set date format to '<date_format>'
        And I log out
        And I login as admin user
        And I navigate to listing <listing_page_name> page
        When I search the page in <date_format>
        Then I should see the <listing_page_name> filter for my input
        Examples:
          | date_format                           | listing_page_name           |
          | MMxDDxYYYY                            | Recent Inspections Listing  |
          | DDxMMxYYYY                            | Recent Deficiencies Listing |
          | DD-MM-YYYY                            | Recent Inspections Listing  |
          | DD.MM.YYYY                            | Recent Deficiencies Listing |
          | YYYY-MM-DD                            | Recent Inspections Listing  |
          | YYYYxMMxDD                            | Recent Deficiencies Listing |
          | YYYY.MM.DD                            | Recent Inspections Listing  |
          | MON DD, YYYY (abbreviated month name) | Recent Inspections Listing  |
          | DD MON YYYY (abbreviated month name)  | Recent Deficiencies Listing |