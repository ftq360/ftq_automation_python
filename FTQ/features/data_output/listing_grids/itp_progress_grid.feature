Feature: ITP Progress Grid

  @p1
  Scenario: Drop-down data (ITP Progress)
    Given I login as admin user
    And I navigate to ITP Progress page
    When I filter for 'ITP Project (4)' on ITP Progress page
    Then the ITP status bar at the top of the page is correct calculation
    When I expand the drop down for plan item '[4-2]'
    And I open an inspection from the ITP Progress page
    When I override the inspection status to Fail (final)
    Then I should see 'Synced to server' inspection status
    Given I navigate to ITP Progress page
    When I expand the drop down for plan item '[4-2]'
    Then the inspection shows new inspection status on the ITP Progress page

  @p2
  Scenario Outline: Icon colors (ITP progress)
    Given I login as admin user
    And I navigate to ITP Progress page
    When I filter for 'Project 1 (1)' on ITP Progress page
    And I click on plan item 'General-Punch List Deficiency (CT147), Qty: 100, [1-1]'
    And I select a Responsible Party to create the plan item with
    Then ITP inspection is successfully created
    When I override the inspection status to <inspection_status>
    Then I should see 'Synced to server' inspection status
    Given I navigate to ITP Progress page
    Then I should see <icon_color> icon for plan item 'General-Punch List Deficiency (CT147), Qty: 100, [1-1]'
    Examples:
        | inspection_status   | icon_color |
        | Incomplete          | grey       |
        | Incomplete (w/Open) | yellow     |
        | Fail (final)        | red filled |
        | Complete (w/Open)   | red        |
        | Draft               | red        |
        | Pass                | green      |
        | Report Complete/WIP | green      |
