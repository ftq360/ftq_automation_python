Feature: Recent inspections listing grid

  @p1
  Scenario: Users should see group inspections from other users
      Given I login as admin user
      When I create an inspection
      Then I wait until Inspection ID value appears on inspection page
      Given I log out
      And I login as standard user
      And I navigate to Recent Inspections Listing page
      Then I should see Inspection ID for the newly created inspection
