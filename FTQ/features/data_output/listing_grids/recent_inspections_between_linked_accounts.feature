Feature: Recent Inspection Listing Grid

  @p1
  Scenario: Recent Inspections Between Linked Accounts
      Given I login as linked account user
      And I navigate to Recent Inspections Listing page
      Then I see only inspections on 'Recent Listing Grid' from my automation group
      Given I navigate to Create Inspection page
      When I expand Show Recent Inspections data
      Then I see only inspections on 'Show Recent Insp' from my automation group