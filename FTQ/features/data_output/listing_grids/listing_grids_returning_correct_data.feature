Feature: Listing Grids

  @p2 @listing_grids @inspection_notes
  Scenario Outline: Listing Grids Returning Correct Data
    Given I login as admin user
    When I create an inspection
    And I enter 'Header note with time' in the Notes field in the inspection header
    And I enter 'checkpoint note with time' note in 'Punch Item/Deficiency:' checkpoint observation field
    Then Inspection syncs to server successfully
    Given I navigate to <listing_grids> page
#    When I use manage columns to make 'Inspection Notes' column visible
    Then I see the correct notes on grid
    Examples:
      | listing_grids               |
      | Recent Inspections Listing  |
      | Inspection Archive Listing  |
      | Recent Deficiencies Listing |
      | Deficiency Archive Listing  |



