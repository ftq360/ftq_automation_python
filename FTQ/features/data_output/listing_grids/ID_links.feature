Feature: ID Links on Listing Grids work as expected

  @p1
  Scenario Outline: ID links on Recent Listing Grids
	Given I login as admin user
	When I create an inspection via project 1, checklist '03.10.01'
	And I click on 'OPN' checkbox in 'Fall of waste lines verified with level, supported with backfill.' checkpoint on inspection page
	Then I should see 'Synced to server' inspection status
	And I wait until Inspection ID value appears on inspection page
	Given I navigate to <listing_grid_page>
	When I click on the <ID_link> for my inspection
	Then I should be directed to <inspection_open_on>
	Examples:
		| listing_grid_page            | ID_link      | inspection_open_on |
		| Recent Deficiencies Listing  | deficiencyId | checkpoint         |
		| Recent Inspections Listing   | inspId       | insp header        |
