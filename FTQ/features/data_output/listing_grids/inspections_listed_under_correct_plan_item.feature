Feature: ITP Progress Grid

  @p1
  Scenario: Inspections are listed under correct plan item
    # Plan items 4-1 and 4-2 have same checklist and assignments
      Given I login as plan mode user
      When I create an ITP inspection via 'ITP Project', plan item '[4-1]'
      And I click on 'Save' button on inspection page
      Then I wait until Inspection ID value appears on inspection page
      And the Inspection ID is inspection1
      When I create an ITP inspection via 'ITP Project', plan item '[4-2]'
      And I click on 'Save' button on inspection page
      Then I wait until Inspection ID value appears on inspection page
      And the Inspection ID is inspection2
      Given I navigate to ITP Progress page
      Then inspection1 is listed under plan item '[4-1]'
      And inspection2 is listed under plan item '[4-2]'
