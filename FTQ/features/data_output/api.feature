Feature: API

  @p2 @API
  Scenario: APIs
    Given I login as admin user
    When I create an inspection
    And I attach file to 'Quality' checkpoint on inspection page
    And I click on 'FTQ' checkbox in 'Quality' checkpoint on inspection page
    Then Inspection syncs to server successfully
    Given I navigate to API Setup page
    And I have an API set up with my FTQ360 admin user
    Then APIs are returning data as expected