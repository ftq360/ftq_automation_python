Feature: Selection Process

  @p1
  Scenario: Option for no equipment
    Given I login as admin user
    And I navigate to Create Inspection page
    When I select 'ITP Project' in the selection process
    Then I should see a 'No equipment' option in the selection process