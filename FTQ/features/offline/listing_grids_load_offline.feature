Feature: Listing Grids Load Offline

  @p1
  Scenario: Show Recent Inspections Loads Offline
	Given I login as admin user
	And I navigate to Create Inspection page
	Then I should see 'Loading data' modal
	And I should wait until 'Loading data' modal will be closed
	Given I go into FTQ offline mode
	When I create an inspection
	And I enter dynamic note {todays_date} in the Notes field in the inspection header
	Then inspection is saved to device
	Given I navigate to Create Inspection page
	When I click on 'Show Recent Inspections' button on Create Inspection page
	Then I should see inspections my inspection with the note listed
	Given I go into FTQ online mode
	Then my offline inspection sync to server successfully
