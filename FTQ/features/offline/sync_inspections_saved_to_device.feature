Feature: Sync inspections saved to device

  @p1
  Scenario: Sync 5 inspections saved to device
    Given I login as admin user
    And I navigate to Create Inspection page
    Then I should see 'Loading data' modal
    And I should wait until 'Loading data' modal will be closed
    Given I go into FTQ offline mode
    And I create 5 inspections
    And I go into FTQ online mode
    Then all 5 inspections automatically sync to server
