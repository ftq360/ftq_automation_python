Feature: Autosave In Offline

  @p2
  Scenario: Autosave In Offline
	Given I login as admin user
	When I create an inspection via Project 2, checklist '03.10.01'
	Given I go into FTQ offline mode
	When I enter 'note' in the Location/Ref field in the inspection header
	And I enter 'note' in the Notes field in the inspection header
	And I override the inspection status to Fail (final)
	And I click on 'OPN' checkbox in 'Footer depth and width per plan and code.' checkpoint on inspection page
	And I attach file to 'Footer depth and width per plan and code.' checkpoint on inspection page
	Then inspection is saved to device
	Given I go into FTQ online mode
	Then I should see 'Synced to server' inspection status
	When I refresh inspection from server
	Then data on inspection should still be present
