Feature: Offline Selection

  @p2
  Scenario: Offline equipment selection
    Given I login as admin user
    And I navigate to Create Inspection page
    Then I should see 'Loading data' modal
    And I should wait until 'Loading data' modal will be closed
    Given I go into FTQ offline mode
    When I create an inspection via project 2, any equipment checklist
    Then equipment is listed on inspection header
    When I save inspection to device
    Given I go into FTQ online mode
    Then I should see 'Synced to server' inspection status
