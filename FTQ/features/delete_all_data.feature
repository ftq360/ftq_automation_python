Feature: Delete All Data

    @p1
    Scenario: Delete All Data
        Given I login as admin user
        When I create an inspection
        And I sync inspection to server
        Given I navigate to System Data page
        When I refresh page
        And I look in the indexedDB there is FTQ360 data
        And I click on 'Delete all Data' button on the FTQ screen
        And I refresh page
        Then I look in the indexedDB and there is no FTQ360 data
