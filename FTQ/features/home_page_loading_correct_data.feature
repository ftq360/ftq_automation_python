Feature: Help Documentation

  @WIP
  Scenario Outline: Home Page Loading Correct Data
    Given I login as homepage user
    Then I am brought to my homepage dashboard
    And I should see the correct info loading on <panel>
    When I create an inspection via project 1, checklist 'CT150'
    Then Inspection syncs to server successfully
    When I click on the home icon
    Then I am brought to my homepage dashboard
    And I should see the new inspection info added to <panel>
    Examples:
      | panel                                        |
      | My Recent Inspections                        |
      | Open Deficiencies Reported by Me             |
      | Open Deficiencies Assigned to my Affiliation |
