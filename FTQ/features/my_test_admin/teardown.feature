Feature: Teardown

  @teardown
  Scenario: Teardown
    Given I login as admin user
    And I navigate to inspection archive listing page
    And I delete inspections from inspections archive listing grid
    And I navigate to project setup page
    And I select 'Project for Data Creation' project in new project setup grid
    And I navigate to project locations grid
    And I delete project data
    And I navigate back to project grid
    And I navigate to project equipment grid
    And I delete project data
    And I navigate back to project grid
    And I set grid filter to 'All'
    And I delete all automation created projects
    And I select 'Project for References' project in new project setup grid
    And I expand References in project details panel
    And I delete all all reference attachments
    And I navigate to checklist setup page
    And I set grid filter to 'All'
    And I delete all automation created and downloaded checklists
    When I select 'Checklist for References' checklist
    Given I expand References in checklist details panel
    And I delete all all reference attachments
    And I navigate to responsible party setup page
    And I delete all responsible parties added by automation

