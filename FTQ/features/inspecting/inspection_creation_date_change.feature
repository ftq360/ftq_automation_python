Feature: Inspection Creation Date Change

  @p1
  Scenario: Inspection Creation Date Change
	Given I login as admin user
	When I create an inspection
	And I click on the inspection creation date
	Then I can change the inspection creation date
	And I should see 'Synced to server' inspection status
	And I wait until Inspection ID value appears on inspection page
	Given I navigate to Recent Inspections Listing page
	Then the changed date is reflected in Recent Inspection Listing Grid
	Given I log out
	And I login as standard user
	When I create an inspection
	And I click on the inspection creation date
	Then I cannot change the inspection creation date
