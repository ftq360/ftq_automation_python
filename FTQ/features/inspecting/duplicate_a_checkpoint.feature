Feature: Checkpoints

  @p1
  Scenario: Duplicate A Checkpoint
    Given I login as admin user
    When I create an inspection via project 1, checklist 'CT150'
    And I click on 'OPN' checkbox in 'Quality Checkpoint' checkpoint on inspection page
    And I attach file to 'Quality Checkpoint' checkpoint on inspection page
    And I enter 'Checkpoint Note' note in 'Quality Checkpoint' checkpoint observation field
    When I sync inspection to server
    When I click on Options Menu for 'Quality Checkpoint' checkpoint on inspection page
    And I click Duplicate button
    Then I should see '{checkpoint}' duplicated
    And I should see duplicated checkpoint has no notes, status or attachments
    When I sync inspection to server
    And I click get PDF on the inspection page
    Then I see duplicated checkpoint on report
