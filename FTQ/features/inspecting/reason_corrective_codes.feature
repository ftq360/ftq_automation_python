Feature: Reason and Corrective Codes

  @p1
  Scenario: Reason and Corrective Codes
    Given I login as admin user
    When I create an inspection
    And I click the reason code button on a checkpoint
    And I select a reason code from the dialog
    Then I should see the reason code populate for the checkpoint
    When I click the corrective action code button on a checkpoint
    And I select a corrective action code from the dialog
    Then I should see the corrective action code populate for the checkpoint
    And Inspection syncs to server successfully

