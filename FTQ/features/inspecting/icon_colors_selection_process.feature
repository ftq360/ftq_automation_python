Feature: Icon Colors (Selection Process)

  @p2
  Scenario Outline: Icon Colors (Selection Process)
        Given I login as admin user
        When I create an inspection via project 1, checklist 'CT135'
        And I override the inspection status to <inspection_status>
        Then I should see 'Synced to server' inspection status
        Given I navigate to create inspection page
        And I select project 1 on selection page
        Then I should see <icon_color> icon for checklist 'CT135'
        Examples:
          | inspection_status   | icon_color |
          | Incomplete          | grey       |
          | Incomplete (w/Open) | yellow     |
          | Fail (final)        | red filled |
          | Complete (w/Open)   | red        |
          | Draft               | red        |
          | Pass                | green      |
          | Report Complete/WIP | green      |
