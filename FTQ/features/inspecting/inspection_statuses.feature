Feature: Inspection Statuses

  @p1
  Scenario: Inspection Statuses
    Given I login as admin user
    When I create an inspection via project 1, checklist 'CT147'
    Then inspection status is 'Incomplete (w/Open)'
    When I click on 'QC' checkbox in 'Punch Item/Deficiency:' checkpoint on inspection page
    Then inspection status is 'Incomplete'
    When I click on 'FTQ' checkbox in 'Quality' checkpoint on inspection page
    Then inspection status is 'Pass'
    When I click on 'OPN' checkbox in 'Quality' checkpoint on inspection page
    Then inspection status is 'Complete (w/Open)'
