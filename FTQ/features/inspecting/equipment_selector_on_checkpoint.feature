Feature: Equipment Selector On Checkpoint

  @p2
  Scenario: Equipment Selector On Checkpoint
    Given I login as admin user
    When I create an inspection via project 2, checklist 'CT150'
    Then I see 'Equipment Checkpoint' has equipment field populated
    When I change equipment selected
    And I sync inspection to server
    Then I see equipment change recorded in inspection activity log
