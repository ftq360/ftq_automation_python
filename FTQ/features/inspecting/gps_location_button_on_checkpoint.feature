Feature: GPS Location Button on Checkpoint

  @p2
  Scenario: GPS Location Button on Checkpoint
    Given I login as admin user
    When I create an inspection
    When I click on Options Menu for 'Punch Item/Deficiency:' checkpoint on inspection page
    And I select 'Add GPS Position'
    Then I should see the GPS coordinates populate on the checkpoint
    And Inspection syncs to server successfully
