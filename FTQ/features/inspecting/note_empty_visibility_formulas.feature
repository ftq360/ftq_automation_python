Feature: Visibility Formulas

  @p2
  Scenario: Note Empty/Not Empty Visibility Formulas
    Given I login as admin user
    When I create an inspection via project 1, checklist 'CT320477'
    Then checkpoint 'Visibility Checkpoint - When Reference Checkpoint Notes Not Empty' is 'not visible'
    And checkpoint 'Visibility Checkpoint - When Reference Checkpoint Notes Empty' is 'visible'
    When I enter 'automation text note' note in 'Quality Checkpoint2' checkpoint observation field
    And I enter 'automation text note' note in 'Quality Checkpoint3' checkpoint observation field
    Then checkpoint 'Visibility Checkpoint - When Reference Checkpoint Notes Not Empty' is 'visible'
    And checkpoint 'Visibility Checkpoint - When Reference Checkpoint Notes Empty' is 'not visible'
    And I should see 'Synced to server' inspection status
