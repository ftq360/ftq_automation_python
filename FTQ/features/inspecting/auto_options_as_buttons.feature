Feature: Auto Options

  @p1
  Scenario: Auto Options As Buttons
    Given I login as admin user
    When I create an inspection via project 1, checklist 'CT125'
    And I select an option button for 'Auto Option Checkpoint' checkpoint
    Then I should see the text from the selected option populated in checkpoint notes
    And Inspection syncs to server successfully
