Feature: Deleting Inspections

  @p2
  Scenario: Deleting Inspections
    Given I login as admin user
    And I navigate to Recent Inspections Listing page
    When I open an inspection
    And I delete the inspection on the inspection page
    Then I am redirected to Recent Inspections Listing page
    And the deleted inspection is not listed
