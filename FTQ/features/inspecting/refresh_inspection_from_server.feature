Feature: Inspection Server Refresh

  @p2
  Scenario: Refresh Inspection From Server
    Given I login as admin user
    When I create an inspection
    And I sync inspection to server
    Given I log out
    And I login as standard user
    When I open the inspection from recent inspections listing grid
    And I enter 'inspection change' in the Notes field in the inspection header
    Then Inspection syncs to server successfully
    Given I log out
    And I login as admin user
    When I open the inspection from recent inspections listing grid
    Then refresh from server icon is orange to show changes
    When I refresh inspection from server using the button on footer
    Then inspection changes are shown