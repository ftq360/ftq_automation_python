Feature: Formulas

    @p2
    Scenario: Info Functions On Checkpoints
        Given I login as admin user
        When I create an inspection via project 1, checklist 'CT648'
        When I enter '200' note in 'Referenced Checkpoint for Info Formulas' checkpoint observation field
        Then I should see 'ISDATE' info formula checkpoint display 'FALSE'
        Then I should see 'ISNUMBER' info formula checkpoint display 'TRUE'
        Then I should see 'ISTEXT' info formula checkpoint display 'FALSE'
        Then I should see 'ISLOGICAL' info formula checkpoint display 'TRUE'
