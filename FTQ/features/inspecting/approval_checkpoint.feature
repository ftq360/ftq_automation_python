Feature: Approval Checkpoints

  @p1
  Scenario: Approval Checkpoint
      Given I login as admin user
      When I create an inspection via project 1, checklist 'CT932'
      And I click on 'NA' checkbox on the Approval Checkpoint
      Then I am given a warning that I need to assign an approver
      When I assign 'Automation, Python' user as the checkpoint approver
      And I click on 'NA' checkbox on the Approval Checkpoint
      And I assign 'Inspector, Restricted' user as the checkpoint approver
      Then I am given a warning that this will reset the checkpoint status
      When I click in popup to confirm
      Then checkpoint status is cleared
      And I should see 'Synced to server' inspection status
