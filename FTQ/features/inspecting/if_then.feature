Feature: If-Then Visibility Formulas

  @p1
  Scenario: If-Then
    Given I login as admin user
    And I navigate to checklist setup page
    When I create a new checklist(checklist1)
    And I add 4 quality checkpoints (checkpoint1, checkpoint2, checkpoint3, checkpoint4)
    And I add 1 safety checkpoint(checkpoint5)
    And I add 1 data checkpoint(checkpoint6)
    And I add 1 image checkpoint(checkpoint7)
    And I set checkpoint2 visibility status to rely on checkpoint1 status is FTQ
    And I set checkpoint3 visibility status to rely on checkpoint2 note is 'no'
    And I add 3 auto-options to checkpoint3(option1, option2, option3)
    And I set checkpoint4 visibility status to rely on checkpoint3 auto-option is option1
    And I set checkpoint6 visibility status to rely on checkpoint5 status is not FAILED
    And I set checkpoint7 visibility status to rely on checkpoint6 note is not 'no'
    Given I navigate to Create Inspection page
    And I create a new inspection with visibility checklist(checklist1)
    When I click on 'OPN' checkbox in 'checkpoint1' checkpoint on inspection page
    Then checkpoint2 should be hidden
    When I click on 'FTQ' checkbox in 'checkpoint1' checkpoint on inspection page
    Then checkpoint2 should be visible
    When I add 'no' to the notes field of checkpoint2
    Then checkpoint3 should be visible
    When I select auto-option option2 for checkpoint3
    Then checkpoint4 should be hidden
    When I select auto-option option1 for checkpoint3
    Then checkpoint4 should be visible
    When I click on 'FAILED' checkbox in 'checkpoint5' checkpoint on inspection page
    Then checkpoint6 should be hidden
    When I add 'no' to the notes field of checkpoint6
    Then checkpoint7 should be hidden
    Then I should see 'Synced to server' inspection status
    When I click get PDF on the inspection page
    Then the PDF should show only visible checkpoints
