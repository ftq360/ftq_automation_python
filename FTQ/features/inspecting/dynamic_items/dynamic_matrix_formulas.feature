Feature: Dynamic Matrices

  @p2
  Scenario Outline: Dynamic matrix formulas
    Given I login as admin user
    When I create an inspection via project 1, checklist 'CT245'
    And I press dynamic button title 'Dynamic Matrix Formulas'
    And I enter '<enter_data>' to cell 'Enter Data'
    And I enter '<more_data>' to cell 'More Data'
    Then cell 'Formula' is '<formula>'
    Examples:
      | enter_data  | more_data | formula    |
      | 5           | 15        | 65         |
      | text        | here      | texthere45 |
