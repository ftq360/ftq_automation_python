Feature: Dynamic Matrix

  @p2 @dynamic_items @inspection_copy @copy_mode
  Scenario: Dynamic Matrix Children Copy Mode
      Given I login as admin user
      When I create an inspection via project 1, checklist 'CT245'
      And I press dynamic button title 'Standard Dynamic Matrix'
      And I press dynamic button title 'Standard Dynamic Matrix'
      And I enter 'automation data added cell1 row1' to cell 'Cell1', row 1
      And I enter 'automation data added cell3 row1' to cell 'Cell3', row 1
      And I enter 'automation data added cell3 row2' to cell 'Cell3', row 2
      And I sync inspection to server
      And I click copy button on inspection page
      Then I am brought to a new copied inspection
      And I see dynamic matrix children copied over correctly