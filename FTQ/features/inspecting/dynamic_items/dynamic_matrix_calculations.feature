Feature: Dynamic Matrices

  @p1
  Scenario: Dynamic Matrix Calculations
	  Given I login as admin user
	  When I create an inspection via project 1, checklist 'CT245'
	  And I press dynamic button title 'Dynamic Matrix Calculations'
	  And I press dynamic button title 'Dynamic Matrix Calculations'
	  And I refresh page
	  And I enter '100' to cell 'Sum', row 1
	  And I enter '100' to cell 'Average', row 1
	  And I enter '100' to cell 'Count Not Empty', row 1
	  And I enter '50' to cell 'Sum', row 2
	  And I enter '50' to cell 'Average', row 2
	  And I enter '50' to cell 'Count Not Empty', row 2
	  Then 'Total' calculation is '150'
	  And 'Average' calculation is '75'
	  And 'Count' calculation is '2'
	  And I should see 'Synced to server' inspection status