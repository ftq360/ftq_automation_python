Feature: Static Building Blocks

  @p1
  Scenario: Static Building Blocks
    Given I login as admin user
    When I create an inspection via project 1, checklist 'CT320476'
    And I enter 'Static Building Block Note' note in 'Common checkpoint Safety Score' checkpoint observation field
    Then Inspection syncs to server successfully