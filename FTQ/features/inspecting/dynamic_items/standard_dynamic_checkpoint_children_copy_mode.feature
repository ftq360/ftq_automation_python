Feature: Dynamic Matrix

  @p2 @dynamic_items @inspection_copy @copy_mode
  Scenario: Standard Dynamic Checkpoint Children Copy Mode
      Given I login as admin user
      When I create an inspection via project 1, checklist 'CT245'
      And I press dynamic button title 'Standard Dynamic Checkpoint'
      And I attach file to 'Dynamic Quality Checkpoint' checkpoint on inspection page
      And I attach file to 'Dynamic Quality Checkpoint 2' checkpoint on inspection page
      And I attach file to 'Dynamic Quality Checkpoint 3' checkpoint on inspection page
      And I enter 'dynamic checkpoint 1 + time' note in 'Dynamic Quality Checkpoint' checkpoint observation field
      And I enter 'dynamic checkpoint 2 + time' note in 'Dynamic Quality Checkpoint 2' checkpoint observation field
      And I enter 'dynamic checkpoint 3 + time' note in 'Dynamic Quality Checkpoint 3' checkpoint observation field
      And I click on 'FTQ' checkbox in 'Dynamic Quality Checkpoint' checkpoint on inspection page
      And I click on 'FTQ' checkbox in 'Dynamic Quality Checkpoint 2' checkpoint on inspection page
      And I click on 'FTQ' checkbox in 'Dynamic Quality Checkpoint 3' checkpoint on inspection page
      And I sync inspection to server
      And I click copy button on inspection page
      Then I am brought to a new copied inspection
      And I see standard dynamic checkpoint children copied over correctly
