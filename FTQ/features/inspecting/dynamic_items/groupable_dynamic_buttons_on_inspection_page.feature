Feature: Dynamic Checkpoints

  @p1
  Scenario: Groupable Buttons On Inspection Screen
    Given I login as admin user
    When I create an inspection via project 1, checklist 'CT320439'
    Then I see dynamic items grouped
    When I press dynamic button title 'Dynamic Checkpoint - Grouped'
    Then I should see a dynamic checkpoint added to the inspection