Feature: Dynamic Checkpoints

  @p2
  Scenario: Dynamic Checkpoints
    Given I login as admin user
    When I create an inspection via project 1, checklist 'CT245'
    And I press dynamic button title 'Standard Dynamic Checkpoint'
    Then I should see a dynamic checkpoint added to the inspection
    When I click on the 'Undo' button
    Then I should see the dynamic checkpoint disappear
