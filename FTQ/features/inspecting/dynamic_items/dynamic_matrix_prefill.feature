Feature: Dynamic Matrices

  @p2
  Scenario: Dynamic matrix prefill
    Given I login as admin user
    When I create an inspection via project 1, checklist 'CT245'
    Then dynamic matrix button titled 'Prefilled Dynamic Matrix' is automatically populated with rows and data
    When I clear text from all cells
    And I add new data to some of the cells
    And I refresh page
    And I click get PDF on the inspection page
    Then the PDF shows the new data in my prefilled dynamic matrix
