Feature: Dynamic Checkpoints

  @p2
  Scenario: Attachments to dynamic checkpoints
    Given I login as admin user
    When I create an inspection via project 1, checklist 'CT245'
    And I press dynamic button title 'Standard Dynamic Checkpoint'
    And I attach file to 'Dynamic Quality Checkpoint' checkpoint on inspection page
    And I click on 'Save' button on inspection page
    Then I should see 'Synced to server' inspection status
    When I refresh page
    Then I should see attached file on 'Dynamic Quality Checkpoint' checkpoint on inspection page
