Feature: Dynamic Checkpoints

  @p1
  Scenario: Groupable Buttons With Visibility Formulas
    Given I login as admin user
    When I create an inspection via project 1, checklist 'CT320477'
    Then I do not see 'Dynamic Checkpoint1 - Grouped' grouped button checkpoint visible on inspection creation
    And I do see 'Dynamic Checkpoint2 - Grouped' grouped button checkpoint visible on inspection creation
    When I click on 'FTQ' checkbox in 'Quality Checkpoint3' checkpoint on inspection page
    Then I see 'Dynamic Checkpoint1 - Grouped' groupable button checkpoint become visible
    And I should see 'Synced to server' inspection status
