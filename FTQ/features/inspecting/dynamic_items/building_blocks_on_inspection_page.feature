Feature: Dynamic Checkpoints

  @p1
  Scenario: Building Blocks On Inspection Screen
    Given I login as admin user
    When I create an inspection via project 1, checklist 'CT320439'
    Then I see building block checkpoint
    When I press dynamic button title 'Dynamic Building Block - Grouped'
    Then I should see the building block added to the inspection
    When I create an inspection via project 1, checklist 'CT320439'
    Then I see building block checkpoint
    When I press dynamic button title 'Dynamic Building Block - Separate'
    Then I should see the building block added to the inspection