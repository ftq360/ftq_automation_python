Feature: Dynamic Checkpoints

  @p1
  Scenario: Building Blocks With Visibility Formulas
    Given I login as admin user
    When I create an inspection via project 1, checklist 'CT320477'
    Then I do not see 'Dynamic Building Block - Grouped' building block checkpoint visible on inspection creation
    And I do not see 'Dynamic Building Block - Separate' building block checkpoint visible on inspection creation
    When I click on 'FTQ' checkbox in 'Quality Checkpoint1' checkpoint on inspection page
    Then I see 'Dynamic Building Block - Grouped' building block checkpoint become visible
    When I enter 'Automation Note' note in 'Quality Checkpoint2' checkpoint observation field
    Then I see 'Dynamic Building Block - Separate' building block checkpoint become visible
    When I press dynamic button title 'Dynamic Building Block - Separate'
    Then I should see the building block added to the inspection
    When I click on 'FTQ' checkbox in 'Quality Checkpoint1 - Building Block' checkpoint on inspection page
    Then I should see 'Quality Checkpoint2 - Building Block' become visible
    And I should see 'Synced to server' inspection status
