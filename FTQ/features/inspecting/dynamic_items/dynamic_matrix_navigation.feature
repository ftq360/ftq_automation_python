Feature: Dynamic Matrices

  @p1
  Scenario: Dynamic matrix navigation
    Given I login as admin user
    When I create an inspection via project 1, checklist 'CT245'
    And I press dynamic button title 'Dynamic Matrix Formulas'
    And I enter 'data' to cell 'Enter Data'
    And I tab through the matrix to create a new line
    Then a new matrix row is created

