Feature: Dynamic Checkpoints

  @p2
  Scenario: Dynamic checkpoint with no children
    Given I login as admin user
    When I create an inspection via project 1, checklist 'CT245'
    And I press dynamic button title 'Dynamic Checkpoint with No Children'
    And I click on 'Save' button on inspection page
    Then I should see 'Synced to server' inspection status