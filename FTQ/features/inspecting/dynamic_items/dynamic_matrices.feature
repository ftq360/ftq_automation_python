Feature: Dynamic Matrices

  @p2
  Scenario: Dynamic matrix
    Given I login as admin user
    When I create an inspection via project 1, checklist 'CT245'
    And I press dynamic button title 'Standard Dynamic Matrix'
    And I add text to all 3 cells
    And I refresh page
    And I clear text from all cells
    And I add new data to 2 of the cells
    And I refresh page
    And I click get PDF on the inspection page
    Then the PDF shows the new data and empty cell
