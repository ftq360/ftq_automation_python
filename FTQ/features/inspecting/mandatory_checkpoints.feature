Feature: Mandatory Checkpoints

  	@p1
	Scenario: Mandatory Checkpoints
	  Given I login as admin user
	  When I create an inspection via project 1, checklist 'CT510'
	  And I navigate away from the inspection
	  Then I see popup with text 'Your inspection is incomplete. Please complete all mandatory items.'
	  When I click 'cancel' on the popup
	  And I click on 'FTQ' checkbox in 'Mandatory checkpoint status' checkpoint on inspection page
	  Then I am able to navigate away from the inspection
