Feature: HTML Editor

      @p1
	  Scenario: HTML Editor
		  Given I login as admin user
		  When I create an inspection via project 1, checklist 'CT320512'
#		  When I select 'bold' HTML button on checkpoint 'Bold'
#		  And I enter 'note' note in 'Bold' checkpoint observation field
#		  Then 'Bold' checkpoint note text is bold
		  When I select 'italic' HTML button on checkpoint 'Italic'
		  And I enter 'note' note in 'Italic' checkpoint observation field
		  Then 'Italic' checkpoint note text is italic
		  When I select 'underline' HTML button on checkpoint 'Underline'
		  And I enter 'note' note in 'Underline' checkpoint observation field
		  Then 'Underline' checkpoint note text is underline
		  And Inspection syncs to server successfully
		  When I select 'bold' and 'italic' HTML button on checkpoint 'Bold Italic'
		  And I enter 'note' note in 'Bold Italic' checkpoint observation field
		  Then 'Bold Italic' checkpoint note text is bold and italic
		  When I select 'bold' and 'underline' HTML button on checkpoint 'Bold Underline'
		  And I enter 'note' note in 'Bold Underline' checkpoint observation field
		  Then 'Bold Underline' checkpoint note text is bold and underline
		  When I select 'underline' and 'italic' HTML button on checkpoint 'Underline Italic'
		  And I enter 'note' note in 'Underline Italic' checkpoint observation field
		  Then 'Underline Italic' checkpoint note text is underline and italic
		  When I select 'bold' and 'underline' and 'italic' HTML button on checkpoint 'Bold Underline Italic'
		  And I enter 'note' note in 'Bold Underline Italic' checkpoint observation field
		  Then 'Bold Underline Italic' checkpoint note text is bold, underline and italic
		  And Inspection syncs to server successfully
