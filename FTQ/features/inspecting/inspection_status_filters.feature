Feature: Inspection Status Filters

  @p1
  Scenario: Inspection Status Filters
    Given I login as admin user
    When I create an inspection via project 1, checklist '03.10.01'
    And I click on 'OPN' checkbox in 'Footer depth and width per plan and code.' checkpoint on inspection page
    And I click on 'OPN' checkbox in 'Fall of waste lines verified with level, supported with backfill.' checkpoint on inspection page
    And I click on 'FTQ' checkbox in 'Rebar and wire mesh diameter and number per specifications.' checkpoint on inspection page
    And I click on 'FTQ' checkbox in 'Backfill and underpinning installed under pipes where needed.' checkpoint on inspection page
    And I click on 'QC' checkbox in 'Foundation location, elevation, and dimensions verified to current version of plans.' checkpoint on inspection page
    And I click on 'QC' checkbox in 'Number of electrical and plumbing risers match number on plan, locations verified:' checkpoint on inspection page
    And I click on the inspection status filter
    And I click on 'Open' status in filter
    Then I should only see checkpoints with OPN statuses
    When I click on the inspection status filter
    And I click on 'Unchecked' status in filter
    Then I should only see checkpoints with no statuses
    When I click on the inspection status filter
    And I click on 'FTQ' status in filter
    Then I should only see checkpoints with FTQ statuses
    When I click on the inspection status filter
    And I click on 'QC' status in filter
    Then I should only see checkpoints with QC statuses
