Feature: Formulas

	@p1
	Scenario: Standard Checkpoint Formulas
	  Given I login as admin user
	  When I create an inspection via project 1, checklist 'CT648'
	  Then I see my formula checkpoint
	  When I enter '5' note in 'Referenced Checkpoint' checkpoint observation field
	  And I click on 'Save' button on inspection page
	  Then the formula checkpoint shows '10' in the observation notes field
	  And I should see 'Synced to server' inspection status