Feature: Hidden Characters

  @p1
  Scenario: Hidden characters pasted to inspection
    Given I login as admin user
    When I create an inspection
    And I enter hidden characters in 'Punch Item/Deficiency:' checkpoint observation field
    Then I should see 'Synced to server' inspection status
