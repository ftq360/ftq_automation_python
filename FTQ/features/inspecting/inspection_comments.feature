Feature: Inspection Comments

  @p1
  Scenario: Inspection Comments
      Given I login as admin user
      When I create an inspection via project 1, checklist 'CT150'
      And I click on Options Menu for 'Quality Checkpoint' checkpoint on inspection page
      And I click on Add Comment in the checkpoint options menu
      Then a comments section appears below the checkpoint
      When I add a comment
      And I click on 'Save' button on inspection page
      Then I should see 'Synced to server' inspection status
      When I edit my comment
      And I type '@us' to tag a user in my comment
      Then I see a list of users available to tag
      When I select a user to tag in the comment
      And I click on 'Save' button on inspection page
      Then I should see 'Synced to server' inspection status
