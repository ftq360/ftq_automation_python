Feature: Key Metrics

  @p1
  Scenario: Key Metric Categories
    Given I login as admin user
    When I create an inspection via project 1, checklist 'CT147'
    Then I see 'Risk Factor' key metric is set to '1'
    And I see 'Cost Level' key metric is set to '2'
    When I create an inspection via project 1, checklist 'CT150'
    Then I see 'Money Spent' key metric is set to '99'
    And I see 'Number of days delayed' key metric is set to '14'
