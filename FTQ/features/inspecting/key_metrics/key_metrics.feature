Feature: Key Metrics

  @p2
  Scenario: Key Metrics
	Given I login as admin user
	When I create an inspection
	Then I see 'Risk Factor' key metric is set to '1'
	And I see 'Cost Level' key metric is set to '2'
	When I set 'Cost Level' key metric to '-1'
	Then I see 'Cost Level' key metric is set to '2'
	When I set 'Risk Factor' key metric to '42'
	And I click on 'NA' checkbox in 'Punch Item/Deficiency:' checkpoint on inspection page
	Then I should see 'Synced to server' inspection status
	When I refresh inspection from server
	And I click on 'OPN' checkbox in 'Punch Item/Deficiency:' checkpoint on inspection page
	Then I see 'Risk Factor' key metric is set to '42'
	And I see 'Cost Level' key metric is set to '2'