Feature: Self Inspect Users

  @p1
  Scenario: Self Inspect user can login and inspect
    Given I login as self inspect user
    When I create an inspection as a self inspect user
    Then only Project 1 is available to me
    And my Responsible Party is automatically selected
    When I click on 'Save' button on inspection page
    Then inspection created successfully
    Given I navigate to Recent Inspections Listing page
    Then I can see Project 1 and Project 2
    But I cannot see 'Project for Data Creation'
