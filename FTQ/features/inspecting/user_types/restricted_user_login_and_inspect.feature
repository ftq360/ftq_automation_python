Feature: Restricted User

  @p1
  Scenario: Restricted user can login and inspect
    Given I login as restricted inspect user
    When I create an inspection via project 1, checklist 'CT147'
    And I click on 'QC' checkbox in 'Punch Item/Deficiency:' checkpoint on inspection page
    Then I should see 'Synced to server' inspection status
