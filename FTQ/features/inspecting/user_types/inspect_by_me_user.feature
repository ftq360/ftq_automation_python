Feature: Inspect By Me User

  @p1
  Scenario: Inspect By Me User
    Given I login as Inspect By Me user
    When I create an inspection
    Given I navigate to Recent Inspections Listing page
    Then I can only see inspections created by me
