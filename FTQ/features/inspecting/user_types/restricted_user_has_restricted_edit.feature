Feature: Restricted User

  @p1
  Scenario: Restricted User has Restricted Edit
    Given I login as admin user
    When I create an inspection via project 1, checklist 'CT150'
    Then I wait until Inspection ID value appears on inspection page
    And the Inspection ID is inspection1
    When I create an inspection via project 2, checklist 'CT150'
    Then I wait until Inspection ID value appears on inspection page
    And the Inspection ID is inspection2
    Given I log out
    And I login as restricted inspect user
    And I navigate to Recent Inspections Listing page
    When I open inspection1
    Then I can make edits to the inspection
    Given I navigate to Recent Inspections Listing page
    When I open inspection2
    Then I cannot make edits to the inspection
