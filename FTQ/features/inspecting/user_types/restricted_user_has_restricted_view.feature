Feature: Restricted User

  @p1
  Scenario: Restricted user has restricted view
    Given I login as restricted user
    And I navigate to Recent Inspections Listing page
    Then I see only inspections from Project 2 (2) in Recent Inspections Listing
    And I only see Project 2 in page filter options
