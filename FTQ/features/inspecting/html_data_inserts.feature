Feature: Checkpoint notes

  @p1
  Scenario: HTML Data Inserts
    Given I login as admin user
    When I create an inspection via project 1, checklist 'CT150'
    And I click to enter notes on any checkpoint
    And I click the GPS button
    Then GPS location is automatically inserted into the notes field
    When I click the Weather button
    Then NOAA weather report is automatically inserted into the notes field
    When I click the Date/Time button
    Then the current date and time is automatically inserted into the notes field
    And I should see 'Synced to server' inspection status
