Feature: Inspection Attachments

  @p2
  Scenario: Paste clipboard as attachment
	Given I login as admin user
	When I create an inspection
	And I press Print Scr key
	And I click on camera button in 'Punch Item/Deficiency:' checkpoint on inspection page
	And I select 'Clipboard' option in camera dropdown on inspection page
	And I press save on the image editor
	Then I should see attached file to 'Punch Item/Deficiency:' checkpoint on inspection page