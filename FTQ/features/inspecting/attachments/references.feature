Feature: References

  @p2
  Scenario: References on Inspection Page
	Given I login as admin user
	When I create an inspection via Project for References, checklist 'Checklist for References'
	Then checklist references are shown
	And project references are shown
	And responsible party references are shown
