Feature: Inspection Attachments

  @p1
  Scenario: Inspection Attachments
    Given I login as admin user
    When I create an inspection
    And I attach '15' files to 'Punch Item/Deficiency:' checkpoint on inspection page
    Then I should see 'Synced to server' inspection status
    When I refresh inspection from server
    Then attachments are displayed correctly

