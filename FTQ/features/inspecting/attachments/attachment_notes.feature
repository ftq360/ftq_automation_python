Feature: Inspection Attachments

  @p2
  Scenario: Attachment Notes
    Given I login as admin user
    When I create an inspection
    And I attach file to 'Punch Item/Deficiency:' checkpoint on inspection page
    And I add notes to the attached file
    Then I should see 'Synced to server' inspection status
    When I refresh inspection from server
    Then my notes are still present on the attachment