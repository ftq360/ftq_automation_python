Feature: Inspection Attachments

  @p2
  Scenario: Low Internet Connection and Inspection Attachments
    Given I login as admin user
    When I create an inspection
    Given I set network to 'Slow 3g' network throttling
    When I attach '15' files to 'Punch Item/Deficiency:' checkpoint on inspection page
    Then I should see 'Synced to server' inspection status
    When I refresh inspection from server
    Then attachments are displayed correctly

