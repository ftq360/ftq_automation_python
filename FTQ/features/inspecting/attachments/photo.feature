Feature: Photo

  @p1
  Scenario: Take photo on inspection
	Given I login as admin user
	When I create an inspection
	And I click on camera button in 'Punch Item/Deficiency:' checkpoint on inspection page
	And I select 'Photo' option in camera dropdown on inspection page
	And I take a picture
	And I press save on the image editor
	Then I should see attached file to 'Punch Item/Deficiency:' checkpoint on inspection page
	And I should see 'Synced to server' inspection status