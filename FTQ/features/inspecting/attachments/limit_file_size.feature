Feature: Limit File Size

    @p2
	Scenario: Limit File Size
	  Given I login as admin user
	  When I create an inspection
	  And I attach large PDF file to 'Punch Item/Deficiency:' checkpoint on inspection page
	  Then I should see 'Upload limit exceeded' popup
	  When I press OK
	  Then large PDF file is not uploaded