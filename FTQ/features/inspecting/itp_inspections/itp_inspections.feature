Feature: ITP Inspections

  @p1
  Scenario: ITP Inspections have correct data in plan field
    Given I login as plan mode user
    When I create an ITP inspection via 'ITP Project (4)', plan item 'ITP Equipment (4-1), General-Punch List Deficiency (CT147), [4-4]'
    Then I see correct plan item selected in ITP plan field on the inspection page
