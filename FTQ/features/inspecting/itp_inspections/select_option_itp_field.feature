Feature: ITP Inspections

  @p1
  Scenario: Select Option in ITP field
    Given I login as admin user
    When I create an inspection via ITP Project, checklist 'CT150'
    Then I see 'Select Option' in ITP plan field on the inspection page