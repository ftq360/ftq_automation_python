Feature: Plan Mode

  @p2
  Scenario: Plan mode checkbox filters the selection Checklists ITP
    Given I login as admin user
    And I navigate to Create Inspection page
    Then I should see 'Loading data' modal
    And I should wait until 'Loading data' modal will be closed
    When I select 'ITP Project (4)' in selection process
    And I select 'ITP Phase 1 (4-3)' in selection process
    And I check plan mode checkbox
    Then only plan items are shown in selection process
