Feature: ITP Dropdown in Selection Process

  @p2
  Scenario: Selection Process ITP Dropdown Permissions
	  Given I login as admin user
	  When I create an ITP inspection via 'ITP Project', plan item '[4-4]'
	  And I sync inspection to server
	  Given I log out
	  And I login as itp permissions user
	  And I navigate to Create Inspection page
	  When I select 'ITP Equipment (4-1)' in selection process
	  And I click dropdown arrow for plan item '[4-4]'
	  Then I see a message that I do not have permission to view the inspection

