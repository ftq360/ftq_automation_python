Feature: Plan Mode

  @p2
  Scenario: Plan mode restricted
    Given I login as plan mode user
    And I navigate to Create Inspection page
    Then I should see 'Loading data' modal
    And I should wait until 'Loading data' modal will be closed
    And the plan mode checkbox cannot but unchecked