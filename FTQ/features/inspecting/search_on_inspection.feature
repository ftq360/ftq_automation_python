Feature: Inspection Search Bar

  @p2
  Scenario: Search On Inspection
    Given I login as admin user
    When I create an inspection via project 1, checklist 'CT411'
    And I click on 'OPN' checkbox in 'Quality Checkpoint' checkpoint on inspection page
    And I enter 'todays time' note in 'Approval Checkpoint' checkpoint observation field
    And I enter 'todays time' note in 'Data Checkpoint' checkpoint observation field
    When I enter 'time' in the inspection search bar
    Then I should see 'Approval Checkpoint, Data Checkpoint' checkpoint(s) on the inspection page
    When I enter 'D1' in the inspection search bar
    Then I should see 'Quality Checkpoint' checkpoint(s) on the inspection page
