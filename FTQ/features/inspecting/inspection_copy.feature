Feature: Inspection Copy

  @p2
  Scenario: Inspection Copy
	Given I login as admin user
	When I create an inspection via project 1, checklist '03.10.01'
	And I click on 'FTQ' checkbox in 'Footer depth and width per plan and code.' checkpoint on inspection page
	And I click on 'OPN' checkbox in 'Fall of waste lines verified with level, supported with backfill.' checkpoint on inspection page
	Then I wait until Inspection ID value appears on inspection page
	When I click copy button on inspection page
	Then I am brought to a new copied inspection
	And checkpoint statuses were copied over correctly
