Feature: SSO

  @p1
  Scenario Outline: MS/Google Login
    Given I navigate to FTQ360 app
    When I click on '<sso_login>' button
    Then an SSO window is opened
    And I can login with my <sso_credentials>
    Examples:
      | sso_login | sso_credentials           |
      | google    | ftq360dev@ftq360.org      |
      | microsoft | microsoftlogin@ftq360.org |
