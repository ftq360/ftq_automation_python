import datetime
import glob
import os
import time
from datetime import date

import pytest
import pytz
import win32clipboard
from imap_tools import MailBox, AND
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from locators.core_locators import *
from locators.inspection_page_locators import *
from locators.selection_process_locators import *


# --- Functions ---

# wait 20 seconds for page load
def wait_page_title_exact(selenium, title):
    try:
        WebDriverWait(selenium, 60).until(
            EC.title_is(title)
        )
    except:
        pytest.fail(f"{title} does not match page title")


# wait 20 seconds until element present
def wait_present(selenium, locator):
    try:
        WebDriverWait(selenium, 60).until(
            EC.presence_of_element_located((By.XPATH, locator))
        )
    except:
        raise Exception(f"Element {locator} not found")


def wait_text_present(selenium, locator, text):
    try:
        WebDriverWait(selenium, 20).until(
            EC.text_to_be_present_in_element((By.XPATH, locator), text)
        )
    except:
        raise Exception(f"Element {locator} not found")


def wait_text_not_present(selenium, locator, text):
    try:
        WebDriverWait(selenium, 20).until(
            EC.text_to_be_present_in_element((By.XPATH, locator), text)
        )
        WebDriverWait(selenium, 20).until_not(
            EC.text_to_be_present_in_element((By.XPATH, locator), text)
        )
    except:
        raise Exception(f"Element {locator} not found")


def wait_text_not_empty(selenium, locator):
    try:
        WebDriverWait(selenium, 20).until(
            EC.invisibility_of_element(
                EC.text_to_be_present_in_element((By.XPATH, locator), "")
            )
        )
    except:
        raise Exception(f"Element {locator} not found")


# wait 20 seconds until element present and then clickable
def wait_clickable(selenium, locator):
    try:
        WebDriverWait(selenium, 60).until(
            EC.presence_of_element_located((By.XPATH, locator))
        )
        WebDriverWait(selenium, 60).until(
            EC.element_to_be_clickable((By.XPATH, locator))
        )
    except:
        raise Exception(f"Element {locator} not found")


# wait until element disappears
def wait_invisible(selenium, locator):
    try:
        WebDriverWait(selenium, 20).until(
            EC.invisibility_of_element((By.XPATH, locator))
        )
    except:
        pytest.fail(f"Element to wait {locator} is still present")


def wait_invisible_long(selenium, locator):
    try:
        WebDriverWait(selenium, 300).until(
            EC.invisibility_of_element((By.XPATH, locator))
        )
    except:
        pytest.fail(f"Element to wait {locator} is still present")


def wait_clickable_long(selenium, locator):
    try:
        WebDriverWait(selenium, 300).until(
            EC.presence_of_element_located((By.XPATH, locator))
        )
        WebDriverWait(selenium, 300).until(
            EC.element_to_be_clickable((By.XPATH, locator))
        )
    except:
        raise Exception(f"Element {locator} not found")


def wait_clickable_short(selenium, locator):
    try:
        WebDriverWait(selenium, 3).until(
            EC.presence_of_element_located((By.XPATH, locator))
        )
        WebDriverWait(selenium, 3).until(
            EC.element_to_be_clickable((By.XPATH, locator))
        )
    except:
        raise Exception(f"Element {locator} not found")


# wait for select element state
def wait_select_state_class(selenium, locator, state):
    WebDriverWait(selenium, 20).until(
        EC.element_located_selection_state_to_be((By.CLASS_NAME, locator), state)
    )


def wait_selected(selenium, locator):
    WebDriverWait(selenium, 20).until(
        EC.element_located_to_be_selected(locator)
    )


def wait_select_state(selenium, locator, state):
    WebDriverWait(selenium, 20).until(
        EC.element_located_selection_state_to_be(locator, state)
    )


# wait for select element state
def wait_select_state_xpath(selenium, locator, state):
    WebDriverWait(selenium, 20).until(
        EC.element_located_selection_state_to_be((By.XPATH, locator), state)
    )


# scroll element into view
def scroll_into_view(selenium, locator):
    locator = locator
    if type(locator) is not WebElement:
        locator = by_xpath(selenium, locator)
    try:
        selenium.execute_script("arguments[0].scrollIntoView({block: 'center', behavior: 'smooth'});", locator)
    except:
        pytest.fail(f"Element {locator} is not found")


def scroll_to_bottom(selenium):
    selenium.execute_script("window.scrollTo(0, document.body.scrollHeight);")


# inner page scroll page
def scroll_into_view_inside(selenium, xpath, scrolling_xpath):
    element_found = [False]
    start_time = time.time()
    while not element_found[-1] and time.time() - start_time < 10:
        try:
            try_by_xpath(selenium, xpath)
            element_found.append(True)
        except:
            selenium.execute_script("arguments[0].scrollTop = arguments[0].scrollHeight", by_xpath(selenium, scrolling_xpath))


# turn text into callable url
def text_to_credential(text):
    credentials_text = text.replace(" ", "_").upper()
    return credentials_text


# --- Find Element Shortcuts ---

def by_xpath(selenium, locator):
    wait_clickable(selenium, locator)
    elem = selenium.find_element(By.XPATH, locator)
    return elem


def by_xpath_simple(selenium, locator):
    elem = selenium.find_element(By.XPATH, locator)
    return elem


def try_by_xpath(selenium, locator):
    wait_clickable_short(selenium, locator)
    elem = selenium.find_element(By.XPATH, locator)
    return elem


def by_xpath_multiple(selenium, locator):
    wait_clickable(selenium, locator)
    elem = selenium.find_elements(By.XPATH, locator)
    return elem


def try_by_xpath_multiple(selenium, locator):
    wait_clickable_short(selenium, locator)
    elem = selenium.find_elements(By.XPATH, locator)
    return elem


def by_contains_text(selenium, text):
    dynamic_text = f"//*[contains(text(), '{text}')]"
    by_xpath(selenium, dynamic_text)


def by_id(selenium, locator):
    elem = selenium.find_element(By.ID, locator)
    return elem


def by_class(selenium, locator):
    elem = selenium.find_element(By.CLASS_NAME, locator)
    return elem


def find_element_loop(selenium, locator):
    try:
        selenium.find_element(By.XPATH, locator)
        return True
    except:
        return False


# -- New checklist setup functions --

def checklist_row(selenium, checklist_name):
    all_checklists = by_xpath_multiple(selenium, "//*[contains(@class, 'item-name')]")
    list_of_checklists = [txt.text for txt in all_checklists]
    index = list_of_checklists.index(checklist_name) + 1
    checklist_row_xpath = f"(//*[@class='can-drag'])[{index}]"
    if by_xpath(selenium, checklist_row_xpath + "//*[@class='item-code']").text == "CT150":
        next_checklist_row_path = f"(//*[@class='can-drag'])[{index + 1}]"
        return next_checklist_row_path
    else:
        return checklist_row_xpath


def checklist_row_by_code(selenium, checkpoint_code):
    all_checklist_codes = by_xpath_multiple(selenium, "//*[@class='item-code']")
    list_of_checklists = [scroll_into_view(selenium, txt) and txt.text for txt in all_checklist_codes]
    index = list_of_checklists.index(checkpoint_code) + 1
    checklist_row_xpath = f"(//*[@class='can-drag'])[{index}]"
    if by_xpath(selenium, checklist_row_xpath + "//*[@class='item-code']").text == "CT150":
        next_checklist_row_path = f"(//*[@class='can-drag'])[{index + 1}]"
        return next_checklist_row_path
    else:
        return checklist_row_xpath


def checkpoint_row_by_name(selenium, checkpoint_name):
    all_checkpoint_names = by_xpath_multiple(selenium, "//*[contains(@class, 'item-name')]")
    list_of_checkpoints = [txt.text for txt in all_checkpoint_names]
    index = list_of_checkpoints.index(checkpoint_name) + 1
    checkpoint_row_xpath = f"(//*[@class='can-drag'])[{index}]"
    return checkpoint_row_xpath


def verify_new_checklists(selenium):
    new_checklists = by_xpath_multiple(selenium, "//tr[contains(@class, 'new')]")
    num_of_new = len(new_checklists)
    return num_of_new


def verify_new_projects(selenium):
    new_projects = by_xpath_multiple(selenium, "//tr[contains(@class, 'new')]")
    num_of_new = len(new_projects)
    assert num_of_new == 2


def return_downloaded_pdfs():
    PATH_TO_DWN_FOLDER = r"C:\Workspace\AutomationDownloads"
    FILE_TYPE = "\*pdf"
    get_all_files = sorted(filter(os.path.isfile, glob.glob(PATH_TO_DWN_FOLDER + FILE_TYPE)), key=os.path.getmtime)
    return get_all_files


def return_downloaded_xls():
    PATH_TO_DWN_FOLDER = r"C:\Workspace\AutomationDownloads"
    FILE_TYPE = "\*xls"
    get_all_files = sorted(filter(os.path.isfile, glob.glob(PATH_TO_DWN_FOLDER + FILE_TYPE)), key=os.path.getmtime)
    return get_all_files


def return_downloaded_zips():
    PATH_TO_DWN_FOLDER = "C:\\Workspace\\AutomationDownloads\\"
    FILE_TYPE = "\*zip"
    get_all_files = sorted(filter(os.path.isfile, glob.glob(PATH_TO_DWN_FOLDER + FILE_TYPE)), key=os.path.getmtime)
    return get_all_files


def return_downloaded_csvs():
    PATH_TO_DWN_FOLDER = r"C:\Workspace\AutomationDownloads"
    FILE_TYPE = "\*csv"
    get_all_files = sorted(filter(os.path.isfile, glob.glob(PATH_TO_DWN_FOLDER + FILE_TYPE)), key=os.path.getmtime)
    return get_all_files


def return_downloaded_jsons():
    PATH_TO_DWN_FOLDER = r"C:\Workspace\AutomationDownloads"
    FILE_TYPE = "\*json"
    get_all_files = sorted(filter(os.path.isfile, glob.glob(PATH_TO_DWN_FOLDER + FILE_TYPE)), key=os.path.getmtime)
    return get_all_files


def return_downloaded_jpgs():
    PATH_TO_DWN_FOLDER = r"C:\Workspace\AutomationDownloads"
    FILE_TYPE = "\*jpg"
    get_all_files = sorted(filter(os.path.isfile, glob.glob(PATH_TO_DWN_FOLDER + FILE_TYPE)), key=os.path.getmtime)
    return get_all_files


def files():
    PATH_TO_DWN_FOLDER = r"C:\Workspace\AutomationDownloads"
    FILE_TYPE = "\*"
    get_all_files = sorted(filter(os.path.isfile, glob.glob(PATH_TO_DWN_FOLDER + FILE_TYPE)), key=os.path.getmtime)
    files_list = get_all_files

    return files_list


def download_wait():
    while "crdownload" in files():
        time.sleep(3)
        files()
    else:
        pass


def send_to_clipboard(clip_type, data):
    win32clipboard.OpenClipboard()
    win32clipboard.EmptyClipboard()
    win32clipboard.SetClipboardData(clip_type, data)
    win32clipboard.CloseClipboard()


def get_clipboard():
    win32clipboard.OpenClipboard()
    data = win32clipboard.GetClipboardData()
    win32clipboard.CloseClipboard()
    return data


def element_boolean(selenium, locator):
    try:
        by_xpath(selenium, locator)
        return True
    except:
        return False


def get_email_by_subject_past_5_min(cred_data, email_subject):
    mailbox = MailBox('imap.gmail.com').login(cred_data['GMAIL_USERNAME'], cred_data['GMAIL_PASSWORD'])

    for msg in mailbox.fetch(AND(subject=email_subject, date=date.today()), limit=1, reverse=True):
        eastern = pytz.timezone('US/Eastern')
        difference = datetime.datetime.today().astimezone(eastern) - msg.date.astimezone(eastern)
        if difference > datetime.timedelta(minutes=2):
            wait_response = mailbox.idle.wait(timeout=300)
            if wait_response:
                for new_msg in mailbox.fetch(AND(subject=email_subject, date=date.today()), limit=1, reverse=True):
                    print("Email received: ", new_msg.date.astimezone(eastern), new_msg.subject, new_msg.html)
            else:
                pytest.fail("No emails received in the past 5 minutes")
        else:
            print("Email received: ", msg.date.astimezone(eastern), msg.subject, msg.html)


def get_email_by_recipient(cred_data, recipient_email):
    username = cred_data['GMAIL_USERNAME'].strip()
    pw = cred_data['GMAIL_PASSWORD'].strip()
    email_data = {"EMPTY": "empty list"}
    with MailBox('imap.gmail.com').login(username, pw) as mailbox:
        for msg in mailbox.fetch(AND(to=(recipient_email.strip()))):
            email_data.update({"returned_subject": msg.subject})
            for att in msg.attachments:
                email_data.update({"returned_attachment_filename": att.filename})
    if len(email_data) > 1:
        return email_data
    else:
        with MailBox('imap.gmail.com').login(username, pw) as mailbox:
            wait_response = mailbox.idle.wait(timeout=600)
            if wait_response:
                for msg in mailbox.fetch(AND(to=recipient_email)):
                    email_data.update({"returned_subject": msg.subject})
                    for att in msg.attachments:
                        email_data.update({"returned_attachment_filename": att.filename})
            else:
                pytest.fail("No emails received for this user in the past 10 minutes")

            if len(email_data) > 1:
                return email_data
            else:
                pytest.fail("unable to return email data")


def create_an_inspection(selenium, cred_data):
    go_to_create_insp = selenium.get(cred_data['BASE_URL'] + cred_data['CREATE_INSPECTION'])
    try_dictionary_loading(selenium)
    try:
        select_division = by_xpath(selenium, SPECIFIC_SELECTION(selenium, "(Undefined)")).click()
    except:
        pass
    select_project1 = by_xpath(selenium, SPECIFIC_SELECTION(selenium, "Project 1 (1)")).click()
    select_checklist_general = by_xpath(
        selenium,
        SPECIFIC_SELECTION(selenium, "General-Punch List Deficiency (CT147)")
    ).click()
    select_rp = by_xpath(selenium, SPECIFIC_SELECTION(selenium, "FTQ Automation Group (D1)")).click()
    check_inspection_created_successfully = wait_clickable(selenium, INSP_HEADER)



def add_signature_to_checkpoint(selenium):
    camera = by_xpath(selenium, INSP_CHECKPOINT(selenium, "Punch Item/Deficiency:") + CP_CAMERA)
    scroll_into_view(selenium, camera)
    click_camera = camera.click()
    click_signature = by_xpath(selenium, CP_SIGNATURE).click()
    verify_canvas = wait_clickable(selenium, CP_SIGNATURE_CANVAS)
    canvas = by_xpath(selenium, CP_SIGNATURE_CANVAS)
    draw = ActionChains(selenium).click_and_hold(canvas) \
        .move_by_offset(100, 100).release().perform()
    by_xpath(selenium, CP_IMAGE_EDITOR_SAVE).click()
    wait_clickable_long(selenium, INSP_SYNCED_TEXT)


def loading_data_modal_appears_and_loads(selenium):
    try:
        modal_appears = wait_clickable_short(selenium, SP_DICT_LOADING_MODAL_OPEN)
        loading_complete = wait_present(selenium, SP_DICT_LOADING_MODAL_CLOSED)
    except:
        pass


# --- Dynamic Locators ---
def INSP_CHECKPOINT(selenium, checkpoint):
    all_checkpoints = by_xpath_multiple(selenium, CP_NAMES)
    index = [txt.text for txt in all_checkpoints].index(checkpoint) + 1
    checkpoint_xpath = f"(//*[contains(@data-bind, 'text: CheckpointText')])[{index}]/../../.."
    return checkpoint_xpath


def INSP_CHECKPOINT_INDEX(selenium, checkpoint):
    all_checkpoints = by_xpath_multiple(selenium, CP_NAMES)
    index = [txt.text for txt in all_checkpoints].index(checkpoint)
    return index + 1


def CP_RP_DROPDOWN_SELECTION(selenium, checkpoint, text):
    rp_dropdown = by_xpath(selenium, INSP_CHECKPOINT(selenium, checkpoint) + CP_RP_DROPDOWN)
    click_rp_dropdown = rp_dropdown.click()
    all_rps_in_list = by_xpath_multiple(selenium, "//div[@class='menu transition visible']/div/div")
    index = [txt.text for txt in all_rps_in_list].index(text) + 1
    dropdown_xpath = f"//div[@class='menu transition visible']/div/div[{index}]"
    return rp_dropdown, dropdown_xpath


def CHECKPOINT_STATUS():
    checkpoint_status_dict = {
        "FTQ"   : "//div[@class='ui special ftq checkbox']",
        "OPN"   : "//div[@class='ui special ca checkbox']",
        "QC"    : "//div[@class='ui special stq checkbox']",
        "NA"    : "//div[@class='ui special na checkbox']",
        "FAILED": "//div[@class='ui special stq checkbox']"
    }
    return checkpoint_status_dict


def SPECIFIC_SELECTION(selenium, text):
    all_selections = by_xpath_multiple(selenium, "//div[@class='item-text']")
    titles_list = [txt.text for txt in all_selections]
    index = [i + 1 for i in range(len(titles_list)) if text in titles_list[i]]
    specific_selection_xpath = f"//ul[@class='cl-menu-ul']/li{index}//a"
    if len(index) == 0:
        return None
    else:
        return specific_selection_xpath


def wait_checkpoint_visibile(selenium, checkpoint):
    names = []
    start_time = time.time()
    while (checkpoint not in names) and time.time() - start_time < 60:
        cp_names_new = by_xpath_multiple(selenium, CP_NAMES)
        get_names = [x.text for x in cp_names_new]
        names.extend(get_names)
    return names


def click_via_menu(selenium, ftq_page):
    page = text_to_credential(ftq_page)
    try:
        by_xpath(selenium, HAMBURGER_TOP_MENU_ICON).click()
    except:
        pass
    top_menu = by_xpath_multiple(selenium, TOP_MENU_OPTIONS)
    parent_tab = ""
    if "nspection" in ftq_page:
        parent_tab = "Start New"
    menu_tabs = [x.text for x in top_menu]
    index_parent = menu_tabs.index(parent_tab) + 1
    click_parent_item = by_xpath(selenium, f"{TOP_MENU_OPTIONS}[{index_parent}]").click()
    menu_item_options = by_xpath_multiple(selenium, f"{TOP_MENU_OPTIONS}[{index_parent}]{TOP_MENU_FROM_OPTIONS_DROPDOWN_OPTIONS}")
    menu_item_options_list = [x.text for x in menu_item_options]
    index_children = menu_item_options_list.index(ftq_page) + 1
    item_to_click = by_xpath(selenium, f"{TOP_MENU_OPTIONS}[{index_parent}]{TOP_MENU_FROM_OPTIONS_DROPDOWN_OPTIONS}[{index_children}]")
    return item_to_click


def try_dictionary_loading(selenium):
    try:
        dict_loading_modal_appears = wait_clickable_short(selenium, SP_DICT_LOADING_MODAL_OPEN)
        dictionaries_load = wait_present(selenium, SP_DICT_LOADING_MODAL_SUCCESS)
        dictionary_loading_complete = wait_present(selenium, SP_DICT_LOADING_MODAL_CLOSED)
    except:
        pass
