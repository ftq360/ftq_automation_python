# --- Checklist Sharing Library Page Locators (CHECKLISTLIB) ---
CHECKLISTLIB_ROW_SELECT = "//input[@name='rowselect']"
CHECKLISTLIB_DOWNLOAD_BUTTON = "//*[contains(@data-bind, 'click: download')]"
CHECKLISTLIB_MODAL_TASK_NAME = "//input[contains(@data-bind, 'taskLabel')]"
CHECKLISTLIB_MODAL_DOWNLOAD_BUTTON = "//*[contains(@data-bind, 'click: dlgDownloadModel.download')]"