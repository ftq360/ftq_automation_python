# --- Account Management Page Locators ---

ACCOUNT_CURRENT_USER_COUNT = "//*[@data-bind='text: currentUserCount']"
ACCOUNT_CURRENT_FREE_USER_COUNT = "//*[@data-bind='text: currentFreeUserCount']"

# --- Company Details Locators ---
OLD_GRID_SETTING_NAMES = "//*[@class='property-name-column']"

# --- Additional Settings --- #
CHECKLIST_CATEGORY_SETTING = "//*[@for='Pref291']"
