# --- GMAIL Locators ---
GMAIL_USERNAME = "//*[@type='email']"
GMAIL_PASSWORD = "//*[@type='password']"
GMAIL_INVITATIONS_LABEL = "//*[@data-tooltip='User Invitations']"
GMAIL_EMAILS = "//div[@role='main']//tr"
GMAIL_EMAIL_MESSAGE = "(//div[@class=''])[1]"
GMAIL_MOST_RECENT_EMAIL_TIME = "//div[@role='main']//tr[1]//td[8]/span"
