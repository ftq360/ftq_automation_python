# --- Universal Listing Grid Locators ---
GRID_PAGE_PROJECT_FILTER = "//*[@class='ui search dropdown']"
GRID_PAGE_PROJECT_FILTER_CURRENT = "//*[@class='ui search dropdown']//*[@class='text']"
GRID_PAGE_PROJECT_FILTER_OPTIONS = "//*[@class='menu transition visible']/*[@class='item']"
GRID_ITEM_NAME = "//*[@class='item-name']"
GRID_RECENT_CHECKPOINTID_LINKS = "//td[2]//*[@class='item-name']"
LISTING_GRID_ROWS = "//tbody/tr"
LISTING_GRID_TITLE = "//*[@class='ui header']/span[@class='non-selectable']"
LISTING_GRID_PROJECT_COLUMN = "//*[@class='item-comm']"
LISTING_GRID_PHASE_COLUMN = "//*[@class='item-job']"
LISTING_GRID_CREATION_DATE = "//*[@class='item-date']"
LISTING_GRID_CREATED_BY = "//*[@class='item-inspector']"
LISTING_GRID_LAST_INSP = "//*[@class='item-inspector-mod']"
LISTING_GRID_LAST_CP_INSP = "//*[@class='item-checkpoint-inspector']"
LISTING_GRID_INSP_NOTE = "//*[@class='item-ca']"
LISTING_GRID_RP_COLUMN = "//*[@class='item-vendor']"
LISTING_GRID_CP_COLUMN = "//*[@class='item-cp']"
LISTING_GRID_CP_NOTE = "//*[@class='item-notes']"
LISTING_GRID_LOADING_MASK = "//*[contains(@class, 'dimmer transition visible')]//*[@class='ui text loader']"
LISTING_GRID_ANY_DATE = "//*[contains(@class, 'item-date')]"

# --- Recent Listing Grid Page Locators (RECENTINSP) ---
GRID_SEARCH = "//input[@id='item-search']"
SHOWRECENT_EXPAND_DATA = "//img[contains(@data-bind, 'PreviousInspections')]"

# --- Archive Listing Grid Page Locators ---
ARCHIVE_GRID_PROJECT_NAME_SEARCH = "//input[@name='community']"
ARCHIVE_GRID_PAGE_NUMBERING = "//*[contains(@class, 'pagination menu')]"
INSP_ARCHIVE_LAST_ACTIVITY_COLUMN_HEAD = "//th[contains(@data-bind, 'dateMod')]"
INSP_ARCHIVE_COLUMN_ASCENDING = "//i[contains(@class, 'arrow up')]"
INSP_ARCHIVE_COLUMN_DESCENDING = "//i[contains(@class, 'arrow down')]"
INSP_ARCHIVE_LAST_ACTIVITY_DATE_SEARCH = "//*[@name='dateMod']"
INSP_LISTING_LAST_ACTIVITY_TEXT = "//*[@class='item-date-mod']"


# --- ITP Progress Grid Page Locators (ITPPROG) ---
ITPPROG_PLAN_ITEM_LINK = "//*[contains(@data-bind, 'link')]"
ITPPROG_PLAN_ITEM_ICON = "//*[@class='cl-menu-item-icon']"
ITPPROG_PLAN_ITEM_ROW = "//*[@class='cl-menu-wrapped-wide']"
ITPPROG_PLAN_ITEM_NAME = "//*[@class='cl-menu-wrapped-wide']//*[@class='item-text']"
ITPPROG_EXPAND_DATA = "//i[contains(@data-bind, 'toggleDetails') and not (contains(@style, 'display: none'))]"
ITPPROG_GRID = "//*[contains(@class, 'cl-itp-table')]"
ITPPROG_GRID_LINK = "//span/a"
ITPPROG_PROJ_FILTER_OPTIONS = "//*[contains(@class, 'active-result')]"
ITPPROG_PROJ_FILTER_CURRENT = "//*[contains(@id, 'project_selector_')]//span"
ITPPROG_INSP_ID_LINK = "//*[contains(@data-bind, 'RecordID')]/a"
ITPPROG_INSP_ID_STATUS = "//*[contains(@data-bind, 'StatusText')]"
ITPPROG_PAGE_TOTAL_CALC = "//h2"
ITPPROG_PLAN_ITEM_CALC = "//*[contains(@data-bind, 'percentDone')]"

# --- SHOW RECENT (SELECTION PROCESS DROP DOWN) ---
SHOW_RECENT_INSP = "//*[contains(@data-bind, 'CrewLabelCustomer')]"

# --- ITP Listing Page Locators ---
ITP_LISTING_CODE_SEARCH = "//input[@name='code']"
ITP_LISTING_CHECKLIST_SEARCH = "//input[@name='taskId']"
ITP_LISTING_PROJECT_SEARCH = "//input[@name='communityId']"
ITP_LISTING_EQUIPMENT_SEARCH = "//input[@name='objectId']"
ITP_LISTING_RP_SEARCH = "//input[@name='vendorId']"
ITP_LISTING_CODES = "//*[@class='item-code']"
ITP_LISTING_CHECKLISTS = "//*[@class='item-task']"
ITP_LISTING_PROJECTS = "//*[@class='item-comm']"
ITP_LISTING_EQUIPMENT = "//*[@class='item-object']"
ITP_LISTING_RPS = "//*[@class='item-vendor']"
