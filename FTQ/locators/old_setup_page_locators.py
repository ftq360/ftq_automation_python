# SETUP PAGE LOCATORS

# -- Universal Setup Locators ---
SETUP_PROCESS_MENU_TITLES = "//div[@class='sidenav-item-title']"
SETUP_ALL_ADD_BUTTONS = "//*[@class='th-plus-icon']"
SETUP_ALL_SAVE_BUTTONS = "//*[contains(@data-bind, 'save')]"
SETUP_ALL_SAVED_CONFIRMATION_MASK = "//h3[contains(text(), 'Saved!')]"
SETUP_EXPORT_BUTTON = "//*[contains(@data-bind, 'visible:canExport')]"
SETUP_PAGE_FILTER = "//div[@id='header-filter']/span"
SETUP_PAGE_FILTER_OPTIONS = "//div[@id='header-filter']//li"
SETUP_NEXT_PAGE = "//*[@data-bind='click: goToPage(curPage() + 1)']"
SETUP_ATTACHMENT_DROPDOWN = "//*[@class='attach-btn-select']"

# --- Project Setup Page Locators (PROJ) ---
PROJECT_NAMES = "//tbody/tr[position()<last()]/td[7]/textarea"
PROJECT_EQUIPMENT_NAME = "//tr[1]/td[8]/textarea"
PROJECT_LOCATION_NAME = "//tr[1]/td[7]/textarea"
PROJECT_ALL_CODES = "//tbody/tr[1]/td[5]/textarea"

# --- User Setup Page Locators (USER) ---
USER_NAME_COLUMN = "//tbody/tr[position()<last()]/td[4]"
USER_NAMES = "//tbody/tr[position()<last()]/td[4]/input"
USER_EMAILS = "//*[@placeholder='Enter Email']"
USER_RADIO_BUTTON = "//*[@name='rowselect']"
# USER_PROJECT_ACCESS_NAMES = "//tbody/tr[position()<last()]/td[4]/span"
USER_PROJECT_ACCESS_EDIT = "//tbody/tr[position()<last()]//label[contains(@for, 'Edit')]/.."
USER_PROJECT_ACCESS_VIEW = "//tbody/tr[position()<last()]//label[contains(@for, 'View')]/.."
USER_PROJECT_ACCESS_CREATE = "//tbody/tr[position()<last()]//label[contains(@for, 'Create')]/.."
USER_ALL_ACCESS_CREATE = "//th//label[contains(@for, 'Create')]"
USER_ENTER_USERNAME = "//*[@placeholder='Enter Username' and @class='error']"
USER_ENTER_FIRSTNAME = "//*[contains(@data-bind, 'FirstName') and @class='error']"
USER_ENTER_LASTNAME = "//*[contains(@data-bind, 'LastName') and @class='error']"
USER_NEW_ENTER_PW = "//*[contains(@data-bind, 'Password') and @class='error']"
USER_ENTER_PW = "//*[contains(@data-bind, 'Password')]"
USER_CONFIRM_PW = "//*[contains(@data-bind, 'Confirmation')]"
USER_ENTER_EMAIL = "//*[contains(@data-bind, 'Email') and @class='error']"
USER_TYPE_DROPDOWN = "//*[contains(@data-bind, 'inspectorTypes')]/..//*[contains(@class, 'chzn-container') and @title='This field is required.']/a"
USER_TYPE_OPTIONS = "/..//li"
USER_RESTRICTED_BOX = "//*[contains(@for, 'Restricted')]"
USER_RESTRICTED_VERIFY = "//*[contains(@data-bind, 'Restricted')]"
# USER_SEND_INVITATION = "//*[@class='big-default-btn' and contains(text(), 'Send Invitation')]"

# --- RP SETUP ---
RP_NAMES = "//*[contains(@data-bind, 'VendorDescription')]"
RP_ENTER_NEW_NAME = "//*[@placeholder='Enter description' and contains(@class, 'error')]"
RP_DELETE_ICON = "//*[@class='delete-icon' and not(@style='display: none;')]"
RP_SETTING_NAMES = "//*[@class='property-name-column']"
RP_SETTING_SELECT = "/../td/select"

# --- IMPORT PAGE ---
IMPORT_MESSAGE = "//*[@id='messageList']"
IMPORT_SUBMIT = "//*[@id='importButton']"
IMPORT_FILEINPUT = "//*[@id='importFile']"

# --- Attachment File Manager ---
FILE_MANAGER_FIRST_ATTACHMENT_LINK = "(//*[contains(@data-bind, 'Filename, attr: { href:($parent.AttachmentUrl')])[1]"

# --- ACTIVITY LOG --- #
ACTIVTY_LOG_COLUMNS = "//span"
ACTIVTY_LOG_ROWS = "//tr[contains(@data-bind, 'has-changes')]"
ACTIVITY_LOG_DETAIL_COLUMN = "//*[contains(@data-bind, 'Details')]"

#--- API ---#
SELECT_API_QUERY = "//select[contains(@data-bind, 'selectedDataQuery')]"
API_KEY = "//*[contains(@data-bind, 'apiKeyStatus')]"
