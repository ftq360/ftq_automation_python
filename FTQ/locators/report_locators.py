#--- Run Report Locators (REPORT) ---
REPORT_NAMES = "//div[contains(@data-bind, 'fullName')]"
REPORT_DATE_RANGE_TEXT = "//input[@class='search']"
REPORT_DATE_RANGE_SELECT = "//*[@id='rpt-dt-date-range']//*[@data-value]"
RUN_REPORT_TAB = "//li[(@class='last active') or (@class='active last')]"
SPLIT_REPORT = "//*[@id='rpt-dt-split']"
SPLIT_REPORT_ENABLED = "//*[@id='rpt-dt-split']//label[text()='Split']"
REPORT_RUN_REPORT_BUTTON = "//*[@id='rpt-dt-run2']"
REPORT_SUMMARY = "//*[@class='reportSummary']"
REPORT_SELECT = "//*[@class='item-select']"


#--- Report History Page (REPORT_HISTORY) ---
REPORT_HISTORY_TABLE = "//*[@class='ui history table']/tbody/tr"
REPORT_HISTORY_TABLE_FIRST_ROW_NAME = "//*[@class='ui history table']/tbody/tr[1]//*[contains(@data-bind, 'ReportCode')]"
REPORT_HISTORY_TABLE_FIRST_ROW_STATUS = "(//span[@class='status'])[1]"
REPORT_HISTORY_REFRESH = "//*[@data-bind='click:load']"
REPORT_DOWNLOAD = "//*[@class='download icon']"
REPORT_CANCEL = "//*[@class='cancel icon']"
REPORT_STATUS = "//span[@class='status']"
REPORT_DELETE = "//*[@class='trash icon']"
REPORT_EMAIL = "//*[@class='envelope icon']"
REPORT_EMAIL_DIALOG_MAIN_RECIPIENT = "(//div[contains(@class, 'selection dropdown')])[1]"
REPORT_EMAIL_DIALOG_CC_RECIPIENT = "(//div[contains(@class, 'selection dropdown')])[2]"
REPORT_EMAIL_FIRST_OPTION = "//div[contains(@class, 'item selected')]"
REPORT_EMAIL_DIALOG_HEADER = "//*[@class='ui modal transition visible active']//*[@class='header']"
REPORT_REQUEST_DATE = "//*[contains(@data-bind, 'RequestDate')]"


#--- Interactive Reports ---
INTERACTIVE_REPORT_TITLE = "//*[@class='list-item-title']"
INTERACTIVE_REPORT_CHART = "//*[@id='report-chart']"
OLD_REPORT_RUN_REPORT_BUTTON = "//*[@class='big-default-btn']"


#--- Activate Reports ---
ACTIVATE_RP_EMAIL_SETTING_NAMES = "//*[@class='property-name-column']"