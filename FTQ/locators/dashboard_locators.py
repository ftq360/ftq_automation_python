# --- Dashboard Locators ---

# --- Dashboard Landing Page Locators (ALLDASH) ---
ALLDASH_ALL_STARS = "//*[contains(@class, 'favorite')]"
ALLDASH_SELECTED_STARS = "//*[@class='favorite selected']"
ALLDASH_DESELECTED_STARS = "//*[@class='favorite']"
ALLDASH_DASHNAME_FROM_STAR ="/../a"
ALLDASH_DASHNAME = "//*[contains(@data-bind, 'DashboardName')]"
ALLDASH_PANEL_TITLE = "//*[@class='db-list-title']"
ALLDASH_STATS_TITLES = "//*[@class='db-info-stat-title']"
ALLDASH_STATS_VALUES_FROM_TITLE = "/../*[@class='db-info-stat-value']"
ALLDASH_PANEL_ITEM = "/../..//*[@class='db-list-item']"
ALLDASH_PANEL_ITEM_TITLE = "/../..//*[@class='db-list-item-title']"
ALLDASH_PANEL_ITEM_COUNT = "/../..//*[@class='db-list-item-count']"
ALLDASH_CLEAR_PANEL_FILTER = "/..//*[@class='db-tools-deselect-all']"
ALL_DASH_FILTER_BUTTONS = "//*[@class='vertical filters']/button"
ALLDASH_DATE_FILTER_OPTIONS = "//*[contains(@data-bind, 'currentDateRangeId')]/option"
ALLDASH_PROJECT_FILTER_OPTIONS = "//*[contains(@data-bind, 'currentCommunityId')]/option"
DEFAULT_DASHBOARD_MENU_ITEMS_LIST = ['Aging Open Deficiency Dashboard', 'Priority Open Deficiency Dashboard', 'Inspections Activity Dashboard', 'ITP % of Inspections Passed Dashboard', 'Responsible Party Performance Dashboard', 'Deficiency Analysis Dashboard', 'Average Days To Fix Dashboard', 'View All Dashboards']
ALLDASH_PANEL_SORT = "/..//*[@class='db-tools-sort db-menu-tools-trigger']"
ALLDASH_PANEL_SORT_OPTIONS = "/..//*[@class='db-menu-tools-option']"
ALLDASH_PANEL_DOWNLOAD = "/..//*[@class='db-tools-download']"
KM_DASH_SHOW_DEFICIENCIES = "//*[@class='details-button']"


# --- Priority Open Item Dashboard Page Locators (PRIORITYDASH) ---
PRIORITYDASH_CALENDAR_ITEMS = "//tr[not(@class='fc-limited')]/td[@class='fc-event-container']/a"
DASH_GRID = "//*[@id='db-wrapper']"
DASH_CALENDAR = "//*[@id='db-calendar']"
DASH_CALENDAR_DAYS = "//*[contains(@class, 'fc-week')]//td[contains(@class, 'fc-widget-content')]"
DASH_CALENDAR_NEXT_MONTH = "//*[contains(@class, 'fc-next-button')]"
DASH_CALENDAR_LAST_DAY_IN_MONTH = "(//*[contains(@class, 'fc-day-number') and not(contains(@class, 'fc-other-month'))])[last()]"
DASH_CALENDAR_ITEMS_IN_LAST_ROW = "(//*[@class='fc-content-skeleton'])[last()]//tbody//td[@class='fc-event-container']/a"
DASH_CALENDAR_ITEM_DETAIL_LINK = "//*[@id='db-calendar-details']/a"
DASH_CALENDAR_VIEW_SEARCH = "//*[@class='cv-search-box']/input"
DASH_GRID_ROW = "//*[@class='db-row']"
DASH_ITEMS = "//*[@class='db-list-item']"
DASH_LAST_ITEM = "(//*[(@class='fc-event-container')])[last()]"
DASH_DATE_FILTER = "//*[contains(@title, 'Last')]"
PRIORITYDASH_CALENDAR_TAB = "//*[contains(@data-bind, 'calendar')]/span"
DASH_DROPDOWN_OPTIONS = "//li[contains(@class, 'select2-results__option')]"
DASH_GRID_SEARCH = "//*[@class='gg-search-box']/input"
DASH_PAGE_HEADER = "//*[@class='wrapper head-wrapper']"
PRIORITYDASH_CP_LINKS = "//*[contains(@data-bind, 'checkpointLink')]/a"
PRIORITYDASH_CP_STATUS = "//*[@title='OPN']"
PRIORITYDASH_GRID_DUEDATES = "//*[contains(@class, 'due-date')]/span"
PRIORITYDASH_INSPECTIONS_TOTAL = "(//*[@class='db-info-stat-value'])[1]"

# --- Inspection Activity Dashboard Page Locators (INSPDASH) ---
INSPDASH_GRID_FIRSTINSP_STATUS = "//*[contains(@class, 'select2-container--focus')]//*[@class='select2-selection__rendered']"
INSPDASH_STATUS_CELL = "//*[contains(@class, 'c3-shapes-Status')]/*"
INSPDASH_INSP_LINKS = "//*[contains(@data-bind, 'inspectionLink')]/a"
INSPDASH_INSP_STATUS= "//tbody//*[contains(@class, 'status')]/span"
INSPDASH_GRID_DATES = "//*[contains(@data-bind, 'formatters.date')]"
INSPDASH_GRID_STATUS_NAME = "//td/..//*[contains(@class, 'status')]"
INSPDASH_GRID_STATUS_OPTIONS = "//*[contains(@class, 'container--open')]//li[@aria-selected='false']"

# --- RP Performance Dashboard Locators ---
RP_DASH_PROJECT_FILTER = "(//*[@class='select2-selection__rendered'])[2]"
RP_DASH_TOTAL_DEF_NUMBER = "(//*[@class='db-info-stat-value'])[2]"
RP_DASH_DATE_FILTER = "(//*[@class='select2-selection__rendered'])[2]"

# --- Shared Dashbaord Locators --- #
DASH_SHOW_DETAILS_BUTTON = "//*[@class='details-button']"
