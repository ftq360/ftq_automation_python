# --- Customization Preference Page Locators (CUSTOMPREF) ---
CUSTOMPREF_SEARCH = "//input[@id='select-menu-filter']"
CUSTOMPREF_VALUE_FIRST_ROW = "(//input[contains(@data-bind, 'PrefValue')])[1]"
CUSTOMPREF_SAVE_BUTTON = "//*[contains(@data-bind, 'save')]"